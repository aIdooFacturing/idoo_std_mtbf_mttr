package com.unomic.dulink.chart.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.PopVo;
import com.unomic.dulink.chart.service.PopService;


@RequestMapping(value = "/pop")
@Controller
public class PopController {
	private static final Logger logger = LoggerFactory.getLogger(PopController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private PopService popService;
	
	@RequestMapping(value = "popIndex")
	public String popIndex() {
		return "pop/popIndex";
	};
	
	@ResponseBody
	@RequestMapping(value = "popLoginChk",produces="text/plain;charset=UTF-8")
	public String popLoginChk(HttpSession session,PopVo popVo) {
		logger.info("run popLoginChk");
		String str="";
		try {
			str = popService.popLoginChk(session,popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "popMainMenu",produces="text/plain;charset=UTF-8")
	public String popMainMenu(HttpSession session) {
		
//		logger.info(""+session.getAttribute("empCd"));
		
		return "pop/popMainMenu";
	};
	@RequestMapping(value = "popmenu",produces="text/plain;charset=UTF-8")
	public String popmenu(HttpSession session) {
//		logger.info(""+session.getAttribute("test"));
//		logger.info(""+session.getAttribute("empCd"));
		
		return "pop/popmenu";
	};

	@RequestMapping(value = "popAttendanceMenu",produces="text/plain;charset=UTF-8")
	public String popAttendanceMenu(HttpSession session) {
//		logger.info(""+session.getAttribute("test"));
//		logger.info(""+session.getAttribute("empCd"));
		
		return "pop/popAttendanceMenu";
	
	};
	@RequestMapping(value = "popWorking",produces="text/plain;charset=UTF-8")
	public String popWorking(HttpSession session) {
//		logger.info(""+session.getAttribute("test"));
//		logger.info(""+session.getAttribute("empCd"));
		
		return "pop/popWorking";
	};
	@RequestMapping(value = "popHistory",produces="text/plain;charset=UTF-8")
	public String popHistory(HttpSession session) {
//		logger.info(""+session.getAttribute("test"));
//		logger.info(""+session.getAttribute("empCd"));
		
		return "pop/popHistory";
	};
	
	/*@ResponseBody
	@RequestMapping(value = "GoWorkSave")
	public String GoWorkSave(PopVo popVo) {
		String str="";
		try {
			str = popService.GoWorkSave(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}*/
	
	@RequestMapping(value = "moveGoWork")
	public String moveGoWork() {
		return "pop/moveGoWork";
	};
	
	@ResponseBody
	@RequestMapping(value = "getGoWorkList")
	public String getGoWorkList(PopVo popVo) {
		String str="";
		try {
			str = popService.getGoWorkList(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "GoWorkSave")
	public String GoWorkSave(PopVo popVo) {
		String str="";
		try {
			str = popService.GoWorkSave(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "moveOffWork")
	public String moveOffWork() {
		return "pop/moveOffWork";
	};
	
	@ResponseBody
	@RequestMapping(value = "getOffWorkList")
	public String getOffWorkList(PopVo popVo) {
		String str="";
		try {
			str = popService.getOffWorkList(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "OffWorkSave")
	public String OffWorkSave(PopVo popVo) {
		String str="";
		try {
			str = popService.OffWorkSave(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "moveStartJob")
	public String moveStartJob() {
		return "pop/moveStartJob";
	};
	
	@ResponseBody
	@RequestMapping(value = "getStartJobList")
	public String getStartJobList(PopVo popVo) {
		String str="";
		try {
			str = popService.getStartJobList(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "StartJobSave")
	public String StartJobSave(PopVo popVo) {
		String str="";
		try {
			str = popService.StartJobSave(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "selectStartJob")
	public String selectStartJob() {
		return "pop/selectStartJob";
	};
	
	@ResponseBody
	@RequestMapping(value = "getHistoryList")
	public String getHistoryList(PopVo popVo) {
		String str="";
		try {
			str = popService.getHistoryList(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "addDevice")
	public String addDevice() {
		return "pop/addDevice";
	};

	@RequestMapping(value = "moveEndJob")
	public String moveEndJob() {
		return "pop/moveEndJob";
	};
	
	@ResponseBody
	@RequestMapping(value = "getEndJobList")
	public String getEndJobList(PopVo popVo) {
		String str="";
		try {
			str = popService.getEndJobList(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "selectEndJob")
	public String selectEndJob() {
		return "pop/selectEndJob";
	};
	
	@ResponseBody
	@RequestMapping(value = "getEndJobInfo")
	public String getEndJobInfo(PopVo popVo) {
		String str="";
		try {
			str = popService.getEndJobInfo(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@ResponseBody
	@RequestMapping(value = "finshEndJob")
	public String finshEndJob(PopVo popVo) {
		String str="";
		try {
			str = popService.finshEndJob(popVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "strTest")
	public String strTest(PopVo popVo) {
		String str="";
		try {
			str = popService.strTest(popVo);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "popGoEndHistory")
	public String popGoEndHistory(PopVo popVo) {
		String str="";
		try {
			str = popService.popGoEndHistory(popVo);
			System.out.println(str);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}

	@ResponseBody
	@RequestMapping(value = "jsonTest")
	public String jsonTest(PopVo popVo) {
		String str="";
		try {
			str = popService.jsonTest(popVo);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "popLineChk")
	public String popLineChk(PopVo popVo) {
		String str="";
		try {
//			System.out.println("test :: " + popVo.getIp());
//			System.out.println("test :: " + popVo.getId());
			str = popService.popLineChk(popVo);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}
	
	@RequestMapping(value = "adminSetting")
	public String adminSetting() {
		return "pop/adminSetting";
	};
	
	@RequestMapping(value = "noticeAdmin")
	public String noticeAdmin() {
		return "pop/noticeAdmin";
	}
	
	@RequestMapping(value = "workerChangeAdmin")
	public String workerChangeAdmin() {
		return "pop/workerChangeAdmin";
	}
	
	@ResponseBody
	@RequestMapping(value = "getPopDevice")
	public String getPopDevice(PopVo popVo) {
		String str="";
		try {
			str = popService.getPopDevice(popVo);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}

	@ResponseBody
	@RequestMapping(value = "getPopChangeWorker")
	public String getPopChangeWorker(String val) {
		String str="";
		try {
			str = popService.getPopChangeWorker(val);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}

	@ResponseBody
	@RequestMapping(value = "getWorkerList")
	public String getWorkerList(PopVo popVo) {
		String str="";
		try {
			str = popService.getWorkerList(popVo);
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return str;
	}
}
