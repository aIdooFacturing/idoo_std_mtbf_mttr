package com.unomic.dulink.chart.controller;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.InventoryVo;
import com.unomic.dulink.chart.service.InventoryService;

// TODO: Auto-generated Javadoc
/**
 * The Class InventoryController.
 */
@RequestMapping(value = "/chart")
@Controller
public class InventoryController {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(InventoryController.class);
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@Autowired
	private InventoryService InventoryService;

	/**
	 * Berttest.
	 *
	 * @param model the model
	 * @return the string
	 */
	@RequestMapping(value = "stockStatusDay", method = RequestMethod.GET)
	public String berttest(Model model) {
		return "chart/stockStatusDay";
	}
	
	@RequestMapping(value = "stockUpPage", method = RequestMethod.GET)
	public String stockUpPage() {
		return "chart/stockUpPage";
	}

	@RequestMapping(value = "stockUptPg", method = RequestMethod.GET)
	public String stockUptPg() {
		return "chart/stockUptPg";
	}

	@RequestMapping(value = "stockProUpPage", method = RequestMethod.GET)
	public String stockProUpPage() {
		return "chart/stockProUpPage";
	}
	
	@ResponseBody
	@RequestMapping(value = "getStockUpList")
	public String getStockUpList(InventoryVo inventoryVo){
		String str="";
		try {
			str = InventoryService.getStockUpList(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "getStockProUpList")
	public String getStockProUpList(InventoryVo inventoryVo){
		String str="";
		try {
			str = InventoryService.getStockProUpList(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "getUpdateOhdCnt")
	public String getUpdateOhdCnt(InventoryVo inventoryVo){
		String str="";
		try {
			str = InventoryService.getUpdateOhdCnt(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	/**
	 * Bert stck master.
	 *
	 * @param inventoryVo the inventory vo
	 * @return the string
	 */
	@ResponseBody
	@RequestMapping(value = "BertStckMaster")
	public String BertStckMaster(InventoryVo inventoryVo){
		String str="";
		try {
			str = InventoryService.BertStckMaster(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "stockLastUpSave")
	public String stockLastUpSave(String val) {
		String str="";
		try {
			str=InventoryService.stockLastUpSave(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug(e.getMessage());
			e.printStackTrace();
		}
		return str;
	}
	
	/**
	 * Bert stck master save.
	 *
	 * @param val the val
	 * @param date the date
	 * @return the string
	 */
	@ResponseBody
	@RequestMapping(value = "BertStckMasterSave")
	public String BertStckMasterSave(String val, String date) {
		String str="";
		try {
			str=InventoryService.BertStckMasterSave(val, date);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug(e.getMessage());
			e.printStackTrace();
		}
		return str;
	}
	
	/**
	 * Finish stck master.
	 *
	 * @param val the val
	 * @param date the date
	 * @return the string
	 */
	@ResponseBody
	@RequestMapping(value="finishStckMaster")
	public String finishStckMaster(String val, String date) {
		String str="";
		System.out.println("Date is : " + date);
		System.out.println("Date is : " + date);
		System.out.println("Date is : " + date);
		try {
			str=InventoryService.finishStckMaster(val, date);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug(e.getMessage());
			e.printStackTrace();
		}
		return str;
	}
	
	/**
	 * Opr nm list.
	 *
	 * @param vo the vo
	 * @return the string
	 */
	@ResponseBody
	@RequestMapping(value="OprNmList")
	public String OprNmList(InventoryVo vo) {
		String str="";
		try {
			str=InventoryService.OprNmList(vo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug(e.getMessage());
			e.printStackTrace();
		}
		return str;
	}
	
	/**
	 * Move stock.
	 *
	 * @param val the val
	 * @return the string
	 */
	@ResponseBody
	@RequestMapping(value="moveStock")
	public String moveStock(String val) {
		String str="";
		try {
			str=InventoryService.moveStock(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug(e.getMessage());
			e.printStackTrace();
		}
		return str;
	}
	
	/**
	 * Move stock with trans.
	 *
	 * @param val the val
	 * @return the string
	 */
	@ResponseBody
	@RequestMapping(value="moveStockWithTrans")
	public String moveStockWithTrans(String val) {
		String str="";
		try {
			str=InventoryService.moveStockWithTrans(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	
	/**
	 * Gets the stock status day.
	 *
	 * @param inventoryVo the inventory vo
	 * @return the stock status day
	 */
	@ResponseBody
	@RequestMapping(value="getStockStatusDay")
	public String getStockStatusDay(InventoryVo inventoryVo) {
		String str="";
		try {
			str=InventoryService.getStockStatusDay(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	/**
	 * 2019-03-31
	 * 기간별 재고마감 조회
	 * @param inventoryVo
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value="getTermStockStatus")
	public String getTermStockStatus(InventoryVo inventoryVo) {
		String str="";
		try {
			str=InventoryService.getTermStockStatus(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	/**
	 * Gets the finished stock status.
	 *
	 * @param inventoryVo the inventory vo
	 * @return the finished stock status
	 */
	@ResponseBody
	@RequestMapping(value="getfinishedStockStatus")
	public String getfinishedStockStatus(InventoryVo inventoryVo) {
		String str="";
		try {
			str=InventoryService.getfinishedStockStatus(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	
	
	
	/**
	 * Gets the lot tracer.
	 * 로트 번호로 재고를 추적 조회
	 * 기존의 재고로트, 완성로트 별로 따로 입력 받아 조회 하
	 * @author John.Joe
	 * @since 2017-12-04
	 * @param inventoryVo the inventory vo
	 * @return the lot tracer
	 */
	@ResponseBody
	@RequestMapping(value="getLotTracer")
	public String getLotTracer(InventoryVo inventoryVo) {
		String str="";
		try {
			str=InventoryService.getLotTracer(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value="stockMoveTrans")
	public String stockMoveTrans(String val) {
		String str="";
		try {
			str=InventoryService.stockMoveTrans(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value="exportMoveTrans")
	public String exportMoveTrans(String val) {
		String str="";
		try {
			str=InventoryService.exportMoveTrans(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value="BarcodeImportMoveTrans")
	public String BarcodeImportMoveTrans(InventoryVo inventoryVo) {
		String str="";
		try {
			str=InventoryService.BarcodeImportMoveTrans(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value="importMoveTrans")
	public String importMoveTrans(String val) {
		String str="";
		try {
			str=InventoryService.importMoveTrans(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value="shipMoveTrans")
	public String shipMoveTrans(String val) {
		String str="";
		try {
			str=InventoryService.shipMoveTrans(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@ResponseBody
	@RequestMapping(value="finishMoveTrans")
	public String finishMoveTrans(String val) {
		String str="";
		try {
			str=InventoryService.finishMoveTrans(val);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "stockProcessSave")
	public String stockProcessSave(String val, String date) {
		String str="";
		try {
			str=InventoryService.stockProcessSave(val, date);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug(e.getMessage());
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "stockInohdCntSave")
	public String stockInohdCntSave(InventoryVo inventoryVo) {
		String str="";
		try {
			str=InventoryService.stockInohdCntSave(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug(e.getMessage());
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "stockSuccessInohdCntSave")
	public String stockSuccessInohdCntSave(InventoryVo inventoryVo) {
		String str="";
		try {
			str=InventoryService.stockSuccessInohdCntSave(inventoryVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug(e.getMessage());
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "stockdeadLineSave")
	public String stockdeadLineSave(String total, String pro) {
		String str="";
		try {
			str=InventoryService.stockdeadLineSave(total, pro);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug(e.getMessage());
			e.printStackTrace();
		}
		return str;
	}
	
	@ResponseBody
	@RequestMapping(value = "getServerTime")
	public String getServerTime() {
		String str="";
		try {
			str=InventoryService.getServerTime();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return str;
	}
}
