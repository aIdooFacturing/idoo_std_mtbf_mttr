package com.unomic.dulink.chart.domain;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AverageVo {
	
	String empCd;
	int average;
	String date;
	int rank;
	
}
