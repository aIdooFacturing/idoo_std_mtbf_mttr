package com.unomic.dulink.chart.controller;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.chart.domain.ChartVo;
import com.unomic.dulink.chart.service.ChartService;
import com.unomic.dulink.chart.service.KpiService;

@Controller
@RequestMapping(value = "/kpi")
public class KpiController {
	
	private static final Logger logger = LoggerFactory.getLogger(KpiController.class);
	
	@Autowired
	private KpiService kpiService;
	
	@RequestMapping(value = "productionKpi")
	public String productionKpi() {
		return "kpi/productionKpi";
	}
	
	@RequestMapping(value = "productionDetailKpi")
	public String productionDetailKpi() {
		return "kpi/productionDetailKpi";
	}
	
	@RequestMapping(value = "productionStatusKpi")
	public String productionStatusKpi() {
		return "kpi/productionStatusKpi";
	}
	@RequestMapping(value = "productionStatusKpi_backUp")
	public String productionStatusKpi_backUp() {
		return "kpi/productionStatusKpi_backUp";
	}
	
	
	@RequestMapping(value = "getWorkerCnt")
	@ResponseBody
	public String getWorkerCnt(ChartVo chartVo) throws Exception {
		String str = "";
		try {
			str = kpiService.getWorkerCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getPrdNoCnt")
	@ResponseBody
	public String getPrdNoCnt(ChartVo chartVo) throws Exception {
		String str = "";
		try {
			str = kpiService.getPrdNoCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getDeviceCnt")
	@ResponseBody
	public String getDeviceCnt(ChartVo chartVo) throws Exception {
		String str = "";
		try {
			str = kpiService.getDeviceCnt(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getDetailProductStatus")
	@ResponseBody
	public String getDetailProductStatus(ChartVo chartVo,HttpServletResponse response) throws Exception {
		String str = "";
		try {
			str = kpiService.getDetailProductStatus(chartVo,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "operationStatus")
	public String operationStatus() {
		return "kpi/operationStatus";
	}
	
	@RequestMapping(value="getoperationStatus")
	@ResponseBody
	public String getoperationStatus(ChartVo chartVo) throws Exception {
		String str="";
		try {
			str = kpiService.getoperationStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getStandard")
	@ResponseBody
	public String getStandard(ChartVo chartVo) throws Exception {
		String str="";
		try {
			str = kpiService.getStandard(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="updateStandard")
	@ResponseBody
	public String updateStandard(ChartVo chartVo) throws Exception {
		String str="";
		try {
			str = kpiService.updateStandard(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getoperationStatusNd")
	@ResponseBody
	public String getoperationStatusNd(ChartVo chartVo) throws Exception {
		String str="";
		try {
			str = kpiService.getoperationStatusNd(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getoperationStatusNight")
	@ResponseBody
	public String getoperationStatusNight(ChartVo chartVo) throws Exception {
		String str="";
		try {
			str = kpiService.getoperationStatusNight(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getoperationStatusToNight")
	@ResponseBody
	public String getoperationStatusToNight(ChartVo chartVo) throws Exception {
		String str="";
		try {
			str = kpiService.getoperationStatusToNight(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="getopearationStatus")
	@ResponseBody
	public String getopearationStatus(ChartVo chartVo) throws Exception {
		String str="";
		try {
			str = kpiService.getopearationStatus(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value="updateOpStandard")
	@ResponseBody
	public String updateOpStandard(ChartVo chartVo) throws Exception {
		String str="";
		try {
			str = kpiService.updateOpStandard(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "getTotalRatio")
	@ResponseBody
	public String getTotalRatio(ChartVo chartVo) throws Exception {
		String str = "";
		try {
			str = kpiService.getTotalRatio(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "getprodataTime")
	@ResponseBody
	public String getprodataTime(ChartVo chartVo) throws Exception {
		String str = "";
		try {
			str = kpiService.getprodataTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	@RequestMapping(value = "getOpdataTime")
	@ResponseBody
	public String getOpdataTime(ChartVo chartVo) throws Exception {
		String str = "";
		try {
			str = kpiService.getOpdataTime(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	@RequestMapping(value = "viewByWorker")
	@ResponseBody
	public String viewByWorker(ChartVo chartVo) throws Exception {
		String str = "";
		try {
			str = kpiService.viewByWorker(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}

	@RequestMapping(value = "viewByDvc")
	@ResponseBody
	public String viewByDvc(ChartVo chartVo) throws Exception {
		String str = "";
		try {
			str = kpiService.viewByDvc(chartVo);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
}
