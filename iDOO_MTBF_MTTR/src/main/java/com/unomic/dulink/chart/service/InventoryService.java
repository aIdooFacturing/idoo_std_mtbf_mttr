package com.unomic.dulink.chart.service;

import java.util.Date;

import com.unomic.dulink.chart.domain.InventoryVo;

public interface InventoryService {

	public String BertStckMaster(InventoryVo inventoryVo) throws Exception;
	
	public String BertStckMasterSave(String val, String date) throws Exception;

	public String finishStckMaster(String val, String date)throws Exception;

	public String OprNmList(InventoryVo vo) throws Exception;
	
	public String moveStock(String val) throws Exception;
	
	public String moveStockWithTrans(String val) throws Exception;

	public String getStockStatusDay(InventoryVo inventoryVo) throws Exception;

	public String getfinishedStockStatus(InventoryVo inventoryVo) throws Exception;

	public String getTermStockStatus(InventoryVo inventoryVo) throws Exception;

	public String getLotTracer(InventoryVo inventoryVo) throws Exception;

	public String stockMoveTrans(String val) throws Exception;

	public String exportMoveTrans(String val) throws Exception;

	public String BarcodeImportMoveTrans(InventoryVo inventoryVo) throws Exception;

	public String importMoveTrans(String val) throws Exception;

	public String shipMoveTrans(String val) throws Exception;

	public String finishMoveTrans(String val) throws Exception;
	
	public String stockProcessSave(String val, String date) throws Exception;

	public String getStockUpList(InventoryVo inventoryVo) throws Exception;

	public String getStockProUpList(InventoryVo inventoryVo) throws Exception;

	public String stockLastUpSave(String val) throws Exception;

	public String getUpdateOhdCnt(InventoryVo inventoryVo) throws Exception;
	
	public String stockInohdCntSave(InventoryVo inventoryVo) throws Exception;
	
	public String stockSuccessInohdCntSave(InventoryVo inventoryVo) throws Exception;
	
	public String stockdeadLineSave(String total, String pro) throws Exception;

	public String getServerTime() throws Exception;


}
