package com.unomic.dulink.chart.service;

import javax.servlet.http.HttpSession;

import com.unomic.dulink.chart.domain.UserVo;

public interface LoginService {

	String loginCheck(UserVo vo, HttpSession session) throws Exception;

	boolean logout(HttpSession session) throws Exception;

}
