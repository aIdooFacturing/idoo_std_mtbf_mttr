<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:choose>
	<c:when test="${!empty listSecuLv}">
		<c:set var="count" value="1"></c:set>
		<c:forEach items="${listSecuLv }" var="secuLv" varStatus="status">
			<li onclick='setSelectedColor(this);loadSecuLvFnc(${secuLv.lvId});' id='secuLv${secuLv.lvId}' class="ui-state-default" style='width:90%'>
				* ${secuLv.lvName }
				<span onclick='delSecuLv(${secuLv.lvId});' class=" ui-icon ui-icon-circle-close" style='float:right;'></span>
				<span onclick='editSecuLv(${secuLv.lvId});' class=" ui-icon ui-icon-pencil" style='float:right;'></span>				
				<img style='background-color:${secuLv.lvColor };  width:10px; height:10px; border: 2px solid white; float:right;'></img>
				
				<c:if test="${status.count==1}">
					<script>
						loadSecuLvFnc('${secuLv.lvId}');
						$('#secuLv${secuLv.lvId}').addClass('ui-selected');
					</script>
				</c:if>
				<script>
					var secuLv = new SecuLv("${secuLv.lvName }", "${secuLv.lvColor }","${secuLv.lvId}","${secuLv.secuTy}");
					mapSecuLv.put("${secuLv.lvId}", secuLv);
				</script>
			</li>
			
		</c:forEach>
	</c:when>
	<c:otherwise>
		<li class="ui-state-default" style='width:90%' ><spring:message code="noLV"></spring:message></li>
	</c:otherwise>
</c:choose>
	
