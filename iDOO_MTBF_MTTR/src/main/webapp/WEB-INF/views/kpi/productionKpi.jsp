<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	height: 100% !important;
/* 	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
  	font-family:'Helvetica'; */
	background-color: black;
/* 	overflow: hidden; */
  	
}
.rangeSelector{
	margin-left: 5px;
	display: inline;
}
.k-footer-template{
	display : none;
}
#detailBtn{
	background: linear-gradient(#3f49c5, #051fff);
	border: 0px solid;
	color: white;
	font-weight: bolder;
}
#detailBtn:hover{
	background: #051fff;
}
#search{
	background: linear-gradient(lightgray, gray);
	border: 0px solid;
	color: black;
	font-weight: bolder;
	border-radius: 5px;
}
#search:hover{
	background: gray;
}
#print{
	background: linear-gradient(lightgray, gray);
	border: 0px solid;
	color: black;
	font-weight: bolder;
	border-radius: 5px;
}
#print:hover{
	background: gray;
}
.dateBtn{
	background: linear-gradient(lightgray, gray);
	border: 0px solid;
	color: black;
	font-weight: bolder;
	border-radius: 5px;
}
.dateBtn:hover{
	background: gray;
}



div#byWorker.k-grid tbody > tr
{
 background : #F6F6F6 !important;
 text-align: center;
 color: black !important;
}

div#byWorker.k-grid tbody > tr:hover
{
 background :  darkgray !important;
}
 
div#byWorker.k-grid tbody > .k-alt
{
 background : #ECECEC !important;
 text-align: center;
 color: black;
}
div#byWorker.k-grid tbody > .k-alt:hover
{
 background :  darkgray !important;
 text-align: center;
}

#byDevice tr td{
	color: black;
}

div#byDevice.k-grid tbody > tr
{
 background : #D8D8EB !important;
 text-align: center;
}

div#byDevice.k-grid tbody > tr:hover
{
 background : darkgray !important;
}
 
div#byDevice.k-grid tbody > .k-alt
{
 background : #CECEE1 !important;
 text-align: center;
 color: black;
}
div#byDevice.k-grid tbody > .k-alt:hover
{
 background :  darkgray !important;
 text-align: center;
}



div#underScoreWorker.k-grid tbody > tr
{
 background : #E2E2E2 !important;
 text-align: center;
 color: black !important;
}

div#underScoreWorker.k-grid tbody > tr:hover
{
 background :  darkgray !important;
}
 
div#underScoreWorker.k-grid tbody > .k-alt
{
 background :  #D8D8D8	 !important;
 text-align: center;
 color: black;
}
div#underScoreWorker.k-grid tbody > .k-alt:hover
{
 background :  darkgray !important;
 text-align: center;
}

#underScoreDevice tr td{
	color: black;
}

div#underScoreDevice.k-grid tbody > tr
{
 background : #ECEBFF !important;
 text-align: center;
 color: white !important;
}

div#underScoreDevice.k-grid tbody > tr:hover
{
 background :  darkgray !important;
}
 
div#underScoreDevice.k-grid tbody > .k-alt
{
 background :  #E2E2F5	 !important;
 text-align: center;
 color: black;
}
div#underScoreDevice.k-grid tbody > .k-alt:hover
{
 background :  darkgray !important;
 text-align: center;
}



div#detailGrid.k-grid tbody > tr
{
 background : #253336  !important;
 text-align: center;
 color: black !important;
}

div#detailGrid.k-grid tbody > tr:hover
{
 background :  darkgray !important;
}
 
div#detailGrid.k-grid tbody > .k-alt
{
 background :   #35484D	 !important;
 text-align: center;
 color: black;
}
div#detailGrid.k-grid tbody > .k-alt:hover
{
 background :  darkgray !important;
 text-align: center;
}
.k-grid-content {
    background: #121212!important;
}
.k-grid tr td{
	color : black;
/* 	background : #505050; */
}
/* .k-grid tr.k-alt td{
	background : #666666;
} */

.k-grid th.k-header,
.k-grid-header
{
    color : white !important;
    text-align: center;
}
.k-grid td{
	color:black ;
	font-weight: bolder;
}
.k-icon{
	color:white;
}

</style> 
<script type="text/javascript">
	var byWorker;
	
	const loadPage = () =>{
		createMenuTree("kpi", "productionKpi")
		
	}

	$(function(){
		getprodataTime();
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		daily();
		setCurrentTime();
	});
	
	function setEl(){
		
		$("#print").css({
			"height" : getElSize(120),
			"width" : getElSize(200),
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
		})
		
		$("#search").css({
			"height" : getElSize(120),
			"font-size" : getElSize(40),
			"margin-left" : getElSize(1260),
			"width" : getElSize(200),
			"border-radius": getElSize(8)
		})
		
		$("#detailBtn").css({
			"height" : getElSize(120),
			"padding" : getElSize(30),
			"width" : getElSize(350),
			"margin-top" : getElSize(20),
			"margin-right" : getElSize(150),
			"margin-left" : getElSize(50),
			"font-size" : getElSize(40),
			"border-radius": getElSize(8)
		})
		
		$("input#sMonth").css({
			"width" : getElSize(300),
			"font-size" : getElSize(40)
		})
		
		$("input#sDate").css({
			"width" : getElSize(300),
			"font-size" : getElSize(40),
		})
		
		$("#eDate").css({
			"width" : getElSize(300),
			"font-size" : getElSize(40),
			"margin-right" : getElSize(250)
		})
		
		$("label").css({
			"margin" : getElSize(10)
		})
		
		$("#selector").css({
			"position" : "absolute",
	    	"top" : getElSize(460),
	    	"width" : "86%"
		})
		
		$(".datePicker").css({
 			"font-size" : getElSize(50),
			"top" : getElSize(44),
			"height" : getElSize(120),
			"padding" : getElSize(30),
			"width" : getElSize(350),
			"right" : getElSize(480),
			"background" : "black",
			"color" : "white",
			"border" : "none",
			"border-radius": getElSize(8)
			
		})
		
		$("#workIdx").css({
 			"font-size" : getElSize(52),
			"top" : getElSize(44),
			"height" : getElSize(120),
			"padding" : getElSize(30),
			"width" : getElSize(350),
			"color" : "white",
 			/*			"position" : "absolute",
			"top" : getElSize(44), */
			"right" : getElSize(480),
			"border-radius": getElSize(8)
		})
		
		$("select").css({
		    "border": "1px black #999",
		    "font-family": "inherit", 
		    "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 98% 50%",
		    "background-color" : "black",
		    "z-index" : "999",
		    "border-radius": "0px",
		    "-webkit-appearance": "none",
		    "-moz-appearance": "none",
		    "appearance":"none",
			"background-size" : getElSize(60),
			"border" : "none"
		})
		
		$("select option").css({
	        "background-color" : "rgba(0,0,0,5)",
	        "color" : "white",
		    "border" : "1",
	        "transition-delay" : 0.05,
	        "border-bottom" : getElSize(10) + "px solid red",
	        "text-align-last": "center",
/* 		    "border-top-color": "rgb(215, 0, 0)",
		    "border-right-color": "rgb(215, 0, 0)",
		    "border-left-color": "rgb(215, 0, 0)", */
	        "padding" : getElSize(15) + "px",
	        "font-size": getElSize(52) + "px",
	        "transition" : "1s"
	    })
// 		$("select::-ms-expand").css({
// 		    "display" : "none"
// 		})
		

// 		select{
// 	width:200px;
//     border: 1px solid #999;
//     font-family: inherit; 
//     background: url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 98% 50%;
//     z-index : 999; 
//     border-radius: 0px;
//     -webkit-appearance: none;
//    -moz-appearance: none;
//     appearance:none;
// }    
		
		$(".contentWidth").css({
			"width" : getElSize(1275),
			"float" : "left"
		})

		$("#monthlyLabel").css({
			"margin-left" : getElSize(180)
		})
		
		$("#margin").css({
			"margin-left" : getElSize(170)
		})
		
		$(".dateBtn").css({
			"height" : getElSize(120),
			"width" : getElSize(180),
			"font-size" : getElSize(40),
			"border-radius" : "5px"
		})
		
		$("#monthlyRadio").css({
			"margin-left" : getElSize(100)
		})
		
		$("#rangeRadio").css({
			"margin-left" : getElSize(100)
		})
		
 		$("#btnGroup").css({
//			"background" : "#37373C"	
		}) 
		
		$(".arrowBtn").css({
			"border-radius":getElSize(13)
		})
		
		$(".title").css({
			"font-size" : getElSize(106),
		    "text-shadow": "gray "+ getElSize(5) +"px "+ getElSize(5)+"px "+getElSize(15)+"px",
		    "width" : "40%"
		})
		
		$(".percent").css({
			"font-size" : getElSize(80),
			"font-weight" : "bolder",
			"color" : "#144105"
		})
		
		$("#standard").css({
			"font-size" : getElSize(106),
			"text-shadow": "gray "+ getElSize(5) +"px "+ getElSize(5)+"px "+getElSize(15)+"px"
		})
		
		$(".score").css({
			"font-size" : getElSize(50),
			"font-weight" : "bolder"
		})
		
		$(".scoreTitle").css({
			"float" : "left",
			"font-size" : getElSize(55),
			"font-weight" : "bolder",
			"width" : "100%"
		})
		
		$(".standardTable").css({
			"height" : getElSize(302)
		})
		
		$(".inputField").css({
			"font-size": getElSize(53),
	    	"width": getElSize(120),
//	    	"height": getElSize(120),
	    	"text-align": "-webkit-match-parent"
		})
		
		$(".workerDateInput").css({
			"width" : getElSize(200),
			"font-size" : getElSize(50),
			"border" : getElSize(3) +"px solid black",
			"border-radius" : getElSize(15),
			"background" : "linear-gradient( to bottom, #F0F8FF, #84C6FF )"
		})
		
		$(".deviceDateInput").css({
			"width" : getElSize(200),
			"font-size" : getElSize(50),
			"border" : getElSize(3) +"px solid black",
			"border-radius" : getElSize(15),
			"background" : "linear-gradient( to bottom, #F0F8FF, #84C6FF )"
		})
		
		$("#workerDay, #deviceDay").css({
			"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
			"background" : "black",
			"color" : "white"
		})
		
		$(".subTitle").css({
			"font-size" : getElSize(50)
		})
		
		$("#workerTitle").css({
			"position" : "relative",
			"left" : getElSize(65)
		})
		
		$("#deviceTitle").css({
			"position" : "relative",
			"left" : getElSize(65)
		})
		
		$("#standardTitle").css({
			"position" : "relative",
			"left" : getElSize(65),
			"top" : -getElSize(59)
		})
		
		$(".yearTd").css({
			"padding" : getElSize(13)+" 0px "+getElSize(13) +" 0px"
		})
		
		$(".monthTd").css({
			"padding-bottom" : getElSize(9)
		})
		
		$(".titleWidth").css({
			"width" : getElSize(1275),
			"float" : "left"
		})
		
		$("#div1").css({
			"height" : getElSize(330)
		})
		$("#div2").css({
			"height" : getElSize(1360)
		})
	};
	var dataTime="";
	function getprodataTime(){
		console.log('dd')
		var url = "${ctxPath}/kpi/getprodataTime.do";
		$.ajax({
			url : url,
			type : "post",
			success : function(data){
				dataTime=data
			}
		});
	}
	
	 var startDate,
     endDate,
     selectCurrentWeek = function () {
         window.setTimeout(function () {
             $('#sDate,#eDate').datepicker('widget').find('.ui-datepicker-current-day a').addClass('ui-state-active')
         }, 1);
     };
	
     var initialization = false;
     
    var checked = 0;
    
    
    function setCurrentTime(){
    	var day = moment(new Date(moment().year(), moment().month(), moment().date(), 8, 00, 0));
    	var night = moment(new Date(moment().year(), moment().month(), moment().date(), 20, 00, 0));
    	if(moment().isAfter(day)){
    		if(moment().isAfter(night)){
    			$("#workIdx").val(1)
    		}else{
    			$("#workIdx").val(2)
    		}
    	}else{
    		$("#workIdx").val(1)
    		$("#sDate").val(moment().subtract('day', 1).format("YYYY-MM-DD"))
    	}
    }
    
    
    
	function daily(){
		
		$("#checkedMark").remove();
		$("#dailyRadio").prepend( '<i id="checkedMark" class="fa fa-check-circle" aria-hidden="true"></i>' );
		
		
		$("#dailyRadio").css({
			"background": "#1a87cd"
		})
		
		$("#monthlyRadio,#rangeRadio").css({
			"background": "linear-gradient(lightgray, gray)"
		})
		
		checked = 0;
		$("input#sDate").val($("input#sDate").val())
		$("input#eDate").val($("input#sDate").val())
		
		if(!initialization){
			var hour = moment().hour();
			var minute = moment().minute();
			var totalMinute = hour * 60 + minute;
			
			var nightWorkTime = 20 * 60;
			var dayWorkTime = 8 * 60;
			
			console.log("==============")
			console.log(hour)
			console.log(minute)
			console.log(totalMinute)
			console.log(nightWorkTime)
			console.log(dayWorkTime)
			console.log("==============")
			if(totalMinute <= nightWorkTime && totalMinute>=dayWorkTime){
				var currentDate = moment().format("YYYY-MM-DD")
				$("input#sDate").val(currentDate)
				$("input#eDate").val(currentDate)
				
				
				var array = currentDate.split('-');
				$("#workerYear").val(array[0] + '년')
				$("#workerMonth").val(array[1] + '월')
				$("#workerDay").val(array[2] + '일')
				
				$("#deviceYear").val(array[0] + '년')
				$("#deviceMonth").val(array[1] + '월')
				$("#deviceDay").val(array[2] + '일')
				
				$("#workIdx").val(1)
			}else{
				var currentDate = moment().format("YYYY-MM-DD")
				if(totalMinute <= nightWorkTime){
					currentDate = moment().subtract(1, 'days').format("YYYY-MM-DD")
				}
				$("input#sDate").val(currentDate)
				$("input#eDate").val(currentDate)
				
				var array = currentDate.split('-');
				$("#workerYear").val(array[0] + '년')
				$("#workerMonth").val(array[1] + '월')
				$("#workerDay").val(array[2] + '일')
				
				$("#deviceYear").val(array[0] + '년')
				$("#deviceMonth").val(array[1] + '월')
				$("#deviceDay").val(array[2] + '일')
				
				$("#workIdx").val(2)
			}
			initialization=true;
		}
		
		$("#workIdx").prop("disabled", false)
		$( "input#sDate" ).datepicker( "destroy" );
		
		$('.ui-weekpicker').on('mousemove', 'tr', function () {
	        $(this).find('td a').removeClass('ui-state-hover');
	    });
		
		$("input#sDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
	        "changeMonth" : true,
	        "changeYear" : true,
			onSelect:function(e){
				$("input#sDate").val(e)
//				$("input#eDate").val(e)
				var array = e.split('-');
				$("#workerYear").val(array[0] + '년')
				$("#workerMonth").val(array[1] + '월')
				$("#workerDay").val(array[2] + '일')
				
				$("#deviceYear").val(array[0] + '년')
				$("#deviceMonth").val(array[1] + '월')
				$("#deviceDay").val(array[2] + '일')
				
			}
		})
		
		$("input#eDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
	        "changeMonth" : true,
	        "changeYear" : true,
			onSelect:function(e){
//				$("input#sDate").val(e)
				$("input#eDate").val(e)
				var array = e.split('-');
				$("#workerYear").val(array[0] + '년')
				$("#workerMonth").val(array[1] + '월')
				$("#workerDay").val(array[2] + '일')
				
				$("#deviceYear").val(array[0] + '년')
				$("#deviceMonth").val(array[1] + '월')
				$("#deviceDay").val(array[2] + '일')
				
			}
		})
		
		$("#dailyDiv input#sDate").css({
			"pointer-events": "auto",
			"background" : "white"
		})

		$("#dailyDiv input#eDate").css({
			"pointer-events": "auto",
			"background" : "white"
		})
		
		$("#rangeDiv input#sDate, #rangeDiv input#eDate, input#sMonth").css({
			"pointer-events": "none",
			"background" : "gray"
		})
		
		$("#dailyDiv").css({
			"display" : "inline"
		})
		$("#monthlyDiv").css({
			"display" : "inline"
		})
		$("#rangeDiv").css({
			"display" : "inline"
		})
	}
	
	function monthly(){
		checked = 1;
		
		$("#checkedMark").remove();
		$("#monthlyRadio").prepend( '<i id="checkedMark" class="fa fa-check-circle" aria-hidden="true"></i>' );
		$("#workIdx").prop("disabled", true)
		$( "input#sDate" ).datepicker( "destroy" );
		
		
		
		$("#monthlyRadio").css({
			"background": "#1a87cd"
		})
		
		$("#rangeRadio, #dailyRadio").css({
			"background": "linear-gradient(lightgray, gray)"
		})
		
		
		$("input#sDate, input#eDate").css({
			"pointer-events": "none"
		})
		
		$("input#sMonth").css({
			"pointer-events": "auto",
			"background" : "white"
		})
		
		$("input#sDate, input#eDate").css({
			"pointer-events": "none",
			"background" : "gray"
		})
		
		$('input#sMonth').val('');
		
		
		$('input#sMonth').datepicker( {
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'yy mm',
	        onClose: function(dateText, inst) { 
	            var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            if(month<10){
	            	month = '0'+month.toString()
	            }
	            $('input#sMonth').val(year+'년'+month.toString()+'월');
	            $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
	            $('input#eDate').val(year+'-'+month.toString()+'-'+31);
	        }
	    });
		
		$(".ui-datepicker-calendar").css({
			"display" : "none"
		})
		
	}
	
	function range(){
		checked = 2;
		
		
		$("#checkedMark").remove();
		$("#rangeRadio").prepend( '<i id="checkedMark" class="fa fa-check-circle" aria-hidden="true"></i>' );
		
		$("#rangeRadio").css({
			"background": "#1a87cd"
		})
		
		$("#monthlyRadio, #dailyRadio").css({
			"background": "linear-gradient(lightgray, gray)"
		})
		
		$("#workIdx").prop("disabled", true)
		$( "input#sDate" ).datepicker( "destroy" );
		$( "input#eDate" ).datepicker( "destroy" );
		
		$("#dailyDiv input#sDate, input#sMonth").css({
			"pointer-events": "none",
			"background" : "gray"
		})
		
		$("#rangeDiv input#sDate, input#eDate").css({
			"pointer-events": "auto",
			"background" : "white"
		})
		
		$("input#sDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(e){
				$("input#sDate").val(e)
			}
		})
		
		$("input#eDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(e){
				$("input#eDate").val(e)
			}
		})
		
	}
	
	var collapseAllGroups = function (grid) {
	    grid.table.find(".k-grouping-row").each(function () {
	        grid.collapseGroup(this);
	    });
	}
	
	function viewTarget(target){
		if(target==""){
			return "X"
		}else{
			return target
		}
	}
	
	function colorChk(chk,cnt){
		if(/* chk!='noCon' &&  */cnt>104 && (cnt==133 || cnt==147)){
//		if(/* chk!='noCon' &&  */cnt>104 && (cnt!=105 && cnt!=106 && cnt!=107 && cnt!=108 && cnt!=109 && cnt!=110 && cnt!=111 && cnt!=112 && cnt!=114 && cnt!=119 && cnt!=125 && cnt!=126 && cnt!=127 && cnt!=128 && cnt!=129 && cnt!=131 && cnt!=132 && cnt!=144 && cnt!=145 && cnt!=146)){
			return 'red'
		}else if(chk=='IOL'){
			return 'blue'
		}else if(cnt==105 || cnt==106 || cnt==107 || cnt==108 || cnt==109 || cnt==110 || cnt==111 || cnt==112 || cnt==114 || cnt==119 || cnt==125 || cnt==126 || cnt==127 || cnt==128 || cnt==129 || cnt==131 || cnt==132 || cnt==144 || cnt==145 || cnt==146){
			return 'black'	//109 빠가, 130 전원 공급 X
		}else{
			return
		}
		return 
	}
	
	var viewNm;
	$(function(){
		
		getProductionStatus();
		
		byWorker = $("#byWorker").kendoGrid({
			height:getElSize(1290),
		    columns: [
		        { title: "${worker} ${ranking}", 
		        	headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important;' ,
	        	    },
		        	columns:[
		        	{
		        		field:"rank", title: "${ranking}", width:getElSize(130),
		        		attributes: {
			        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
		        	    },headerAttributes: {
		        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
		        	    }
		        	},
		        	{field:"worker", title: "${worker}",attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{field:"target", title: "${target}",attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{field:"cnt", title: "${Performance}",attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{
	        	    	field:"goalRatio", title: "${Achievement_rate}",
		        	    attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(36)+'px !important; overflow: initial;' +'#=(goalRatio>=100)?"color :rgb(79, 169, 95)":(goalRatio<100 &&goalRatio>=80)?"color: rgb(0, 255, 45)":(goalRatio<80 &&goalRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
						headerAttributes: { style: "font-size:"+getElSize(36)+'px !important; overflow: initial;'},
						template:"#=goalRatio#%"
		        	}
		        ]}	
		    ]
		}).data("kendoGrid");
		
		byDevice = $("#byDevice").kendoGrid({
			height:getElSize(1290),
			columns: [
		        { title: "${by_Equipment} ${ranking}",
		        	headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },
		        	columns:[
		        	{field:"rank", title: "${ranking}", width:getElSize(130),
		        		attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }
		        	},
		        	{field:"name", title: "${device}",attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; color : #=colorChk(chk,dvcId)#; overflow: initial;',		        	      
//		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{field:"target", title: "${target}", width:getElSize(180),attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{field:"cnt", title: "${Performance}", width:getElSize(170),attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{
	        	    	field:"goalRatio", 
	        	    	title: "${Achievement_rate}", 
	        	    	width:getElSize(180),
	        	    	attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(36)+'px !important; overflow: initial;' +'#=(goalRatio>=100)?"color :rgb(79, 169, 95)":(goalRatio<100 &&goalRatio>=80)?"color: rgb(0, 255, 45)":(goalRatio<80 &&goalRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
						headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    	},template:"#=goalRatio#%"
		        	 }
		        ]}	
		    ]
		}).data("kendoGrid");
		
		underScoreDevice = $("#underScoreDevice").kendoGrid({
			height:getElSize(645),
			columns: [
		        { title: "${below_standard} ${device}",
		        	headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },
		        	columns:[
		        	{
		        		field:"rank", title: "${ranking}", width:getElSize(130),
		        		attributes: {
		        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
		        	    },headerAttributes: {
		        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
		        	    }
		        	},
		        	{field:"name", title: "${device}", attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; color : #=colorChk(chk,dvcId)#; overflow: initial;',		        	      
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{field:"target", title: "${target}", template : "#=viewTarget(target)#", width:getElSize(180),attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{field:"cnt", title: "${Performance}", width:getElSize(170),attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{
	        	    	field:"goalRatio", 
	        	    	title: "${Achievement_rate}", 
	        	    	width:getElSize(180),
	        	    	attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(36)+'px !important; overflow: initial;' +'#=(goalRatio>=100)?"color :rgb(79, 169, 95)":(goalRatio<100 &&goalRatio>=80)?"color: rgb(0, 255, 45)":(goalRatio<80 &&goalRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
						headerAttributes: {
		        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
		        	    },template:"#=goalRatio#%"
	        	    }
		        ]}	
		    ]
		}).data("kendoGrid")
		
		underScoreWorker = $("#underScoreWorker").kendoGrid({
			height:getElSize(645),
			columns: [
		        { title: "${below_standard} ${worker}",
		        	headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },
		        	columns:[
		        	{
		        		field:"rank", title: "${ranking}", width:getElSize(130),
		        		attributes: {
		        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
		        	    },headerAttributes: {
		        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
		        	    }
		        	},
		        	{field:"worker", title: "${worker}", attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{field:"target", title: "${target}", width:getElSize(180),attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{field:"cnt", title: "${Performance}", width:getElSize(170),attributes: {
		        	      style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
	        	    }},
		        	{
	        	    	field:"goalRatio", 
	        	    	title: "${Achievement_rate}", 
	        	    	width:getElSize(180),
	        	    	attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(36)+'px !important; overflow: initial;' +'#=(goalRatio>=100)?"color :rgb(79, 169, 95)":(goalRatio<100 &&goalRatio>=80)?"color: rgb(0, 255, 45)":(goalRatio<80 &&goalRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
						headerAttributes: {
		        	    	 style: "font-size:"+getElSize(36)+'px !important; overflow: initial;',
		        	    },template:"#=goalRatio#%"
	        	    }
		        ]}	
		    ]
		}).data("kendoGrid")
		
		/* byPrdNo = $("#byPrdNo").kendoGrid({
			height:getElSize(1560),
			columns: [
		        { title: "${By_car} ${ranking}",
		        	headerAttributes: {
	        	    	 style: "font-size:"+getElSize(38)+'px !important',
	        	    },
		        	columns:[
		        	{
		        		field:"rank", title: "${ranking}", width:getElSize(130),
		        		attributes: {
		        	    	 style: "font-size:"+getElSize(38)+'px !important',
		        	    },headerAttributes: {
		        	    	 style: "font-size:"+getElSize(38)+'px !important',
		        	    }
		        	},
		        	{field:"prdNo", title: "${Car_type}", attributes: {
		        	      style: "font-size:"+getElSize(38)+'px !important',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(38)+'px !important',
	        	    }},
		        	{field:"target", title: "${target}", width:getElSize(180),attributes: {
		        	      style: "font-size:"+getElSize(38)+'px !important',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(38)+'px !important',
	        	    }},
		        	{field:"cnt", title: "${Performance}", width:getElSize(170),attributes: {
		        	      style: "font-size:"+getElSize(38)+'px !important',
	        	    },headerAttributes: {
	        	    	 style: "font-size:"+getElSize(38)+'px !important',
	        	    }},
		        	{
	        	    	field:"goalRatio", 
	        	    	title: "${Achievement_rate}", 
	        	    	width:getElSize(180),
	        	    	attributes: { style: "font-size:"+getElSize(38)+'px !important;'+ 'border-top:0px; border-bottom:0.1px solid black; border-left: 0.1px solid black;'+'#=(goalRatio>=100)?"background:rgb(0, 65, 16)":(goalRatio<100 &&goalRatio>=80)?"background-color: green":(goalRatio<80 &&goalRatio>=60)?"background-color: yellow; color:black":"background-color: red"#'},
		        	    headerAttributes: {
		        	    	 style: "font-size:"+getElSize(38)+'px !important',
		        	    },template:"#=goalRatio#%"
	        	    }
		        ]}	
		    ]
		}).data("kendoGrid"); */
		//상세수정
		detailGrid = $("#detailGrid").kendoGrid({
    		scrollable: true,
    		height: getElSize(1600),
    		sortable : true,
    		filterable: {
    			messages: {
    		          search: "${search_Placeholder}"
    		        }
            },
            dataBound : function(e){
                collapseAllGroups(this);
            },
    		columns: [
   		    {
   		        field: "idx",
   		        hidden: true,
   		    	groupHeaderTemplate: '<div id="group" style="width:100%; display:inline; color:white;">'+
     			'<div id="rank" style="display: -webkit-inline-box">순위 : #=aggregates.rank.max# (#=aggregates.worker.max#)</div>' +
//     			'<div id="rank" style="display: -webkit-inline-box">순위 : #=(aggregates.rank.max>0)?aggregates.rank.max:"미등록"# (#=(aggregates.rank.max==aggregates.lastRank.max)?aggregates.worker.max+" 외 "+ aggregates.cntWorker.max + "명":aggregates.worker.max#)</div>' + 
     			'<div id="prdNoCnt" style="display: -webkit-inline-box">　</div> ' +
//     			'<div id="prdNoCnt" style="display: -webkit-inline-box">#=aggregates.prdNoCnt.max# 종</div> ' +
     			'<div id="dvcCnt" style="display: -webkit-inline-box">#=aggregates.name.count# 대</div> ' + 
     			'<div id="target" style="display: -webkit-inline-box">#=getTargetCnt(aggregates.worker.max)#</div> ' + 
     			'<div id="cnt" style="display: -webkit-inline-box">#=getCnt(aggregates.worker.max)#</div> ' + 
     			'<div id="faultCnt" style="display: -webkit-inline-box">#=getFaultyCnt(aggregates.worker.max)#</div> ' + 
     			'<div id="average" style="display: -webkit-inline-box">#=aggregates.average.max+"%"# </div>'+
     			'<div style="display: -webkit-inline-box">#=getOkRatio(aggregates.worker.max)#% </div>'+
     			'</div>'

/*    		    	groupHeaderTemplate: '<div id="group" style="width:100%; display:inline; color:white;">'+
     			'<div id="rank" style="display: -webkit-inline-box">순위 : #=(aggregates.rank.max==aggregates.lastRank.max)?aggregates.rank.max:"미등록"# (#=(aggregates.cntWorker.max==aggregates.lastRank.max)?aggregates.worker.max+" 외 "+ aggregates.cntWorker.max + "명":aggregates.worker.max#)</div>' + 
//     			'<div id="rank" style="display: -webkit-inline-box">순위 : #=(aggregates.rank.max>0)?aggregates.rank.max:"미등록"# (#=(aggregates.rank.max==aggregates.lastRank.max)?aggregates.worker.max+" 외 "+ aggregates.cntWorker.max + "명":aggregates.worker.max#)</div>' + 
     			'<div id="prdNoCnt" style="display: -webkit-inline-box">　</div> ' +
//     			'<div id="prdNoCnt" style="display: -webkit-inline-box">#=aggregates.prdNoCnt.max# 종</div> ' +
     			'<div id="dvcCnt" style="display: -webkit-inline-box">#=aggregates.name.count# 대</div> ' + 
     			'<div id="target" style="display: -webkit-inline-box">#=getTargetCnt(aggregates.worker.max)#</div> ' + 
     			'<div id="cnt" style="display: -webkit-inline-box">#=getCnt(aggregates.worker.max)#</div> ' + 
     			'<div id="faultCnt" style="display: -webkit-inline-box">#=getFaultyCnt(aggregates.worker.max)#</div> ' + 
     			'<div id="average" style="display: -webkit-inline-box">#=(aggregates.rank.max==aggregates.lastRank.max)?"1% 미만":aggregates.average.max+"%"# </div>'+
     			'<div style="display: -webkit-inline-box">#=getOkRatio(aggregates.worker.max)#% </div>'+
     			'</div>'
 */   		     },
   		     
				
			{
				field: "worker",
				title: "${worker}",
				sortable:false,
				filterable: false,
				width: getElSize(160),
				attributes: {
				    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
				},headerAttributes:{
					style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
			    }
			},
    		{
                field: "prdNo",
                title: "${prdct_machine_line}",
                width: getElSize(300),
                sortable:false,
                filterable: false,
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                field: "oprNo",
                title: "${operation}",
                sortable:false,
                filterable: false,
                width: getElSize(200),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                field: "name",
                title: "${device}",
                width: getElSize(250),
                sortable:false,
                filterable: false,
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                field: "nd",
                title: "  ${division}",
                width: getElSize(160),
                sortable:false,
                filterable: false,
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                
                title: "${work_Performance}",
                headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                },
                columns: [
               	{
                	field:"target",
                	title: "${target}${Quantity}",
                    sortable:false,
                    filterable: false,
                	width: getElSize(160),
                    attributes: {
                        style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                    },headerAttributes:{
                    	style: "text-align: center; font-size:" + getElSize(30) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                    },aggregates: ["sum"]
                },
                {
                	title:"${Performance} ${Quantity}",
                    field: "cnt",
                    width: 80,
                    sortable:false,
                    filterable: false,
                    attributes: {
                        style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                    },headerAttributes:{
                    	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                    }
                },{
                	title:"${faulty}",
                	field:"faultCnt",
                	width: getElSize(160),
                	sortable:false,
                	filterable: false,
                    attributes: {
                        style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                    },headerAttributes:{
                    	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                    }
                },{
                	title:"${achievement_ratio}",
                	field:"goalRatio",
                	width: getElSize(110),
                	sortable:false,
                	filterable: false,
                	aggregates: ["average"],
                	attributes: { style: "font-weight:bolder;  text-shadow: -1px 0 black, 0 1px black, 1px 0 black, 0 -1px black; font-size:"+getElSize(45)+'px !important; overflow: initial;' +'#=(goalRatio>=100)?"color :rgb(79, 169, 95)":(goalRatio<100 &&goalRatio>=80)?"color: rgb(0, 255, 45)":(goalRatio<80 &&goalRatio>=60)?"color: yellow":"color: rgb(255, 122, 122)"#'},
					headerAttributes:{
                    	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                    },template:"#=goalRatio#%"
                },{
                	title:"${ok_ratio}",
                	field:"okRatio",
                	width: getElSize(110),
                	sortable:false,
                	filterable: false,
                    attributes: {
                        style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                    },headerAttributes:{
                    	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                    } 
                }]
            }
            ]
    	}).data("kendoGrid");
		
		//팝업창
/* 		getProductionStatus();
		getprodataTime()
		$("#time").html("데이터수신 시간 : "+dataTime+ " 현재 시간: " + getToday());
 */		
		$("#viewByWorker").kendoDialog({
/* 			scrollable: true
			, */
			height: getElSize(900)
			,title: viewNm+"작업 내역"
		 	/* ,content: "<table border=1 style='color:black;'> <tr> <th> Tool Cycle Count </th> <th> RunTime (h) </th> </tr>" +
		 			 "<tr> <td> <input id='preCnt' type='number' value=0> </td> <td>  <input id='preSec' type='number' value=0> </td> </tr> </table>"
			 */,actions: [{
				text: "OK"
				,action: function(e){
				//	alert("popup")
				},
				primary: true
			}]
		});
		
		$("#viewByWorker").css({
			"background" : "#D9E5FF",
			"overflow": "auto"
		});
		$("#viewByWorker").data("kendoDialog").close();
		
		//작업자 순위 더블클릭시
		$("#byWorker").on("dblclick", "tbody>tr", function (e) {
			var dataItem = $("#byWorker").data("kendoGrid").dataSource.getByUid(this.dataset.uid);
			console.log(dataItem.worker)
			
			viewNm=dataItem.worker
			$("#viewByWorker").data("kendoDialog").title(viewNm + " 작업 내역")
			
			var time1=dataTime.substr(14,2)
			var time2=moment().format("mm")
			var diffTime = Number(time1)-Number(time2);
			if(diffTime>=2 || diffTime<=-2){
				getProductionStatus();
				getprodataTime()
				$("#time").html("데이터수신 시간 : "+dataTime+ " 현재 시간: " + getToday())
			}else{
			}
			//getProductionStatus();
			
			var sDate = $("input#sDate").val();
			var	eDate = sDate;
			var url = "${ctxPath}/kpi/viewByWorker.do";
			var param = "sDate=" + sDate +
						"&eDate=" + eDate +
						"&workIdx=" + $("#workIdx").val() +
						"&worker=" + dataItem.worker;
			$.ajax({
				url : url,
				data : param,
				dataType : "json",
				type : "post",
				success : function(data){
					console.log("--backUp--")
					console.log(data);
					var json=data.dataList;
					var table ="<div><table style='color:black;'>" +
							"<tr class='viewCss'><td> 장비명 </td> <td> 차종 </td> <td> 계획수량 </td> <td> 실적수량 </td></tr>"
					
					//총합계
					var tgCyl=0;
					var cntCyl=0;
					var dvc=0;
//					$("#viewByWorker").empty();
					$(json).each(function(idx,data){
						table += "<tr class='viewCss'><td>" + decode(data.name) + "</td>"
						table += "<td>" + data.prdNo + "</td>"
						table += "<td>" + data.tgCyl + "</td>"
						table += "<td>" + data.cntCyl + "</td></tr>"
						dvc++;
						tgCyl+=Number(data.tgCyl);
						cntCyl+=Number(data.cntCyl);
					})
					table += "<tr id='viewTotalCss'><td> 합계 </td><td></td><td>"+tgCyl+"</td><td>"+cntCyl+"</td></tr>"
					table += "</table></div>";
					$("#viewByWorker").data("kendoDialog").content(table);
					
					$(".viewCss td").css({
						"padding-left":getElSize(100),
						"padding-right":getElSize(100),
						"padding-bottom":getElSize(5),
						"padding-top":getElSize(5),
						"font-size" : getElSize(52.1),
						"text-align":"center"
					});
//					$(".viewCss").css("text-align","center")
					$("#viewTotalCss td").css({
						"background":"#C7D3ED",
						"text-align":"center",
						"font-weight": "bold"

					});
					
					$("#viewByWorker").data("kendoDialog").open();

				},error : function(){
					alert("관리자 문의")
				}
			})
			
		});

		//기준 미달 작업자 더블클릭시
		$("#underScoreWorker").on("dblclick", "tbody>tr", function (e) {
			var dataItem = $("#underScoreWorker").data("kendoGrid").dataSource.getByUid(this.dataset.uid);
			console.log(dataItem.worker)
			
			viewNm=dataItem.worker
			$("#viewByWorker").data("kendoDialog").title(viewNm + " 작업 내역")
			var time1=dataTime.substr(14,2)
			var time2=moment().format("mm")
			var diffTime = Number(time1)-Number(time2);
			if(diffTime>=2 || diffTime<=-2){
				getProductionStatus();
				getprodataTime()
				$("#time").html("데이터수신 시간 : "+dataTime+ " 현재 시간: " + getToday())
			}else{
			}
			//getProductionStatus();
			
			var sDate = $("input#sDate").val();
			var	eDate = sDate;
			var url = "${ctxPath}/kpi/viewByWorker.do";
			var param = "sDate=" + sDate +
						"&eDate=" + eDate +
						"&workIdx=" + $("#workIdx").val() +
						"&worker=" + dataItem.worker;
			$.ajax({
				url : url,
				data : param,
				dataType : "json",
				type : "post",
				success : function(data){
					console.log("--backUp--")
					console.log(data);
					var json=data.dataList;
					var table ="<table style='color:black;'>" +
							"<tr class='viewCss'><td> 장비명 </td> <td> 차종 </td> <td> 계획수량 </td> <td> 실적수량 </td></tr>"
					
					//총합계
					var tgCyl=0;
					var cntCyl=0;
					var dvc=0;
//					$("#viewByWorker").empty();
					$(json).each(function(idx,data){
						table += "<tr class='viewCss'><td>" + decode(data.name) + "</td>"
						table += "<td>" + data.prdNo + "</td>"
						table += "<td>" + data.tgCyl + "</td>"
						table += "<td>" + data.cntCyl + "</td></tr>"
						dvc++;
						tgCyl+=Number(data.tgCyl);
						cntCyl+=Number(data.cntCyl);
					})
					table += "<tr id='viewTotalCss'><td> 합계 </td><td></td><td>"+tgCyl+"</td><td>"+cntCyl+"</td></tr>"
					table += "</table>";
					$("#viewByWorker").data("kendoDialog").content(table);
					
//					$("#viewByWorker").data("kendoDialog").open();
					
					$(".viewCss td").css({
						//"width":getElSize(460),
						"padding-left":getElSize(100),
						"padding-right":getElSize(100),
						"padding-bottom":getElSize(5),
						"padding-top":getElSize(5),
						"font-size" : getElSize(52.1),
						"text-align":"center"
					});

//					$(".viewCss").css("text-align","center")
					$("#viewTotalCss td").css({
						"background":"#C7D3ED",
						"text-align":"center",
						"font-weight": "bold"

					});
					
					$("#viewByWorker").data("kendoDialog").open();

				},error : function(){
					alert("관리자 문의")
				}
			})
		});
		
		//장비별 순위 더블클릭시
		$("#byDevice").on("dblclick", "tbody>tr", function (e) {
			var dataItem = $("#byDevice").data("kendoGrid").dataSource.getByUid(this.dataset.uid);
			console.log(dataItem.name)
			
			viewNm=dataItem.name
			var likeNm="";
			if(viewNm.indexOf("/")==-1){
				likeNm = viewNm.substring(0,viewNm.indexOf(" "))
			}else{
				likeNm = viewNm.substring(0,viewNm.indexOf("/"))
			}
			
			$("#viewByWorker").data("kendoDialog").title(viewNm)
			
			var sDate = $("input#sDate").val();
			var	eDate = sDate;
			var url = "${ctxPath}/kpi/viewByDvc.do";
			var param = "sDate=" + sDate +
						"&eDate=" + eDate +
						"&workIdx=" + $("#workIdx").val() +
						"&name=" + likeNm;
			$.ajax({
				url : url,
				data : param,
				dataType : "json",
				type : "post",
				success : function(data){
					var json=data.dataList;
					var table ="<table style='color:black;'><tr class='viewCss'><td>장비</td><td>차종</td><td>공정</td><td>작업자</td><td>aIdoo 연결여부</td><td>계획</td><td>실적</td><td>달성율</td></tr>" ;
					
					//총합계
					var tgCyl=0;
					var cntCyl=0;
					var dvc=0;
//					$("#viewByWorker").empty();
					$(json).each(function(idx,data){
						var oprNo;
						var chkColor="normal";
						if(data.oprNo=="0010"){
							oprNo="R 삭"
						}else if(data.oprNo=="0020"){
							oprNo="MCT 삭"
						}else if(data.oprNo=="0030"){
							oprNo="CNC 삭"
						}else{
							oprNo=data.oprNo
						}
						if(decode(data.dvcName)==viewNm){
							chkColor="bold"
						}
						
						table += "<tr class='viewCss' style='font-weight:" + chkColor + "'><td> " + decode(data.dvcName) + "</td>"
						table += "<td> " + data.prdNo + " </td>"
						table += "<td> " + oprNo + " </td>"
						table += "<td> " + decode(data.name) + " </td>"
//						if(Number(data.dvcId)<=104 || (data.dvcId==105 || data.dvcId==106 || data.dvcId==107 || data.dvcId==108 || data.dvcId==109 || data.dvcId==110 || data.dvcId==111 || data.dvcId==112 || data.dvcId==114 || data.dvcId==119 || data.dvcId==125 || data.dvcId==126 || data.dvcId==127 || data.dvcId==128 || data.dvcId==129 || data.dvcId==131 || data.dvcId==132 || data.dvcId==144 || data.dvcId==145 || data.dvcId==146)){
						if(data.dvcId!=133 && data.dvcId!=147){
							table += "<td> " + "O" + " </td>"
						}else{
							table += "<td> " + "X" + " </td>"
						}
						table += "<td> " + data.tgCyl + " </td>"
						table += "<td> " + data.cnt + " </td>"
						table += "<td> " + data.goalRatio + "% </td></tr>"
						dvc++;
						tgCyl+=Number(data.tgCyl);
						cntCyl+=Number(data.cntCyl);
					})
					table += "</table>";
					$("#viewByWorker").data("kendoDialog").content(table);
					$("#viewByWorker").data("kendoDialog").open();
					
					$(".viewCss td").css({
						//"width":getElSize(460),
						"padding-left":getElSize(22),
						"padding-right":getElSize(22),
						"padding-bottom":getElSize(5),
						"padding-top":getElSize(5),
						"font-size" : getElSize(52.1),
						"text-align":"center"
					});
//					$(".viewCss").css("text-align","center")
					$("#viewTotalCss td").css({
						"background":"#C7D3ED",
						"text-align":"center",
						"font-weight": "bold"

					});
					console.log(data.dataList)
				},error : function(){
					alert("관리자 문의")
				}
			}) 
			
		});
	    	
		//기준 미달 장비 더블클릭시
		$("#underScoreDevice").on("dblclick", "tbody>tr", function (e) {
			var dataItem = $("#underScoreDevice").data("kendoGrid").dataSource.getByUid(this.dataset.uid);
			console.log(dataItem.name)
			
			viewNm=dataItem.name
			var likeNm="";
			if(viewNm.indexOf("/")==-1){
				likeNm = viewNm.substring(0,viewNm.indexOf(" "))
			}else{
				likeNm = viewNm.substring(0,viewNm.indexOf("/"))
			}
			
			$("#viewByWorker").data("kendoDialog").title(viewNm)
			

			var sDate = $("input#sDate").val();
			var	eDate = sDate;
			var url = "${ctxPath}/kpi/viewByDvc.do";
			var param = "sDate=" + sDate +
						"&eDate=" + eDate +
						"&workIdx=" + $("#workIdx").val() +
						"&name=" + likeNm;
			$.ajax({
				url : url,
				data : param,
				dataType : "json",
				type : "post",
				success : function(data){
					var json=data.dataList;
					var table ="<table style='color:black;'><tr class='viewCss'><td>장비</td><td>차종</td><td>공정</td><td>작업자</td><td>aIdoo 연결여부</td><td>계획</td><td>실적</td><td>달성율</td></tr>" ;
					
					//총합계
					var tgCyl=0;
					var cntCyl=0;
					var dvc=0;
//					$("#viewByWorker").empty();
					$(json).each(function(idx,data){
						var oprNo;
						var chkColor="normal";

						if(data.oprNo=="0010"){
							oprNo="R 삭"
						}else if(data.oprNo=="0020"){
							oprNo="MCT 삭"
						}else if(data.oprNo=="0030"){
							oprNo="CNC 삭"
						}else{
							oprNo=data.oprNo
						}
						if(decode(data.dvcName)==viewNm){
							chkColor="bold"
						}
						
						table += "<tr class='viewCss' style='font-weight:" + chkColor + "'><td> " + decode(data.dvcName) + "</td>"
						table += "<td> " + data.prdNo + " </td>"
						table += "<td> " + oprNo + " </td>"
						table += "<td> " + decode(data.name) + " </td>"
//						if(Number(data.dvcId)<=104 || (data.dvcId==105 || data.dvcId==106 || data.dvcId==107 || data.dvcId==108 || data.dvcId==109 || data.dvcId==110 || data.dvcId==111 || data.dvcId==112 || data.dvcId==114 || data.dvcId==119 || data.dvcId==125 || data.dvcId==126 || data.dvcId==127 || data.dvcId==128 || data.dvcId==129 || data.dvcId==131 || data.dvcId==132 || data.dvcId==144 || data.dvcId==145 || data.dvcId==146)){
						if(data.dvcId!=133 && data.dvcId!=147){
							table += "<td> " + "O" + " </td>"
						}else{
							table += "<td> " + "X" + " </td>"
						}
						table += "<td> " + data.tgCyl + " </td>"
						table += "<td> " + data.cnt + " </td>"
						table += "<td> " + data.goalRatio + "% </td></tr>"
						dvc++;
						tgCyl+=Number(data.tgCyl);
						cntCyl+=Number(data.cntCyl);
					})
					
					table += "</table>";
					$("#viewByWorker").data("kendoDialog").content(table);
					$("#viewByWorker").data("kendoDialog").open();
					
					$(".viewCss td").css({
						//"width":getElSize(460),
						"padding-left":getElSize(22),
						"padding-right":getElSize(22),
						"padding-bottom":getElSize(5),
						"padding-top":getElSize(5),
						"font-size" : getElSize(52.1),
						"text-align":"center"
					});
//					$(".viewCss").css("text-align","center")
					$("#viewTotalCss td").css({
						"background":"#C7D3ED",
						"text-align":"center",
						"font-weight": "bold"

					});
					console.log(data.dataList)
				},error : function(){
					alert("관리자 문의")
				}
			}) 
			
		});
		
	})
	
	function getTargetCnt(name){
		list = detailGrid.dataSource.data()
		
		var length = list.length
		var targetCnt=0;
		for(var i=0;i<length;i++){
			if(list[i].worker==name){
				targetCnt+=Number(list[i].target)
			}
		}
		return targetCnt;
	}
	
	function getCnt(name){
		list = detailGrid.dataSource.data()
		
		var length = list.length
		var cnt=0;
		for(var i=0;i<length;i++){
			if(list[i].worker==name){
				cnt+=Number(list[i].cnt)
			}
		}
		return cnt;
	}
	
	function getFaultyCnt(name){
		list = detailGrid.dataSource.data()
		
		var length = list.length
		var faultCnt=0;
		for(var i=0;i<length;i++){
			if(list[i].worker==name){
				faultCnt+=Number(list[i].faultCnt)
			}
		}
		return faultCnt;
	}
	
	function getOkRatio(name){
		list = detailGrid.dataSource.data()
		
		var length = list.length
		var okRatio=0;
		var okRatioCnt=0;
		for(var i=0;i<length;i++){
			if(list[i].worker==name){
				if(Number(list[i].okRatio!=0)){
					okRatio+=Number(list[i].okRatio);
					okRatioCnt++;
				}
			}
		}
		var value;
		if(isNaN(okRatio/okRatioCnt)){
			value=0
		}else{
			if((okRatio/okRatioCnt)==100){
				value=100
			}else{
				value=(okRatio/okRatioCnt).toFixed(1)
			}
		}
		
		return value;
//		return (isNaN(okRatio/okRatioCnt))?0:(okRatio/okRatioCnt).toFixed(1);
	}
	
	var workerStandard, deviceStandard;
	function getProductionStatus(eDate){
		
		$.showLoading();
		
		var url = "${ctxPath}/kpi/getStandard.do";
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				workerStandard=json[0].workerStandard
				deviceStandard=json[0].deviceStandard
				$("#workerStandard").val(workerStandard)
				$("#deviceStandard").val(deviceStandard)
			}
		})
		
		var sDate = $("input#sDate").val();
		var eDate = $("input#eDate").val();
		if(eDate==null || eDate=='' || typeof eDate=='undefined'){
			eDate=sDate
		}
		
		console.log(sDate)
		console.log(eDate)
		
		var url = "${ctxPath}/kpi/getWorkerCnt.do";
		var param = "sDate=" + sDate +
					"&eDate=" + eDate	
		if(checked==0){
			param = param + "&workIdx=" + $("#workIdx").val()
		}else if(checked==1){
			if($('input#sMonth').val()==''){
				kendo.alert("월별 검색시 검색 할 월을 선택해주세요.")
				$.hideLoading();
				return;
			}
		}else{
			
		}
		var underScoreWorkerData = new kendo.data.DataSource({});
		var underScoreDeviceData = new kendo.data.DataSource({});

		console.log(param)

		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				var dataSource = new kendo.data.DataSource({});
				var total=0, percent=0;
				$(json).each(function(idx, data){
						data.worker = decode(data.worker)
						data.goalRatio = Number(data.goalRatio)
						total += data.goalRatio;
						
						if(data.goalRatio>=workerStandard){
							dataSource.add(data)
						}else{
							underScoreWorkerData.add(data)
						}
				});
				percent = total/json.length
				
				
				var array = (percent.toFixed(2)).split('.')
				
				if(isNaN(array[0])){
					
					$("#workerPer").html('0.')
					$("#workerPerPoint").html('00%')
				}else{
					
					$("#workerPer").html(array[0]+'.')
					$("#workerPerPoint").html(array[1]+'%')
				}
				
				$("#workerPerPoint").css({
					"font-size" : getElSize(40),
					"position" : "relative",
					"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
					"top" : getElSize(13)
				})
				
				$("#workerPer").css({
					"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
					"float" : "right"
				})
				if(percent>=100){
					$("#workerPer, #workerPerPoint").css({
						"color" : "rgb(79, 169, 95)"
					})
				}else if(percent<100 && percent>=80){
					$("#workerPer, #workerPerPoint").css({
						"color" : "#008000"
					})
				}else if(percent<80 && percent>=60){
					$("#workerPer, #workerPerPoint").css({
						"color" : "yellow"
					})
				}else{
					$("#workerPer, #workerPerPoint").css({
						"color" : "#ff7a7a"
					})
				}
				underScoreWorker.setDataSource(underScoreWorkerData)
				byWorker.setDataSource(dataSource);
				
				
				$(".workerDateInput, .deviceDateInput").css({
					"background" : "linear-gradient(rgb(240, 248, 255), rgb(132, 198, 255))",
					"color" : "black",
					"text-shadow" : ""
				})
			
				$("#workerDay, #deviceDay").css({
					"background" : "rgb(132, 198, 255)",
					"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
					"color" : "white"
				})
				
				$(".ui-datepicker.ui-widget.ui-widget-content.ui-helper-clearfix.ui-corner-all").css("font-size",getElSize(42))

				
				$(".k-grid").css({
					"border-radius": getElSize(8)
				})
				
				$(".k-grid tbody tr td").css({
					"height" : getElSize(96)
				})
			}
		});
		
		var url = "${ctxPath}/kpi/getDeviceCnt.do";
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				
				
				var json = data.dataList;
				var dataSource = new kendo.data.DataSource({
					
				});
				var total=0, percent=0;
				$(json).each(function(idx, data){
						data.name = decode(data.name)
						data.goalRatio = Number(data.goalRatio);
						total += data.goalRatio;
						if(data.goalRatio>=deviceStandard){
							dataSource.add(data)
						}else{
							underScoreDeviceData.add(data)
						}
				});
				percent = total/json.length
				if(isNaN(percent)){
					percent=0
				}
				
				var array = (percent.toFixed(2)).split('.')
				
				$("#devicePer").html(array[0]+'.')
				$("#devicePerPoint").html(array[1]+'%')
				
				$("#devicePerPoint").css({
					"font-size" : getElSize(40),
					"position" : "relative",
					"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
					"top" : getElSize(13)
				})
				
				$("#devicePer").css({
					"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
					"float" : "right"
				})
				if(percent>=100){
					$("#devicePer, #devicePerPoint").css({
						"color" : "rgb(79, 169, 95)"
					})
				}else if(percent<100 && percent>=80){
					$("#devicePer, #devicePerPoint").css({
						"color" : "#008000"
					})
				}else if(percent<80 && percent>=60){
					$("#devicePer, #devicePerPoint").css({
						"color" : "yellow"
					})
				}else{
					$("#devicePer, #devicePerPoint").css({
						"color" : "#ff7a7a"
					})
				}
				underScoreDevice.setDataSource(underScoreDeviceData)
				byDevice.setDataSource(dataSource);
				$.hideLoading();
			}
		});
		
		getDetailStatus(); 

		$('#print').attr('disabled', false);
		
	}
	
	
	var detailCheck = false;
	function getDetailProduction(){
		
		if(detailCheck){
			
			$("#detailBtn").prop('value', '${Detail_screen}');
			$("#simpleStatus, #simpleStatus2").css({
				"display":"table-row"
			})
			$("#detailStatus").css({
				"display":"none",
			})
			$(".k-grid-content.k-auto-scrollable").css({"height":getElSize(1126.3)})
			$("#underScoreWorker div.k-grid-content.k-auto-scrollable").css({"height":getElSize(482)})
			$("#underScoreDevice div.k-grid-content.k-auto-scrollable").css({"height":getElSize(482)})
			detailCheck= false;
		}else{
			$("#detailBtn").prop('value', '${Simple_screen}');
			$("#simpleStatus, #simpleStatus2").css({
				"display":"none"
			})
			$("#detailStatus").css({
				"display":"table-row",
				"position": "absolute",
				"top" : $("#div1").offset().top + getElSize(10),
				"width" : getElSize(3840)
			})
			$(".k-grid-content.k-auto-scrollable").css({"height":getElSize(1307)})
			detailCheck= true;
		}
		
		
	}
	
	function getDetailStatus(){
		
		$.showLoading();
		
		var sDate = $("input#sDate").val();
		var eDate = $("input#eDate").val();
		
		var dataSource = new kendo.data.DataSource({});
		
		$("#print_table").empty();
		
		var table ="<table class='p_table' border='5' width='95%' align='center'><tr><th colspan='5' rowspan='3'>생산완료입력 조회" +
								"<br>"+ $("#sDate").val() +'~' + $("#eDate").val() +"</th><th style='background-color:lightgray' colspan='3'>결제</th></tr>" +
								"<tr style='background-color:lightgray'><th>담당</th><th>검토</th><th>승인</th></tr>" +
								"<tr class='sign' ><th> </th><th> </th><th> </th></tr>" +
								"<tr style='background-color:lightgray'><th>차종</th><th>공정</th><th>장비</th><th>목표수량</th><th>실적수량</th><th>불량</th><th>달성율</th><th>양품율</th></tr>" ;
								
								
		var url = "${ctxPath}/kpi/getDetailProductStatus.do";
		var param = "sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() +
					"&workIdx=" + $("#workIdx").val();
		var checked = $(":input:radio[name=datePicker]:checked").val();
					
		if(checked==0){
			param = param + "&workIdx=" + $("#workIdx").val()
		}else if(checked==1){
			if($('input#sMonth').val()==''){
				kendo.alert("월별 검색시 검색 할 월을 선택해주세요.")
				$.hideLoading();
				return;
			}
		}else{
		}
		//상세수정
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				$(json).each(function(idx, data){
						var nd;
						if(data.nd==1){
							data.nd = "${night}"
						}else{
							data.nd = "${day}"
						};
						var oprNo;
						if(data.oprNm=="0010"){
							data.oprNo = "R삭";
						}else if(data.oprNm=="0020"){
							data.oprNo = "MCT 1차";
						}else if(data.oprNm=="0030"){
							data.oprNo = "CNC 2차";
						}
						
						data.name = decode(data.name)
						data.worker = decode(data.worker)
						var diff = moment(data.eDate).diff(moment(data.sDate), 'hours')
						if(isNaN(diff)){
						}else{
							data.workTime=diff
						}
						data.rank = Number(data.rank)
						data.goalRatio = Number(data.goalRatio);
						data.diff_cnt = (Number(data.workerCnt) - Number(data.cnt))
						dataSource.add(data)
					
				});
				
				dataSource.group([
					{ field:'idx', dir:'asc',aggregates: [
					      { field: "rank", aggregate: "max"},
					      { field: "worker", aggregate: "count"},
					      { field: "worker", aggregate: "max"},
					      { field: "average",  aggregate: "max"},
					      { field: "name",  aggregate: "count"},
					      { field: "cntWorker",  aggregate: "max"},
					      { field: "lastRank",  aggregate: "max"},
					      { field: "dvcCnt",  aggregate: "max"},
					      { field: "prdNoCnt",  aggregate: "max"},
					      { field: "target",  aggregate: "sum"},
					      { field: "cnt",  aggregate: "sum"},
					      { field: "faultCnt",  aggregate: "sum"},
					      { field: "okRatio",  aggregate: "average"}
					    ]}
				]);
				
				dataSource.sort([
					{ field: "average", dir: "desc" }
					,{ field: "worker", dir: "asc" }
				])
				
				dataSource.aggregate([
					{ field: "rank", aggregate: "max"},
					{ field: "average", aggregate: "max"},
					{ field: "cntWorker", aggregate: "max"},
					{ field: "dvcCnt", aggregate: "max"},
					{ field: "prdNoCnt", aggregate: "max"}
				])
				 
				detailGrid.setDataSource(dataSource);
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("body, html").css({
					"overflow" : "hidden"	
				})
				
			var dataItem=detailGrid.dataSource.data();
			
					
			//달성률로 object 정렬
			dataItem.sort(function (a, b) {
				if(a.average == b.average){
					var x = a.worker, y=b.worker;
					return x<y ? -1 : x>y ? 1 : 0;
				}
				return b.average - a.average;
			});
			//print table 그리는 부분  (행:9)
	
			var chk=1;
			
			//프린트 그리는 총 테이블 갯수 구하기 chk
			$(dataItem).each(function(idx,data){
				chk++;
				if(idx!=0){
					if(dataItem[idx].worker!=dataItem[idx-1].worker){
						chk++;
					}
				}
			});
			//테이블 몇번째 열 까지 뽑을지 (A4사이즈 맞추기 위해서)
			var divideNum=37;		
			var page=1;		//page size표시
			var totalpage=Math.ceil(chk/divideNum);		//총 페이지수
			var index=0;	//행 갯수 세기
			var rank=1;
			var countDvc=[]; //장비 갯수 세기
			
			var target=0//목표수
			var cnt=0//실적수
			var goalRatio=0;//달성율
			var faultCnt=0; //불량수
			var okRatio=0;//양품율
			var okRatio_cnt=0;//양품율총갯수

			$(dataItem).each(function(idx,data){
				
					//한개의 tr
					if(idx==0){
						for(i=0;i<dataItem.length;i++){
							if(data.worker==dataItem[i].worker){
								target+=Number(dataItem[i].target);
								cnt+=Number(dataItem[i].cnt);
								faultCnt+=Number(dataItem[i].faultCnt);
								if(dataItem[i].cnt!=00){
									okRatio+=Number(dataItem[i].okRatio);
									okRatio_cnt++;
								}
								if(countDvc.indexOf(dataItem[i].name)==-1){
									countDvc.push(dataItem[i].name)
								}
							}
						}
						table +="<tr style='background-color:lightgray' class='totalwork'><td> 작업자 : "+ data.worker +"</td><td>순위 : " + rank + "</td><td> 총 :  " + countDvc.length + "  대</td><td> "+ target +" </td><td> "+ cnt +" </td><td> "+ faultCnt +" </td><td> 총 : " + data.average + " %</td> <td> " + Math.ceil(okRatio/okRatio_cnt) +" %</td>";
						rank++;
					}else{
						countDvc=[];
						target=0//목표수
						cnt=0//실적수
						goalRatio=0;//달성율
						faultCnt=0; //불량수
						okRatio=0;//양품율
						okRatio_cnt=0;//양품율총갯수
						
						for(i=0;i<dataItem.length;i++){
							if(data.worker==dataItem[i].worker){
								target+=Number(dataItem[i].target);
								cnt+=Number(dataItem[i].cnt);
								faultCnt+=Number(dataItem[i].faultCnt);
								if(dataItem[i].cnt!=00){
									okRatio+=Number(dataItem[i].okRatio);
									okRatio_cnt++;
								}
								if(countDvc.indexOf(dataItem[i].name)==-1){
									countDvc.push(dataItem[i].name)
								}
							}
						}

						index++;
						if(dataItem[idx].worker!=dataItem[idx-1].worker){
							index++;
							table +="<tr style='background-color:lightgray' class='totalwork'><td> 작업자 : "+ data.worker +"</td><td>순위 : " + rank + "</td><td> 총 :  " + countDvc.length + "  대</td><td> "+ target +" </td><td> "+ cnt +" </td><td> "+ faultCnt +" </td><td> 총 : " + data.average + " %</td> <td> " + ((isNaN(Math.ceil(okRatio/okRatio_cnt)))?0:(Math.ceil(okRatio/okRatio_cnt))) +" %</td>";
							
							if(data.average!=0){
								rank++;
							}
						}
					}
					
					//tr
					table +="<tr><td>" + data.prdNo + "</td><td>" +	data.oprNo + "</td><td>" + data.name + "</td><td>" + data.target + "</td><td>" + data.cnt + "</td><td>" + data.faultCnt + "</td><td>" + data.goalRatio + "</td><td>" + data.okRatio  + "</td></tr>" ;
					
					//a4 size page 넘기기
					if(idx!=0 && index>divideNum ){	
						page++;
						index=1;
						table +="</table>" +
								"<div style='page-break-before:always'></div>";
								
						table +="<br><br><br><br> <table class='p_table' border='5' width='95%' align='center'><tr class='sign'><th colspan='7' rowspan='2'>생산완료입력 조회"+
								"<br>"+ $("#sDate").val() +'~' + $("#eDate").val() +
								"</th><th colspan='2' style='background-color:lightgray'>페이지</th></tr>" +
								"<tr class='sign'><th colspan='2'>" + page +" / " + totalpage +"</th></tr>" +
								"<tr style='background-color:lightgray'><th>차종</th> <th>공정</th> <th>장비</th> <th>목표수량</th> <th>실적수량</th> <th class='sign'>불량</th> <th class='sign'>달성율</th> <th class='sign'>양품율</th></tr>";
								
						}
				})
				
				table +="</table>"
				$("#print_table").append(table);
				
				//print css
				$(".sign").css({
					"height" : "50px"
				})
				$(".sign th").css({
					"width" : "70px"
				})
	
				$(".p_table tr th").css("font-size","11px")
				$(".p_table tr td").css("font-size","11px")
				
				$("div#rank").css({
					"width" : getElSize(500)
				})
				
				$("div#prdNoCnt").css({
					"width" : "26%"
				})
				
				$("div#group").css({
					"font-size" : getElSize(40)
				})
				
				$("div#dvcCnt").css({
					"width" : getElSize(620)
				})
				
				$("div#target").css({
					"width" : getElSize(305)
				})
				
				$("div#cnt").css({
					"width" : getElSize(356)
				})
				
				$("div#faultCnt").css({
					"width" : getElSize(188)
				})
				
				$("div#average").css({
					"width" : getElSize(150)
				})
				
				//$(".k-grid-content.k-auto-scrollable").css({"height":getElSize(1300)})
				
			},error:function(request,status,error){
				$.hideLoading(); 
		    }
		});
	};
	
	var printChkWin=false;
	
	function pop_print(){
		printChkWin=true;
		win = window.open("","","height=" + screen.height + ",width=" + screen.width + "fullscreen=yes");
        self.focus();
        win.document.open();
        win.document.write('<'+'html'+'><'+'head'+'><'+'style'+'>');
        win.document.write('body, td { font-family: Verdana; font-size: 10pt;}');
        win.document.write('<'+'/'+'style'+'><'+'/'+'head'+'><'+'body'+'>');
        win.document.write(document.getElementById('print_table').innerHTML);
        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
        win.document.close();
        
        if(printChkWin==true){
			$(window).click(function() {
				//Hide the menus if visible
				alert('프린터 창이 열려 있습니다')
			});
		}

        win.print();
        
        printChkWin=false;
		$(window).off("click");
        
        win.close();
	}
	
	function changeProductionStatus(e){
		var id = $(e).attr("id")
		var val = $("#"+id).val()
		
		var sDate, eDate;
		
		$.showLoading();
		
		if($(e).attr("class")=='workerDateInput'){
			$(".workerDateInput").css({
				"background" : "linear-gradient(rgb(240, 248, 255), rgb(132, 198, 255))",
				"color" : "black",
				"text-shadow" : ""
			})
			
			$("#"+id).css({
				"background" : "rgb(132, 198, 255)",
				"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
				"color" : "white"
			})
			
			if(id=='workerDay'){
				
				var array = ($("#sDate").val()).split('-');
				var sDate=$("#sDate").val();
				var eDate=array[0]+'-'+array[1]+'-'+$("#"+id).val()
				
				var result = eDate.indexOf('일');
				
				eDate=eDate.substring(0,result);
				console.log("sDate : " +$("#sDate").val())
				console.log("eDate : " +eDate)
				
				
			}else if(id=='workerMonth'){
				
				var array = ($("#sDate").val()).split('-');
				var sDate=array[0]+'-'+array[1]+'-'+ '01'
				var eDate=array[0]+'-'+array[1]+'-'+ '31'
				
				
				console.log("sDate : " + sDate)
				console.log("eDate : " + eDate)
				
			}else if(id=='workerYear'){
				var array = ($("#sDate").val()).split('-');
				var sDate=array[0]+'-'+'01'+'-'+ '01'
				var eDate=array[0]+'-'+'12'+'-'+'31'
				
				console.log("sDate : " + sDate)
				console.log("eDate : " + eDate)
			}
			
			var underScoreWorkerData = new kendo.data.DataSource({});
			
			var url = "${ctxPath}/kpi/getWorkerCnt.do";
			var param = "sDate=" + sDate + "&eDate=" + eDate
			
			if(checked==0){
				param = param + "&workIdx=" + $("#workIdx").val()
			}
			
			$.ajax({
				url : url,
				data : param,
				dataType : "json",
				type : "post",
				success : function(data){
					var json = data.dataList;
					var dataSource = new kendo.data.DataSource({});
					var total=0, percent=0;
					$(json).each(function(idx, data){
							data.worker = decode(data.worker)
							data.goalRatio = Number(data.goalRatio)
							total += data.goalRatio;
							
							if(data.goalRatio>=workerStandard){
								dataSource.add(data)
							}else{
								underScoreWorkerData.add(data)
							}
					});
					percent = total/json.length
					
					
					var array = (percent.toFixed(2)).split('.')
					
					if(isNaN(array[0])){
						
						$("#workerPer").html('0.')
						$("#workerPerPoint").html('00%')
					}else{
						
						$("#workerPer").html(array[0]+'.')
						$("#workerPerPoint").html(array[1]+'%')
					}
					
					
					$("#workerPerPoint").css({
						"font-size" : getElSize(40),
						"position" : "relative",
						"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
						"top" : getElSize(13)
					})
					
					$("#workerPer").css({
						"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
						"float" : "right"
					})
					if(percent>=100){
						$("#workerPer, #workerPerPoint").css({
							"color" : "rgb(79, 169, 95)"
						})
					}else if(percent<100 && percent>=80){
						$("#workerPer, #workerPerPoint").css({
							"color" : "#008000"
						})
					}else if(percent<80 && percent>=60){
						$("#workerPer, #workerPerPoint").css({
							"color" : "yellow"
						})
					}else{
						$("#workerPer, #workerPerPoint").css({
							"color" : "#ff7a7a"
						})
					}
					underScoreWorker.setDataSource(underScoreWorkerData)
					byWorker.setDataSource(dataSource);
					$.hideLoading();
				}
			});
			
		}else{
			$(".deviceDateInput").css({
				"background" : "linear-gradient(rgb(240, 248, 255), rgb(132, 198, 255))",
				"color" : "black",
				"text-shadow" : ""
			})
			
			$("#"+id).css({
				"background" : "rgb(132, 198, 255)",
				"text-shadow" : "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
				"color" : "white"
			})
			
			if(id=='deviceDay'){
				var array = ($("#sDate").val()).split('-');
				sDate=$("#sDate").val()
				eDate=array[0]+'-'+array[1]+'-'+$("#"+id).val()
				
				var result = eDate.indexOf('일');
				
				eDate=eDate.substring(0,result);
				console.log("sDate : " +$("#sDate").val())
				console.log("eDate : " +eDate)
			}else if(id=='deviceMonth'){
				var array = ($("#sDate").val()).split('-');
				sDate=array[0]+'-'+array[1]+'-'+ '01'
				eDate=array[0]+'-'+array[1]+'-'+ '31'
				
				
				console.log("sDate : " + sDate)
				console.log("eDate : " + eDate)
			}else if(id=='deviceYear'){
				var array = ($("#sDate").val()).split('-');
				sDate=array[0]+'-'+'01'+'-'+ '01'
				eDate=array[0]+'-'+'12'+'-'+'31'
				
				console.log("sDate : " + sDate)
				console.log("eDate : " + eDate)
			}
			
			var underScoreDeviceData = new kendo.data.DataSource({});
			
			var url = "${ctxPath}/kpi/getDeviceCnt.do";
			var param = "sDate=" + sDate + "&eDate=" + eDate
			if(checked==0){
				param = param + "&workIdx=" + $("#workIdx").val()
			}
			
			$.ajax({
				url : url,
				data : param,
				dataType : "json",
				type : "post",
				success : function(data){
					var json = data.dataList;
					console.log(json)
					var dataSource = new kendo.data.DataSource({});
					var total=0, percent=0;
					$(json).each(function(idx, data){
							data.name = decode(data.name)
							data.goalRatio = Number(data.goalRatio);
							total += data.goalRatio;
							if(data.goalRatio>=deviceStandard){
								dataSource.add(data)
							}else{
								underScoreDeviceData.add(data)
							}
					});
					percent = total/json.length
					if(isNaN(percent)){
						percent=0
					}
					
					var array = (percent.toFixed(2)).split('.')
					
					$("#devicePer").html(array[0]+'.')
					$("#devicePerPoint").html(array[1]+'%')
					
					$("#devicePerPoint").css({
						"font-size" : getElSize(40),
						"position" : "relative",
						"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
						"top" : getElSize(13)
					})
					
					$("#devicePer").css({
						"text-shadow": "-1px 0 black, 0 1px black, 1px 0 black, 0 -1px black",
						"float" : "right"
					})
					if(percent>=100){
						$("#devicePer, #devicePerPoint").css({
							"color" : "rgb(79, 169, 95)"
						})
					}else if(percent<100 && percent>=80){
						$("#devicePer, #devicePerPoint").css({
							"color" : "#008000"
						})
					}else if(percent<80 && percent>=60){
						$("#devicePer, #devicePerPoint").css({
							"color" : "yellow"
						})
					}else{
						$("#devicePer, #devicePerPoint").css({
							"color" : "#ff7a7a"
						})
					}
					underScoreDevice.setDataSource(underScoreDeviceData)
					byDevice.setDataSource(dataSource);
					$.hideLoading();
				}
			
			})
		}
	}
	
	function updateStandard(){
		
		var url = "${ctxPath}/kpi/updateStandard.do";
		var param ="workerStandard=" + $("#workerStandard").val() +"&deviceStandard=" + $("#deviceStandard").val()
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				
				deviceStandard = $("#deviceStandard").val()
				workerStandard = $("#workerStandard").val()
				
				var byWorkerData = byWorker.dataSource.data()
				var underScoreWorkerData = underScoreWorker.dataSource.data()
				
				var workerStandard = $("#workerStandard").val()
				
				var byWorkerDataLength = byWorkerData.length
				var underScoreWorkerDataLength = underScoreWorkerData.length
				
				var byWorkerdataSource = new kendo.data.DataSource({});
				var underScoreWorkerdataSource = new kendo.data.DataSource({});
				
				
				for(var i=0;i<byWorkerDataLength;i++){
					if(byWorkerData[i].goalRatio>=workerStandard){
						byWorkerdataSource.add(byWorkerData[i])
					}else{
						underScoreWorkerdataSource.add(byWorkerData[i])
					}				
				}
				
				for(var i=0;i<underScoreWorkerDataLength;i++){
					if(underScoreWorkerData[i].goalRatio>=workerStandard){
						byWorkerdataSource.add(underScoreWorkerData[i])
					}else{
						underScoreWorkerdataSource.add(underScoreWorkerData[i])
					}	
				}
				
				byWorker.setDataSource(byWorkerdataSource)
				underScoreWorker.setDataSource(underScoreWorkerdataSource)
				
				var byDeviceData = byDevice.dataSource.data()
				var underScoreDeviceData = underScoreDevice.dataSource.data()
				
				var deviceStandard = $("#deviceStandard").val()
				
				var byDeviceDataLength = byDeviceData.length
				var underScoreDeviceDataLength = underScoreDeviceData.length
				
				var byDeviceDataSource = new kendo.data.DataSource({});
				var underScoreDeviceDataSource = new kendo.data.DataSource({});
				
				for(var i=0;i<byDeviceDataLength;i++){
					if(byDeviceData[i].goalRatio>=deviceStandard){
						byDeviceDataSource.add(byDeviceData[i])
					}else{
						underScoreDeviceDataSource.add(byDeviceData[i])
					}				
				}
				
				for(var i=0;i<underScoreDeviceDataLength;i++){
					if(underScoreDeviceData[i].goalRatio>=deviceStandard){
						byDeviceDataSource.add(underScoreDeviceData[i])
					}else{
						underScoreDeviceDataSource.add(underScoreDeviceData[i])
					}	
				}
				
				byDevice.setDataSource(byDeviceDataSource)
				underScoreDevice.setDataSource(underScoreDeviceDataSource)
			}
		})
		
	}
	
	</script>
</head>
<body>
	
	<div id="container">
		<div id="btnGroup">
			<input type="button" id="detailBtn" onclick="getDetailProduction()" value="${Detail_screen}">
			
			<input class="datePicker" type="text" id="sDate" readonly> ~ 
			<input class="datePicker" type="text" id="eDate" readonly>
			<select id="workIdx">
				<option value='0'><spring:message code="total"></spring:message></option>
				<option value='1'><spring:message code="night"></spring:message></option>
				<option value='2'><spring:message code="day"></spring:message></option>
			</select>
			<button id="search" onclick="getProductionStatus()"><i class="fa fa-search" aria-hidden="true"></i><spring:message code="Search"></spring:message></button>
			<button id="print" onclick="pop_print()" disabled><i class="fa fa-print" aria-hidden="true"></i> <spring:message  code="print"></spring:message></button>
		</div>
		<div id="div1">
			<div id="simpleStatus">
				<div id="workerScore" class="titleWidth">
					<table style="width: 98%; border-radius: 5px; background: #2C3031;">
						<tr>
							<td rowspan="3" class="title" style="color: white; vertical-align: text-bottom">
								<div id="workerTitle">${worker }</div>
							</td>
							<td>
							</td>
							<td>
							</td>
							<td>
							</td>
							<td class="yearTd">
								<div class="subTitle" style="color: white; font-weight : bolder">
									<center><input id="workerYear" class="workerDateInput" type="button" onclick="changeProductionStatus(this)"></center>
								</div>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
							</td>
							<td>
							</td>
							<td class="monthTd">
								<div class="subTitle" style="color: white; font-weight : bolder">
									 <center><input id="workerMonth" class="workerDateInput" type="button" onclick="changeProductionStatus(this)"></center>
								</div>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td class="percent" id="workerPer">
							00.
							</td>
							<td class="percent" id="workerPerPoint">
							00%
							</td>
							<td>
								<div class="subTitle" style="color: white; font-weight : bolder">
									 <center><input id="workerDay" class="workerDateInput" type="button" onclick="changeProductionStatus(this)"></center>
								</div>
							</td>
						</tr>
					</table>
				</div>
				<div id="deviceScore" class="titleWidth">
					<table style="width: 98%; border-radius: 5px; background: #2C3031;">
						<tr>
							<td rowspan="3" class="title" style="color: white; vertical-align: text-bottom">
								<center id="deviceTitle">${device }</center>
							</td>
							<td>
								
							</td>
							<td>
							</td>
							<td>
							</td>
							<td class="yearTd">
								<div class="subTitle" style="color: black; font-weight : bolder">
									<center><input id="deviceYear" class="deviceDateInput" type="button" onclick="changeProductionStatus(this)"></center>
								</div>
							</td>
						</tr>
						<tr>
							<td>
							</td>
							<td>
							</td>
							<td>
							</td>
							<td class="monthTd">
								<div class="subTitle" style="color: black; font-weight : bolder">
									 <center><input id="deviceMonth"  class="deviceDateInput" type="button" onclick="changeProductionStatus(this)"></center>
								</div>
							</td>
							
						</tr>
						<tr>
							<td>
							</td>
							<td class="percent" id="devicePer">
							00.
							</td>
							<td class="percent" id="devicePerPoint">
							00%
							</td>
							<td>
								<div class="subTitle" style="color: black; font-weight : bolder">
									 <center><input id="deviceDay"  class="deviceDateInput" type="button" onclick="changeProductionStatus(this)"></center>
								</div>
							</td>
						</tr>
						</table>
				</div>
				<div id="scoreDiv" class="titleWidth">
					<table class="standardTable" style="width: 100%; background: #2C3031; border-radius:5px; color: white;">
						<tr>
							<td id="standard" rowspan="3">
								<div id="standardTitle">${standard2 }</div>
							</td>
						</tr>
						
						<tr>
							<td>
								
							</td>
							<td style="vertical-align: bottom;">
							</td>
							
							<td>
								
							</td>
							<td style="vertical-align: bottom;">
							</td>
						</tr>
						
						<tr>
							<td class="scoreTitle">
							<center style="font-weight: bolder; color: white;">${worker }</center>
							</td>
							<td class="score">
							<center><input class="inputField" id="workerStandard" onkeyup="updateStandard()" type="number" min="0" max="100">%</center>
							</td>
							<td class="scoreTitle">
							<center style="font-weight: bolder; color: white;">${device }</center>
							</td>
							<td class="score">
								<center><input class="inputField" id="deviceStandard" onkeyup="updateStandard()" type="number" min="0" max="100">%</center>
							</td>
						</tr>
						
						<tr>
							<td>
							</td>
							
							<td>
							</td>
							<td>
							</td>
							<td>
							</td>
							<td>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div id="div2">
			<div id="simpleStatus2">
				<div id="byWorker" class="contentWidth"></div>
				<div id="byDevice" class="contentWidth"></div>
				<div id="underScoreWorker" class="contentWidth"></div>
				<div id="underScoreDevice" class="contentWidth"></div>
			</div>
		</div>

		<div id="detailStatus" style="display: none;">
			<div id="detailGrid"></div>
		</div>
		
		
		<%-- <table id="table" style="border-collapse: collapse;">
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
				
<!-- 					<div id="selector"> -->
						<input type="button" id="detailBtn" onclick="getDetailProduction()" value="${Detail_screen}">
						
<!-- 						<div id="btnGroup" style="display: inline;"> -->
<!-- 							<input class="datePicker" type="text" id="sDate" readonly> ~  -->
<!-- 							<input class="datePicker" type="text" id="eDate" readonly> -->
<!-- 							<select id="workIdx"> -->
								<option value='0'><spring:message code="total"></spring:message></option>
								<option value='1'><spring:message code="night"></spring:message></option>
								<option value='2'><spring:message code="day"></spring:message></option>
<!-- 							</select> -->
							<button id="search" onclick="getProductionStatus()"><i class="fa fa-search" aria-hidden="true"></i><spring:message code="Search"></spring:message></button>
							<button id="print" onclick="pop_print()" disabled><i class="fa fa-print" aria-hidden="true"></i> <spring:message  code="print"></spring:message></button>
<!-- 						</div>  -->
<!-- 					</div> -->
					<div>
						<table style="width: 100%;" >
							<tr>
								<td colspan="3">
									<div id="btnGroup" style="">
										<input type="button" id="detailBtn" onclick="getDetailProduction()" value="${Detail_screen}">
							
										<input class="datePicker" type="text" id="sDate" readonly> ~ 
										<input class="datePicker" type="text" id="eDate" readonly>
										<select id="workIdx">
											<option value='0'><spring:message code="total"></spring:message></option>
											<option value='1'><spring:message code="night"></spring:message></option>
											<option value='2'><spring:message code="day"></spring:message></option>
										</select>
										<button id="search" onclick="getProductionStatus()"><i class="fa fa-search" aria-hidden="true"></i><spring:message code="Search"></spring:message></button>
										<button id="print" onclick="pop_print()" disabled><i class="fa fa-print" aria-hidden="true"></i> <spring:message  code="print"></spring:message></button>
									</div> 
									
								</td>
							</tr>
							<tr>
								<td class="tableWidth" style="width: 33%">
									<center>
										<div id="workerScore" style="display:inline">
											<table style="width: 98%; border-radius: 5px; background: #2C3031;">
											<tr>
												<td rowspan="3" class="title" style="color: white; vertical-align: text-bottom">
													<div id="workerTitle">${worker }</div>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td class="yearTd">
													<div class="subTitle" style="color: white; font-weight : bolder">
														<center><input id="workerYear" class="workerDateInput" type="button" onclick="changeProductionStatus(this)"></center>
													</div>
												</td>
											</tr>
											<tr>
												<td>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td class="monthTd">
													<div class="subTitle" style="color: white; font-weight : bolder">
														 <center><input id="workerMonth" class="workerDateInput" type="button" onclick="changeProductionStatus(this)"></center>
													</div>
												</td>
											</tr>
											<tr>
												<td>
												</td>
												<td class="percent" id="workerPer">
												00.
												</td>
												<td class="percent" id="workerPerPoint">
												00%
												</td>
												<td>
													<div class="subTitle" style="color: white; font-weight : bolder">
														 <center><input id="workerDay" class="workerDateInput" type="button" onclick="changeProductionStatus(this)"></center>
													</div>
												</td>
											</tr>
											</table>
										</div>
									</center>
								</td>
								<td class="tableWidth" style="width: 33%;">
									<center>
										<div id="deviceScore" style="display: inline">
											<table style="width: 98%; border-radius: 5px; background: #2C3031;">
											<tr>
												<td rowspan="3" class="title" style="color: white; vertical-align: text-bottom">
													<center id="deviceTitle">${device }</center>
												</td>
												<td>
													
												</td>
												<td>
												</td>
												<td>
												</td>
												<td class="yearTd">
													<div class="subTitle" style="color: black; font-weight : bolder">
														<center><input id="deviceYear" class="deviceDateInput" type="button" onclick="changeProductionStatus(this)"></center>
													</div>
												</td>
											</tr>
											<tr>
												<td>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td class="monthTd">
													<div class="subTitle" style="color: black; font-weight : bolder">
														 <center><input id="deviceMonth"  class="deviceDateInput" type="button" onclick="changeProductionStatus(this)"></center>
													</div>
												</td>
												
											</tr>
											<tr>
												<td>
												</td>
												<td class="percent" id="devicePer">
												00.
												</td>
												<td class="percent" id="devicePerPoint">
												00%
												</td>
												<td>
													<div class="subTitle" style="color: black; font-weight : bolder">
														 <center><input id="deviceDay"  class="deviceDateInput" type="button" onclick="changeProductionStatus(this)"></center>
													</div>
												</td>
											</tr>
											</table>
										</div>
									</center>
								</td>
								<td class="tableWidth">
									<center>
										<div id="scoreDiv" style="display: inline">
											<table class="standardTable" style="width: 100%; background: #2C3031; border-radius:5px; color: white;">
												<tr>
												<td id="standard" rowspan="3">
													<div id="standardTitle">${standard2 }</div>
												</td>
												</tr>
												
												<tr>
												
												<td>
													
												</td>
												<td style="vertical-align: bottom;">
												</td>
												
												<td>
													
												</td>
												<td style="vertical-align: bottom;">
												</td>
												</tr>
												
												<tr>
												<td class="scoreTitle">
												<center style="font-weight: bolder; color: white;">${worker }</center>
												</td>
												<td class="score">
												<center><input class="inputField" id="workerStandard" onkeyup="updateStandard()" type="number" min="0" max="100">%</center>
												</td>
												<td class="scoreTitle">
												<center style="font-weight: bolder; color: white;">${device }</center>
												</td>
												<td class="score">
												<center><input class="inputField" id="deviceStandard" onkeyup="updateStandard()" type="number" min="0" max="100">%</center>
												</td>
												</tr>
												
												<tr>
												<td>
												</td>
												
												<td>
												</td>
												<td>
												</td>
												<td>
												</td>
												<td>
												</td>
												</tr>
											</table>
										</div>
									</center>
								</td>
							</tr>
						</table>
					</div>
					
				
					<table id="content_table" style="width: 100%;">
						<tr id="simpleStatus">
							<td rowspan="2" width="33%">
								<div id="byWorker"></div>
							</td>
							<td rowspan="2" width="33%">
								<div id="byDevice"></div>
							</td>
							<td>
								<div id="underScoreWorker"></div>
							</td>
						</tr>
						<tr id="simpleStatus2">
							<td>
								<div id="underScoreDevice"></div>
							</td>
							<td>
								
							</td>
							<td>
								
							</td>
						</tr>
						<tr id="detailStatus" style="display :none;">
							<td></td>
							<td></td>
							<td>
								<div id="detailGrid"></div>
							</td>
						</tr>
					</table>
				</td>
			</Tr>
		</table> --%>
	 </div>
	<div id="print_table" style="display: none;"></div>
	<div id="viewByWorker">
	</div>
</body>
</html>	