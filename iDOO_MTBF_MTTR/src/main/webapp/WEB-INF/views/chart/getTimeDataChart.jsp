<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/detailChart_test.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/card.css">
</head>

<body >
	<div id="panel">
			<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu">샵 레이아웃</td>
				</tr>
				<tr>
					<td id="menu9" class="menu">개별 장비 가동 현황</td>
				</tr>
				<tr>
					<td id="menu1" class="menu">장비별 가동실적 분석</td>
				</tr>
				<tr>
					<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
				</tr>
				<tr>
					<td id="menu3" class="menu">무인장비 가동 현황</td>
				</tr>
				<!-- <tr>
					<td id="menu4" class="menu">야간무인장비 가동 현황</td>
				</tr> -->
			</table>
		</div>
		<img src="${ctxPath }/images/menu.png" id="menu_btn" >
	<div id="corver"></div>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	
	<img alt="" src="${ctxPath }/images/close_btn.png" id="close_btn">
	<%-- <img alt="" src="${ctxPath }/images/logo.png" id="main_logo" style="display: none"> --%>
		<table id="time_table">
			<tr>
				<td>
					(주) 부광정밀
				</td>
			</tr>
			<tr>
				<Td>
					<font id="date"></font>
					<font id="time"></font>				
				</Td>
			</tr>
		</table>
		
	<div id="alarmBox">
		<table id="alarmTable" style="width: 100%">
			<tr align="center" class="alarm_header">
				<td width="20%" style="background-color: white; color: black;">시작</td>
				<td width="20%" style="background-color: white; color: black;">완료</td>
				<td width="60%" style="background-color: white; color: black;">내용</td>
			</tr>
		</table>
	</div>
	
	<div id="repairBox">
		<table id="repairTable" style="width: 100%">
			<tr align="center" class="repair_header">
				<td width="10%" style="background-color: white; color: black;">시작</td>
				<td width="10%" style="background-color: white; color: black;">완료</td>
				<td width="10%" style="background-color: white; color: black;">작업자</td>
				<td width="70%" style="background-color: white; color: black;">내용</td>
			</tr>
		</table>
	</div>
	
	<div id="corver"></div>
	
	<!-- Part1 -->
	<div id="part1" class="page" style="background-color:  rgb(16, 18, 20)">
		<span id="detailRepBox"></span>
		<div id="mainTable2">
			<center>
				<table class="mainTable" >
					<tr>
						<Td align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="title" colspan="6" id="title_main">
							개별 장비 가동 현황
						</Td>
					</tr>
					<Tr class="tr1" >
						<td colspan="4" width="60%" rowspan="2" valign="top">
						<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
								상태
							</div>
							<div id="container"></div>
						</td>
						<td width="40%" valign="top" id="machine_name_td" colspan="2">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;vertical-align: middle;" class="subTitle">
								<span id="toolLabel">장비</span>	<img alt="" src="${ctxPath }/images/tool.png" id="tool" style="vertical-align: middle;">
							</div>
							<div id="machine_name">
							</div>
						</td>		
					</Tr >
					<tr class="tr1">
						<td valign="top" id="alarm_td" colspan="2">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle" id="test_div">
								알람	<img alt="" src="${ctxPath }/images/alarm_mark.png" id="alarm_mark" style="vertical-align: middle;">
							</div>
							<div id="alarm" align="left">
							</div>
						</td>
					</tr>
					<tr class="tr2" valign="top" id="chartTr">
						<td width="16%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
							 	일일 생산 목표				
							</div>	
							<div class="neon1" id="daily_target_cycle">
							 	-
							 </div>
						</td>
						<td width="16%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
								일일 생산량		
							</div>
							<div class="neon2">
								<span id="complete_cycle">
									-
								</span>
								<font id="downValue" class="remains_cycle">
									-
								</font>
							</div>
						</td>
						<td width="16%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
								일 평균 사이클 타임 (분)			
							</div>
							<div class="neon1" id="daily_avg_cycle_time">
								-
							</div>
						</td>
						<td width="16%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle">
								일일 시간당 생산량
							</div>
							<div class="neon3"  id="daily_length">
								-
							</div>
						</td>
						<td width="16%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle" id="tool_title">
								Feed Override
							</div>
							<div class='neon3' id="feedOverride">-</div>
						</td>
						<td width="16%">
							<div align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="subTitle" id="tool_title">
								Spindle Load
							</div>
							<div class='neon3' id="spdLoad">-</div>
						</td>
					</tr>
					<tr>
						<Td align="center" style="color:white; font-weight: bolder;" class="title" colspan="6" id="lastCell">
							<div id="barChart"></div>
						</Td>
					</tr>
				</table>
			</center> 
		</div>
	</div>
</body>
</html>