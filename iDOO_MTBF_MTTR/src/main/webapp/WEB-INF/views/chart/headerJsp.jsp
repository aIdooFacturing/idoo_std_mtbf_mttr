<%@ page language="java" pageEncoding="UTF-8"%>
<%@ page session = "true" %>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
</head>
<style>
body {
	color: white;
}

#logoutBtn:hover {
	-webkit-transition-duration: 0.4s; /* Safari */
	transition-duration: 0.4s;
	background: darkred;
}

#logoutBtn {
	-webkit-transition-duration: 0.4s; /* Safari */
	transition-duration: 0.4s;
	background: red;
}
</style>
<body>
	<script>
	$(function(){
		<c:if test="${empCd ne null}">
			loginSuccess();
		</c:if>
	})
function loginCheck(){
	var login = window.sessionStorage.getItem("login");
	var login_time = window.sessionStorage.getItem("login_time");
	var time = new Date().getTime();
	if(login!=null && (time - login_time) / 1000 < 60*20)
	if(login=='success'){
		loginSuccess();
		$("#loginForm").css({
			"display":"none"
		})
		$("#logout").css({
			"display":"inline"
		})
	}else{
		
	}
}

function logout(){
	
}

function loginSuccess(){ 
	$("img, span").css({
		"transition" : "0.5s",
		"-webkit-filter" : "blur(0px)"
	});
	
	$("#loginForm").css({
		"z-index" : "-99",
		"opacity" : 0
	});
	
	$("#logout").css({
		"display" : "inline"
	})
	
	closeCorver();
};

function login(){
	var url = "${ctxPath}/user/login.do";
	var param = "empCd=" + $("#email").val() + 
				"&passWord=" + $("#pwd").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			console.log(data)
			if(data!="fail" && data!=""){
				loginSuccess();
				window.sessionStorage.setItem("login", "success");
				window.sessionStorage.setItem("lv", data);
				window.sessionStorage.setItem("id", $("#email").val());
				window.sessionStorage.setItem("login_time", new Date().getTime());
				login_lv = data;
				$("#logout").css({
					"display":"inline"
				})
			}else{
				//alert("계정 정보가 올바르지 않습니다.")
				$("#errMsg").html("계정 정보가 올바르지 않습니다.")
			}
		}
	});
};
</script>
	<div id="logout" style="display: none;">
		<form action="${ctxPath }/user/logOut.do" method="post">
			<button id="logoutBtn">로그아웃</button>
		</form>
	</div>
	<c:if test="${empCd eq null}">
		<div id="loginForm">
			<form id="login" action="${ctxPath }/user/loginCheck.do" method="post">
				<Center>
					<img src="${ctxPath }/images/SmartFactory.png" id="logo"><br>
					<img src="${ctxPath }/images/logo.png" id="logo2"> <input
						type="text" name="empCd" placeholder="ID" id="email"
						data-role="none" onkeyup="chkKeyCd(event)"> <input
						type="password" name="passWord" placeholder="Password" id="pwd"
						data-role="none" onkeyup="chkKeyCd(event)"><br> <br>
					<input type="submit">
					<%-- <img src="${ctxPath }/images/enter.png" id="enter"> --%>
					<br>
					<hr id="hr">
					<!-- <font id="joinBtn1" >Don't have a factory911 ID? </font> <u><b><font id="joinBtn" >Create one now.</font></b></u> -->
					<div id="errMsg" class="errMsg"></div>
				</Center>
			</form>
		</div>
	</c:if>
</body>
</html>