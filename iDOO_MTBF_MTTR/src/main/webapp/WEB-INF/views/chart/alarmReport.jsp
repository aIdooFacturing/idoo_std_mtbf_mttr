<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
}
html{
	overflow : hidden;
}
 .k-calendar .k-link {
    color: black !important;
}
.k-calendar .k-header .k-link {
    color: black;
    background: #D5D5D5;
}
.k-calendar .k-footer .k-link {
    color: black;
    background: #D5D5D5;
}
/*요일 */
.k-calendar th {
  background:#D5D5D5;
  color : black; 
}
/* 몸통 날짜 
.k-calendar, .k-calendar td{
	background : black;
} */
/* 주말 날짜
.k-calendar, .k-calendar td.k-weekend{
	background : red;
}
/* 이전 다음달 날짜 */
.k-calendar, .k-calendar td.k-other-month{
	background : #EAEAEA;
}

#grid .k-grid-header th.k-header{
	background-color: black;
	color:white;
}

.k-grid tbody > tr
{
 background :  #C8C8C9 !important;
 text-align: center;
}

.k-grid tbody > tr:hover
{
 background :  #353535 !important;
}
.k-grid-content-locked{
 background: darkgray !important;
}
.k-grid tbody > .k-alt
{
 background :  #DADADB !important;
 text-align: center;
}
.k-grid tbody > .k-alt:hover
{
 background :  #353535 !important;
 text-align: center;
}
.k-grid-content{
	background: darkgray !important;
}
.k-grid tbody > .alarmExp{
	background : red !important;
}
</style> 
<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("kpi", "alarmReport")
	}

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	function getDvcList(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		
		var url =  ctxPath + "/chart/getDvcNameList.do";
		var param = "shopId=" + shopId + 
					"&line=ALL" + 
					"&sDate=" + today + 
					"&eDate=" + today + " 23:59:59" + 
					"&fromDashboard=" + fromDashboard;
		
		dvcArray = [];
		
		$.ajax({
			url : url,
			data :param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var list = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					dvcArray.push(data.id)
					list += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi," ") + "</option>";					
				});

				$("#group").html(list);
				
				getAlarmData();
			}
		});	
	};
	
	function ChkBox(chk,idx){
		if(chk=='fa'){
			return "<input type='checkbox' id="+idx+" class='check' style='width:"+getElSize(52)+"; height:"+getElSize(52)+"20px;'>";
			
		}else{
			return "<input type='checkbox' id="+idx+" checked='true' class='check' style='width:"+getElSize(52)+"; height:"+getElSize(52)+"20px;'>";
		}
	}
	var grid;
	
	$(function(){
		getActionList();
		setDate();
		getDvcList();
		getWorkerList();
		getResultList();
		//getGroup();

		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
		grid = $("#grid").kendoGrid({
			height:getElSize(725),
			width:getElSize(500),
			editable:true,
			dataBound : function(e){
				$("#grid tbody td").css("font-size",getElSize(36));
				$("#grid tbody td").css("height",getElSize(96));
				$("#grid tbody td").css("text-align","center");
				$("#grid thead th").css("font-size",getElSize(36));
				$("#grid thead th").css("text-align","center");
				$(".k-grid").css({
					"border-radius": getElSize(8)
				})
				
				var items = e.sender.items();
				items.each(function (index) {
					var dataItem = grid.dataItem(this);
					if(index%2==0){
						$("#grid tbody tr:eq("+index+") td:eq(3)").css("background","#E7E7E7")
						$("#grid tbody tr:eq("+index+") td:eq(4)").css("background","#E7E7E7")
						$("#grid tbody tr:eq("+index+") td:eq(5)").css("background","#E7E7E7")
						$("#grid tbody tr:eq("+index+") td:eq(6)").css("background","#E7E7E7")
					}else{
						$("#grid tbody tr:eq("+index+") td:eq(3)").css("background","#F6F6F6")
						$("#grid tbody tr:eq("+index+") td:eq(4)").css("background","#F6F6F6")
						$("#grid tbody tr:eq("+index+") td:eq(5)").css("background","#F6F6F6")
						$("#grid tbody tr:eq("+index+") td:eq(6)").css("background","#F6F6F6")
					}
				})
				
				$('#grid tbody tr').each(function(){
				if($(this).text().indexOf("alarmException")==-1){
				}else{
					$(this).addClass('alarmExp')
				}
				});
				
				$(".k-grid tbody > .alarmExp").css("background","red")
			},
			columns: [{
				title:"${Except_alarm}"
				//,template:"<input type='checkbox' id='#=idx#' class='check' style='width:"+getElSize(52)+"; height:"+getElSize(52)+"20px;'>"
				,template:"#=ChkBox(line,idx)#"
				//template: '<input type="checkbox" #= Discontinued ? \'checked="checked"\' : "" # #= checkdisable ? \'disabled="checked"\' : "" # class="checkbox"  id="#=id#"  style="width: '+getElSize(80)+'; height: '+getElSize(80)+';"/>' 
				,locked: true
				,lockable: false
				,width : getElSize(200)
				,attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
			},{
                field: "name",
                title: "${device}",
                width: getElSize(300),
                locked: true,
				lockable: false,
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
                title: "${alarm}${content}",
                field: "alarm",
                width: getElSize(1200),
                locked: true,
				lockable: false,
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
				field:"line"
				,locked: true
				,lockable: false
				,width: getElSize(1)
			},{
                title: "${alarm}<br>${start_time}",
                field: "startDateTime",
                width: getElSize(440),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                title: "${alarm}<br>${end_time}",
                field: "endDateTime",
                width: getElSize(440),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },
            {
                title: "${fixed_time}",
                field: "delayTimeSec",
                width: getElSize(230),
                attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
            	title : "${cause}",
            	field : "cause",
            	template : "#=causeName(cause)#",
            	editor : dropdowncauseList,
            	width: getElSize(380),
            	attributes: {
                    style: "color:black; text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
            	title : "${Action_Result}",
            	field : "result",
            	template : "#=resultName(result)#",
            	editor : dropdownresultList,
            	width: getElSize(360),
            	attributes: {
                    style: "color:black; text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
            	title : "${Actioner}",
            	field : "empCd",
            	editor : dropdownworkerList,
            	template : "#=workerName(empCd)#",
            	width: getElSize(210),
            	attributes: {
                    style: "color:black; text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
            	title : "${Action_date}",
            	field : "date"	,
            	format:"{0:yyyy-MM-dd HH:mm}" ,
            	editor : dateTimeEditor,
            	width: getElSize(620),
            	attributes: {
                    style: "color:black; text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                },headerAttributes:{
                	style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important; vertical-align: middle;"
                }
            },{
				field:"line"
				,width: getElSize(1)
			}]
			}).data("kendoGrid");
		
//		$("#grid").css("width",getElSize(3340))
		
	});
	function dateTimeEditor(container, options) {
		 $('<input id="datePicker" data-text-field="' + options.field + '" data-value-field="' + options.field + '" data-bind="value:' + options.field + '" data-format="' + options.format + '"/>')
        .appendTo(container)
        .kendoDateTimePicker({
        	format:"{0:yyyy-MM-dd HH:mm}"
        });
	}
	var causeList=[];
	var resultList=[];
	var worker=[];
	
	function getActionList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 7;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					var arr={};
					arr.id=data.id;
					arr.name=decode(data.name);
					causeList.push(arr);
				});
				
			}
		});	
	};

	function getResultList(){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 16;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					var arr={};
					arr.id=data.id;
					arr.name=decode(data.name);
					resultList.push(arr);
				});
				
			}
		});	
	};
	
	function causeName(name){
		name = Number(name);
		for(i=0; i<causeList.length; i++){
			if(causeList[i].id==name){
				return causeList[i].name
			}
		}
		return "";
	}
	function resultName(name){
		name = Number(name);
		for(i=0; i<resultList.length; i++){
			if(resultList[i].id==name){
				return resultList[i].name
			}
		}
		return "";
	}
	function workerName(name){
		name = Number(name);
		for(i=0; i<worker.length; i++){
			if(worker[i].id==name){
				return worker[i].name
			}
		}
		return "";
	}
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.showLoading();
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				console.log(json)
				$(json).each(function(idx, data){
					var arr={};
					arr.id=data.id;
					arr.name=decode(data.name);
					worker.push(arr);
				});
				
			}
		});
	}
	//원인 선택박스
	function dropdowncauseList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "name",
			 height: 3300,
			 dataValueField: "id",
			 dataSource: causeList
		});
		console.log(container);
		console.log(options);
	}
	//조치결과 선택박스
	function dropdownresultList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "name",
			 height: 3300,
			 dataValueField: "id",
			 dataSource: resultList
		});
		console.log(container);
		console.log(options);
	}
	//조치자 선택박스
	function dropdownworkerList(container, options){
		$('<input name="' + options.field + '" />')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "name",
			 height: 300,
			 dataValueField: "id",
			 dataSource: worker
		});
		console.log(container);
		console.log(options);
	}
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function setEl(){
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		

		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
			"height" : getElSize(88),
			"font-size" : getElSize(47),
			"border-radius": getElSize(8)
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
			"height" : getElSize(88),
			"font-size" : getElSize(47),
			"border-radius": getElSize(8)

		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
		
		$("#contentTable td").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		$(".btC").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(275)//600
		});
		
		$(".text1").css({
			"font-size" : getElSize(60),
//			"background" : "linear-gradient( to bottom, #00A04B, #005026 )",
//			"background" : "linear-gradient( to bottom, #00A04B, #005026 )",
			"padding" : getElSize(14),
			
		});
		
		$("select").css("font-size",getElSize(47));
		$("select").css("height",getElSize(88))
		$("input").css("height",getElSize(88))
		$("input").css("font-size",getElSize(47));
		
		$(".pushExc").css({
			"width" : getElSize(50),
			"height" : getElSize(50)
		});
		
		$("select").css({
		    "border": "1px black #999",
		    "font-family": "inherit", 
		    "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 98% 50%",
		    "background-color" : "black",
		    "z-index" : "999",
		    "border-radius": "0px",
		    "-webkit-appearance": "none",
		    "-moz-appearance": "none",
		    "appearance":"none",
			"background-size" : getElSize(60),
			"color" : "white",
			"border" : "none"
		})
		
		$("select option").css({
	        "background-color" : "rgba(0,0,0,5)",
	        "color" : "white",
		    "border" : "1",
	        "transition-delay" : 0.05,
	        "border-bottom" : getElSize(10) + "px solid red",
	        "text-align-last": "center",
/* 		    "border-top-color": "rgb(215, 0, 0)",
		    "border-right-color": "rgb(215, 0, 0)",
		    "border-left-color": "rgb(215, 0, 0)", */
	        "padding" : getElSize(15) + "px",
	        "font-size": getElSize(52) + "px",
	        "transition" : "1s"
	    })
	    
	    $(".date").css({
	    	"background" : "black",
	    	"color" : "white",
	    	"border" : "none"
	    })
	    
	    $("button").css({
			"background" : "linear-gradient(lightgray, gray)",
	    })
	    
	    $(".contWidth").css("width",getElSize(3840))
		/* 
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		 */
	};
	
	function getGroup(){
		var url = "${ctxPath}/chart/getGroup.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.group + "'>" + data.group + "</option>"; 
				});
				
				$("#group").html(option);
				
				getAlarmData();
			}
		});
	};
	
	var className = "";
	var classFlag = true;
	var dataSource;
	
	function alarmException(id){
		var checked=$('input:checkbox[class=check]:checked')
		var kendolist=grid.dataSource.data();
		var Exlist=[] // 예외할 항목들
		var duplecheck=[];
		
		if(checked.length==0){
			alert("예외처리할 항목을 선택해주세요")
			return;
		}
		
		
		$(checked).each(function(idx,data){
			for(var i=0;i<kendolist.length;i++){
				if(kendolist[i].idx==id){
					var arr={};
					arr.dvcId=kendolist[i].dvcId;
					arr.alarm=kendolist[i].ncAlarmNum1;

					if(duplecheck.indexOf(arr.dvcId+arr.alarm)==-1){	// 중복없을시 추가
						Exlist.push(arr);
						duplecheck.push(arr.dvcId+arr.alarm);
					}
				}
			}
		});
		
		console.log('----예외리스트----')
		console.log(Exlist)
		
		var url = "${ctxPath}/chart/alarmExcept.do";
		var obj=new Object();
		obj.val = Exlist;
		var param = "val=" + JSON.stringify(obj)
		console.log(param);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				alert("알람예외 처리 되었습니다.");
				getAlarmData();
			}
		})
	}
	function alarmreturn(id){
		var dvcId="";
		var alarm="";
		var kendolist=grid.dataSource.data();
		for(i=0,length=kendolist.length; i<length; i++){
			if(id==kendolist[i].idx){
				var arr={};
				dvcId=kendolist[i].dvcId;
				alarm=kendolist[i].ncAlarmNum1;
			}
		}
		
		var url = "${ctxPath}/chart/alarmreturn.do";
		var param = "dvcId=" + dvcId +
					"&alarm=" + alarm;
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				alert("알람예외에서 제외처리 되었습니다.");
				getAlarmData();
			}
		})
	}
	
	function saveRow(){
		var afterDate=grid.dataSource.data();
		var updateDate=[];
		for(i=0;i<afterDate.length;i++){
			for(j=0; j<beforeData.length; j++){
				//같은 기본키값 찾기
				if(afterDate[i].dvcId==beforeData[j].dvcId && afterDate[i].startDateTime==beforeData[j].startDateTime && afterDate[i].endDateTime==beforeData[j].endDateTime && afterDate[i].ncAlarmNum1==beforeData[j].ncAlarmNum1){
					//바뀐값들 찾기
					if(afterDate[i].cause!=beforeData[j].cause || afterDate[i].result!=beforeData[j].result || afterDate[i].empCd!=beforeData[j].empCd || afterDate[i].date!=beforeData[j].date){
						updateDate.push(afterDate[i])
						console.log(updateDate)
					}
				}
			}
		}
		
		if(updateDate.length==0){
			alert("변경사항이 없습니다.")
			return;
		}else{
			for(i=0; i<updateDate.length; i++){
				if(updateDate[i].cause==""){
					alert("원인을 입력해주세요.")
					return;
				}else if(updateDate[i].result==""){
					alert("조치결과를 입력해주세요.")
					return;
				}else if(updateDate[i].empCd==""){
					alert("조치자를 입력해주세요.")
					return;
				}else if(updateDate[i].date==""){
					alert("조치일자를 입력해주세요.")
					return;
				}
			}
		}
		for(i=0; i<updateDate.length; i++){
			
			if(isNaN(updateDate[i].date)==true){
				console.log(updateDate[i].date)
			}else{
				var year = updateDate[i].date.getFullYear();                                 //yyyy
				
				var month = (1 + updateDate[i].date.getMonth());                     //M
				month = month >= 10 ? month : '0' + month;     // month 두자리로 저장
				var day = updateDate[i].date.getDate();                                        //d
				day = day >= 10 ? day : '0' + day;                            //day 두자리로 저장
				var hour = updateDate[i].date.getHours();  
				hour = hour >= 10 ? hour : '0' + hour;                            //hour 두자리로 저장
				var min = updateDate[i].date.getMinutes();  
				min = min >= 10 ? min : '0' + min;                            //min 두자리로 저장
				var full = year + '-' + month + '-' + day + " " + hour + ":" + min + ":00";
				updateDate[i].date=full;
	//			updateDate[i].date=updateDate[i].date.getFullYear()+"-"+updateDate[i].date.getDay()-1+"-"+updateDate[i].date.getDate()+" "+updateDate[i].date.getHours()+":"+updateDate[i].date.getMinutes()+":00"
			}
		}

		$.showLoading();
		var obj=new Object();
		obj.val = updateDate;
		var url = "${ctxPath}/chart/getAlarmAction.do";
		var param = "val=" + JSON.stringify(obj)
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				getAlarmData();
				alert("저장되었습니다")
				$.hideLoading();
				console.log("suc")
			}
		})
	}
	
	var beforeData=[];
	var aa=[];
	function getAlarmData(){
		
		dataSource = new kendo.data.DataSource({
			schema: {
				model: {
					id: "id",
					fields: {
						name: { editable: false },
						alarm: { editable: false },
						startDateTime: { editable: false },
						endDateTime: { editable: false },
						delayTimeSec: { editable: false },
					}
				}
			}
		});
		
		var url = "${ctxPath}/chart/getAlarmData.do";
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		
		var checked=$('input:checkbox[id=pushExc]:checked')
		if(checked.length==0){//check 안됐을경우
			checked="false"
		}else{
			checked="true"
		}
		var param = "sDate=" + sDate +
					"&eDate=" + eDate + 
					"&shopId=" + shopId + 
					"&dvcId=" + $("#group").val() +
					"&checker=" + checked;
		console.log(param);
		
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.alarmList;
				console.log(json)
//				csvOutput = "${device},${alarm}${start_time}, ${alarm}${end_time}, ${fixed_time}, ${alarm}${content}LINE";
				var index=1;
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					var endDateTime;
					
					if(data.endDateTime!=null){
						endDateTime = data.endDateTime.substr(0,19);
					};
					var delayTimeSec = data.delayTimeSec;
					
					if((data.endDateTime == null || data.endDateTime=="") && (data.delayTimeSec == null || data.delayTimeSec=="")){
						endDateTime = "-";
						delayTimeSec = "처리 중";
					}else{
						delayTimeSec = Math.ceil(delayTimeSec/60)
					}
					
					var alarm;
					if(data.ncAlarmNum1!=""){
						data.alarm =  data.ncAlarmNum1 + "-" + decodeURIComponent(data.ncAlarmMsg1).replace(/\+/gi, " ");
					};
					if(data.ncAlarmNum2!=""){
						data.alarm = data.ncAlarmNum2 + "-" + decodeURIComponent(data.ncAlarmMsg2).replace(/\+/gi, " "); 
					};
					if(data.ncAlarmNum3!=""){
						data.alarm = data.ncAlarmNum3 + "-" + decodeURIComponent(data.ncAlarmMsg3).replace(/\+/gi, " "); 
					};
					
					data.name = decode(data.name)
					data.startDateTime = data.startDateTime.substr(0,19)
					data.endDateTime = data.endDateTime
					data.delayTimeSec = delayTimeSec
					data.idx=index;
					index++;
					/* csvOutput += data.name + "," + 
								data.startDateTime.substr(0,19) + "," + 
								endDateTime + "," + 
								Math.ceil(delayTimeSec/60) + "," + 
								decodeURIComponent(alarm) + "LINE"; */
					
					dataSource.add(data)
					
				});
				
				dataSource.fetch(function(){
					$.hideLoading(); 
				});
				
				var data=dataSource._view
				
				for(i=0; i<data.length;i++){
					beforeData.push(JSON.parse(JSON.stringify(data[i])));
				}
				
				grid.setDataSource(dataSource);
				
				/* $(".alarmTable").html(tr).css({
					"font-size": getElSize(40),
				}); */
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid rgb(50,50,50)"
				});
				
				$("#wrapper").css({
					"height" :getElSize(1650),
					"overflow" : "hidden"
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				scrolify($('.alarmTable'), getElSize(1400));
				$("#wrapper div:last").css("overflow", "auto")
				
				$("#grid").css({
					"background-color":"gray",
					"color" : "white"
				}) 
				
				$("#grid tr:odd").css({
				      "background-color":"rgb(50,50,50)",
				      "color" : "white"
			   });
				   
				$("#grid tr:even").css({
				    "background-color":"rgb(33,33,33)",
				    "color" : "white"
				});
				
				/* var target = 101;
				var originalNum = 1000000001;
				var case_Sql = "when "+"'"+originalNum+"'"+" then "+"'"+target+"'";
				var when_Sql = "WHERE EMP_CD IN ("+originalNum ;
				var end = ')';
				var num=71;
				
				for(var i=0;i<num;i++){
					target = target + 1
					originalNum = originalNum + 1
					var case_Sql_temp = " when "+"'"+originalNum+"'"+" then "+"'"+target+"'";
					var when_Sql_temp = ","+originalNum ;
					case_Sql = case_Sql + case_Sql_temp
					when_Sql =when_Sql + when_Sql_temp
				}
				
				console.log(case_Sql)
				console.log(when_Sql) */
				$(".check").click(function(){
					if($(this)[0].checked==true){//예외처리하기
						
						var con_test = confirm("예외처리를 하시겠습니까?");
						if(con_test == false){
							$(this)[0].checked=false
						  return false;
						}
						alarmException($(this)[0].id)
					}else if($(this)[0].checked==false){//예외처리된거 지우기
						console.log($(this)[0].id)
						var con_test = confirm("예외처리된 항목입니다 예외제외 하시겠습니까?");
						if(con_test == false){
							$(this)[0].checked=true
						  return false;
						}
						alarmreturn($(this)[0].id);
					}
				})
				
				console.log("-----Pie Chart-----")
				console.log(json)
				
				var dupChk=[];
				var count=0;
				var list=[];
				var name;

				//pie 그림 그리기
				
				//장비별
				for(i=0,length=json.length; i<length; i++){
					if(dupChk.indexOf(json[i].name)==-1){
						for(j=0,length=json.length; j<length; j++){
	
							if(json[i].dvcId==json[j].dvcId){
								count++;
								name=json[i].name;
							}							
							if(j==length-1 && count!=0){
								var arr={};
								arr.value=count;
								arr.category=name;
								list.push(arr);
								dupChk.push(name);
								count=0;
								
							}
						}					
					
					}

				}
				byDvcPie(list);
				
				
				var dupChk=[];
				var count=0;
				var list=[];
				var name;

				//pie 그림 그리기
				
				//알람별
				for(i=0,length=json.length; i<length; i++){
					if(dupChk.indexOf(json[i].ncAlarmNum1)==-1){
						for(j=0,length=json.length; j<length; j++){	
	
							if(json[i].ncAlarmNum1==json[j].ncAlarmNum1){
								count++;
								name=json[i].ncAlarmNum1;
							}							
							if(j==length-1 && count!=0){
								var arr={};
								arr.value=count;
								arr.category=name;
								list.push(arr);
								dupChk.push(name);
								count=0;
								
							}
						}					
					
					}

				}
				byAlarmPie(list);
				
				
				var dupChk=[];
				var count=0;
				var list=[];
				var name;

				//pie 그림 그리기
				
				
				console.log(json);
				//시간분포도
				for(i=0,length=json.length; i<length; i++){
					var arr={}
					arr.hours=Number(json[i].startDateTime.substr(11,2))
					arr.min=Number(json[i].startDateTime.substr(14,2))
					arr.category=json[i].name
					arr.size=1;
					list.push(arr);
				}
				
				var arr={};
				arr.hours=-5
				arr.min=30
				arr.category="무쓸모"
				arr.size=15;
				list.push(arr);
				
				byWorkerPie(list);
				$.hideLoading()
			}
		});
	};

	function byWorkerPie(list) {
		/* list.sort(function(a, b) { // 오름차순
		    return a.value > b.value ? -1 : a.value < b.value ? 1 : 0;
		    // 광희, 명수, 재석, 형돈
		});
		
		var xData=[];
		var yData=[];
		for(i=0; i<list.length; i++){
			if(i==5){
				break;
			}
			xData.push(list[i].value)
			yData.push(list[i].category);
			
		} */
		/* var ccc=["red","green"]
		var xx=[10,14,18,23]
        var yy=[5,1,10,2]
        var sizee =[5,1,10,2]
        var categoryy = ["FS_RR_LH","OS_RR_RW","FS/#1","TR/S#2"]
        
        var list=[]
        var arr={}
        arr.xx=10
      	arr.yy=5
      	arr.sizee=5
      	arr,categoryy="FS_RR_LH";
      	list.push(arr);

      	var arr={}
        arr.xx=14
      	arr.yy=1
      	arr.sizee=1
      	arr,categoryy="OS_RR_RW";
        list.push(arr); */
      
      	console.log("chart")
      	console.log(list)
        $("#byWorkerPie").kendoChart({
            title: {
                text: "${Alarm_distribution}(${time}/${minute})",
                position: "bottom",
                color:"white",
                font:getElSize(80) + "px sans-serif"
            },
            legend: {
                visible: false
            },
            seriesDefaults: {
                type: "bubble"
            },
            chartArea: {
				background:"#121212",
			},
          	dataSource: list,
          	 series: [{
               	type:"bubble",
                xField: "hours",
                yField: "min",
                sizeField: "size",
                categoryField: "category",
                color:"#FF0000"
            }],
          
            xAxis: {
            	majorGridLines: {	//차트안 x축 실선
                    visible: true,
                	color:"#353535"
				},
                labels: {
                    format: "{0:N0}h",
                    skip: 1,
                    rotation: "auto",
                    visible:true,
                    visual: function(e){
                      
                      if(e.text>24){
                      	e.text=e.text-24
                      }
                     
                      return new kendo.drawing.Text(e.text, e.rect.origin, {
                        
                        fill: {
                          color: "white"
                        }
                      });
                    }

                },
              
                axisCrossingValue: 0,
                majorUnit: 1,
              	max:24,//33,
              	min:00,//08,

                plotBands: [{
                    from: 8.5,
                    to: 20.5,
                    color: "#00f",
                    opacity: 0.2
                }]
            },
            yAxis: {
            	majorGridLines: {	//차트안 x축 실선
                    visible: true,
                	color:"#353535"
				},
                labels: {
                    format: "{0:N0}m",
                    color:"white"
                },
                /* line: {
                    width: 0
                } */
            },
            tooltip: {
                visible: true,
                font:getElSize(48) + "px sans-serif",	
                format: "{3}:  {2} EA  {0}시{1}분",
                opacity: 1
            },
            valueAxis: {
//        	  	majorUnit: 1,
                line: {
                    visible: true,
                    color : "white"
                },
               
                labels: {
                    rotation: "auto",
                    step : 2,
                    color:"white",
                    font:getElSize(50) + "px sans-serif",
                }
            }
        });
    }
	
	function byDvcPie(list) {
		var totalCnt=0;
		/* for(i=0; i<list.length; i++){
			totalCnt+=Number(list[i].value);
			list[i].category=decode(list[i].category);
		}
		for(i=0; i<list.length; i++){
			list[i].value=(Number((list[i].value)/totalCnt)*100).toFixed(1);
		} */
		list.sort(function(a, b) { // 오름차순
		    return a.value > b.value ? -1 : a.value < b.value ? 1 : 0;
		    // 광희, 명수, 재석, 형돈
		});
		var copy=[]
		for(i=0; i<list.length; i++){
			if(i==5){
				break;
			}
			copy.push(list[i]);
		}
		list=copy;
        $("#byDvcPie").kendoChart({
        	chartArea: {
				background:"#121212",
			},
            title: {
            	font:getElSize(80) + "px sans-serif",	
            	position: "bottom",
                text: "TOP 5 ${by_Equipment}",
                color : "white"
            },
            legend: {
                visible: false
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    font:getElSize(45) + "px sans-serif",
                    padding:{
                    	top:-getElSize(30)
                    },
                    background: "transparent",
                    template: "#= category # \n #= value# EA"
                }
            },
            dataSource: {
                data: list
            },
            seriesColors:["red","#0080FF","#1D8B15","#990085","#4D00ED","#DB005B","#FFBB00"],
            series: [{
				overlay: { gradient: "none" },
                type: "pie",
                startAngle: 100,
                field: "value",
                padding:0,
                labels: {
                    visible: true,
                    position: "center",
                    align: "column",
                    template: "#:category # : #: value # EA"
                }
            }],
            tooltip: {
                visible: true,
                font:getElSize(48) + "px sans-serif",	
                template: "#= category #: #= value # EA"
//                format: "{0}EA"
            }
        });
    }
	
    function byAlarmPie(list) {
		list.sort(function(a, b) { // 오름차순
		    return a.value > b.value ? -1 : a.value < b.value ? 1 : 0;
		    // 광희, 명수, 재석, 형돈
		});
		
		var xData=[];
		var yData=[];
		for(i=0; i<list.length; i++){
			if(i==5){
				break;
			}
			xData.push(list[i].value)
			yData.push(list[i].category);
			
			
		}
        $("#byAlarmPie").kendoChart({
            title: {
                text: "Top 5 ${By_alarm}",
              	position : "bottom",
              	color : "white",
                font:getElSize(80) + "px sans-serif",
            },
            legend: {
                visible: false
            },
            seriesDefaults: {
                type: "bar"
            },
            dataSource: {
                data: list
            },
            series: [{
				overlay: { gradient: "none" },
                name: "Total Visits",
                data: xData,
                color : "red"
            }],
          chartArea: {
            background:"#121212"
          },
          valueAxis: {
//        	  	majorUnit: 1,
                line: {
                    visible: true,
                    color : "white"
                },
                majorGridLines: {
                	visible:true,
                	color : "#353535"
                },
                
                labels: {
                    rotation: "auto",
                    step : 2,
                    color:"white",
                    font:getElSize(50) + "px sans-serif",
                }
            },
            categoryAxis: {
                categories: yData,
                line:{
                	viible:true,
                	color:"white"
                },
                majorGridLines: {
                	color:"121212"
                },
                color:"white",
                labels:{
                    font:getElSize(48) + "px sans-serif",
                }
            },
           
            tooltip: {
                visible: true,
                font:getElSize(48) + "px sans-serif",	
                template: "#= category #: #= value # EA"
            }
        });
    }
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table id="contentTable" style="width: 100%">
						<Tr>
							<td>
								<label class='text1'><spring:message code="device"></spring:message></label>
								<select id="group"></select>
								<label class='text1'><spring:message code="op_period"></spring:message></label>
								<input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate">
								<%-- <button> <spring:message code="excel"></spring:message></button> --%>
								<label id="pushExc" class="text1"><input type="checkbox" id="pushExc" class="pushExc">push ${Excluded_included }</label>
								<button id="search" onclick="getAlarmData()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
<!-- 								<button id="alarmException" class='btC' onclick="alarmException()" style="cursor: pointer;">알람예외</button> -->
								<button id="saverow" class='btC' onclick="saveRow()" style="cursor: pointer;">${save }</button>
							</td>
						</Tr>
					</table>
					<div class="contWidth" style="position: absolute; background: blue; height: 81% ;">
						<div style="height: 55%;width:100%; float: left; position: relative;">
						<div id="byDvcPie" style="height: 100%; width: 28.33%; background: black; float: left;" >
							<!-- 장비별 -->
						</div>
						
						<div id="byAlarmPie" style="height: 100%; width: 28.33%; background: black; float: left;" >
							<!-- 알람별 -->
						</div>
						
						<div id="byWorkerPie" style="height: 100%; width: 43.33%; background: black; float: left;" >
							<!-- 작업자별 -->
						</div>
						
						
						</div>
						<div id="grid" style="height: 45%;width:100%; background-color:black; float: left; position:  relative;"></div>
					</div>
					<!-- <div id="wrapper">
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1">
						</table>
					</div> -->
				</td>
			</Tr>
		</table>
	 </div>
</body>
</html>	