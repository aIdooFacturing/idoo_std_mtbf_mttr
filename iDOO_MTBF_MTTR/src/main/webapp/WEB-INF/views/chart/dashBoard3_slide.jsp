<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;
	
	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return originWidth/(targetWidth/n);
	};
	
	var loopFlag = null;
	var flag = false;
	function stopLoop(){
		flag = !flag;
		
		if(!flag){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
	var menu = false;
	function getMousePos(){
		$(document).on("mousemove", function(e){
			var target = $("#menu_btn").width();

			if((e.pageX <= target && e.pageY <= target) || panel){
				if(!menu){
					$("#menu_btn").animate({
						"left" : 0,
						"top" : getElSize(20)
					});
					menu = true;
				};
			}else{
				if(menu){
					$("#menu_btn").animate({
						"left" : -getElSize(100),
					});	
					menu = false;
				};
			};
		});
	};
	
	var panel = false;
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 99999
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : -getElSize(100)
		});
		
		$("#corver").css({
			"z-index" : -1
		});
	};
	
	function goReport(){
		var type = this.id;
		var url;
		if(type=="menu0"){
			url = "${ctxPath}/chart/main.do";
			location.href = url;
		}else if(type=="menu1"){
			url = "${ctxPath}/chart/performanceReport.do";
			location.href = url;
		}else{
			url = "${ctxPath}/chart/alarmReport.do";
			location.href = url;
		};
	};
	
	$(function(){
		setTimeout(function(){
			location.href = ctxPath + "/chart/main_en.do";
		},1000*60*1);
		
		$(".menu").click(goReport);
		getMousePos();
		$("#menu_btn").click(function(){
			if(!panel){
				showPanel();
			}else{
				closePanel();
			};
			panel = !panel;
		});
		
		
		setElement();
		
		
		$("#title_main").click(function(){
			location.href = "${ctxPath}/chart/multiVision.do";
		});

		$("#title_right").click(function(){
			location.href = "${ctxPath}/chart/main4.do";
		});
		
		$("#title_left").click(function(){
			location.href = "${ctxPath}/chart/main2.do";
		});
		
		setInterval(time, 1000);

		getDvcIdList();
	});
	
	function getDvcIdList(){
		var url = "${ctxPath}/chart/getBarChartDvcId.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dvcId;
				for(var i = 0; i <=21; i++){
					drawBarChart2("status2_" + i, json[i].name);	
					getStatusChart2(json[i].dvcId, i);
				}
			}
		});
	};
	
	function getStatusChart2(dvcId, idx){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		var today = year + "-" + month + "-" + day + "%20" + hour + ":" + minute + ":" + second;
		
		var url = "${ctxPath}/chart/getTimeChart.do";
		var param = "dvcId=" + dvcId + 
					"&targetDateTime=" + today;
		
		setInterval(function (){
			var minute = String(new Date().getMinutes());
			if(minute.length!=1){
				minute = minute.substr(1,2);
			};
			
			if(minute==2 && eval("dvcMap" + idx).get("initFlag") || minute==2 && typeof(eval("dvcMap" + idx).get("initFlag")=="undefined")){
				getStatusChart2(dvcId, idx);
				console.log("init")
				eval("dvcMap" + idx).put("initFlag", false);
				eval("dvcMap" + idx).put("currentFlag", true);
			}else if(minute!=2){
				eval("dvcMap" + idx).put("initFlag", true);
			};
		}, 1000 * 10);
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				eval("dvcMap" + idx + " = new JqMap();");
				
				if(data==null || data==""){
					eval("dvcMap" + idx).put("noSeries", true);
					getCurrentDvcStatus(dvcId, idx);
					return;
				}else{
					eval("dvcMap" + idx).put("noSeries", false);
				};
	
				var json = $.parseJSON(data);
				var status = $("#status2_" + idx).highcharts();
				var options = status.options;
				
				options.series = [];
				//options.title = null;
				
				var flag = true;
				$(json).each(function (i, data){
					var bar = data.data[0].y;
					var startTime = data.data[0].startTime;
					var endTime = data.data[0].endTime;
					var color = eval(data.color);
					
					if(flag){
						options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : color
					        	}],
					    });	
					}else{
						options.series[0].data.push({
							y : Number(200),
							segmentColor : color
						});
					};
					flag =  false;
				});  
				
				status = new Highcharts.Chart(options);
				getCurrentDvcStatus(dvcId, idx);
			}
		});
	};
	
	function getCurrentDvcStatus(dvcId, idx){
		var url = "${ctxPath}/chart/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				var name = data.name;
				
				var status = $("#status2_" + idx).highcharts();
				var options = status.options;

				if(eval("dvcMap" + idx).get("currentFlag") || typeof(eval("dvcMap" + idx).get("currentFlag"))=="undefined"){
					if(eval("dvcMap" + idx).get("noSeries")){
		      			options.series = [];
		      			options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : slctChartColor(chartStatus) 
					        	}],
					    });
		      		}else{
		      			options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : slctChartColor(chartStatus)
			  			});		      			
		      		};

		      		var now = options.series[0].data.length;
					var blank = 144 - now;
					
					for(var i = 0; i <= blank; i++){
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : "rgba(0,0,0,0.0)"
			  			});	
					};
					
					status = new Highcharts.Chart(options);
					eval("dvcMap" + idx).put("currentFlag", false);
				};
				setTimeout(function (){
					getCurrentDvcStatus(dvcId, idx);
				}, 3000);
				$("#dvcName" + idx).html(name);
			}
		});	
	};
	
	function slctChartColor(status){
		var color;
		if(status.toLowerCase()=="in-cycle"){
			color = colors[0];
		}else if(status.toLowerCase()=="wait"){
			color = colors[1];
		}else if(status.toLowerCase()=="alarm"){
			color = colors[2];
		}else if(status.toLowerCase()=="no-connection"){
			color = colors[3];
		};
		
		return color;
	};
	
	function drawBarChart2(id, name){
		var fontColor = "white;"
		if(name=="NB13" || name=="NB14W"){
			fontColor = "black";
		}
		var perShapeGradient = {
	            x1: 0,
	            y1: 0,
	            x2: 1,
	            y2: 0
	        };
	        colors = Highcharts.getOptions().colors;
	        colors = [{
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#148F01'],
	                [1, '#1ABB02']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#C7C402'],
	                [1, '#F5F104']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#ff0000'],
	                [1, '#FF2626']
	                ]
	           },{
		            linearGradient: perShapeGradient,
		            stops: [
		                [0, '#8c9089'],
		                [1, '#A9ADA6']
		                ]}
	        ]
	         
		var options = {
			chart : {
				type : 'coloredarea',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height : $("#mainTable").height()*0.8/11,
				marginTop: -60,
				marginBottom: 25
			},
			credits : false,
			exporting: false,
			title : {
				text :name,
				align :"left",
				y:10,
				style : {
					color : "white",
					fontSize: getElSize(20) + "px"
				}
			},
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false,
	                rotation: 0,
	                "textAlign": 'right',
	                x:100,
	                y: -getElSize(10),
					style : {
						color : "white",
						fontSize: getElSize(20) + "px"
					}
				},
			},
			xAxis:{
		           categories:[20,0,0,0,0,0,
		                       21,0,0,0,0,0,
		                       22,0,0,0,0,0,
		                       23,0,0,0,0,0,
		                       24,0,0,0,0,0,
		                       1,0,0,0,0,0,
		                       2,0,0,0,0,0,
		                       3,0,0,0,0,0,
		                       4,0,0,0,0,0,
		                       5,0,0,0,0,0,
		                       6,0,0,0,0,0,
		                       7,0,0,0,0,0,
		                       8,0,0,0,0,0,
		                       9,0,0,0,0,0,
		                       10,0,0,0,0,0,
		                       11,0,0,0,0,0,
		                       12,0,0,0,0,0,
		                       13,0,0,0,0,0,
		                       14,0,0,0,0,0,
		                       15,0,0,0,0,0,
		                       16,0,0,0,0,0,
		                       17,0,0,0,0,0,
		                       18,0,0,0,0,0,
		                       19,0,0,0,0,0,
		                       20,0,0,0,0,0,
		                       ],
		            labels:{
		                 formatter: function () {
			                        	var val = this.value
			                        	if(val==0){
			                        		val = "";
			                        	};
			                        	return val;   
			                        },
			                        style :{
			    	                	color : fontColor,
			    	                	fontSize : "9px"
			    	                },
		            }
		        },
		       
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			plotOptions: {
			    line: {
			        marker: {
			            enabled: false
			        }
			    },
			    series : {
			    	animation : false
			    }
			},
			legend : {
				enabled : false
			},
			series: []
		}

	   	$('#' + id).highcharts(options);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	function setElement(){
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#title_main").css({
			"left" : width/2 - $("#title_main").width()/2
		});
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2
		});
		
		$("#mainTable").css({
			"left" : width/2 - $("table").width()/2
		});
		
		$(".status").css({
			"width" : contentWidth*0.45,
			"height" : contentHeight*0.9/11,
			"margin-bottom": -contentHeight/(targetHeight/10)	
		});
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2)
		});
		
		$("#title_main").css({
			"font-size" : getElSize(100),
			"top" : $("#container").offset().top + (getElSize(50)),
		});
		
		$("#title_main").css({
			"left" : originWidth/2 - ($("#title_main").width()/2)
		});
		
		$("#title_left").css({
			"width" : getElSize(300),
			"left" : $("#container").offset().left + getElSize(50),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_right").css({
			//"width" : getElSize(300),
			"font-size" : getElSize(45),
			"right" : $("#container").offset().left + getElSize(50),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#time").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(50),
			"font-size" : getElSize(30)
		});
		
		$("#date").css({
			"top": $("#container").offset().top + getElSize(170),
			"right": $("#container").offset().left + getElSize(250),
			"font-size" : getElSize(30)
		});
		
		$("hr").css({
			"width" : contentWidth * 0.95,
			"top" : $("#container").offset().top + getElSize(250),
		});
		
		$("hr").css({
			"left" : $("#container").offset().left + contentWidth/2 - $("hr").width()/2,
		});
		
		$("#mainTable").css({
			"width" : contentWidth * 0.95,
			"top" : $("#container").offset().top + getElSize(300)
		});
		
		$("#mainTable").css({
			"left" : $("#container").offset().left + contentWidth/2 - $("#mainTable").width()/2,
		});
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(100),
			"left" : -getElSize(100),
			"top" : getElSize(20),
			"cursor" : "pointer",
			"z-index" : 999999
		});
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(60)
		});

		$("#panel_table td").css({
			"padding" : getElSize(50),
			"cursor" : "pointer"
		});
		
		$("#panel_table td").addClass("unSelected_menu");
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
</script>
<style>
#container{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	background-color: white;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	background-color: black;
  	font-family:'Helvetica';
	background-size : 100% 100%;
	overflow: hidden;
}


#title_main{
	z-index: 999;
	position:absolute;
}
#title_left{
	position:absolute;
	left : 50px;
	width: 300px;
}

#title_right{
	position:absolute;	
}

#time{
	font-size : 30px;
	color: white;
	position:absolute;
} 
#date{
	font-size : 30px;
	color: white;
	position:absolute;
} 

hr{
	z-index: 99;
	border: 2px solid white;
	position: absolute;
}

#mainTable{
	z-index: 99;
	position: absolute;
}
.leftTd{
	border-right: 5px solid white; width: 50%; padding: 0 0 0 0";
}
.status{
	margin-top: 0px;
	margin-bottom: -10px;
}

.unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}

.menu:HOVER{
	background-color: white;
	color: gray;
}
</style>
<script type="text/javascript">
	
</script>

</head>


<body>
	<div id="container" >
		<div id="panel">
			<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu">샵 레이아웃</td>
				</tr>
				<tr>
					<td id="menu1" class="menu">장비별 가동실적 분석</td>
				</tr>
				<tr>
					<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
				</tr>
			</table>
		</div>
		<img src="${ctxPath }/images/menu.png" id="menu_btn" >
		<div id="svg"></div>
	</div>
	<div id="popup" style="width: 95%;height:200px;background-color:white; position: absolute; display: none;">
		<div id='popupChart' style="width: 100%" ></div>
	</div>
	
	<img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title">
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right" class="title"> --%>	
	<div id="title_right" style="color: white;" class="title">Doosan Infracore<br>Machine Tools</div>
	<%-- <center><img src="${ctxPath }/images/DashBoard/title3.svg" id="title_main" class="title"></center>	 --%>
	<div id="title_main" style="color: white; font-weight: bolder;" class="title">Automatic Machine Status</div>
	
	
	<font id="time"></font>
	<font id="date"></font>
	<hr>
	<Center>
		<table id="mainTable" style="border-collapse: collapse;">
			<tr>
				<td class="leftTd">
					<div id="status2_0" class="status"></div>
				</td>
				<td>
					<div id="status2_1" class="status"></div>			
				</td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_2" class="status"></div>
				</td>
				<td>
					<div id="status2_3" class="status"></div>
				</td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_4" class="status"></div>
				</td>
				<td>
					<div id="status2_5" class="status"></div>
				</td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_6" class="status"></div>
				</td>
				<td>
					<div id="status2_7" class="status"></div>
				</td>
			</tr>	
			<tr>		
				<td class="leftTd">
					<div id="status2_8" class="status"></div>
				</td>
				<td>
					<div id="status2_9" class="status"></div>
				</td>
			<tr>	
				<td class="leftTd">
					<div id="status2_10" class="status"></div>
				</td>
				<td>
					<div id="status2_11" class="status"></div>
				</td>
			</tr>	
			<tr>
				<td class="leftTd">
					<div id="status2_12" class="status"></div>
				</td>
				<Td>
					<div id="status2_13" class="status"></div>
				</Td>
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_14" class="status"></div>
				</td>
				<td>
					<div id="status2_15" class="status"></div>
				</td>	
			</tr>
			<tr>
				<td class="leftTd">
					<div id="status2_16" class="status"></div>
				</td>
				<td>
					<div id="status2_17" class="status"></div>
				</td>
			</tr>	
			<tr>		
				<Td class="leftTd">
					<div id="status2_18" class="status"></div>
				</Td>	
				<td>
					<div id="status2_19" class="status"></div>
				</td>
			</tr>	
			<tr>
				<td class="leftTd">
					<div id="status2_20" class="status"></div>
				</td>		
				<td>
					<div id="status2_21" class="status"></div>
				</td>
			</tr>
		</table>
	</Center>
</body>
</html>