<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}
.k-grid-header th.k-header{
    background: linear-gradient( to top, black, gray);
    text-align: center;
    color: white;
}

#wrapper thead{
	background-color: gray;
}

</style> 
<script type="text/javascript">
	
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	var kendotable="";
	$(function(){
		
		//kendo grid	delbert 2017-11-08
		kendotable = $("#wrapper").kendoGrid({
			height:getElSize(1665)
			,editable:true
			,selectable: "row"
			,dataBound:function(){
				grid = this;
				grid.tbody.find('tr').each(function(){
					var item = grid.dataItem(this);
					item.date=getToday2();
					item.itm="undefined";
					kendo.bind(this,item);
					
				})
			}
/* 			,edit:function(e){
				var row = $(this).closest("tr");
				console.log(e.container);
				console.log(e.container[0]);
				console.log(e.container[0].firstElementChild.name)
				var field=e.container[0].firstElementChild.name;
				if(field=="date"){
					$( ".date" ).datepicker({
					    onSelect : function(e){
					    	//e == 날짜 
					    	$(".date").val(e);
					    	kendodate(e);
					    }
				    })
				    $(grid.tbody).off("change").on("change", "td", function (e) {
						console.log("dd")
					});
				}
				
				
			} */
			,columns:[{
				title:"<input type='checkbox' id='checkall' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'>",width:getElSize(150)
				,template: '<input type="checkbox" #= Discontinued ? \'checked="checked"\' : "" # #= checkdisable ? \'disabled="checked"\' : "" # class="checkbox"  id="#=idx#"  style="width: '+getElSize(80)+'; height: '+getElSize(80)+';"/>' 
			},{
				field : "idx", title : "${income_no}" , width : getElSize(190)
			},{
				field : "prdNo" , title : "${prd_no}" ,  width : getElSize(330)
			},{
				field : "spec" , title : "${spec}" , width : getElSize(190), template:'SPEC'
			},{
				field : "lotNo" , title : "Lot No" , width : getElSize(190)
			},{
				field : "lotCnt" , title : "${income_cnt}" , width : getElSize(190)
			},{
				field : "lotStock" , title : "${lot_cnt}" , width : getElSize(190)
			},{
				field : "barcode" , title : "${ship_lot_no}"  , width : getElSize(250)
				,attributes: {
    				style: "text-align: center; color:black; font-size:" + getElSize(35)
    			}
			},{
				field : "date" , title : "${ship_date}"   , width : getElSize(430), editor : function(container, options){
					var input = $("<input type='date' class='date' style='width : " + getElSize(390) + "px; font-size:"+getElSize(32)+"'/>");
				    input.attr("name", options.field);
				    input.appendTo(container);
				    
				} ,format: "{0:yyyy-MM-dd}"
				,attributes: {
    				style: "text-align: center; font-size:" + getElSize(35)
    			}
			}/* ,{
				field : "date" , title : "${ship_date}" ,template: "<input text='text' class='date' value='#=date#' > "
			} */,{
				field : " shipmentCnt" , title : "${ship_cnt}" , width : getElSize(210)
				,attributes: {
    				style: "text-align: center; font-size:" + getElSize(35)
    			}
			},{
				field : "divide" ,title : "${divide}" , template : "#if(divide=='0'){# <input type='button' value='${del}' style='background-color:red; width:"+getElSize(150)+"; height:"+getElSize(70)+" font-size:"+getElSize(32)+"' id='#=id#' onclick='removelotNo(this)'> #}else{#<input type='button' value='${divide}' style='width:"+getElSize(150)+"; height:"+getElSize(70)+" font-size:"+getElSize(32)+"' id='#=id#' onclick='addNewlotNo(this)'> #}#"
				, width : getElSize(200)
			}]									
		}).data("kendoGrid");
		
		
		$("#wrapper tbody").css("font-size",getElSize(32));
		$("#wrapper thead th").css("font-size",getElSize(32));

		getPrdNo();
		createNav("inven_nav", 6);
		
		setEl();
		setDate();	
		time();
		
		setEvt();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	
	function stylecss(style){
		console.log(style);
	}
	function itmcontroll(itm){
		if(itm=="undefined" || itm==null){
			return "--선택--"
		}else{
			return itm
		}
		
	}
	
	function categoryDropDownEditor(container, options) {
		
		
//		console.log(container,options);
//		console.log(options.model.prdNo)
		var prdNos=[];
		var prdNo=decodeURIComponent(options.model.prdNo).replace(/\+/gi, " ");
		var url="${ctxPath}/chart/getbertmstmatno.do";	//service ,,sql 이름 다름
		var param=	"RWMATNO=" + prdNo;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			async: false,
			success : function(data){
				var json=data.dataList;
				$(json).each(function(idx,data){	
					var prdNo = {"prdNo" : decode(data.MATNO)};
					prdNos.push(prdNo);
				});
			}
		});
		
		
		$('<input name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 valuePrimitive: true,
			 autoBind: false,
			 dataTextField: "prdNo",
			 dataValueField: "prdNo",
			 dataSource: prdNos/* ,
			 select: onSelect,
			 change: onChange */
		});
   }
	
	function datepic(container, options){
		console.log(container)
		console.log(options)
		
	}
	function kendodate(item){
		alert("d")
		console.log(item)
	}
	function removelotNo(item){
		var scroll=$("div.k-grid-content").scrollTop();
		var row = $(item).closest("tr");
		dataItem = kendotable.dataItem(row);
		grid.dataSource.remove(dataItem);
		$("div.k-grid-content").scrollTop(scroll);

	}
	
	function addNewlotNo(item){
		var scroll=$("div.k-grid-content").scrollTop();
		var row = $(item).closest("tr");
		var rowIdx = $("tr", grid.tbody).index(row);
		console.log("---------------------------------------------------------------------------")
		dataItem = kendotable.dataItem(row);
		console.log(dataItem)
		grid.dataSource.insert(rowIdx+1, { Discontinued : "true" , checkdisable : "true",idx : dataItem.idx, id : dataItem.id ,prdNo: dataItem.prdNo,
			itm: dataItem.itm, spec: dataItem.spec ,lotNo: dataItem.lotNo ,rcvCnt : dataItem.rcvCnt,
			stockCnt : dataItem.stockCnt , cmplCnt : dataItem.cmplCnt , sendCnt : dataItem.sendCnt , inputCnt : dataItem.inputCnt,
			prdCnt : dataItem.prdCnt
			,divide :"0"});
		
		dataItem.set("Discontinued" , true)
		grid.dataSource.fetch();
		$("div.k-grid-content").scrollTop(scroll);

//		console.log(rowIdx)
//		console.log(dataItem);
		
		
	}
	
	function setEvt(){

	};
	
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function getToday2(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		return year + "-" + month + "-" + day;	
	};
	
	//품번
	
	function getPrdNo(){
		var url = "${ctxPath}/common/getPrdNo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){

				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>"; 
				});
				
				$("#group").html(option);
				$("#group").on("change", function(){
					getReleaseInfo();
//					$("#group").val($("#group").val())
				});
				getReleaseInfo();
			}
		});
	};
	
	//테이블 그리는듯
	function getReleaseInfo(scroll){
		$.showLoading(); 
		classFlag = true;
		var url = "${ctxPath}/chart/getReleaseInfo.do";
		var sDate = $("#sDate").val();
		
		var param = "prdNo=" + $("#group").val();
		
		$.ajax({	
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				cntArray = [];
				console.log(json)
				
				 var kendodata = new kendo.data.DataSource({
			        	data:json,
			            batch: true,
			            schema: {
			                   model: {
			                     id: "id",
			                     fields: {
			                    	 idx : {editable:false},
			                    	 prdNo : {editable:false},
			                    	 spec : {editable:false,
			                    		 defaultValue:'SPEC'},
			                    	lotNo : {editable:false},
			                    	lotCnt : {editable:false},
			                    	lotStock : {editable:false},
			                    	shipmentCnt : {},
			                    	date : {},
		                            Discontinued:{ type: "boolean" }, //체크박스 선택 기억하기
		                            checkdisable:{ type: "boolean"}, //checkbox disable
		                            divide:{editable : false , nullable:true}, //분할 삭제 
			                     	barcode:{ editable : true, nullable: true}
			                     }
			                   }
		               }
		    	})
				 
				kendotable.setDataSource(kendodata);
				grid.dataSource.fetch();
				 /* 				$(json).each(function(idx, data){
					var array = [data.cmplCnt, "stock_" + data.id];
					cntArray.push(array);
					
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						tr += "<tr class='contentTr " + className + " parent" + data.lotNo + "' >" ;
						tr += "<td><input type='checkbox' id='"+data.id+"' class='checkbox'></td>" ;
						tr +="<td id="+data.inputCnt+">" + data.id + "</td>" ;
						tr +="<td>" + decodeURIComponent(data.prdNo).replace(/\+/gi, " ") + "</td>" ; 

						var prdNo=decodeURIComponent(data.prdNo).replace(/\+/gi, " ");
						var url="${ctxPath}/chart/getbertmstmatno.do";	//service ,,sql 이름 다름
						var param=	"RWMATNO=" + prdNo;
						var options="";
						$.ajax({
							url : url,
							data : param,
							type : "post",
							dataType : "json",
							async: false,
							success : function(data){
								var json=data.dataList;
								options+="<td><select>"
								$(json).each(function(idx,data){								
									options+="<option>" + data.MATNO + "</option>"											
								});
								options+="</select></td>"
								tr+=options;
							}
						});
						
//						tr +="<td>" + decodeURIComponent(data.prdNo).replace(/\+/gi, " ") + "</td>" ; 
						tr +="<td>" + decodeURIComponent(data.spec).replace(/\+/gi, " ") + "</td>" ;
						tr +="<td>" + data.lotNo + "</td>" ; 
						tr +="<td>" + data.rcvCnt + "</td>" ; //입고수량
//									"<td>" + data.HOUSECNT + "</td>" ; //자재재고
						tr +="<td>" + data.prdCnt + "</td>" ;  //공정재고
						tr +="<td>" + data.sendCnt + "</td>" ; //불출수량
//									"<td>" + data.CNCCNT + "</td>" ; 	//CNT 재고   cnc추가함								
						tr +="<td>" + data.cmplCnt + "</td>" ; // 완성수량
						tr +="<td><input type='text' size='6'> </td>" ;
						tr +="<td><input type='date' value='" + getToday2() + "' </td>" ; 
						tr +="<td><input type='text' size='6' class='stock_" + data.id + "'> </td>" ; 
						tr +="<td><button onclick='addNewLot(this,"+data.id+");' class='" + data.lotNo + "' style='padding:" + getElSize(10) + "'>${divide}</button></td>" ;
						tr +="</tr>";					
					}
				}); */
				
				//수정시 체크박스 초기화되서 초기화방지
				$("#wrapper .k-grid-content").off("change").on("change", "input.checkbox", function(e) {
					
					var scroll=$("div.k-grid-content").scrollTop();
					
					var grid = $("#wrapper").data("kendoGrid");
					var gridlist=grid.dataSource.data();
					dataItem = grid.dataItem($(e.target).closest("tr"));
			        dataItem.set("Discontinued", this.checked);
					dataItem.set("id",dataItem.idx)
			        console.log("?")
			        console.log(dataItem)
					//id값 같은 checkbox 연결
					$(gridlist).each(function(idx,data){
						if(dataItem.id==data.id && dataItem.Discontinued==true ){
							console.log(data)
							console.log("??")
							
							data.set("Discontinued",true)
						}else if(dataItem.id==data.id && dataItem.Discontinued==false){
							console.log("???")
							
							data.set("Discontinued",false)
						}
					})

					$("div.k-grid-content").scrollTop(scroll);

			    });
				
				//전체선택
				$("#checkall").click(function(e){
					var grid = $("#wrapper").data("kendoGrid");
					var overlap=[];
					var gridlist=grid.dataSource.data();
					var chk;
					//클릭되었으면
					$(gridlist).each(function(idx,data){
						if(chk==data.id){
							overlap.push(data.id)
						}
						chk=data.id;
					})
					console.log(overlap);
					
			        if($("#checkall").prop("checked")){ //전체선택
			        	console.log("선택")
			        	$(gridlist).each(function(idx,data){
//			        		if(data.checkdisable!="true"){
				        		data.set("Discontinued", true);
//			        		}
			        	});	        	
			        }else{  //전체해제
			        	console.log("해제")
			        	$(gridlist).each(function(idx,data){  
			        		data.set("Discontinued", false);
			        		//분할되어있는값들은 선택해제 안되게
			        		/* if(data.checkdisable!="true" || data.Discontinued=="true"  && overlap.indexOf(data.id)==-1 ){
			        			data.set("Discontinued", false);
			        		} */
			        	});
			        }
			    })
			    
				$(".alarmTable input, .alarmTable button").css({
					"font-size" : getElSize(40)
				});
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				//scrolify($('.alarmTable'), getElSize(1450));
				
				$("body, html").css({
					"overflow" : "hidden"	
				})
				$("div.k-grid-content").scrollTop(scroll);

				$.hideLoading();
			}
		});
	};
	
	var valueArray = new Array();
	var valid = true;

	var cntArray =[];
	function saveRow(){
		var scroll=$("div.k-grid-content").scrollTop();
		valid = true;
		//완성 출하 조건문비교
		 for(var i = 0; i < cntArray.length; i++){
			var stock =  cntArray[i][0];
			var sum = 0;
			$("." + cntArray[i][1]).each(function(idx, data){
				sum += Number($(data).val());
			});
			if(stock<sum){
				valid = false;
				alert("${cnt_more_than_complete_stock}");
				$("." + cntArray[i][1] + ":nth(0)").focus();
				return;
			}
		 };
		
		valueArray = [];
		
		var url = "${ctxPath}/chart/addCmplStockHistory.do";
		
//		var checked=$("input[type=checkbox]:checked")
		var checked = $('input:checkbox[class=checkbox]:checked');
		if(checked.length==0){
			alert("출하 항목을 선택하세요")
			return;
		}
		
		var overlap=[];
		//2017-09-29 13:00 delbert save

		var gridlist=grid.dataSource.data();
		$(gridlist).each(function(idx,dataItem){
			if(dataItem.Discontinued){
				var totalsendCnt=0;

				var id=dataItem.idx;
					var inputCnt=dataItem.inputCnt;
					var sendCnt=Number(dataItem.shipmentCnt);
					var lotNo=dataItem.lotNo;
					var prdNo=dataItem.prdNo;
					var item=dataItem.itm;
					var date=dataItem.date;
					var outLot=dataItem.barcode;
					var precnt=Number(dataItem.prdCnt);
					var obj=new Object();
					var lotStock = Number(dataItem.lotStock);
					$(gridlist).each(function(id,data){
						if(Number(data.idx)==Number(dataItem.idx)){
							totalsendCnt+=Number(data.shipmentCnt);
						}
					})
					
					obj.id = id;
					obj.sendCnt = sendCnt;
					obj.lotNo = lotNo;
					obj.prdNo = prdNo;
					obj.item = item;
					obj.date = date;
					obj.outLot = outLot;
					obj.inputCnt = inputCnt;
					obj.precnt = precnt;
					
					console.log("------------------------")
					console.log("preCnt ::"+precnt);
					console.log("sendCnt ::"+sendCnt);
					console.log("totalsendCnt ::"+totalsendCnt);
					console.log("stock ::"+lotStock)
					console.log("--------------END------------")
					if(sendCnt==0 || sendCnt=="" || sendCnt==undefined || sendCnt=="NaN" || sendCnt=="null" || sendCnt==null || isNaN(sendCnt)){
						alert("출하수량을 입력하세요")
						valid = false;
						return valid;
					}else if(Number(lotStock)-Number(sendCnt)<0 || Number(lotStock)<Number(totalsendCnt)){
						alert("재고 수량보다 출하수량이 더많습니다.")
						valid = false;
						return  valid;
					}else if(outLot=="" || outLot==undefined){
						alert("출하로트번호가 비었습니다.");
						valid = false;
						return valid;
					}
					if(valid) valueArray.push(obj);
					return;
			}
			
			if(valid==false)return valid;
		});
		
		if(valid==false)return valid;
		console.log(valueArray)
		var obj = new Object();
		obj.val = valueArray;
		
		var param = JSON.stringify(obj); 
		$.showLoading();
		$.hideLoading();
		console.log(param);
		if(valid==true){
			$.ajax({
				url : url,
				data :"val=" + param,
				type : "post",
				dataType : "text",
				success : function(data){
					alert("${save_ok}")
					getReleaseInfo(scroll);
				}
			});
		}else{
			$.hideLoading();
			return;
		}
		
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/inven_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td> 
								<spring:message code="prd_no"></spring:message>
								<select id="group"></select>
								<button id="search" onclick="getReleaseInfo()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
								<button onclick="saveRow()"><spring:message code="save" ></spring:message></button>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="wrapper">
									<table style="width: 100%; text-align: center; border-collapse: collapse;" class="alarmTable" id="table2" border="1" >
									</table>
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	