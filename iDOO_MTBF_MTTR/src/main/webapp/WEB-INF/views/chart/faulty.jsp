<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	/* overflow : hidden; */
	background-color: black;
  	font-family:'Helvetica';
  	overflow: hidden;
}

</style> 
<script type="text/javascript">
	const loadPage = () =>{
		createMenuTree("kpi", "faulty")
	}
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month);
	};
	
	
	$(function(){
		getGroup();
		setEl();
		setDate();	
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
/* 		$(".date").change(getFaultyList);
		$("#group").change(getFaultyList);
		$("#dvclist").change(getFaultyList); */
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function setEl(){
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		
		$("#content_table td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});

		$("label").css({
//			"background" :"#EBF7FF",
			"color" : "white",
			"padding-top" : getElSize(17),
			"padding-bottom" : getElSize(17)
		});
		
		$("select").css({
			"height" :getElSize(85),
			"width" : getElSize(455),
			"margin" : getElSize(0),
		});
		$("input").css({
			"height" :getElSize(85),
			"width" : getElSize(355),
		});
		
		
		$("#statusText font").css({
			"font-size" :getElSize(100),
			"color" : "white"
		});
		
		$("#resultText font").css({
			"font-size" :getElSize(100),
			"color" : "white"
		});
		
		$("#causeText font").css({
			"font-size" :getElSize(100),
			"color" : "white"
		});

		$("#statusText").css({
			"margin-top" :getElSize(100),
			"margin-left" :getElSize(50)
		});
		
		$("#resultText").css({
			"margin-top" :getElSize(300),
			"margin-left" :getElSize(50)
		});
		
		$("#causeText").css({
			"margin-top" :getElSize(300),
			"margin-left" :getElSize(50)
		});

		$("#search").css({
			"width" :getElSize(170),
			"cursor" : "pointer",
			"padding" : 0,
			"margin" :0,
			"height" : getElSize(85),
			"font-size" : getElSize(46),
			"background" : "#9B9B9B",
			"border-radius": getElSize(8)
		});
		
		$(".dayText").css({
		    "font-size":getElSize(100),
			"margin-top":getElSize(100)
		})
		
		$("#dayPie1").css({
			"border" : getElSize(2.4) +"px solid #C0DEED"
		})
		$("#dayPie11").css({
			"border" : getElSize(2.4) +"px solid #C0DEED"
		})

		$("#dayPie2").css({
			"border" : getElSize(2.4) +"px solid #C0DEED"
		})
		$("#dayPie22").css({
			"border" : getElSize(2.4) +"px solid #C0DEED"
		})
		
		$("#dayPie3").css({
			"border" : getElSize(2.4) +"px solid #C0DEED"
		})
		$("#dayPie33").css({
			"border" : getElSize(2.4) +"px solid #C0DEED"
		})
		
		$("select").css({
		    "border": "1px black #999",
		    "font-family": "inherit", 
		    "background": "url(${ctxPath}/images/FL/default/btn_drop_menu_default.svg) no-repeat 98% 50%",
		    "background-color" : "black",
		    "z-index" : "999",
		    "border-radius": "0px",
		    "-webkit-appearance": "none",
		    "-moz-appearance": "none",
		    "appearance":"none",
			"background-size" : getElSize(60),
	        "font-size": getElSize(52) + "px",
			"color" : "white",
			"border" : "none"
		})
		
		$("select option").css({
	        "background-color" : "rgba(0,0,0,5)",
	        "color" : "white",
		    "border" : "1",
	        "transition-delay" : 0.05,
	        "border-bottom" : getElSize(10) + "px solid red",
	        "text-align-last": "center",
/* 		    "border-top-color": "rgb(215, 0, 0)",
		    "border-right-color": "rgb(215, 0, 0)",
		    "border-left-color": "rgb(215, 0, 0)", */
	        "padding" : getElSize(15) + "px",
	        "transition" : "1s",
	    })
	    
		$(".date").css({
			"margin" : 0,
			"background" : "black",
	    	"color" : "white",
	        "font-size": getElSize(52) + "px",
	    	"border" : "none"
		}) 
		
		/* 
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		*/
		
	};
	
	function getGroup(){
		var url = "${ctxPath}/common/getPrdNoList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				console.log("장비")
				console.log(json)
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#group").html(option);
				dvcList();
//				getFaultyList();
			}
		});
	};
	function dvcList(){
		var url = "${ctxPath}/common/dvcList.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				json = data.dataList;
				console.log(json)
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.dvcId + "'>" + data.dvcName + "</option>"; 
				});
				
				$("#dvclist").html(option);
				
				getFaultyList();
			}
		});
	};
	
	function comma(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}


	var className = "";
	var classFlag = true;
	var kendotable;
	
	$(document).ready(function(){
		
		$("#detailDay").kendoDialog({
/* 			scrollable: true
			, */
			height: getElSize(2000)
			,width: getElSize(3600)
			,title: "월 상세보기"
		 	/* ,content: "<table border=1 style='color:black;'> <tr> <th> Tool Cycle Count </th> <th> RunTime (h) </th> </tr>" +
		 			 "<tr> <td> <input id='preCnt' type='number' value=0> </td> <td>  <input id='preSec' type='number' value=0> </td> </tr> </table>"
			 */,actions: [{
				text: "OK"
				,action: function(e){
				//	alert("popup")
				},
				primary: true
			}]
		});
		$("#detailDay").data("kendoDialog").close();
		
		$("#detailDay").keyup(function(e){
			if(e.keyCode==13){
				$("#detailDay").data("kendoDialog").close();
			}
		})
		
		var tempDate=$("#sDate").val();
		var templist;
		templist=tempDate.split("-");
		templist[0]=Number(templist[0])-1;
		console.log(templist[0])
		console.log(templist[0])
		tempDate=templist[0]+"-"+templist[1];
		console.log(tempDate)
		$("#sDate").val(tempDate);
		kendotable = $("#grid").kendoGrid({
			height:getElSize(700),
			columns:[{
//				field:"sDate",title:"년/월", attributes: {
			field:"sDate",title:"year/month", attributes: {
    				style: "text-align: center; color:white !important; background:rgb(44,44,54); font-size:" + getElSize(38)
    			}
				,width:getElSize(120)
			},{
				field:"totalCnt" , title:"Production Quantity" , template : "#=comma(totalCnt)#"
				,width:getElSize(150)
			},{
				title:"Accepted quantity" , template:"#=comma(totalCnt-faultCnt)#"
				,width:getElSize(150)
			},{
				field:"faultCnt" , title:"Total bad quantity"
				,width:getElSize(150)
			},{
				field:"processFaultyCnt" , title:"Poor process quantity"
				,width:getElSize(150)
			},{
				field:"customerFaultyCnt" , title:"Customer bad quantity"
				,width:getElSize(150)
			},{
				field:"countDvc" , title:"Bad material"
				,width:getElSize(150)
			},{
				field:"count" , title:"Poor machining"
				,width:getElSize(150)
			},{
				field:"ratio" , title:"Defective rate (PPM)" , template:"#=ratio# ppm"
				,width:getElSize(200)
			}],
			dataBound:function(e){
				$(".k-grid tbody tr td").css("font-size",getElSize(36));
				$(".k-grid tbody tr td").css({
					"height" : getElSize(96)
				})
				$(".k-grid thead tr th").css("font-size",getElSize(36));
				$(".k-grid").css({
					"border-radius": getElSize(8)
				})
			}
		}).data("kendoGrid")
		
	})
	
	var sDatelist=[];
	var ratiolist=[];
	
	var processFaultyCnt=0;
	var customerFaultyCnt=0;
	var countDvc=0;
	var count=0;
	function getFaultyList(){
		$.showLoading();
		
		processFaultyCnt=0;
		customerFaultyCnt=0;
		countDvc=0;
		count=0;
		
		sDatelist=[];
		ratiolist=[];
		
		
		classFlag = true;
		var url = "${ctxPath}/chart/getFaultyList.do";
		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val() + "-31";
		
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + 
					"&line=" + $("#group").val() +
					"&dvcId=" + $("#dvclist").val() ;

		console.log("getFaultyList")
		console.log(param)
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				$.showLoading();
				var json = data.dataList;
				console.log(data)
				var tr = "<thead><Tr style='background-color:#222222'>" + 
									"<Td>" + 
										"${month}" + 
									"</Td>" + 
									"<Td>" + 
										"${prd_no}총생산" + 
									"</Td>" + 
									"<Td>" + 
										String("${prd_cnt}").replace(/&nbsp;/gi,"") +"합격수량" + 
									"</Td>" +
									"<Td>" + 
										"${operation} 불량" + 
									"</Td>" +
									"<Td>" + 
										"${faulty_cnt_operation}" + 
									"</Td>" +
									"<Td>" + 
										"${faulty_cnt_customer}" + 
									"</Td>" + 
									"<Td>" + 
										"${operation_faulty_operation}Bad material" + 
									"</Td>" + 
									"<Td>" + 
										"${operation_faulty_customer}가공불량" + 
									"</Td>" + 
									"<Td>" + 
										"${operation_faulty_customer}불량율" + 
									"</Td>" + 
								"</Tr>" + 
							"</thead><tbody>";
					
				var kendodata = new kendo.data.DataSource({});			
				
				$(json).each(function(idx, data){
					data.ratio=Number(data.ratio).toFixed(1);
					sDatelist.push(data.sDate);
					ratiolist.push(data.ratio);
					processFaultyCnt += Number(data.processFaultyCnt);
					customerFaultyCnt += Number(data.customerFaultyCnt);
					countDvc += Number(data.countDvc);
					count += Number(data.count);
					
					if(data.prdNo!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						
						var opPrdCnt = decodeURIComponent(data.prd).replace(/\+/gi," "); 
						var cstmrPrdCnt = decodeURIComponent(data.cstmPrdCnt);
						if(opPrdCnt=="선택"){
							opPrdCnt = "";
						}
						if(cstmrPrdCnt=="선택"){
							cstmrPrdCnt = "";
						}
						
						
						var processFaultyRate = Number(data.processFaultyCnt/data.prdCnt*100).toFixed(1)
						var customerFaultyRate = (data.customerFaultyCnt/data.prdCnt*100).toFixed(1)
						
						if(data.prdCnt==0){
							processFaultyRate = "0.0";
							customerFaultyRate = "0.0";
						};
						
						tr += "<tr class='contentTr " + className + "'>" +
									"<td>" + data.sDate + "</td>" +
									"<td>" + data.totalCnt + "</td>" + 
									"<td>" + data.cnt + "</td>"+
									"<td>" + data.faultCnt + "</td>" + 
									"<td>" + data.processFaultyCnt + "</td>"+
									"<td>" + data.customerFaultyCnt + "</td>"+
									"<td>" + data.countDvc + "</td>"+
									"<td>" + data.count + "</td>"+
									"<td>" + data.ratio + "</td>"+
							"</tr>";					
					}

					kendodata.add(data)
				});
				
				kendodata.sort([
					{ field: "sDate", dir: "desc" }
				])
				
				kendotable.setDataSource(kendodata);

				tr += "</tbody>";
				
				$(".alarmTable").html(tr).css({
					"font-size": getElSize(40),
				});
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
				$("#wrapper").css({
					"height" :getElSize(1650),
					"overflow" : "hidden"
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				scrolify($('.alarmTable'), getElSize(1550));
				$("#wrapper div:last").css("overflow", "auto")
				
				createChart();
				createStatusChart(data.statusFaultyList); //현상차트
				createCauseChart(data.causeFaultyList); // 원인차트
				createResultChart(data.resultFaultyList); //조치 차트
				
				$.hideLoading()
			}
		});
	};

	function createChart(){
		$("#kendoChart").kendoChart({
			chartArea: {
				height: getElSize(970),
				width: getElSize(2885),
				background:"#121212",
			},
			title: false,
            legend: {	//범례표?
             	labels:{
            		font:getElSize(48) + "px sans-serif",
            		color:"white"
            	},
            	stroke: {
            		width:100
            	},
            	position: "bottom",
            	orientation: "horizontal",
                offsetX: getElSize(910),
//                offsetY: getElSize(800)
           
            },
            render: function(e) {	//범례 두께 조절
                var el = e.sender.element;
                el.find("text")
                    .parent()
                    .prev("path")
                    .attr("stroke-width", getElSize(20));
            },
			seriesDefaults: {	//데이터 기본값 ()
				gap: getElSize(5),
				type: "line" ,
				stack:true,
				spacing: getElSize(1),
 				labels:{
 					font:getElSize(45) + "px sans-serif",	//no working
 					margin:0,
 					padding:0
				}/* , 
				visual: function (e) {
	                return createColumn(e.rect, e.options.color);
	            } */
			},	
			categoryAxis: {	//x축값
                categories: sDatelist,
                line: {
                    visible: true
                },
                labels:{
                	font:"italic "+ getElSize(48) + "px sans-serif",	
                	color:"white"
				} ,majorGridLines: {	//차트안 x축 실선
                    visible: false
                }
            },
            tooltip: {	//커서 올리면 값나옴
                visible: true,
                font:getElSize(48) + "px sans-serif",	
                format: "{0} ppm",
                template: "#= series.name #: #= value # ppm"
            },
            valueAxis: {	//간격 y축
                labels: {
                	font:"italic "+ getElSize(48) + "px sans-serif",	
                    format: "{0} ppm",
                    color:"white"
                },majorGridLines: {	//차트안 x축 실선
                    visible: true,
                    color: "#5D5D5D"
                }
//                majorUnit: 100	//간격
            },
            //데이터 클릭시
            seriesClick: function(e) {
				var chartMon = e.category.substring(0,e.category.indexOf("/"))
				var chartDay = e.category.substring(e.category.indexOf("/")+1,e.category.length)
				$("#detailDay").data("kendoDialog").open();
				console.log("월 :"+chartMon);
				console.log("일 :"+chartDay);
/* 				var ss="20"+chartMon+"-"+chartDay+"-"+"01";
				alert(ss); */
				var lastDay = new Date("20"+chartMon,chartDay,"")
					lastDay = lastDay.getDate(); 

				$.showLoading()
				var url = "${ctxPath}/chart/dayFaulty.do";
				var param = "mDay=" + chartMon +
							"&dDay=" +chartDay +
							"&sDate=" + "20"+chartMon+"-"+chartDay+"-"+"01" +
							"&eDate=" + "20"+chartMon+"-"+chartDay+"-"+lastDay +
							"&line=" + $("#group").val() +
							"&dvcId=" + $("#dvclist").val() ;
				$.ajax({
					url : url,
					data : param,
					type : "post",
					dataType : "json",
					success : function(data){
						console.log(data)
						var json=data.dataList;
						var sDatelist=[];
						var ratiolist=[];
						
						for(i=0,len=json.length;i<len;i++){
							sDatelist.push(json[i].tgDate.substring(8,10));
							ratiolist.push(json[i].ratio);
						}
						
						$("#detailDay").data("kendoDialog").title(json[0].tgDate.substring(5,7) + " 월 상세보기")
						drawDayChart(sDatelist,ratiolist);	//chart 그리기
						drawDayTable(json);	//table 그리기						
						draDayStatusChart(data.statusFaultyList); // day 현상차트
						draDayCauseChart(data.causeFaultyList); // day 원인차트
						draDayResultChart(data.resultFaultyList); // day 조치 차트
						$.hideLoading()
					}
				})
				
			},
			//x,y축
			axisLabelClick: function(e) {
				console.log("x,y 축 클릭시 : " + e.axis.type, e.value);
				if(e.axis.type=="category"){
					var chartMon = e.value.substring(0,e.value.indexOf("/"))
					var chartDay = e.value.substring(e.value.indexOf("/")+1,e.value.length)
					$("#detailDay").data("kendoDialog").open();
					console.log("월 :"+chartMon);
					console.log("일 :"+chartDay);
	/* 				var ss="20"+chartMon+"-"+chartDay+"-"+"01";
					alert(ss); */
					var lastDay = new Date("20"+chartMon,chartDay,"")
						lastDay = lastDay.getDate(); 

					$.showLoading()
					var url = "${ctxPath}/chart/dayFaulty.do";
					var param = "mDay=" + chartMon +
								"&dDay=" +chartDay +
								"&sDate=" + "20"+chartMon+"-"+chartDay+"-"+"01" +
								"&eDate=" + "20"+chartMon+"-"+chartDay+"-"+lastDay +
								"&line=" + $("#group").val() +
								"&dvcId=" + $("#dvclist").val() ;
					$.ajax({
						url : url,
						data : param,
						type : "post",
						dataType : "json",
						success : function(data){
							console.log(data)
							var json=data.dataList;
							var sDatelist=[];
							var ratiolist=[];
							
							for(i=0,len=json.length;i<len;i++){
								sDatelist.push(json[i].tgDate.substring(8,10));
								ratiolist.push(json[i].ratio);
							}
							
							$("#detailDay").data("kendoDialog").title(json[0].tgDate.substring(5,7) + " 월 상세보기")
							drawDayChart(sDatelist,ratiolist);	//chart 그리기
							drawDayTable(json);	//table 그리기						
							draDayStatusChart(data.statusFaultyList); // day 현상차트
							draDayCauseChart(data.causeFaultyList); // day 원인차트
							draDayResultChart(data.resultFaultyList); // day 조치 차트
							$.hideLoading()
						}
					})
					
				}
			},
            series: [{	//데이터
                labels: {
                    visible: true,
                    color:"#DB0000",	//데이터 값 표시
                    format: "{0}ppm",
                    font:getElSize(48) + "px sans-serif",	
              	    position: "top",
                    margin: {
                  		right : 45
                 	}
                  },
                  color : "#FF0000",	//선색
                  width : getElSize(13),
                  name: "Defective rate(PPM)",
                  data: ratiolist		
                }]
		})
	}
	
	function drawDayTable(list){
		dayTable = $("#dayTable").kendoGrid({
			dataSource:list,
			columns:[{
				field:"tgDate",title:"년/월", attributes: {
    				style: "text-align: center; color:white !important; background:linear-gradient( to bottom, gray, black ); font-size:" + getElSize(38)
    			}
				,width:getElSize(170)
			},{
				field:"totalCnt" , title:"생산수량" , template : "#=comma(totalCnt)#"
				,width:getElSize(140)
			},{
				title:"합격수량" , template:"#=comma(cnt)#"
				,width:getElSize(140)
			},{
				field:"fault" , title:"총<br>불량수량"
				,width:getElSize(140)
			},{
				field:"processFaultyCnt" , title:"공정<br>불량수량"
				,width:getElSize(140)
			},{
				field:"customerFaultyCnt" , title:"고객<br>불량수량"
				,width:getElSize(140)
			},{
				field:"countDvc" , title:"Bad material"
				,width:getElSize(140)
			},{
				field:"count" , title:"가공불량"
				,width:getElSize(140)
			},{
				field:"ratio" , title:"불량율<br>(PPM)" , template:"#=ratio# ppm"
				,width:getElSize(200)
			}],
			dataBound:function(e){
				$(".k-grid tbody tr td").css("font-size",getElSize(36));
				$(".k-grid thead tr th").css("font-size",getElSize(36));
				
				$(".k-grid tbody tr td").css({
					"height" : getElSize(96)
				})
				$(".k-grid").css({
					"border-radius": getElSize(8)
				})
			}
		}).data("kendoGrid")
	}
	
	function drawDayChart(sDatelist,ratiolist){

		$("#dayChart").kendoChart({
		chartArea: {
			background:"#121212",
		},
		pannable: {
            lock: "y"
        },
        zoomable: {
            mousewheel: {
                lock: "y"
            },
            selection: {
                lock: "y"
            }
        },
		title: false,
        legend: {	//범례표?
         	labels:{
        		font:getElSize(48) + "px sans-serif",
        		color:"white"
        	},
        	stroke: {
        		width:100
        	},
        	position: "bottom",
        	orientation: "horizontal",
            offsetX: getElSize(910),
//                offsetY: getElSize(800)
       
        },
        render: function(e) {	//범례 두께 조절
            var el = e.sender.element;
            el.find("text")
                .parent()
                .prev("path")
                .attr("stroke-width", getElSize(20));
        },
		seriesDefaults: {	//데이터 기본값 ()
			gap: getElSize(5),
			type: "line" ,
			stack:true,
			spacing: getElSize(1),
				labels:{
					font:getElSize(45) + "px sans-serif",	//no working
					margin:0,
					padding:0
			}/* , 
			visual: function (e) {
                return createColumn(e.rect, e.options.color);
            } */
		},	
		categoryAxis: {	//x축값
            categories: sDatelist,
            line: {
                visible: true
            },
            labels:{
            	font:"italic "+ getElSize(48) + "px sans-serif",	
            	color:"white"
			} ,majorGridLines: {	//차트안 x축 실선
                visible: false
            }
        },
        tooltip: {	//커서 올리면 값나옴
            visible: true,
            font:getElSize(48) + "px sans-serif",	
            format: "{0} ppm",
            template: "#= series.name #: #= value # ppm"
        },
        valueAxis: {	//간격 y축
            labels: {
            	font:"italic "+ getElSize(48) + "px sans-serif",	
                format: "{0} ppm",
                color:"white"
            },majorGridLines: {	//차트안 x축 실선
                visible: true,
                color: "#5D5D5D"
            }
//                majorUnit: 100	//간격
        },
        series: [{	//데이터
            /* labels: {
                visible: true,
                color:"#DB0000",	//데이터 값 표시
                format: "{0}ppm",
                font:getElSize(48) + "px sans-serif",	
          	    position: "top",
                margin: {
              		right : 45
             	}
              }, */
              color : "#FF0000",	//선색
              width : getElSize(13),
              name: "불량율(PPM)",
              data: ratiolist		
            }]
		})
	}
	
	function draDayStatusChart(list){
		var totalCnt=0;
		console.log(list)
		for(i=0; i<list.length; i++){
			totalCnt+=Number(list[i].value);
			list[i].category=decode(list[i].category);
		}
		for(i=0; i<list.length; i++){
			list[i].value=(Number((list[i].value)/totalCnt)*100).toFixed(1);
		}
		
        var piechart = $("#dayPie1").kendoChart({
        	chartArea: {
				background:"black",
			},
/*             title: {
            	font:getElSize(48) + "px sans-serif",	
            	position: "right",
                text: "현상"
                
            }, */	
            legend: {
            	position: "right"
          		,labels: {
          		      color: "white"
       		    	  ,font: getElSize(33)+"px sans-serif"
         		}
            },
            //범례에 마우스 오버시 이벤트
            legendItemHover: function(e){
            	//tooltip method 보여주기
				piechart.showTooltip(function(point) {
				    return point.index === e.pointIndex;
				});
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    font:getElSize(45) + "px sans-serif",
                    padding:{
                    	top:-getElSize(30)
                    },
                    background: "transparent",
                    template: "#= category #: \n #= value#%"
                }
            },
            dataSource: {
                data: list
            },//
            seriesColors:["#1D8B15","#1266FF","#E14FCA","red","#00C6ED","#EDA900","#870073"],
            series: [{
				overlay: { gradient: "none" },
            	padding:0,
                type: "pie",
                startAngle: 100,
                field:"value",
                labels: {
                    visible: false,
                    position: "insideEnd"
                },
            }],
            tooltip: {
                visible: true,
                format: "{0}%",
               	font:getElSize(48) + "px sans-serif",	
               	template: "#:category# : #: value# %"
            }
        }).data("kendoChart");
	}
	function draDayCauseChart(list){
		var totalCnt=0;
		for(i=0; i<list.length; i++){
			totalCnt+=Number(list[i].value);
			list[i].category=decode(list[i].category);
		}
		for(i=0; i<list.length; i++){
			list[i].value=(Number((list[i].value)/totalCnt)*100).toFixed(1);
		}
		
		var piechart = $("#dayPie2").kendoChart({
        	chartArea: {
				background:"black",
			},
           /*  title: {
            	font:getElSize(48) + "px sans-serif",	
            	position: "bottom",
                text: "원인"
            }, */
            legend: {
                //visible: false
            	position: "right"
         		,labels: {
					color: "white"
					,font: getElSize(33)+"px sans-serif"
       		    }

            },
         	//범례에 마우스 오버시 이벤트
            legendItemHover: function(e){
            	//tooltip method 보여주기
				piechart.showTooltip(function(point) {
				    return point.index === e.pointIndex;
				});
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    font:getElSize(45) + "px sans-serif",
                    padding:{
                    	top:-getElSize(30)
                    },
                    background: "transparent",
                    template: "#= category #: \n #= value#%"
                }
            },
            dataSource: {
                data: list
            },
            seriesColors:["red","#1D8B15","blue","#870073","#00C6ED","#EDA900","#E14FCA"],
            series: [{
                type: "pie",
				overlay: { gradient: "none" },
                startAngle: 100,
                field: "value",
                color : "white",
                padding:0,
                labels: {
                    visible: false,
                    position: "outsideEnd",
                    color : "white"
                }
            }],
            tooltip: {
                visible: true,
                font:getElSize(48) + "px sans-serif",	
                format: "{0}%"
               	,template: "#:category# : #: value# %"
            }
        }).data("kendoChart");
	}
	
	function draDayResultChart(list){
		var totalCnt=0;
		for(i=0; i<list.length; i++){
			totalCnt+=Number(list[i].value);
			list[i].category=decode(list[i].category);
		}
		for(i=0; i<list.length; i++){
			list[i].value=(Number((list[i].value)/totalCnt)*100).toFixed(1);
		}
		console.log("-----")
		console.log(list)
        var piechart = $("#dayPie3").kendoChart({
        	chartArea: {
				background:"black",
			},
            /* title: {
            	font:getElSize(48) + "px sans-serif",	
            	position: "bottom",
                text: "조치",
            }, */
            legend: {
            	position: "right"
				,labels: {
					color: "white"
					,font: getElSize(33)+"px sans-serif"
				}
            },
	        //범례에 마우스 오버시 이벤트
            legendItemHover: function(e){
            	//tooltip method 보여주기
				piechart.showTooltip(function(point) {
				    return point.index === e.pointIndex;
				});
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    font:getElSize(45) + "px sans-serif",
                    padding:{
                    	top:-getElSize(30)
                    },
                    background: "transparent",
                    template: "#= category #: \n #= value#%"
                }
            },
            tooltip: {
                visible: true,
                font:getElSize(48) + "px sans-serif",	
                format: "{0}%"
                ,template: "#:category# : #: value# %"
            },
            dataSource: {
                data: list
            },
            seriesColors:["red","#1D8B15","blue","#870073","#00C6ED","#EDA900","#E14FCA"],
            series: [{
            	padding: 0,
                type: "pie",
				overlay: { gradient: "none" },
                startAngle: 100,
                field: "value",
                visibleInLegendField: "visible",
                labels: {
                    visible: false,
                    position: "outsideEnd"
                }
            }]
           
        }).data("kendoChart");
	}

	
	function createStatusChart(list) {
		var totalCnt=0;
		console.log(list)
		for(i=0; i<list.length; i++){
			totalCnt+=Number(list[i].value);
			list[i].category=decode(list[i].category);
		}
		for(i=0; i<list.length; i++){
			list[i].value=(Number((list[i].value)/totalCnt)*100).toFixed(1);
		}
		
        var piechart = $("#createStatusChart").kendoChart({
        	chartArea: {
				height: getElSize(550),
				background:"black",
			},
/*             title: {
            	font:getElSize(48) + "px sans-serif",	
            	position: "right",
                text: "현상"
                
            }, */	
            legend: {
            	position: "right"
          		,labels: {
          		      color: "white"
       		    	  ,font: getElSize(33)+"px sans-serif"
         		}
            },
            //범례에 마우스 오버시 이벤트
            legendItemHover: function(e){
            	//tooltip method 보여주기
				piechart.showTooltip(function(point) {
				    return point.index === e.pointIndex;
				});
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    font:getElSize(45) + "px sans-serif",
                    padding:{
                    	top:-getElSize(30)
                    },
                    background: "transparent",
                    template: "#= category #: \n #= value#%"
                }
            },
            dataSource: {
                data: list
            },//
            seriesColors:["#1D8B15","#1266FF","#E14FCA","red","#00C6ED","#EDA900","#870073"],
            series: [{
				overlay: { gradient: "none" },
            	padding:0,
                type: "pie",
                startAngle: 100,
                field:"value",
                labels: {
                    visible: false,
                    position: "insideEnd"
                },
            }],
            tooltip: {
                visible: true,
                font:getElSize(48) + "px sans-serif",	
                format: "{0}%"
               	,template: "#:category# : #: value# %"
            }
        }).data("kendoChart");
    }
	
	function createCauseChart(list) {
		var totalCnt=0;
		for(i=0; i<list.length; i++){
			totalCnt+=Number(list[i].value);
			list[i].category=decode(list[i].category);
		}
		for(i=0; i<list.length; i++){
			list[i].value=(Number((list[i].value)/totalCnt)*100).toFixed(1);
		}
		
		var piechart = $("#createCauseChart").kendoChart({
        	chartArea: {
        		height: getElSize(550),
				background:"black",
			},
           /*  title: {
            	font:getElSize(48) + "px sans-serif",	
            	position: "bottom",
                text: "원인"
            }, */
            legend: {
                //visible: false
            	position: "right"
         		,labels: {
					color: "white"
					,font: getElSize(33)+"px sans-serif"
       		    }

            },
         	//범례에 마우스 오버시 이벤트
            legendItemHover: function(e){
            	//tooltip method 보여주기
				piechart.showTooltip(function(point) {
				    return point.index === e.pointIndex;
				});
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    font:getElSize(45) + "px sans-serif",
                    padding:{
                    	top:-getElSize(30)
                    },
                    background: "transparent",
                    template: "#= category #: \n #= value#%"
                }
            },
            dataSource: {
                data: list
            },
            seriesColors:["red","#1D8B15","blue","#870073","#00C6ED","#EDA900","#E14FCA"],
            series: [{
                type: "pie",
                startAngle: 100,
				overlay: { gradient: "none" },
                field: "value",
                color : "white",
                padding:0,
                labels: {
                    visible: false,
                    position: "outsideEnd",
                    color : "white"
                }
            }],
            tooltip: {
                visible: true,
                font:getElSize(48) + "px sans-serif",	
                format: "{0}%"
               	,template: "#:category# : #: value# %"
            }
        }).data("kendoChart");
    }
	
	function createResultChart(list) {
	
		var totalCnt=0;
		for(i=0; i<list.length; i++){
			totalCnt+=Number(list[i].value);
			list[i].category=decode(list[i].category);
		}
		for(i=0; i<list.length; i++){
			list[i].value=(Number((list[i].value)/totalCnt)*100).toFixed(1);
		}
		console.log("-----")
		console.log(list)
        var piechart = $("#createResultChart").kendoChart({
        	chartArea: {
				height: getElSize(550),
				background:"black",
			},
            /* title: {
            	font:getElSize(48) + "px sans-serif",	
            	position: "bottom",
                text: "조치",
            }, */
            legend: {
            	position: "right"
				,labels: {
					color: "white"
					,font: getElSize(33)+"px sans-serif"
				}
            },
	        //범례에 마우스 오버시 이벤트
            legendItemHover: function(e){
            	//tooltip method 보여주기
				piechart.showTooltip(function(point) {
				    return point.index === e.pointIndex;
				});
            },
            seriesDefaults: {
                labels: {
                    visible: true,
                    font:getElSize(45) + "px sans-serif",
                    padding:{
                    	top:-getElSize(30)
                    },
                    background: "transparent",
                    template: "#= category #: \n #= value#%"
                }
            },
            tooltip: {
                visible: true,
                font:getElSize(48) + "px sans-serif",	
                format: "{0}%"
                ,template: "#:category# : #: value# %"
            },
            dataSource: {
                data: list
            },
            seriesColors:["red","#1D8B15","blue","#870073","#00C6ED","#EDA900","#E14FCA"],
            series: [{
            	padding: 0,
				overlay: { gradient: "none" },
                type: "pie",
                startAngle: 100,
                field: "value",
                visibleInLegendField: "visible",
                labels: {
                    visible: false,
                    position: "outsideEnd"
                }
            }]
           
        }).data("kendoChart");
    }
	
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="container">
		<table id="table" style="border-collapse: collapse;">
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td> 
								<label class="requirementCss"><spring:message code="prd_no"></spring:message></label>
								<select id="group" ></select>
								<label class="requirementCss"><spring:message code="operation"></spring:message></label>						
								<select id="dvclist"></select>
								<label class="requirementCss"><spring:message code="op_period"></spring:message></label>
								<input type="month" class="date" id="sDate"> ~ <input type="month" class="date" id="eDate">
								<button id='search' onclick="getFaultyList()"><spring:message code="search"></spring:message></button>
<%-- 								<button onclick="saveUpdatedStock()"><spring:message code="save" ></spring:message></button> --%>
							</td>
						</Tr>		
						<tr>
							<td>
								<div style="position: absolute;">
									<div style="width: 70%; float: left;">
										<div id="kendoChart" style="float: left; background:"blue""></div>
										<div id="grid" style="float: left;"></div>
									</div>
									<div id ="chart" style="width: 25%; height: 100%; float: left;">
										<div id="createStatusChart" style="float: left; margin: 2px; width:100%;height: 32%; background: yellow;"></div>
										<div id="createCauseChart" style="float: left; margin : 2px; width:100%;height: 32%; background: yellow;"></div>
										<div id="createResultChart" style="float: left; margin : 2px; width:100%;height: 32%; background: yellow;"></div>
									</div>
								<!-- 	<div style="width: 4%;float: left; position: relative;">
										<div id="statusText"><font>Status</font></div>
										<div id="causeText"><font>Cause</font></div>
										<div id="resultText"><font>Action</font></div>
									</div> -->
									
<!-- 									<div id="wrapper" style="float: left;">
										<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" id="table2" border="1" >
										</table>
									</div> -->
								
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
		</table>
	 </div>
	
	<div id="detailDay">
		<div style="position: absolute; height: 100%;width: 100%;">
			<div id="dayDiv" style="background: #121212;height: 90%;width: 70%; float: left;">
				<div id="dayChart" style="background: #121212;height: 50%;width: 100%; float: left;">
				</div>
				
				<div id="dayTable" style="height: 50%;width: 100%; float: left;">
				</div>
								
			</div>
			<div id="dayDiv2" style="background: black;height: 90%;width: 27%; float: left;">
				<div id="dayPie1" style="background: black; height: 33.3%;width: 85%; float: left;">
				</div>
				<div id="dayPie11" style="background: lime; height: 33.3%;width: 13.8%; float: left; text-align: center;">
					<div class="dayText" >현<br>상</div>
				</div>
				<div id="dayPie2" style="background: black; height: 33.3%;width: 85%; float: left;">
				</div>
				<div id="dayPie22" style="background: lime; height: 33.3%;width: 13.8%; float: left; text-align: center;">
					<div class="dayText" >원<br>인</div>
				</div>
				<div id="dayPie3" style="background: black; height: 33.3%;width: 85%; float: left;">
				</div>
				<div id="dayPie33" style="background: lime; height: 33.3%;width: 13.8%; float: left; text-align: center;">
					<div class="dayText" >조<br>치</div>
				</div>
<!-- 				<div id="dayPie" style="background: aqua;height: 90%;width: 27%; float: left;">
				</div> -->
			</div>
		</div>
	</div>
</body>
</html>	