<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script src="http://kendo.cdn.telerik.com/2018.2.516/js/jszip.min.js"></script>
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.common.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.min.css" />
<link rel="stylesheet" href="${ctxPath }/resources/styles/kendo.default.mobile.min.css" />
<script src="${ctxPath }/resources/js/kendo.all.min.js"></script>
<script type="text/javascript" src="http://jsgetip.appspot.com"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;

</style> 
<script type="text/javascript">
	
const loadPage = () =>{
	createMenuTree("maintenance", "Alarm_Manager")
	
}
	
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
		
	
	var className = "";
	var classFlag = true;
	
	$(function(){
		setDate();
	//	setEl();
		time();
		getData();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	var beforelist=[];
	var sarr1=new Object();
	var sarr2=new Object();
	sarr1.text="무인";
	sarr1.value="1";
	sarr2.text="유인";
	sarr2.value="0";
	var selectlist=[];
	var selectlist1=[];
	selectlist1.push(sarr1);
	selectlist1.push(sarr2);
	function getData(){
		
		$.showLoading()
		
		$("#content_table").off("click")
		var url = ctxPath + "/chart/getFacilitiesStatus.do";
		var param = "shopId=" + shopId;
		var list=[];
		var selectlist=[];
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				beforelist=json;
				console.log(json)
				var table = "<tbody>";
				$(json).each(function(idx, data){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					var jig = decodeURIComponent(data.JIG);
					var isAuto = decodeURIComponent(data.isAuto);
					
					if(chkLang!="ko"){
						jig = "";
						isAuto = "";
					};

					var arr=new Object();
					var arrsel=new Object();
					arr.a=data.dvcId;
					arr.b=data.cd;
					arr.c=decode(data.name);
//					arr.d=decode(data.nameAbb);
					arr.d=data.nameAbb;
					arr.e=decode(data.WCCD);
					arr.f=data.jig;
					arr.g=decode(data.isAuto);
					arrsel.text=decode(data.isAuto);
					arrsel.value=data.isvalue;
					arr.h=decodeURIComponent(data.type);
					arr.i=data.EXCD;
					arr.j=data.NC;
//					arr.k=data.PRDPRGM;
					arr.k=data.CNTMCD;
					arr.l=decodeURIComponent(data.empN);
					arr.m=decode(data.empD);
					arr.n=data.empCdN;
					arr.o=data.empCdD;
					arr.y=data.isvalue;
					//arr.cntPerCyl=data.cntPerCyl;
					arr.z=" ";
//					arr.m=data.tgCnt;
//					arr.n=data.tgRunTime;
//					arr.o=" ";
					list.push(arr);					
//					beforelist.push(arr);
					selectlist.push(arrsel);
				});//json
				
               var dataSource = new kendo.data.DataSource({
//                   pageSize: 20,
                   data: list,
                   autoSync: true,
                   schema: {
                       model: {
                         id: "TraceManger",
                         fields: {
                            a: { editable: false, nullable: true },
                            b: { editable: true, nullable: true },
                            c: { editable: true, nullable: true },
//                          d: { type: "number", validation: { required: true, min: 1} },
                            d: { editable: true, nullable: true },
                            e: { editable: true, nullable: true },
                            f: { editable: true, nullable: true },
                            g: { editable: true, nullable: true },
                            h: { editable: true, nullable: true },
                            i: { editable: true, nullable: true },
                            j: { editable: true, nullable: true },
                            k: { editable: true, nullable: true },
//                            l: { defaultValue: { CategoryID: 1, CategoryName: "Beverages"} },
                            l: { editable: true, nullable: true },
                            m: { editable: true, nullable: true },
                            //cntPerCyl: { type: "number", editable: true, nullable: true },
                            z: { editable: false, nullable: true }  
                         }
                       }
                   }
                });

				$("#content_table").kendoGrid({
					dataSource:dataSource,
					height:getElSize(1775),
					editable:true,
					sortable:true,
					//정렬시키면 저장안됨
					toolbar:[{name : "Create",template:'#=create()#'},{name : "save",template:'#=save()#'},  {name:"excel"} ],
					
					excel : {
						fileName : "보유장비관리.xlsx"
					},
					
					columns:[{
						field:"a",title:$("#order").val(),width:getElSize(110),attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate: '<spring:message code="order"></spring:message>'							
					},{
						field:"b",title:$("#machine_cd").val(),width:getElSize(270),attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate : '<spring:message code="machine_cd"></spring:message>'							
					},{
						field:"c",title:$("#machine_name").val(),width:getElSize(330),attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate : '<spring:message code="machine_name"></spring:message>'							
					},/* {
						field:"cntPerCyl",title:"Cavity" ,width:getElSize(230),attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate : 'Cavity'							
					}, */
					/* {
						field:"d",title:$("#machine_name_min").val(),width:getElSize(270),attributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(33)
	            			}							
					}, */{
						field:"e",title:$("#wc_cd").val(),width:getElSize(220),attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate : '<spring:message code="wc_cd"></spring:message>' 							
					},{
						field:"f",title:$("#jig_list").val(),width:getElSize(170),attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate : 	'<spring:message code="jig_list"></spring:message>'						
					},{
						field:"g",title:$("#auto_chk").val(),width:getElSize(180),editor: lDropDownEditor, template: "#=prdName(g)#",attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate : 	'<spring:message code="auto_chk"></spring:message>'						
					},{
						field:"h",title:$("#machine_ty").val(),width:getElSize(170),attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate : 	'<spring:message code="machine_ty"></spring:message>'						
					},{
						field:"i",title:$("#existed_machine_cd").val(),width:getElSize(270),attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate : '<spring:message code="existed_machine_cd"></spring:message>'							
					},{
						field:"j",title:"NC",width:getElSize(190),attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate : 'NC' 							
					},{
						field:"k",title:$("#m_cd").val(),width:getElSize(300),attributes: {
	            				style: "text-align: center; font-size:" + getElSize(29) 
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
	            			},headerTemplate : '<spring:message code="m_cd"></spring:message>'					
					},/* {
						title:$("#worker").val(),attributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(29) 
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(33)
	            			},							
							columns:[{
								field:"l",title:$("#day").val(),editor: lDropDownEditor, template: "#=l#",attributes: {
			            				style: "text-align: center; background-color:black; font-size:" + getElSize(29) 
			            			},headerAttributes: {
			            				style: "text-align: center; background-color:black; font-size:" + getElSize(33)
			            			}							
//								template:"#for(var i=0;i<3;i++){alert(i)}#"
//								template:"#alert(json.length)#"
//								template:"<select>#for(var iz=0;iz<5;iz++){#<option value=#=iz#>#=iz#</option>#}#</select>"
							},{
								field:"m",title:$("#night").val(),editor: lDropDownEditor, template: "#=m#",attributes: {
			            				style: "text-align: center; background-color:black; font-size:" + getElSize(29) 
			            			},headerAttributes: {
			            				style: "text-align: center; background-color:black; font-size:" + getElSize(33)
			            			}							
							}]
					}, *//* {
						field:"z",title:$("#deltext").val() ,width:getElSize(150),template:"<input type='button' id='#=a#' class='del' value='del' >"
						,headerAttributes: {
            				style: "text-align: center; background-color:black; font-size:" + getElSize(33)
            				},attributes: {
	            				style: "text-align: center; background-color:black; font-size:" + getElSize(29) 
	            			}
					} */{ command: { text: "${del}" ,click: del }, title: " ", width:  getElSize(150) ,
							attributes: {
		        				style: "text-align: center; font-size:" + getElSize(29) 
		        			},headerAttributes: {
		        				style: "text-align: center; background-color:black; font-size:" + getElSize(35)
		        			}}],
					dataBound: function (e) {
		            }//dataBound
				})//kendo
								
				$('.k-grid-content table tbody').css({
					"color":"white",
					"background-color":"black"
				});
			/* 	$('.k-grid-header-wrap table thead tr th').css({
					"color":"white",
					"background-color":"black"					
				})
				$('.k-grid-header-wrap table thead tr th a').css({
					"color":"white",
					"background-color":"black"										
				}) */
				
				
				$('.k-alt').css("background-color","#5D5D5D");
				$('.k-header').css("background-color","black");
				$('.k-button').css("background-color","#5D5D5D");
				
			  	$(".k-link").css({
					"color" : "white"
				}) 
				
				/* $("#content_table tr:odd").css({
					"background-color":"rgb(50,50,50)",
					"color" : "white"
				});
				
				$("#content_table tr:even").css({
					"background-color":"rgb(33,33,33)",
					"color" : "white"
				}); */
				
				var url = ctxPath + "/common/getWorkerList.do";
				$('.k-grid-${del}').css("background-color","white");
				/* var url = ctxPath + "/common/getWorkerList.do";
>>>>>>> branch 'master' of https://jay__kim@bitbucket.org/openfacturing/unos_bk_new.git
				var param = "shopId=" + shopId;
				$.ajax({
					url : url,
					data : param,
					type : "post",
					dataType : "json",
					success : function(data){
						var json = data.dataList;
						var table = "<tbody>";
						$(json).each(function(idx, data){
							var arr=new Object();
							arr.text=decode(data.name);
							arr.value=decode(data.name);
							selectlist.push(arr)
						});
					}
				}); */
				//dlehd
				function lDropDownEditor(container, options) {
                    $('<input required name="' + options.field + '"/>')
                        .appendTo(container)
                        .kendoDropDownList({
                            autoBind: false,
                            dataTextField: "text",
                            dataValueField: "value",
                            dataSource: selectlist1
                        });
                }
				/* public ActionResult FilterMenuCustomization_Cities()
				{
				    var db = new NorthwindDataContext();
				    return Json(db.Employees.Select(e => e.City).Distinct(), JsonRequestBehavior.AllowGet);
				} */
				table += "</tbody>";
				
				$("#content_table").css("background-color","#EAEAEA")
				$.hideLoading()
			}//success
			, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});
	};
	function del(e){
		e.preventDefault();
        var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
        var grid = $("#content_table").data("kendoGrid");
//        var dataItem = grid.dataItem($(this).closest("tr")); // retrieve the data record for the row
        tdelete1(dataItem.a,dataItem);
        console.log(dataItem);
	}
	function prdName(prdId) {
		if (prdId==0) {
			return '유인';
		}else if(prdId==1){
			return '무인';
		}else if(prdId=='무인'){
			return '무인';
		}else if(prdId=='유인'){
			return '유인';
		}
	}
	function create(e){
		return '<a class="k-button" href="#" id="toolbar-add_user" onclick="t_create()">${ add}</a>';
	}
	 function t_create(){
		var grid = $("#content_table").data("kendoGrid");
		 
		//sort 되어있을 경우 sort 초기화 후 추가	
		if(grid.dataSource._sort!=undefined){
			if(grid.dataSource._sort.length!=0){
				console.log(grid.dataSource._sort[0].field);
			    console.log(grid.dataSource._sort[0].dir);
			    grid.dataSource._sort[0].field=undefined;
			    grid.dataSource._sort[0].dir=undefined;
			    grid.dataSource.fetch();
			}
		}
		
		    
	    var ssss=grid.dataSource.data();
		var a;
		$(ssss).each(function(idx, data){
			a=data.a    //filed f 에 c값 넣기
		});
		
		var length=Number(a)+1;
		checkedIds = {}
		grid.dataSource.add({
			"a" : length,
			"b" : "",
			"c" : "",
			"d" : "",
			"f" : "",
			"g" : "1",
			"h" : "",
			"i" : "",
			"j" : "",
			"k" : "",
			"l" : "작업자",
			"m" :"작업자"
		});
		
		console.log(grid)
		grid.bind("sort", function(e) {
		    console.log(e.sort.field);
		    console.log(e.sort.dir);
		  });
		var cl=ssss.length-1;

		$('.k-grid-content table tbody tr:eq('+cl+') td:eq(1)').trigger('click');
		
		$("#grid td").on("keyup", function (e) {
			        //if current key is Enter
			        var row = $(this).closest("tr");
			        grid = $("#content_table").data("kendoGrid"),
			        dataItem = grid.dataItem(row);
		})	
		$('.k-grid-content table tbody').css({
			"color":"white",
			"background-color":"black"
		});
		$('.k-grid-header-wrap table thead tr th').css({
			"color":"white",
			"background-color":"black"					
		})
		/* $('.k-grid-header-wrap table thead tr th a').css({
			"color":"white",
			"background-color":"black"										
		}) */
		$('.k-alt').css("background-color","#5D5D5D");
		$('.k-header').css("background-color","black");
		$('.k-button').css("background-color","#5D5D5D");
		$('.k-grid-${del}').css("background-color","white");
		
		$("div.k-grid-content").scrollTop($("div.k-grid-content")[0].scrollHeight+1000);
	 } 
	function save(e){
		return '<a class="k-button" href="#" id="toolbar-add_user" onclick="t_save()">${save}</a>';
	}
	function t_save(){
		
			
	
		grid = $("#content_table").data("kendoGrid");
		var ssss=grid.dataSource.data();
		var valueArray=[];
		var updatelist=[];
		var update = new Object();

		var obj1 = new Object();
		obj1.val = beforelist;
//		console.log(obj1);
//		console.log(obj1.val[0]);

		$(ssss).each(function(idx, data){
		    var obj = new Object();
			obj.dvcId= data.a;
			obj.cd= data.b;
			obj.name= data.c;
			obj.nameAbb= data.d;
			obj.WCCD= data.e;
			obj.jig= data.f;
			obj.isAuto= data.g;
//			obj.isAuto= data.g;
			obj.type=data.h;
			obj.EXCD=data.i;
			obj.NC=data.j;
//			obj.k=data.PRDPRGM;
			obj.CNTMCD=data.k;
			obj.empN=data.l;
			obj.empD=data.m;
			obj.empCdN=data.n;
			obj.empCdD=data.o;
			obj.isvalue=data.y;
			obj.ip=ip();
			//obj.cntPerCyl=data.cntPerCyl;

			valueArray.push(obj);
		});//json

		//update
		var obj = new Object();
		obj.val = valueArray;
//		console.log(obj);
//		console.log(obj.val[0]);
		//수정한거만 들고가기
		for(i=0;i<beforelist.length;i++){
			var str="";
			var str1="";
			str=String(obj1.val[i].dvcId);
			str+=String(obj1.val[i].cd);
			str+=String(obj1.val[i].name);
			str+=String(obj1.val[i].nameAbb);
			str+=String(obj1.val[i].WCCD);
			str+=String(obj1.val[i].jig);
			str+=String(decode(obj1.val[i].isAuto));
			str+=String(decode(obj1.val[i].type));
			str+=String(obj1.val[i].EXCD);
			str+=String(obj1.val[i].NC);
			str+=String(obj1.val[i].CNTMCD);
			str+=String(decode(obj1.val[i].empN));
			str+=String(decode(obj1.val[i].empD));
			//str+=String(decode(obj1.val[i].cntPerCyl));
//			console.log(str);
			if(obj.val[i]==undefined){
				update.val=updatelist;
				break;
			}
			str1=String(obj.val[i].dvcId);//바뀐데이
			str1+=String(obj.val[i].cd);
			str1+=String(obj.val[i].name);
			str1+=String(obj.val[i].nameAbb);
			str1+=String(obj.val[i].WCCD);
			str1+=String(obj.val[i].jig);
			str1+=String(obj.val[i].isAuto);
			str1+=String(obj.val[i].type);
			str1+=String(obj.val[i].EXCD);
			str1+=String(obj.val[i].NC);
			str1+=String(obj.val[i].CNTMCD);
			str1+=String(obj.val[i].empN);
			str1+=String(obj.val[i].empD);
			//str1+=String(obj.val[i].cntPerCyl);
//			console.log(str1);
			if(str!=str1){
				updatelist.push(obj.val[i]);
//				console.log("--zz--")
				/* if(updatelist[i].isAuto=='무인'){
					updatelist[i].isAuto=1;
				}else if(updatelist[i].isAuto=='유인'){
					updatelist[i].isAuto=0;
				} */
				upsize=updatelist.length;
				for(j=0; j<upsize; j++){
					if(updatelist[j].isAuto=='무인'){
						updatelist[j].isAuto=1;
					}else if(updatelist[j].isAuto=='유인'){
						updatelist[j].isAuto=0;
					}
//					console.log(updatelist[j]);
				}
				update.val=updatelist;
				/* if(update.val[i].isAuto=='무인'){
					update.val[i].isAuto=1;
				}else if(update.val[i].isAuto=='유인'){
					update.val[i].isAuto=0;
				}  */
			}
		}
		//dlehd
		for(i=beforelist.length;i<valueArray.length;i++){
			updatelist.push(obj.val[i]);
			update.val=updatelist;
		}
//		console.log("----beforelist(obj1)----");
//		console.log(beforelist);
//		console.log("----afterlist(obj)----");
//		console.log(valueArray);
//		console.log("----updatelist----");
//		console.log(updatelist);
		console.log(updatelist)
		 //조건걸기
		for(i=0;i<updatelist.length;i++){
			if(updatelist[i].cd==undefined || updatelist[i].cd==""){
				alert("설비코드를 입력하세요")
				return;
			}else if(updatelist[i].name==undefined || updatelist[i].name==""){
				alert("설비명을 입력하세요")
				return;
			}else if(updatelist[i].WCCD==undefined){
				alert("WC코드를 입력하세요")
				return;
			}/*else if(updatelist[i].cntPerCyl==undefined || updatelist[i].cntPerCyl=="" || Number(updatelist[i].cntPerCyl)<=0){
				alert("Cavity를 똑바로 입력해주세요")
				return;
			} else if(updatelist[i].jig==undefined){
				alert("보유직을 입력하세요")
				return;
			}else if(updatelist[i].type==undefined){
				alert("설비유형을 입력하세요")
				return;
			}else if(updatelist[i].EXCD==undefined){
				alert("기존코드를 입력하세요")
				return;
			}else if(updatelist[i].NC==undefined){
				alert("NC를 입력하세요")
				return;
			}else if(updatelist[i].CNTMCD==undefined){
				alert("M코드를 입력하세요")
				return;
			} */else if(updatelist[i].cd.length>11){
				alert("실버코드 길이가 너무깁니다.");
				return;
			}
		}
		if(updatelist.length==0){
			$.hideLoading();	
			return;
		}
		$.showLoading();
		console.log(JSON.stringify(update));
		var url =ctxPath + "/chart/traceManagerUpdate.do";
		var param = "val="+JSON.stringify(update)+
					"&length="+valueArray.length;
		  $.ajax({
			url:url,
			data:param,
			type:"post",
			dataType:"text",
			success: function(data){
				if(data=="success"){
					alert("${save_ok}");
				}
				getData();
				$.hideLoading();	
			}
		}) 	 	//ajax	
		
		$('.k-grid-content table tbody').css({
			"color":"white",
			"background-color":"black"
		});
		$('.k-grid-header-wrap table thead tr th').css({
			"color":"white",
			"background-color":"black"					
		})
		$('.k-alt').css("background-color","#5D5D5D");
		$('.k-header').css("background-color","black");
		$('.k-button').css("background-color","#5D5D5D");
		$('.k-grid-${del}').css("background-color","white");
	}
	function tdelete1(id,dataItem){
//		var delname = $(this).attr('id');
		var url=ctxPath + "/chart/traceManagerDelete.do";
		var param="delname="+id;		
        if (confirm("${chk_del}")) {
			$.ajax({
				url:url,
				data:param,
				type:"post",
				dataType:"text",
				success: function(data){
			        var grid = $("#content_table").data("kendoGrid");
			        grid.dataSource.remove(dataItem);
					console.log(data);					
					/* if(data=="success"){
						alert($save_ok);
					} */
				}
			})		//ajax
        }return false
	}
	function delDeliverHistory(a){
		alert(a);
	}
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		
	};
	
	function goGraph(){
		location.href = "${ctxPath}/chart/jigGraph.do"
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<div id="container">
		<div id="content_table"></div>
	</div>

</body>
</html>
