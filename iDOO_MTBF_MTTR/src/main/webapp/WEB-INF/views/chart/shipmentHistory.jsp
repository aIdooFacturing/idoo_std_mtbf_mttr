<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
#wrapper.k-grid.k-widget.k-display-block{
	background-color:black;
	color:white;
	fo
}
$(".k-grid-header-wrap.k-auto-scrollable thead tr th").css("background-color","black")
</style> 
<script type="text/javascript">
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	var handle = 0;
	
	$(function(){
		
		$( "#sDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜   
		    	getReleaseInfo();
		    	$("#sDate").val(e);
		    }
	    })
	    $( "#eDate" ).datepicker({
		    onSelect : function(e){
		    	//e == 날짜 
		    	getReleaseInfo();
		    	$("#eDate").val(e);
		    }
	    })
	    
//		getLotNoByPrdNo();
		getPrdNo();
		createNav("inven_nav", 7);
		
		setEl();
		setDate();	
		time();
		
		setEvt();
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function setEvt(){

	};
	
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#content_table td").css({
			"color" : "##BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			//"z-index" : -1,
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	//bert 170921 16:03
	//LotNo 선택 옵션
	function getLotNoByPrdNo(){
		$.showLoading(); 
		var url = "${ctxPath}/chart/getLotNoByPrdNo.do";
		var param = "prdNo=" + $("#group").val();
		console.log("---"+param+","+$("#lotNo").val());
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.lotNo + "'>" + data.lotNo + "</option>"; 
				});
				
				$("#lotNo").html(option);
				
 				$("#lotNo").on("change", function(){
 						getReleaseInfo();
				})
				$.hideLoading(); 

			}
		});
	};
	
	//품번 선택 옵션	
	function getPrdNo(){
		var url = "${ctxPath}/common/getPrdNo.do";
		var param = "shopId=" + shopId;
		console.log("--"+param+","+$("#lotNo").val());
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='ALL'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>"; 
				});
				
				$("#group").html(option);
				
				$("#group").on("change",function(){
					getLotNoByPrdNo();
					getReleaseInfo();
				})
				
				getLotNoByPrdNo();
				getReleaseInfo();
			}
		});
	};
	
	//테이블 그릴듯
	function getReleaseInfo(){
		var tablelist=[];
		$.showLoading(); 
		
		classFlag = true;
		var url = "${ctxPath}/chart/getReleaseInfoHist.do";
		var sDate = $("#sDate").val();
		var lotNo="";
		if($("#lotNo").val()==null){
			lotNo='ALL';
			console.log(lotNo);
		}else{
			lotNo=$("#lotNo").val();	
			console.log(lotNo);
		}
		var param = "prdNo=" + $("#group").val() +
					"&lotNo=" + lotNo +
					"&sDate=" + $("#sDate").val() + 
					"&eDate=" + $("#eDate").val() + " 23:59:59";
		console.log("-"+param); 
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				cntArray = [];
				
				/* var tr = "<thead>" + 
							"<Tr style='background-color:#222222'>" +
								"<Td>" + 
									"${prd_no}" +
								"</Td>" +
								"<Td>" + 
									"${spec}" +
								"</Td>" +
								"<Td>" +
									"Lot No" +
								"</Td>" +
								"<Td>" +
									"${ship_lot_no}" +
								"</Td>" +
								"<Td>" +
									"${ship_date}" +
								"</Td>" +
								"<Td>" +
									"${ship_cnt}" +
								"</Td>" +
								"<Td>" +
									"총 금액" +
								"</Td>" +																	
								"</Tr></thead><tbody>"; */
				
								
				$(json).each(function(idx, data){
					var array = [data.cmplCnt, "stock_" + data.id];
					cntArray.push(array);
					
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						var totalprc=data.prc*data.outCnt;
						totalprc=totalprc.toString();
						totalprc=totalprc.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
						/* tr += "<tr class='contentTr " + className + " parent" + data.lotNo + "' >" +
									"<td>" + decodeURIComponent(data.prdNo).replace(/\+/gi, " ") + "</td>" + 
									"<td>" + decodeURIComponent(data.spec).replace(/\+/gi, " ") + "</td>" +
									"<td>" + data.lotNo + "</td>" + 
									"<td>" + data.outLot + "</td>" + 
									"<td>" + data.date + "</td>" + 
									"<td>" + data.outCnt + "</td>" + 
									"<td>" + totalprc + "</td>" +
							"</tr>";	 */
						var arr=new Object();
						arr.prdNo=decodeURIComponent(data.prdNo).replace(/\+/gi, " ");
						arr.spec=decodeURIComponent(data.spec).replace(/\+/gi, " ");
						arr.lotNo=data.lotNo;
						arr.outLot=data.outLot;
						arr.date=data.date;
						arr.outCnt=data.outCnt;
						arr.totalprc=totalprc;
						tablelist.push(arr);
					}
					
				});
				console.log('----delber----')
				console.log(tablelist)
				console.log(tablelist.length)
				if(tablelist.length==0){
					var arr=new Object();
					arr.prdNo=""
					arr.spec=""
					arr.lotNo=""
					arr.outLot=""
					arr.date=""
					arr.outCnt=""
					arr.totalprc=""
					tablelist.push(arr);
				}
				$("#wrapper").kendoGrid({
					dataSource:tablelist,
					height:getElSize(1675),
					columns:[{
						field:"prdNo",title:"${prd_no}"
							,attributes: {
	            				style: "text-align: center;  color:white; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(37)
	            			}
					},{
						field:"spec",title:"${spec}"
							,attributes: {
	            				style: "text-align: center;  color:white; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(37)
	            			}
					},{
						field:"lotNo",title:"Lot No"
							,attributes: {
	            				style: "text-align: center;  color:white; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(37)
	            			}
					},{
						field:"outLot",title:"${ship_lot_no}"
							,attributes: {
	            				style: "text-align: center;  color:white; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(37)
	            			}
					},{
						field:"date",title:"${ship_date}"
							,attributes: {
	            				style: "text-align: center;  color:white; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(37)
	            			}
					},{
						field:"outCnt",title:"${ship_cnt}"
							,attributes: {
	            				style: "text-align: center;  color:white; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(37)
	            			}
					},{
						field:"totalprc",title:"${total_amount}"
							,attributes: {
	            				style: "text-align: center;  color:white; font-size:" + getElSize(35)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(37)
	            			}
					}]
				});
				
				/* tr += "</tbody>"; */
				
				/* $(".alarmTable").html(tr).css({
					"font-size": getElSize(40),
				}); */
				
				$(".alarmTable input, .alarmTable button").css({
					"font-size" : getElSize(40)
				});
				
				$(".alarmTable td").css({
					"padding" : getElSize(20),
					"font-size": getElSize(40),
					"border": getElSize(5) + "px solid black"
				});
				
/* 				$("#wrapper").css({
					"height" :getElSize(1550),
					"width" : "100%",
					"overflow" : "hidden"
				}); */
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				scrolify($('.alarmTable'), getElSize(1450));
				
				$("body, html").css({
					"overflow" : "hidden"	
				})
				$("#wrapper div:last").css("overflow", "auto")

				$.hideLoading(); 
			}
		});
	};
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/inven_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td> 
								<spring:message code="prd_no"></spring:message>								
								<select id="group"></select>
								<spring:message code="lot_no"  ></spring:message>&nbsp;<select id="lotNo"></select>
								<input type="text" id="sDate" class='date'> ~ <input type="text" id="eDate" class='date'>
								<button id="search" onclick="getReleaseInfo()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="wrapper">
<!-- 									<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" id="table2" border="1" >
									</table>
 -->								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	