<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Machine List</title>

<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	/* alert(originWidth + ", " + originHeight) */
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<script type="text/javascript">
	var shopId = 1;
	$(function(){
		setElement();
		
		setInterval(time, 1000);
		getBarChartDvcId2();
	});

	function getTime(){
		var url = "${ctxPath}/chart/getTime.do";
		var time = "";
		$.ajax({
			url : url,
			dataType : "text",
			type : post,
			success : function(data){
				time = data;	
			}
		});
		
		return time;
	};
	
	function getMachineOrder(array, item) {
 		var rtn;
		for (var i = 0; i < array.length; i++) {
 			for (var j = 0; j < array[i].length; j++) {
 				if (array[i][j] == item) {
 					rtn = array[i][j+1]; 
 				};
 	        };
 	    };
		return rtn;
	};
	
	function getBarChartDvcId2(){
		var url = "${ctxPath}/chart/getAllDvcId.do";
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day; 
		
		var param = "workDate=" + today + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data : param,
			success : function(json){
				console.log(json)
				var obj = json.dvcId;
				
				$(obj).each(function(i, data){
					var operationTime = Number(data.operationTime/60/60).toFixed(1);
					var tr = "<tr onclick='goMobilePage(" + data.dvcId + ")'>" + 
									"<td style='font-size: 40px; font-weight: bold; padding: 20px;' valign='top' >" + data.name + "</td>" + 
									"<td style='font-size: 30px; font-weight: bold; padding: 20px;' valign='top' align='center'><img src=" + ctxPath + "/images/DashBoard/" + data.chartStatus +".png width='50px'></td>" + 
									"<td style='font-size: 40px; font-weight: bold; padding: 20px;' valign='top' >" + operationTime + "</td>" + 
								"</tr>";
								
					$("#mainTable").append(tr);
					
					$("#mainTable").css({
						"height" : 500
					});
				});
			},
			error : function(e1,e2,e3){
			}
		});
	};
	
	function goMobilePage(dvcId){
		window.sessionStorage.setItem("dvcId", dvcId);
		location.href="${ctxPath}/chart/mobile2.do";
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	function setElement(){
		$(".flag").remove()
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#wraper").css({
			"height" : height * 0.8
		});
		
		$("#title_main").css({
			"color" : "white",
			"font-weight" : "bolder",
			"font-size" : getElSize(200) + "px"
		});
		
		$("#title_main").css({
			"left" : width/2 - $("#title_main").width()/2
		});
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2
		});
		
		$(".status").css({
			"width" : width*0.45,
			"height" : height*0.8
		});
		
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function changePage(){
		location.href="${ctxPath}/chart/mobile2.do";
	};
</script>
<style>
body{
	background: url("../images/DashBoard/back_mobile.jpg");
	background-size : 100% 100%;
}

.title{
	top : 50px;
	position: absolute;
}

#title_left{
	left : 55px;
	width: 150px;
}

#title_right{
	right: 60px;
	color : white;
	font-size : 20px;
	top : 70px;
}

#time{
	position: absolute;
	top: 150px;
	right: 50px;	
	font-size : 30px;
	color: white;
} 
#date{
	position: absolute;
	top: 150px;
	right: 210px;	
	font-size : 30px;
	color: white;
} 

hr{
	position: absolute;
	top: 250px;
	z-index: 99;
	width: 90%;
	border: 2px solid white;
}

#mainTable{
	width: 90%;
	z-index: 99;
	background: blue; /* Standard syntax */
 	opacity : 1;
 	text-align: center;
}

#header{
	width: 90%;
	height : 200px;
	margin-top: 20px;
	z-index: -99;
	opacity : 1;
	background-color: blue;
}

#mainTable tr:last-child td:first-child {
    -moz-border-radius-bottomleft:10px;
    -webkit-border-bottom-left-radius:10px;
    border-bottom-left-radius:10px;
    
    -moz-border-radius-topleft:10px;
    -webkit-border-top-left-radius:10px;
    border-top-left-radius:10px
}

#mainTable tr:last-child td:last-child {
    -moz-border-radius-bottomright:10px;
    -webkit-border-bottom-right-radius:10px;
    border-bottom-right-radius:10px;
    
    -moz-border-radius-topright:10px;
    -webkit-border-top-right-radius:10px;
    border-top-right-radius:10px
}

#wraper{
	overflow: scroll;
}

.tr_table_fix_header
{
 position: relative;
 top: expression(this.offsetParent.scrollTop);
 z-index: 20;
}
</style>
</head>
<body>
	<Center>
		<div id="header" ></div>
	</Center>	
	
	
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right" class="title"> --%>
	<div id="title_main" class="title">가공장비 가동 현황</div> 
	<div id="title_right" class="title">(주) 부광정밀</div>
	
	<hr>

	<font id="date"></font>
	<font id="time"></font>
	
	<br>
	<br>

	<center>
		<div id="wraper">
			<table id="mainTable" border="2px" style="color: white" > 
				<tr style="font-size: 40px; font-weight: bold;" class="tr_table_fix_header">
					<td   align="center">
						장비
					</td>
					<td   align="center">
						가동상태
					</td>
					<td   align="center">
						가동시간
					</td>
				</tr>
			</table>
		</div>
	</center>
</body>
</html>