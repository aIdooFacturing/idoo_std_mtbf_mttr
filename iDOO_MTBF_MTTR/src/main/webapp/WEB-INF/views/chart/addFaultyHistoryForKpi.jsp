<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
#datePicker{
    background : rgb(121, 218, 76);
    border-radius: 5px;
    font-weight: bolder;
}

</style> 
<script type="text/javascript">
	
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	function setDate(el, val){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		el.val(year + "-" + month + "-" + day);
		if(typeof(val)!="undefined"){
			el.val(val);	
		}
	};
	
	var handle = 0;
	
	$(function(){
		createNav("kpi_nav", 6);
		
		setDate($("#sDate"));
		setDate($("#eDate"));
		getPrdNoList($("#prdNo"));
		getCheckTyList($(" #checkTy"));
		getProcessList($(" #process"));
		getPartList($(" #part"));
		getSituationTyList($(" #situationTy"));
		getSituationList($(" #situation"));
		getCauseList($(" #cause"));
		getGChTyList($(" #gchTy"));
		getGChList($(" #gch"));
		getActionList($(" #action"));
		getDevieList($(" #device"));
		
		//$(".date").change(getLeadTime);
		//$("#group").change(getLeadTime);
		
		getFaultList();
		if(addFaulty!=""){
			addRow();
			$("#insertForm").css("z-index", 999);
			showCorver();
		}
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	
	function pop_print(){
		win = window.open();
        self.focus();
        win.document.open();
        win.document.write('<'+'html'+'><'+'head'+'><'+'/'+'head'+'><'+'body'+'>');
        win.document.write(document.getElementById('print_table').innerHTML);
        win.document.write('<'+'/'+'body'+'><'+'/'+'html'+'>');
        win.document.close();
        win.print();
        win.close();
	}
	
	
	var addFaulty = "${addFaulty}";
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(200),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#delDiv").css({
			"position" : "absolute",
			"width" : getElSize(700),
			"height" :getElSize(200),
			"background-color" : "#444444",
			"color" : "white"
		});
		
		$("#delDiv").css({
			"top" : (window.innerHeight/2) - ($("#delDiv").height()/2), 
			"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
			"display" : "none",
			"border-radius" : getElSize(10),
			"padding" : getElSize(20)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(70),
			"padding" : getElSize(15),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(60),
			"margin" : getElSize(15)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white",
			"padding" : getElSize(10)
		});
		
		$(".alarmTable, .alarmTable tr, .alarmTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".alarmTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100),
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	

	function getFaultList(){
		var url = ctxPath + "/chart/getFaultList.do";
		var prdNo = $("#prdNo").val();
		var chkTy = $(" #checkTy").val();
		var process = $(" #process").val();
		var part = $(" #part").val();
		var situation = $(" #situation").val();
		var situationTy = $(" #situationTy").val();
		var dvcId = $(" #device").val();
		var cause = $(" #cause").val();
		var gchTy = $(" #gchTy").val();
		var gch = $(" #gch").val();
		var action = $(" #action").val();
		
		if(prdNo==null) prdNo = "0";
		if(chkTy==null) chkTy = "0";
		if(process==null) process = "0";
		if(part==null) part = "0";
		if(situation==null) situation = "0";
		if(situationTy==null) situationTy = "0";
		if(dvcId==null) dvcId = "0";
		if(cause==null) cause = "0";
		if(gchTy==null) gchTy = "0";
		if(gch==null) gch = "0";
		if(action==null) action = "0";
		
		
		//print table
		
		var table ="<table border='1' class='top_table' width='95%' align='center'><tr>"+
		"<th rowspan='3' colspan='2'>알람 발생 이력 <br>"+ 
		"<br>"+ $("#sDate").val() + "</th><th style='background : lightgray' colspan='3'>결제</th></tr>" +
		"<tr style='background : lightgray'><th>담당</th><th>검토</th><th>승인</th></tr>" +
		"<tr class='sign' ><th> </th><th> </th><th> </th></tr>" +
		"</table>"+
		"<table class='p_table' border='1' width='95%' align='center'>"+
		"<tr style='background : lightgray'><th>검사구분</th> <th>품번</th> <th>공정</th> <th>장비</th> <th>날짜</th> <th>신고자</th> <th>부위</th> <th>현상구분</th> <th>현상</th> <th>원인</th> <th>귀책구분</th> <th>귀책</th> <th>수량</th> <th>조치</th></tr>";
		
		var param = "prdNo=" + prdNo + 
					"&sDate=" + $(" #sDate").val() +
					"&eDate=" + $(" #eDate").val() +
					"&chkTy=" + chkTy +
					"&prdPrc=" + process +
					"&part=" + part +
					"&situTy=" + situationTy + 
					"&situ=" + situation + 
					"&dvcId=" + dvcId + 
					"&cause=" + cause + 
					"&gchTy=" + gchTy + 
					"&gch=" + gch +
					"&action=" + action + 
					"&shopId=" + shopId;
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "json",
			data : param,
			success : function(data){
				var json = data.dataList;
				$("#tbody").empty();
				var tr = "";
				var gch = "";
				$(json).each(function(idx, data){
					
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					if(decode(data.gchTy)=="업체"){
						gch = decode(getComList(data.gch));
					}else{
						gch = decode(getWorkerList(data.gch));	
					};
					
					//보여주는 테이블
					tr = "<tr class='" + className + "'>" + 
								"<td>" + decode(data.chkTy) + "</td>" +
								"<td>" + decode(data.prdNo) + "</td>" + 
								"<td>" + decode(data.prdPrc) + "</td>" + 
								"<td>" + decode(data.dvcName) + "</td>" + 
								"<td>" + data.sDate + "</td>" + 
								"<td>" + decode(data.checker) + "</td>" + 
								"<td>" + decode(data.part) + "</td>" + 
								"<td>" + decode(data.situTy) + "</td>" + 
								"<td>" + decode(data.situ) + "</td>" +
								"<td>" + decode(data.cause) + "</td>" + 
								"<td>" + decode(data.gchTy) + "</td>" + 
								"<td>" + gch + "</td>" + 
								"<td>" + data.cnt + "</td>" + 
								"<td>" + decode(data.action) + "</td>" + 
							"</tr>";
					$("#tbody").append(tr);
					
					
					//프린트 테이블
					table+=tr;
			
				});
				
				//setEl();
				$(".alarmTable, .alarmTable tr, .alarmTable td").css({
					"border": getElSize(5) + "px solid rgb(50,50,50)"
				});
				
				$(".alarmTable td").css({
					"padding" : getElSize(10),
					"height": getElSize(100),
				});
				
				$(".contentTr").css({
					"font-size" : getElSize(60)
				});
				
				$(".row1").not(".tr_table_fix_header").css({
					"background-color" : "#222222"
				});

				$(".row2").not(".tr_table_fix_header").css({
					"background-color": "#323232"
				});
				
				$("#wrapper div:last").remove();
				scrolify($('.alarmTable'), getElSize(1200));
				$("#wrapper div:last").css("overflow", "auto") 
				
				$("select, button").css("font-size", getElSize(40))
				
				//프린트
				table+="</table>"
				$("#print_table").append(table)
				
				//print css
				$(".sign").css({
					"height" : "50px"
				})
				$(".sign th").css({
					"width" : "70px"
				})
				$(".p_table tr th").css({
					"font-size" : "0.1px"	
				})
				$(".p_table tr td").css({
					"font-size" : "0.1px"	
				})
				
				$(".top_table").css({
					"font-size" : "10px"
				})
			}
		});
	};

	function getComList(cd){
		var url = ctxPath + "/chart/getComList.do"		
		var param = "cd=" + cd; 
		
		var rtn;
		$.ajax({
			url : url,
			data : param,
			async : false,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				rtn = json[0].name
			}
		});	
		
		return rtn;
	};

	function getWorkerList(cd){
		var url = ctxPath + "/common/getWorkerList.do"		
		var param = "cd=" + cd; 
		
		var rtn;
		$.ajax({
			url : url,
			data : param,
			async : false,
			dataType : "json",
			type : "post",
			success : function(data){
				console.log("test")
				var json = data.dataList;
				console.log(json)
				if(json.length==0){
					rtn = "???"
				}else{
					rtn = json[0].name
				}
			}
		});	
		
		return rtn;
	};

	var faultId;
	function chkDel(id){
		$("#delDiv").css("z-index",9);
		
		faultId = id;
	};

	var className = "";
	var classFlag = true;
	
	function noDel(){
		$("#delDiv").css("z-index",-1);
	};

	function okDel(){
		var url = ctxPath + "/chart/okDel.do";
		var param = "id=" + faultId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				noDel();
				$("#tr" + faultId).remove();
			}
		});
	};

	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};

	var shopId = 1;
	function getDevieList(el, val){
		var url = ctxPath + "/chart/getDevieList.do"
		var param = "shopId=" + shopId; 	
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getProcessList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 3; 	
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getPartList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 4;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getSituationTyList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 5;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCnt(el, id){
		var url = ctxPath + "/chart/getCnt.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				el.val(data);
			}
		});	
	};

	function getChecker(el, id){
		var url = ctxPath + "/chart/getChecker.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				el.val(decode(data));
			}
		});	
	};

	function getSituationList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 6;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCauseList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 7;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getGChTyList(el,val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 8;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getGChList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 9;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getActionList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 10;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCheckTyList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 2;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getPrdNoList(el, val){
		var url = ctxPath + "/common/getPrdNoList.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
				});
				
				el.html(options);
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});
	};
	
	
	$(function(){
		$("input#sDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(){
			}
		})
		$("input#eDate").datepicker({
			"minDate": new Date(2017, 12-1, 1),
	        "maxDate": 0,
			onSelect:function(){
			}
		})
		$(".day").css({
			"background" : "green",
			"color" : "white"
		})
	})
	
	
	var startDate,
    endDate,
    selectCurrentWeek = function () {
        window.setTimeout(function () {
            $('#sDate,#eDate').datepicker('widget').find('.ui-datepicker-current-day a').addClass('ui-state-active')
        }, 1);
    };
	
	function daily(){
		
		$( "input#sDate" ).datepicker( "destroy" );
		$( "input#eDate" ).datepicker( "destroy" );
		
		$('.ui-weekpicker').on('mousemove', 'tr', function () {
	        $(this).find('td a').removeClass('ui-state-hover');
	    });
		
		$("input#sDate").datepicker({
	        "maxDate": 0,
			onSelect:function(){
				//showLoading()
			}
		})
		$("input#eDate").datepicker({
	        "maxDate": 0,
			onSelect:function(){
				//showLoading()
			}
		}) 
		
		$("#dailyRange").css({
			"display" : "inline"
		})
		$("#weeklyRange").css({
			"display" : "none"
		})
		$("#monthlyRange").css({
			"display" : "none"
		})
		
		$(".day").css({
			"background" : "green",
			"color" : "white"
		})
		$(".week").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		}) 
		$(".month").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$("#actionBtn").css({
			"display":"inline"
		})
	}
	
	function weekly(){
		
		$( "input#sDate" ).datepicker( "destroy" );
		$( "input#eDate" ).datepicker( "destroy" );
		
		$('input#sDate').datepicker({
	        "maxDate": 0,
	        "showOtherMonths": false,
	        "selectOtherMonths": false,
	        "onSelect": function (dateText, inst) {
	            var date = $(this).datepicker('getDate'),
	                dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
	            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
	            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
	            $('#sDate').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
	            $('#eDate').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
	            selectCurrentWeek();
	        },
	        "beforeShow": function () {
	            selectCurrentWeek();
	        },
	        "beforeShowDay": function (date) {
	            var cssClass = '';
	            if (date >= startDate && date <= endDate) {
	                cssClass = 'ui-datepicker-current-day';
	            }
	            return [true, cssClass];
	        },
	        "onChangeMonthYear": function (year, month, inst) {
	            selectCurrentWeek();
	        }
	    }).datepicker('widget').addClass('ui-weekpicker');
	    
	    $('input#eDate').datepicker({
	        "maxDate": 0,
	        "showOtherMonths": false,
	        "selectOtherMonths": false,
	        "onSelect": function (dateText, inst) {
	            var date = $(this).datepicker('getDate'),
	                dateFormat = inst.settings.dateFormat || $.datepicker._defaults.dateFormat;
	            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
	            endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay() + 6);
	            $('#sDate').val($.datepicker.formatDate(dateFormat, startDate, inst.settings));
	            $('#eDate').val($.datepicker.formatDate(dateFormat, endDate, inst.settings));
	            selectCurrentWeek();
	        },
	        "beforeShow": function () {
	            selectCurrentWeek();
	        },
	        "beforeShowDay": function (date) {
	            var cssClass = '';
	            if (date >= startDate && date <= endDate) {
	                cssClass = 'ui-datepicker-current-day';
	            }
	            return [true, cssClass];
	        },
	        "onChangeMonthYear": function (year, month, inst) {
	            selectCurrentWeek();
	        }
	    }).datepicker('widget').addClass('ui-weekpicker');
	    
	    
	    $('.ui-weekpicker').on('mousemove', 'tr', function () {
	        $(this).find('td a').addClass('ui-state-hover');
	    });
	    $('.ui-weekpicker').on('mouseleave', 'tr', function () {
	    	$(this).find('td a').removeClass('ui-state-hover');
	    });
		
		$("#dailyRange").css({
			"display" : "inline"
		})
		$("#monthlyRange").css({
			"display" : "none"
		})
		$(".day").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$(".week").css({
			"background" : "green",
			"color" : "white"
		})
		$(".month").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$("#actionBtn").css({
			"display":"inline"
		})
	}
	
	function monthly(){
		$( "input#sDate" ).datepicker( "destroy" );
		$( "input#eDate" ).datepicker( "destroy" );
		
		$('input#sDate').datepicker( {
	        "maxDate": 0,
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'yy mm',
	        onClose: function(dateText, inst) { 
	            var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            if(month<10){
	            	month = '0'+month.toString()
	            }
	            
	            $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
	            $('input#eDate').val(year+'-'+month.toString()+'-'+31);
	        }
	    });
		
		$('input#eDate').datepicker( {
	        "maxDate": 0,
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'yy mm',
	        onClose: function(dateText, inst) { 
	            var month = Math.abs($("#ui-datepicker-div .ui-datepicker-month :selected").val()) + 1;
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            
	            if(month<10){
	            	month = '0'+month.toString()
	            }
	            
	            $('input#sDate').val(year+'-'+month.toString()+'-'+'01');
	            $('input#eDate').val(year+'-'+month.toString()+'-'+31);
	        }
	    });
		
		$("#dailyRange").css({
			"display" : "inline"
		})
		
		$(".day").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$(".week").css({
			"background" : "rgb(121, 218, 76)",
			"color" : "black"
		})
		$(".month").css({
			"background" : "green",
			"color" : "white"
		})
		$("#actionBtn").css({
			"display":"inline"
		})
	}
	
	</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="delDiv">
		<Center>
			<font><spring:message code="chk_del" ></spring:message></font><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/quality_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<table style="width: 100%">
						<Tr>
							<Td class='table_title'>
								<font class='label'><spring:message code="prd_no"></spring:message></font>		
								<select id="prdNo"></select>
								<div class="rangeSelector" style="display: -webkit-inline-box;">
									<button id="datePicker" class="day" onclick="daily()">일간</button>					
									<button id="datePicker" class="week" onclick="weekly()">주간</button>					
									<button id="datePicker" class="month" onclick="monthly()">월간</button>
								</div>
								<div id="dailyRange" style="display: inline">
									<font class='label'><spring:message code="search_term"></spring:message></font>
									<input type="text" id="sDate" readonly> ~ <input type="text" id="eDate" readonly>
								</div>
								<div id="actionBtn"style="display: inline; float: right; margin:auto;">
									<button id="search" onclick="getFaultList()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
									<button id="print" class="disable" onclick="pop_print()"><i class="fa fa-print" aria-hidden="true"></i>프린트</button>
								</div>
							</Td>
						</tr>	
					</table>
					<div id="wrapper" >
						<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1" id="faultList">
							<thead>
								<Tr>
									<Td class='table_title'>
										<spring:message code="check_ty"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="prd_no"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="operation"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="device"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="date_"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="reporter"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="part"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="divide_situ"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="situ"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="cause"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="gch_ty"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="gch"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="count"></spring:message>
									</Td>
									<Td class='table_title'>
										<spring:message code="action"></spring:message>
									</Td>
								</Tr>
							</thead>
							<tbody id="tbody">
							
							</tbody>
						</table>					
					</div>
				</td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class="nav_span"></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="print_table" style="display: none;"></div>
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	