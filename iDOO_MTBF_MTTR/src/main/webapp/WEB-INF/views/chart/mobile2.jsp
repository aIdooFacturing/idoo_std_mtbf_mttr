<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Machine Details</title>
<meta name="viewport" content="user-scalable=no" >
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
</script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/chart/highcharts.js"></script>
<script src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script src="${ctxPath }/js/chart/solid-gauge.js"></script>
<script src="${ctxPath }/js/chart/multicolor_series.js"></script>
<script src="${ctxPath }/js/jQ_drag.js"></script>
<script type="text/javascript">
	var shopId = 1;
	var dvcId = window.sessionStorage.getItem("dvcId");
	var spindle1;
	var arrayIndex = 0;
	var feed1;
	var operation1;
	var jeolboon1;
	var machineArray = new Array();
	var index = 0;
	var colors;
	var barChart;
	var detailBar;
	var labelPosition = new Array();
	
	var preDateVal;
	$(function(){
		drawBarChart2("barChart2");
		setStartTime();
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		//var today = year + "-" + month + "-" + day + "%20" + hour + ":" + minute + ":" + second;
		var today = year + "-" + month + "-" + day;
		$("#sdate").val(year + "-" + month + "-" + day);
		preDateVal = $("#sdate").val(); 
		$("#sdate").change(function(){
			var now = new Date(); 
			var todayAtMidn = new Date(now.getFullYear(), now.getMonth(), now.getDate());
			
			var date = new Date();
			
			var year = date.getFullYear();
			var month = date.getMonth()+1;
			var day = date.getDate();
			var hour = addZero(String(date.getHours()));
			var minute = addZero(String(date.getMinutes()));
			var second = addZero(String(date.getSeconds()));
			
			// Set specificDate to a specified date at midnight.
			
			var selectedDate = $("#sdate").val();
			var s_year = selectedDate.substr(0,4);
			var s_month = selectedDate.substr(5,2);
			var s_day = selectedDate.substr(8,2);

			var specificDate = new Date(s_month + "/" + s_day + "/" + s_year);
			
			var time = year + "-" + month + "-" + day;
			
			if(todayAtMidn.getTime()<specificDate.getTime()){
				alert("오늘 이후의 날짜는 선택할 수 없습니다.");
				var date = new Date();
				var year = date.getFullYear();
				var month = addZero(String(date.getMonth()+1));
				var day = addZero(String(date.getDate()));
				var hour = addZero(String(date.getHours()));
				var minute = addZero(String(date.getMinutes()));
				var second = addZero(String(date.getSeconds())); 
				
				today = year + "-" + month + "-" + day;
				$("#sdate").val(preDateVal);
				return;
			};
			
			if(today!=$("#sdate").val()){
				time = $("#sdate").val();
			}else{
				time = $("#sdate").val();
			}
			targetDate = time;
			
			//getStatusData(targetDate);
			
			drawBarChart("barChart", time);
			preDateVal = $("#sdate").val();
		});
		
		time();
		setElement();
		//drawBarChart("barChart");
		drawGaugeChart("gauge");
		popup("popupChart");
		
		statusChart = $("#barChart").highcharts();
		barChart = $("#barChart2").highcharts();
		jeolboon1 = $("#gauge1").highcharts();
		operation1 = $("#gauge2").highcharts();
		
		//detailBar = $("#popupChart").highcharts();
		
		//options = statusChart.options;
		//detailOptions = detailBar.options;
		
		//getStatusData(today);
		
		$("body").click(function(){
			$("#chartDataBox").fadeOut();
			$("#popup").fadeOut();
		});
		
		$("body").bind("touchstart", function(){
			$("#chartDataBox").fadeOut();
			$("#popup").fadeOut();
		});
		
		var position = $("#barChart").offset().left+9;
		
		var array = [20, position];
		labelPosition.push(array);
		
		for(var i = 1; i < 24; i++){
			position += ($("#barChart").width()-22)/24;
			var array = [labelsArray[i], position];
			
			labelPosition.push(array);
		};
		
		array = [20, $("#barChart").offset().left + $("#barChart").width()-22];
		labelPosition.push(array);
		
		$("#pointer").draggable({
			axis:"x",
			containment : "#pointerContainer",
			scroll : false,
			drag: function(){
				/* var offset = $(this).offset();
	            var x = offset.left;
	            var y = offset.top;
	            
	            var pointer = x + ($("#pointer").width()/2);
	        	for(var i = 0; i < labelPosition.length; i++){
	            	if(pointer >= (labelPosition[i][1]-10) && pointer <= (labelPosition[i][1]+10)){
	            		Highcharts.setOptions({
	            	        yAxis:{
	            	        	labels:{
	            	        		formatter: function () {
	            	        			var color;
	            	        			if(labelsArray[this.value]==labelPosition[i][0]){
	            	        				color = '<span style="fill: red; font-size:20px;font-weight:bold">';
	            	                   	}else{
	            	                    	color = '<span style="fill: white;">';
	            	                   	};
	            	                   	
	            	                	return  color + labelsArray[this.value] + '</span>';
	            		        	},
	            	         	}   
	            	      	}
	            	    });

	            		$('#barChart').highcharts(options);							
	            	};
	            }; */
	        },
	        stop: function() {
	        	var offset = $(this).offset();
	            var x = offset.left;
	            var y = offset.top;
//	            console.log('x: ' + x);
	            
	            var pointer = x + ($("#pointer").width()/2);
	        	for(var i = 0; i < labelPosition.length; i++){
	            	if(pointer >= (labelPosition[i][1]-10) && pointer <= (labelPosition[i][1]+10)){
	            		/* $("#ruler").css({
	            			width : $("#barChart").width()/24,
	            			top : $("#barChart").offset().top,
	            			left : labelPosition[i][1]
	            		}); */
	            		
	            		$(".box").css({
	            			"z-index" : 99999999,
	            			top : $("#barChart").offset().top,
	            			left : labelPosition[i][1]
	            		});
	            		
						/* $("#arrowEnd").css({
							top : $("#barChart").offset().top,
							left : labelPosition[i+1][1] - ($(".arrowDown").width()/2)
	            		}); */
	            		
		            	getDetailStatus(labelPosition[i][0]);
	            	};
	            };
	        }
		});
		
		//getDetailStatus(20);
		
		
		/* $("#arrowLeft").css({
			top : $("#pointer").offset().top + ($("#pointer").height()/2) - $("#arrowLeft").height()/2,
			left : $("#pointer").offset().left - $("#arrowLeft").width()
		});
		
		$("#arrowRight").css({
			top : $("#pointer").offset().top + ($("#pointer").height()/2) - $("#arrowRight").height()/2,
			left : ($("#pointer").offset().left + $("#pointer").width())
		});
		
		arrowAnim($("#arrowLeft"), $("#arrowLeft").offset().left, "start", "left");
		arrowAnim($("#arrowRight"), ($("#pointer").offset().left + $("#pointer").width()), "start","right"); */
		
		$(".box").css({
			height : $("#barChart").height()*0.75,
			width :  $("#barChart").width()/24
		});
	});
	
	function arrowAnim(el, dist, ty, direc){
		if(ty=="start"){
			ty = "end";
			if(direc=="left"){
				dist -= 20;	
			}else{
				dist += 20;
			};
		}else{
			ty = "start";
			if(direc=="left"){
				dist += 20;	
			}else{
				dist -= 20;
			};
		};
		
		el.animate({
			left : dist
		},500, function(){
			arrowAnim(el, dist, ty, direc)
		});		
	};
	
	function removeSpace(str){
		return str = str.replace(/ /gi, "");
	};
	
	function parsingModal(obj){
		var json = $.parseJSON(obj);
	
		var auxCode = json.AUX_CODE;
		var gModal = json.G_MODAL;
		
		var auxTR = "<tr>";
		var auxTD;
		var gModalTR = "<tr>";
		var gModalTD;
		
		$.each(auxCode, function(key, data){
			$.each(auxCode[key], function (key, data){
				auxTD += "<td>" + key + " : " + data + "</td>"; 
			});
		}); 
		
		auxTR += auxTD + "</tr>";
		
		$.each(gModal, function(key, data){
			$.each(gModal[key], function (key, data){
				gModalTD += "<td>" + key + " : " + data + "</td>";
			});
		}); 
		
		gModalTR += gModalTD + "</tr>";
		
		$("#modalTbl").html(auxTR);
		$("#modalTbl").html(gModalTR);
	};
		
	function slctChartColor(status){
		var color;
		if(status.toLowerCase()=="in-cycle"){
			color = colors[0];
		}else if(status.toLowerCase()=="wait"){
			color = colors[1];
		}else if(status.toLowerCase()=="alarm"){
			color = colors[2];
		}else if(status.toLowerCase()=="no-connection"){
			color = colors[3];
		};
		
		return color;
	};
	
	var detailOptions;
	var detailBarArray = new Array();
	function addDetailBar(bar, color){
		var _bar = new Array();
		_bar.push(bar);
		_bar.push(color);
		
		detailBarArray.push(_bar);
	};
	
	function reDrawDetailBar(){
		detailOptions.series = [];
		detailOptions.title = null;
		for (var i = 0; i < detailBarArray.length; i++){
			detailOptions.series.push({
		        data: [{
		        		y : detailBarArray[i][0],
		        	}],
		        color : detailBarArray[i][1]
		    });	
		};
		
		detailBar = new Highcharts.Chart(detailOptions);
	};
	
	var barSize = 1/6;
	var detailBarMap = new JqMap();
	
	function getDetailStatus(hour){
		if(detailBarArray.length!=0){
			detailBarArray = new Array();
			for(var i = 0; i < detailBarArray.length; i++){
				detailBar.series[0].remove(true);
				console.log("remove series");
			};			
		};
		
		var url = "${ctxPath}/chart/getDetailStatus.do";
		var data = "dvcId=" + dvcId +
					"&startDateTime=" + hour;
		
		$.ajax({
			url : url,
			data : data,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.chartStatus;
				
				$(json).each(function(i, data){
					var startTime = removeHypen(data.startDateTime, "690").substr(9,8);
					var endTime = removeHypen(data.endDateTime, "691").substr(9,8);
					var color = slctChartColor(removeSpace(data.chartStatus));
					
					var startH = Number(startTime.substr(0,2));
					var startM = String(startTime.substr(3,2));
					var startS = Number(startTime.substr(6,2));

					var endH = Number(endTime.substr(0,2));
					var endM = String(endTime.substr(3,2)); 
					var endS = Number(endTime.substr(6,2));

					var start = (startH*60*60) + (startM*60) + (startS);
					var end = (endH*60*60) + (endM*60) + (endS);
					
					if(i==0)start=hour*60*60;
					var bar = (end - start)/10*barSize;
					
					if(bar<0.16)bar=0.16;
					detailBarMap.put("chart", detailBar);
					
					console.log(startTime, endTime, removeSpace(data.chartStatus));
					addDetailBar(bar, color)
				});
				
				reDrawDetailBar();
				
				$("#popup").fadeIn();
				$("#hour").html(hour + ":00 ~ " + hour + ":59");	
			}
		});
	};
	
	function popup(id){
		$('#' + id).highcharts({
			chart : {
				
				type : 'bar',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height: 200*0.7
			},
			credits : false,
			//exporting: false,
			title : false,
			xAxis : {
				categories : [""],
				labels : {
					style : {
						fontSize : '15px',
						fontWeight:"bold",
						color : "white"
					}
				}
			},
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			yAxis : {
				min : 0,
				max : 59,
				tickInterval:1,
				reversedStacks: false,
				title : {
					text : false
				},
				labels: {
	                formatter: function () {
	                	var value = labelsArray_hour[this.value]
	                    return value;
	                },
	                style :{
	                	color : "white",
	                	fontSize : "15px"
	                },
	            },
			},
			legend : {
				enabled : false
			},
			plotOptions : {
				series : {
					  dataGrouping : {
		                    forced : true,
		                    units : [['minute', [5]]]
		                },
					stacking : 'normal',
					pointWidth:120, 
					borderWidth: 0,
					animation: false,
					cursor : 'pointer',
					point : {
						events: {
							click: function (e) {
		                   	}
		              	}
					}
				},
			},
			series : []
		});
	};
	
	var noSeries = false;
	function getTimeData(options, time){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth() + 1));
		var day = addZero(String(date.getDate()));
		var hour = date.getHours();
		var minute = addZero(String(date.getMinutes())).substr(0,1);
		
		
		if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
			day = addZero(String(new Date().getDate()+1));
		};
		
		var today = year + "-" + month + "-" + day;
		if(typeof(time)!="undefined") today = time;

		var url = "${ctxPath}/chart/getTimeData.do";
		var param = "dvcId=" + dvcId + 
					"&workDate=" + today;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				$("#loader").css("display","none")
				$("#mainTable").css("opacity",1)
				//$("#arrowLeft, #arrowRight").css("opacity", "0.7");
				setTimeout(function(){
					$(".arrowDirection").css("opacity", 0);
				}, 3000);
				
				/* if(data==null || data==""){
					noSeries = true;
					//getCurrentDvcStatus();
					return;
				};
				
				var json = $.parseJSON(data);
				
				options.series = [];
				options.title = null;
				
				var flag = true;
				$(json).each(function (i, data){
					var bar = data.data[0].y;
					var startTime = data.data[0].startTime;
					var endTime = data.data[0].endTime;
					var color = eval(data.color);
					
					if(flag){
						options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : color
					        	}],
					    });	
					}else{
						options.series[0].data.push({
							y : Number(200),
							segmentColor : color
						});
					};
					flag = false;
				});   */
				
				
				var json = data.statusList;
				
				var color = "";
				
				var status = json[0].status;
				if(status=="IN-CYCLE"){
					color = "green"
				}else if(status=="WAIT"){
					color = "yellow";
				}else if(status=="ALARM"){
					color = "red";
				}else if(status=="NO-CONNECTION"){
					color = "gray";
				};
				
				var blank;
				var f_Hour = json[0].startDateTime.substr(11,2);
				var f_Minute = json[0].startDateTime.substr(14,2);
				
				var startN = 0;
				if(f_Hour==startHour && f_Minute==(startMinute*10)){
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : color
						} ],
					});
				}else{
					if(f_Hour>=20){
						startN = (((f_Hour*60) + (f_Minute)) - ((startHour*60) + (startMinute*10)))/2; 
					}else{
						startN = ((24*60) - ((startHour*60) + (startMinute*10)))/2;
						startN += (f_Hour*60/2) + (f_Minute/2);
					};
					
					options.series.push({
						data : [ {
							y : Number(20),
							segmentColor : "gray"
						} ],
					});
						
					for(var i = 0; i < startN-1; i++){
						options.series[0].data.push({
							y : Number(20),
							segmentColor : "gray"
						});
					};
				};
				
				
				$(json).each(function(idx, data){
					if(data.status=="IN-CYCLE"){
						color = "green"
					}else if(data.status=="WAIT"){
						color = "yellow";
					}else if(data.status=="ALARM"){
						color = "red";
					}else if(data.status=="NO-CONNECTION"){
						color = "gray";
					};
					options.series[0].data.push({
						y : Number(20),
						segmentColor : color
					});
				});
				
				for(var i = 0; i < 719-(json.length+startN); i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : "rgba(0,0,0,0)"
					});
				};
				
				statusChart = new Highcharts.Chart(options);
				
				
				/* setInterval(function (){
					var minute = String(new Date().getMinutes());
					if(minute.length!=1){
						minute = minute.substr(1,2);
					};
					
					if(minute==2 && initFlag){
						getStatusData(today);
						initFlag = false;
						currentFlag = true;
						console.log("init")
					}else if(minute!=2){
						initFlag = true;
					};
				}, 1000 * 10); */
			}
		});
	};
	
	var initFlag = true;
	function getCurrentDvcStatus(){
		var workDate = $("#sdate").val();
		var url = "${ctxPath}/chart/getCurrentDvcData.do";
		var param = "dvcId=" + dvcId + "&workDate=" + workDate + "&shopId=" +shopId;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var chartStatus = data.chartStatus;
				var type = data.type;
				var progName = data.lastProgramName;
				var progHeader = data.lastProgramHeader;
				var name = data.name;
				var inCycleTime = data.inCycleTime;
				var waitTime = data.waitTime;
				var alarmTime = data.alarmTime;
				var noConTime = data.noConnectionTime;
				var spdLoad = data.spdLoad;
				var feedOverride = data.feedOverride;
				var opRatio = data.opRatio;
				var cuttingRatio = data.cuttingRatio;
				var alarm = data.alarm;
				
				$("#alarmCode0").html("");
				$("#alarmMsg0").html("");
				$("#alarmCode1").html("");
				$("#alarmMsg1").html("");
				
				if(alarm!=null && alarm!="" && typeof(alarm)!="undefined") parsingAlarm(alarm);
				
				
				if(String(type).indexOf("IO")!=-1){
					$("#header").html("I/O Board");
					$("#gauge1").css("opacity",0)
				};
				
				$("#dvcName").html(name);
				$("#progName").html(progName);
				$("#progHeader").html(progHeader);
				$("#statusLamp").attr("src", ctxPath + "/images/DashBoard/" + chartStatus +".png");
				
				$("#feedOverride").html(Number(feedOverride));
		      	$("#spdLoad").html(Number(spdLoad));
		      	
		      	//가동시간 차트
		      	barChart.series[0].data[0].update(Number(Number(inCycleTime/60/60).toFixed(1)));
				barChart.series[1].data[0].update(Number(Number(waitTime/60/60).toFixed(1)));
				barChart.series[2].data[0].update(Number(Number(alarmTime/60/60).toFixed(1)));
				barChart.series[3].data[0].update(Number(Number(noConTime/60/60).toFixed(1)));
			
				var operationChart = operation1.series[0].points[0];
		      	operationChart.update(Number(Number(opRatio).toFixed(1)));
		      	
		      	var cuttingChart = jeolboon1.series[0].points[0];
		      	cuttingChart.update(Number(Number(cuttingRatio).toFixed(1)));
		      	
		      /* 	if(currentFlag){
		      		if(noSeries){
		      			options.series = [];
						options.title = null;
						
		      			options.series.push({
					        data: [{
					        		y : Number(200),
					        		segmentColor : slctChartColor(chartStatus) 
					        	}],
					    });
		      		}else{
		      			options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : slctChartColor(chartStatus)
			  			});		      			
		      		};
				
		      	 	var now = options.series[0].data.length;
					var blank = 144 - now;
					
					for(var i = 0; i <= blank; i++){
						options.series[0].data.push({
			        		y : Number(200),
			        		segmentColor : "rgba(0,0,0,0.0)"
			  			});	
					};
					
		      	  	statusChart = new Highcharts.Chart(options);
					currentFlag = false;
		      	}; */
		      	
		      	setTimeout(getCurrentDvcStatus, 3000);
			}
		});
		
		
	};
	
	var currentFlag = true;
	function parsingAlarm(alarm){
		var json = $.parseJSON(alarm);
		
		$(json).each(function (i,data){
			$("#alarmMsg" + i).html(data.alarmCode + " - " + data.alarmMsg);
		});
	};
	
	var options;
	var barArray = new Array();
	
	function removeHypen(str, line){
		var rtn;
		try{
			rtn = str.replace(/-/gi,"");	
		}catch(err){
			console.log(err.message + ":" + line + "line");
		};
		
		return rtn;
	};
	
	function isItemInArray(array, item) {
		for (var i = 0; i < array.length; i++) {
			for (var j = 0; j < array[i].length; j++) {
				if (array[i][j] == item) {
					return true;   // Found it
		        };
	        };
	    };
		return false;   // Not found
	};
	
	var block = 1/6;
	
	function calcTime(startTime, endTime){
		var startH = Number(startTime.substr(0,2));
		var startM = Number(startTime.substr(3,2));
		var startS = Number(startTime.substr(6,2));

		var endH = Number(endTime.substr(0,2));
		var endM = Number(endTime.substr(3,2)); 
		var endS = Number(endTime.substr(6,2));
		
		return ((endH*60*60) + (endM*60) + endS) - ((startH*60*60) + (startM*60) + startS);  
	};
	
	function drawGaugeChart(id){
		var gaugeOptions = {
				chart: {
					type: 'solidgauge',
			 		backgroundColor : 'rgba(255, 255, 255, 0)',
				},
				credits : false,
			   	//exporting : false,
			  	title: {
			  		text :""
			  	},
			  	events: {
			  	    redraw: function(){
			  	        console.log(this.xAxis);
			  	        console.log(this.yAxis);
			  	    }
			  	},
				pane: {
					center: ['50%', '80%'],
	            	size: '100%',
	            	startAngle: -90,
	            	endAngle: 90,
	            	background: {
	                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
	                innerRadius: '60%',
	                outerRadius: '100%',
	                shape: 'arc'
	            	}
	        	},
			  	tooltip: {
			  		enabled: false
			   	},
	        	yAxis: {
	        		stops: [
	        		        [1, '#55BF3B'], // green
                			
	            	],
	            	lineWidth: 0,
	            	minorTickInterval: null,
	            	tickPixelInterval: 400,
	            	tickWidth: 0,
	            	title: {
	                y: -70
	            	},
	            	labels: {
	                y: 16,
	                enabled:false
	            	}
	        	},
	        	plotOptions: {
	            	solidgauge: {
	                dataLabels: {
	                	y: 5,
	                   borderWidth: 0,
	                   	useHTML: true
	                	}
	            	}
	        	}
	    	};

			
			$('#gauge1').highcharts(Highcharts.merge(gaugeOptions, {
				yAxis: {
					min: 0,
					max: 100,
	            	title: {
	                text: '절분율',
	               	y: -100,
	               	style:{
	            	   		color : "white",
	            	   		fontSize : '35px',
							fontWeight:"bold"
	               		}
	    			},
	        	},
	        	credits: {
	         		enabled: false
	        	},
	        	series: [{
	            	data: [0],
	            	dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:50px;color:white">{y}</span>' +
	                       '<span style="font-size:30px;color:white">%</span></div>'
	            	},
	            tooltip: {
	                valueSuffix: ' %'
	            	}
	        	}]
	    	}));


			$('#gauge2').highcharts(Highcharts.merge(gaugeOptions, {
				yAxis: {
					min: 0,
		         	max: 100,
		         	title: {
		         		text: '가동율',
	               	y: -100,
	               	style:{
		            	   	color : "white",
		            	   	fontSize : '35px',
							fontWeight:"bold"
		               	}
		          	},
		      	},
		     	series: [{
		     		data: [0],
	            	dataLabels: {
	                format: '<div style="text-align:center"><span style="font-size:50px;color:white">{y}</span>' +
	                       '<span style="font-size:30px;color:white">%</span></div>'
	            	},
	            	tooltip: {
	            		valueSuffix: '%'
	            	}
	        	}]
	
		    }));
	};
	
	function drawBarChart2(id){
		var perShapeGradient = {
	            x1: 0,
	            y1: 0,
	            x2: 1,
	            y2: 0
	        };
	        colors = Highcharts.getOptions().colors;
	        colors = [{
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#148F01'],
	                [1, '#1ABB02']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#C7C402'],
	                [1, '#F5F104']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#ff0000'],
	                [1, '#FF2626']
	                ]
	           },{
		            linearGradient: perShapeGradient,
		            stops: [
		                [0, '#8c9089'],
		                [1, '#A9ADA6']
		                ]}
	        ]
	        
		$('#' + id).highcharts({
			chart : {
				type : 'bar',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height: 200
			},
			credits : false,
			//exporting: false,
			title : false,
			xAxis : {
				categories : [""],
				labels : {
					style : {
						fontSize : '35px',
						fontWeight:"bold",
						color : "white"
					}
				}
			},
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				}
			},
			yAxis : {
				min : 0,
				max : 24,
				tickInterval:1,
				reversedStacks: false,
				title : {
					text : false
				},
				labels : {
					enabled: false
				}
			},
			legend : {
				enabled : false
			},
			plotOptions : {
				 bar: {
	                    stacking: 'normal',
	                    dataLabels: {
	                    	style : {
	                    		fontSize : "40px",
	                    	},
	                    	enabled: true,	
	                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
	                        formatter: function () {
	                        	var val = this.y
	                        	if(val==0){
	                        		val = "";
	                        	};
	                        	return val;   
	                        }
	                    }
	          	},
				series : {
					stacking : 'normal',
					pointWidth:120, 
					borderWidth: 0
				}
			},
			series : [{
				data : [0],
				color :colors[0]
			},{
				data : [0],
				color :colors[1]
			},{
				data : [0],
				color :colors[2]
			},{
				data : [0],
				color :colors[3]
			}]
		});
	};
	
	var labelsArray = [20,21,22,23,24,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
	var labelsArray_hour = [0,1,2,3,4,5,6,7,8,9,
	                        10,11,12,13,14,15,16,17,18,19,
	                        20,21,22,23,24,25,26,27,28,29,
	                        30,31,32,33,34,35,36,37,38,39,
	                        40,41,42,43,44,45,46,47,48,49,
	                        50,51,52,53,54,55,56,57,58,59
	                        ];
	
	var timeArray = new Array();
	
	function setStartTime(){
		var url = ctxPath + "/chart/getStartTime.do";
		var param = "shopId=1";
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				startHour = data.split("-")[0]
				startMinute = data.split("-")[1]

				drawBarChart("barChart");
				getCurrentDvcStatus();
			}, error : function(e1,e2,e3){
				console.log(e1,e2,e3)
			}
		});
	};
	
	var timeLabel = [];
	
	var startHour;
	var startMinute;
	var startTimeLabel = new Array();
	function drawBarChart(id, date){
		var m0 = "",
		m02 = "",
		m04 = "",
		m06 = "",
		m08 = "",
		
		m1 = "";
		m12 = "",
		m14 = "",
		m16 = "",
		m18 = "",
		
		m2 = "";
		m22 = "",
		m24 = "",
		m26 = "",
		m28 = "",
		
		m3 = "";
		m32 = "",
		m34 = "",
		m36 = "",
		m38 = "",
		
		m4 = "";
		m42 = "",
		m44 = "",
		m46 = "",
		m48 = "",
		
		m5 = "";
		m52 = "",
		m54 = "",
		m56 = "",
		m58 = "";
	
	var n = Number(startHour);
	if(startMinute!=0) n+=1;
	
	for(var i = 0, j = n ; i < 24; i++, j++){
		eval("m" + startMinute + "=" + j);
		
		startTimeLabel.push(m0);
		startTimeLabel.push(m02);
		startTimeLabel.push(m04);
		startTimeLabel.push(m06);
		startTimeLabel.push(m08);
		
		startTimeLabel.push(m1);
		startTimeLabel.push(m12);
		startTimeLabel.push(m14);
		startTimeLabel.push(m16);
		startTimeLabel.push(m18);
		
		startTimeLabel.push(m2);
		startTimeLabel.push(m22);
		startTimeLabel.push(m24);
		startTimeLabel.push(m26);
		startTimeLabel.push(m28);
		
		startTimeLabel.push(m3);
		startTimeLabel.push(m32);
		startTimeLabel.push(m34);
		startTimeLabel.push(m36);
		startTimeLabel.push(m38);
		
		startTimeLabel.push(m4);
		startTimeLabel.push(m42);
		startTimeLabel.push(m44);
		startTimeLabel.push(m46);
		startTimeLabel.push(m48);
		
		startTimeLabel.push(m5);
		startTimeLabel.push(m52);
		startTimeLabel.push(m54);
		startTimeLabel.push(m56);
		startTimeLabel.push(m58);
		
		if(j==24){ j = 0}
	};
	
		var perShapeGradient = {
	            x1: 0,
	            y1: 0,
	            x2: 1,
	            y2: 0
	        };
	        colors = Highcharts.getOptions().colors;
	        colors = [{
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#148F01'],
	                [1, '#1ABB02']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#C7C402'],
	                [1, '#F5F104']
	                ]
	            }, {
	            linearGradient: perShapeGradient,
	            stops: [
	                [0, '#ff0000'],
	                [1, '#FF2626']
	                ]
	           },{
		            linearGradient: perShapeGradient,
		            stops: [
		                [0, '#8c9089'],
		                [1, '#A9ADA6']
		                ]}
	        ]
	         
	//	$('#' + id).highcharts({
		options = {
			chart : {
				
				type : 'coloredarea',
				backgroundColor : 'rgba(255, 255, 255, 0)',
				height: 200,
				marginTop: -100
			},
			credits : false,
			exporting: false,
			title : false,
			yAxis : {
				labels : {
					enabled : false,
				},
				title : {
					text : false
				},
			},
			xAxis:{
		           categories:startTimeLabel,
		           labels : {
						step: 1,
						formatter : function() {
							var val = this.value

							return val;
						},
						style : {
							color : "white",
							fontSize : 20,
							fontWeight : "bold"
						},
					}
		        },
		       
			tooltip : {
				headerFormat : "",
				style : {
					fontSize : '10px',
				},
				enabled : false
			}, 
			plotOptions: {
			    line: {
			        marker: {
			            enabled: false
			        }
			    }
			},
			legend : {
				enabled : false
			},
			series: []
		}
		
		//});
	        
	      /*   Highcharts.setOptions({
    	        yAxis:{
    	        	labels:{
    	        		formatter: function () {
    	                	var value = labelsArray[this.value];
    	                    return value;
    	                }
    	         	}   
    	      	}
    	    }); */

	   	$('#barChart').highcharts(options);
    	    

	    	var status = $("#barChart").highcharts();
	    	var options = status.options;

	    	options.series = [];
	    	options.title = null;
	    	options.exporting = false;
	    	
	    	getTimeData(options, date);
	    	
	    	
	    	setInterval(function(){
	    		var cMinute = String(addZero(new Date().getMinutes())).substr(0,1);
	    		if(targetMinute!=cMinute){
	    			targetMinute = cMinute;
	    			drawBarChart(id);
	    		};
	    	},5000)
	};
	
	var targetMinute = String(addZero(new Date().getMinutes())).substr(0,1);
	
	function showDetailEvent(startTime, endTime, x, y){
		//console.log(startTime, endTime)
		$("#chartDataBox").fadeOut(1);
		var url = "${ctxPath}/chart/showDetailEvent.do";
		var param = "startTime=" + replaceAt(startTime,15,"0") + 
					"&endTime=" + replaceAt(endTime,15,"9") + 
					"&dvcId=" + dvcId;

		$.ajax({
			url : url,
			data :param,
			type : "post",
			dataType : "json",
			success :function(data){
				var json = data.chartData;
				
				var str = "";
				$(json).each(function(i, data){
					str += 
						"<tr>" + 
							"<td>" + data.startDateTime.substr(11,5) + "</td><td> ~ </td><td>" + data.endDateTime.substr(11,5) + "</td><td><img src='${ctxPath}/images/DashBoard/" + data.chartStatus +".png' width='40px'></td>" + 
						"</tr>"
				});
				
				$("#chartDataBox").css({
					"left" : x,
					"top" : y,
					//"display" : "inline"
				}); 
				
				$("#chartDataBox").fadeIn();
				
				$("#chartDataBox").html(str);
				
				//alert(str);
			}
		});
	};
	
	function replaceAt(str, n, t) {
	    return str.substring(0, n) + t + str.substring(n + 1);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
		
		setTimeout(time, 1000);
	};
	
	function setElement(){
		$(".flag").remove()
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#popup").css({
			"top" : $("#barChart").offset().top,
			"left" : width/2 - ($("#popup").width()/2),
			"z-index" : 99999
		});
		
		$("#loader").css({
			"top" : height/2 - ($("#loader").height()/2),
			"left" : width/2 - ($("#loader").width()/2)
		});
		
		$("#title_main").css({
			"left" : width/2 - $("#title_main").width()/2
		});
		
		$("hr").css({
			"left" : width/2 - $("hr").width()/2
		});
		
		$("#mainTable").css({
			"left" : width/2 - $("#mainTable").width()/2
		});
		
		$("#sdate").css({
			"font-size" : 20,
			//"position" : "absolute",
			//"top" : $("#time").offset().top + $("#time").height(),
			//"right" : 50
		});
		
		$("#back").css({
			"width" : getElSize(200),
			"margin-left" : getElSize(30),
		}).click(function(){
			history.back();
		})
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function show(){
		alert($("#barChart").offset().left, $("#barChart .highcharts-tracker:eq(0)").offset().left)
		alert($("#barChart .highcharts-tracker:eq(0)").offset().left)
	}
</script>
<style>
body {
	background: url("../images/DashBoard/Background.png");
	background-size: 100% 100%;
}

.title {
	top: 50px;
	position: absolute;
}

#title_main {
	width: 400px;
	z-index: 999;
}

#title_left {
	left: 50px;
	width: 150px;
}

#title_right {
	barChart right: 50px;
	width: 150px;
}

#time {
	position: absolute;
	top: 50px;
	right: 50px;
	font-size: 30px;
	color: white;
}

#date {
	position: absolute;
	top: 50px;
	right: 220px;
	font-size: 30px;
	color: white;
}

hr {
	z-index: 99;
	width: 100%;
	border: 2px solid white;
}

#mainTable {
	width: 90%;
	position: absolute;
	top: 50px;
	z-index: 99; 
	opacity : 0;
}
.td{
	font-weight: bold; 
	color: white; 
	font-size: 40px;
	text-align: left;
}
#loader{
	position: absolute;
	z-index: 99999;
	-webkit-filter: brightness(10);
}
#chartDataBox{
	background-color: white;
	border-radius : 10px;
	position: absolute;
	font-size: 40px;
	font-weight: bolder;
	padding: 10px;
	z-index: 9999;
	display: none;
}

#popup{
	width: 100%;
}

#pointer{
	-webkit-transform: rotate(180deg);
	width : 80px;
}

#arrowLeft{
	-webkit-transform: rotate(180deg);
	position: absolute;
	z-index: 99999;
}
#arrowRight{
	position: absolute;
	z-index: 99999;
}
.arrowDirection{
	opacity : 0;
	height: 60px;	
}

#ruler{
	position: absolute;
	z-index: 99999;
	left: -1000px;
}

.box{
	position: absolute;
	background-color: skyblue;
	opacity : 0.5;	
	width: 100px;
	height: 50px;
	left : -500px;
	border: 5px solid black;
	
}
#alarmMsg0{
	display: block;
}
</style>
<script type="text/javascript"> 
	
</script>

</head>


<body>
	<%-- <img src="${ctxPath }/images/DashBoard/arrowDirection.png" id="arrowLeft" class="arrowDirection">
	<img src="${ctxPath }/images/DashBoard/arrowDirection.png" id="arrowRight" class="arrowDirection"> --%>
	<%-- <img src="${ctxPath }/images/DashBoard/arrow_down.png" class="arrowDown" id="arrowStart">
	<img src="${ctxPath }/images/DashBoard/arrow_down.png" class="arrowDown" id="arrowEnd"> --%>
	<img src="${ctxPath }/images/DashBoard/ruler.png" id="ruler">
	<div id="leftBox" class="box"> </div>
	<div id="rightBox" class="box"> </div>
	<!-- <div id="popup" style="background-color:white; position: absolute; display: none;border-radius : 20px;">
		<center><div id="hour" style="font-size: 40px; font-weight: bold;"></div></center>
		<div id='popupChart' style="width: 100%" ></div>
	</div> -->
	<div id="chartDataBox">
	</div>
	<img alt="" src="${ctxPath }/images/DashBoard/loader.png" id="loader">

	<font id="date"></font>
	<font id="time"></font>
	
	<br>
	<br>
	<br>
	
	<table id="mainTable">
		<tr style="color: black; font-size: 30px; font-weight: bold;">
			<td style="font-weight: bold; color: white; font-size: 40">
				<font id="dvcName" ></font><img src="" width="40px;" id="statusLamp"><br>
				<font id="header"></font>
			</td>
		</tr>
		<!-- <tr>
			<td align="right" valign="bottom">
				
			</td>
		</tr> -->
		<tr>
			<td align="left" >
				<span style="font-weight: bold; color: white; font-size: 40px" id="progName"></span> 
				<span style="font-weight: bold; color: white; font-size: 40px" id="progHeader"></span>
				<input type="date" id="sdate" style="float: right;">
				<hr> 
			</td>
		</tr>
		<tr class="tr">
			<td colspan="2" id="pointerContainer">
				<!-- <div id='popupChart' style="width: 70%" > -->
				<div id="barChart" style="width: 100%" class="barChart"></div>
				<%-- <img src="${ctxPath }/images/DashBoard/pointer.png" id="pointer" oncontextmenu="return false" style="-webkit-touch-callout:none" > --%>
			</td>
		</tr>
		<tr class="tr">
			<td class="td" >
				
				<div id="gauge" style="width: 100%">
					<div id="gauge2" style="float: left; width: 350px;"></div>
					<div id="gauge1" style="float: right; width: 350px;"></div>
				</div>
			</td>
		</tr>
		<tr>
			<td class="td" width="0px"><span id="spdLoad" style="position: absolute; left: 200px;"></span> S.Load</td>
		</tr>
		<tr>
			<td class="td" width="300px"><span id="feedOverride" style="position: absolute; left: 200px;"></span>F.Oride</td>
		</tr>
		<tr>
			<td class="td" style="color: red">Alarm</td>
		</tr>
		<tr>
			<td class="td"> <span id="alarmMsg0" style="font-size: 20px;"></span></td>
		</tr>
		<tr>
			<td class="td" > <span id="alarmMsg1" style="font-size: 20px;"></span></td>
		</tr>
		<tr>
			<td>
				<br>
				<span class="td">가동시간 <font style="font-size: 25px;">(단위 : hr)</font>	</span><br>
				<div id="barChart2"></div>
			</td>
		</tr>
		<tr>
			<table id="modalTbl" width="100%">
			
			</table>
		</tr>		
	</table>
</body>
</html>