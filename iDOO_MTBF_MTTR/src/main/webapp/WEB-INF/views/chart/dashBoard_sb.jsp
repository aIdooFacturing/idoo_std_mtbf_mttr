<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highchart.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/svg_controller.js"></script>

<script type="text/javascript" src="${ctxPath }/js/chart/SVG_.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/SVG_draggable.js"></script>
<style>

@font-face {font-family:MalgunGothic; src:url(../fonts/malgun.ttf);}

*{
	font-family:'MalgunGothic';
}
#chart{
	overflow: hidden;
}

#container{
	background: url("../images/DashBoard/Background.png");
	background-size : 100% 100%;
	background-color: white;
}

body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
  	font-family:'Helvetica';
}

.title{
	top : 50px;
	position: absolute;
	z-index: 99;
}

#title_main{
	left: 1300px;
	width: 1000px;
}

#title_left{
	left : 50px; 
	width: 300px;
}

#pieChart1{
	position: absolute;
	z-index: 99;
}

#pieChart2{
	position: absolute;
	z-index: 99;
}

#tableDiv{
	left: 20px;
	width: 600px; 
	position: absolute; 
	z-index: 999;
	top: 450px;
}

#table{
	border: 1px solid white;
	border-collapse: collapse;
	z-index: 999;
	background-color: black;
}

.tr{
	font-size: 30px;
}

.td{
	padding: 10px;
	border: 1px solid white; 
}


#time{
	position: absolute;
	top: 170px;
	font-size : 30px;
	color: white;
} 
#logo{
	position: absolute;
	top: 170px;
	right: 60px;	
}
#date{
	position: absolute;
	top: 170px;
	right: 250px;	
	font-size : 30px;
	color: white;
} 
.unSelected_menu{
	background-color :gray;
	color : white
}

.selected_menu{
	background-color :rgb(33,128,250);
	color : white
}

.menu:HOVER{
	background-color: white;
	color: gray;
}
</style> 
<script type="text/javascript">
	var loopFlag = null;
	var session = window.sessionStorage.getItem("auto_flag");
	if(session==null) window.sessionStorage.setItem("auto_flag", true);
	
	var flag = false;
	function stopLoop(){
		var flag = window.sessionStorage.getItem("auto_flag");
		
		if(flag=="true"){
			flag = "false"
		}else{
			flag = "true"
		}
		
		window.sessionStorage.setItem("auto_flag", flag);
		
		if(flag=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
		
		alert("페이지 자동 이동 : " + flag);
	};
	
	
	var menu = false;
	function getMousePos(){
		$(document).on("mousemove", function(e){
			var target = $("#menu_btn").width();

			if((e.pageX <= target && e.pageY <= target) || panel){
				if(!menu){
					$("#menu_btn").animate({
						"left" : 0,
						"top" : getElSize(20)
					});
					menu = true;
				};
			}else{
				if(menu){
					$("#menu_btn").animate({
						"left" : -getElSize(100),
					});	
					menu = false;
				};
			};
		});
	};
	
	var panel = false;
	function showPanel(){
		$("#panel").animate({
			"left" : 0
		});
		
		$("#menu_btn").animate({
			"left" : contentWidth * 0.2 + getElSize(50)	
		});
		
		$("#corver").css({
			"z-index" : 99999
		});
	};

	function closePanel(){
		$("#panel").animate({
			"left" : -contentWidth * 0.2 - getElSize(40)
		});
		
		$("#menu_btn").animate({
			"left" : 0
		});
		
		$("#corver").css({
			"z-index" : -1
		});
	};
	
	function goReport(){
		var type = this.id;
		var url;
		if(type=="menu0"){
			closePanel();
			panel = false;
		}else if(type=="menu1"){
			url = "${ctxPath}/chart/performanceReport.do";
			location.href = url;
		}else if(type=="menu2"){
			url = "${ctxPath}/chart/alarmReport.do";
			location.href = url;
		}else if(type=="menu3"){
			url = "${ctxPath}/chart/main3.do";
			location.href = url;
		}else if(type=="menu4"){
			url = "${ctxPath}/chart/DIMM.do";
			location.href = url;
		}else if(type=="menu9"){
			url = "${ctxPath}/chart/singleChartStatus2.do";
			window.localStorage.setItem("dvcId", 1)
			location.href = url;
		}
		
	};
	
	var openCal = false;
	
	var table_flag = true;
	var table_8 = false;
	
	function dateUp(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() + 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		changeDateVal();
	};
	
	function changeDateVal(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var today = year + "-" + month + "-" + day;
		
		if(today==$("#sDate").val()){
			isToday = true;
		}else{
			isToday = false;
		};
		
		table_8 = false;
	}
	
	function dateDown(){
		var $date = $("#sDate").val();
		var year = $date.substr(0,4);
		var month = $date.substr(5,2);
		var day = $date.substr(8,2);
		
		var current_day = month + "/" + day + "/" + year;
		
		var date = new Date(current_day);
		date.setDate(date.getDate() - 1);
		
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		$("#sDate").val(today);
		changeDateVal();
	};
	
	$(function(){
		$("#up").click(dateUp);
		$("#down").click(dateDown);
		
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		var hour = date.getHours();
		var minute = date.getMinutes();
		
		$("#excel").click(csvSend);
		
		$(".menu").click(goReport);
		//getMousePos();
		$("#menu_btn").click(function(){
			if(!panel){
				showPanel();
			}else{
				closePanel();
			};
			panel = !panel;
		});
		document.oncontextmenu = function() {stopLoop()}; 
		setDivPos();
		setInterval(time, 1000);
		
		$("#table_toggle_btn").click(function(){
			$("#tableSubTitle").html("기준 시간 : 18시 ~ 07시");
			table_8 = false;
			if(table_flag){
				$("#tableDiv").hide();				
			}else{
				$("#tableDiv").show();
			}
			table_flag=!table_flag;
		});
		
		if(hour>=7 && (hour<=8 && minute<=30)){
			$("#tableDiv").show();
			table_flag = true;
		}else{
			$("#tableDiv").hide();
			table_flag = false;
		};
		
		$("#title_main").click(function(){
			//location.href = "${ctxPath}/chart/multiVision.do";
			
			$("#machineListForTarget").animate({
				"opacity" : 1
			},500, function(){
				$(this).css("z-index", 999)
			});
		});
		
		$("#title_right").click(function(){
			//location.href = "${ctxPath}/chart/main3.do";
		});
		
		$("#title_left").click(function(){
			//location.href = "${ctxPath}/chart/main3.do";
		});
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		
		$(".menu").click(goReport);
		$("#menu0").addClass("selected_menu");
		$("#menu0").removeClass("unSelected_menu");
		getTargetData();
		
		if(session=="false"){
			clearInterval(loopFlag);	
		}else{
			startPageLoop();	
		};
	});
	
	var targetMap = new JqMap();
	var targetMap2 = new JqMap();
	function getTargetData(){
		var url = ctxPath + "/order/getTargetData.do";
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var  json = data.dataList;
				
				var tr = "<tr>" + 
							"<td>장비</td><td style='text-align: center;'>사이클</td><td style='text-align: center;'>가동 시간 (h)</td>" + 
						"</tr>";
				var class_name = "";
				$(json).each(function(idx, data){
						class_name = " ";
						targetMap.put("t" + data.dvcId, data.tgCnt);
						targetMap2.put("c" + data.dvcId, data.tgRunTime);
					var name = decodeURIComponent(data.name).replace(/\+/gi, " ");
					tr += "<tr>" + 
								"<td>" + name + "</td>" +
								"<td style='text-align: center;'> <input type='text' id=t" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgCnt + "><span id=s" + data.dvcId + " class='span'></td>" + 
								"<td style='text-align: center;'> <input type='text' id=c" + data.dvcId +" class='targetCnt t" + class_name + "' value=" + data.tgRunTime/3600 + "><span id=s2" + data.dvcId + " class='span'></td>" +
						  "</tr>";
					
				});
				
				
				tr += "<tr>" + 
							"<td colspan='3' style='text-align:center'><span style='cursor : pointer' id='save_btn'>확인</span></td>" + 
						"</tr>";
				
				$("#machineListTable").html(tr);
				$("#save_btn").click(addTarget);
				
				$(".targetCnt").css({
					"font-size" : getElSize(40),
					"width" : getElSize(250),
					"outline" : "none",
					"border" : "none"
				});
				
				$(".span").css({
					"font-size" : getElSize(40),
					"color" : "red",
					"margin-left" : getElSize(10),
					"font-weight" : "bolder"
				});
				
				$("#save_btn").css({
					"background-color" : "white",
					"color" : "black",
					"border-radius" : getElSize(10),
					"font-weight" : "bolder",
					"padding" : getElSize(10)
				});
				
				$(".tdisable").each(function(idx, data){
					this.disabled = true;
					this.value = "";
				});
				
				$(".tdisable").css({
					"background-color" : "rgba(	4,	238,	91,0.5)"
				});
				
				$("#machineListTable td").css({
					"color" : "white",
					"font-size" : getElSize(50),
				});
				
				$("#machineListForTarget").css({
					"width" : getElSize(1200),
					"z-index" : -1
				});
				
				$("#machineListForTarget").css({
					"top" : (originHeight/2) - ($("#machineListForTarget").height()/2)
				});
			}
		});
	};
	
	function chkTgCnt(el){
		var target = targetMap.get(el.id);
		var source = $(el).val();
		
		if(target!=source){
			$("#s" + el.id.substr(1)).html(target);
			
//			var url = ctxPath + "/order/addTargetCnt.do";
//			var param = "dvcId=" + el.id.substr(1) + 
//						"&tgCnt=" + source + 
//						"&tgDate=" + today;
//			
//			console.log(param)
//			$.ajax({
//				url : url,
//				data : param,
//				dataType : "text",
//				type : "post",
//				success : function(data){
//					
//				}
//			});
		};
	};
	
	var tgArray = new Array();
	var tgArray2 = new Array();
	var target_i = 0;
	function addTarget(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		today = year + "-" + month + "-" + day;
		
		tgArray = targetMap.keys();
		tgArray2 = targetMap2.keys();
		var url = ctxPath + "/order/addTargetCnt.do";
		
		var cnt = $("#" + tgArray[target_i]).val();
		if(cnt==null || cnt == "") cnt = 0;
		
		var time = $("#" + tgArray2[target_i]).val();
		if(time==null || time == "") time = 0;
		
		var param = "dvcId=" + tgArray[target_i].substr(1) + 
					"&tgCnt=" + cnt +
					"&tgRunTime=" + (time*3600) +  
					"&tgDate=" + today;

		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				if(data=="success") {
					if (target_i<(tgArray.length-1)){
						target_i++;
						console.log(target_i, tgArray.length)
						addTarget(target_i);
					}else{
						target_i = 0;
						$("#machineListForTarget, #close_btn").animate({
							"opacity" :0
						}, function(){
							$("#machineListForTarget").css("z-index",0);
							$("#close_btn").css("z-index",0	);
							getTargetData();
						});
					}
					
				}else{
					//i
				}
			}
		});
	};
	
	function startPageLoop(){
		/* loopFlag = setInterval(function(){
			location.href = ctxPath + "/chart/multiVision.do";
		},1000*60*10); */
		
		
		loopFlag = setInterval(function(){
			//location.href = ctxPath + "/chart/multiVision.do";
			
			window.sessionStorage.setItem("dvcId", "1");
			window.sessionStorage.setItem("name", "UM/F C#1");
			
			location.href=ctxPath + "/chart/singleChartStatus.do";
			
		},1000*10);
	};
	
	var isToday = true;
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setDivPos(){ 
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$("#container").css({
			"width": contentWidth,
			"height" : contentHeight,
			"margin-top" : height/2 - ($("#container").height()/2)
		});
		
		$("#container").css({
			"margin-top" : height/2 - ($("#container").height()/2),
			"margin-left" : width/2 - ($("#container").width()/2),
		});
		
		
		
		$("#title_main").css({
			"width" : getElSize(1000),
			"top" : $("#container").offset().top + (getElSize(50))
		});

		$("#title_main").css({
			"left" : originWidth/2 - ($("#title_main").width()/2)
		});
		
		$("#title_left").css({
			"width" : getElSize(300),
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_right").css({
			"top" : $("#container").offset().top + (getElSize(50))
		});
		
		$("#title_left").css({
			"left" : $("#container").offset().left + getElSize(50)
		});
		
		$("#title_right").css({
			"right" : $("#container").offset().left + getElSize(50),
			"color" : "white",
			"font-weight" : "bolder",
			"font-size" : getElSize(40)
		});
		
		$("#tableDiv").css({
			"left" : $("#container").offset().left + getElSize(20),
			"width" : getElSize(600),
			"top" : $("#container").offset().top + getElSize(450)
		});
		
		//$("#table").css("margin-top", contentHeight/(targetHeight/50));
		$("#tableTitle").css("font-size", getElSize(40));
		$("#tableSubTitle").css("font-size", getElSize(30));
		$(".tr").css("font-size", getElSize(30));
		$(".td").css("padding", getElSize(10));
		
		$("#time").css({
			"top": $("#container").offset().top + getElSize(120),
			"right": $("#container").offset().left + getElSize(50),
			"font-size" : getElSize(30)
		});
		
		$("#date").css({
			"top": $("#container").offset().top + getElSize(120),
			"right": $("#container").offset().left + getElSize(250),
			"font-size" : getElSize(30)
		});
		
		$("#Legend").css({
			"font-size" : getElSize(45),
			"top" : marginHeight + getElSize(600),
			"left" : marginWidth + getElSize(100),
			"color" : "white"
 		});
		
		
		$("#menu_btn").css({
			"position" : "absolute",
			"width" : getElSize(130),
			"left" : 0,
			"top" : getElSize(20),
			"cursor" : "pointer",
			"z-index" : 999999
		});
		$("#panel").css({
			"background-color" : "rgb(34,34,34)",
			"border" : getElSize(20) + "px solid rgb(50,50,50)",
			"position" : "absolute",
			"width" : contentWidth * 0.2,
			"top" : 0,
			"left" : -contentWidth * 0.2 - getElSize(40),
			"z-index" : 99999
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(60)
		});

		$("#panel_table td").css({
			"padding" : getElSize(50),
			"cursor" : "pointer"
		});
		
		$("#panel_table td").addClass("unSelected_menu");
		
		$("#corver").css({
			"width" : originWidth,
			"height" : originHeight,
			"position" : "absolute",
			"z-index" : -1,
			"background-color" : "black",
			"opacity" : 0.4
		});
		
		$("#panel_table").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		
		$("#excel").css({
			"width" : getElSize(70),
			"cursor" : "pointer"
		});
		
		$("#table_toggle_btn").css({
			"position" : "absolute",
			"left" : marginWidth + getElSize(150),
			"top" : getElSize(300)
		});
		
		$("#machineListForTarget").css({
			"position" : "absolute",
			"width" : getElSize(1200),
			//"top" : getElSize(50),
			//"background-color" : "rgb(34,34,34)",
			"background-color" : "green",
			"color" : "white",
			"font-size" : getElSize(50),
			"padding" : getElSize(50),
			"overflow" : "auto",
			"border-radius" : getElSize(50),
			"border" : getElSize(10) + "px solid white",
		});
		
		$("#machineListForTarget").css({
			"left" : (originWidth/2) - ($("#machineListForTarget").width()/2),
		});
	};
	
	var border_interval = null;
	function bodyNeonEffect(color) {
		var lineWidth = getElSize(100);
		var toggle = true;
		border_interval = setInterval(function() {
			$("#container").css("box-shadow",
					"inset 0px 0px " + lineWidth + "px " + color);

			if (toggle) {
				lineWidth -= getElSize(3);
				if (lineWidth <= 0) {
					toggle = false;
				};
			} else if (!toggle) {
				lineWidth += getElSize(3);
				if (lineWidth >= getElSize(100)) {
					toggle = true;
				};
			};
		},50);
	};
	
	function time(){
		var date = new Date();
		var month = date.getMonth()+1;
		var date_ = addZero(String(date.getDate()));
		var day = date.getDay();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		if(day==1){
			day = "Mon";
		}else if(day==2){
			day = "Tue";
		}else if(day==3){
			day = "Wed";
		}else if(day==4){
			day = "Thu";
		}else if(day==5){
			day = "Fri";
		}else if(day==6){
			day = "Sat";
		}else if(day==0){
			day = "Sun";
		};
		
		$("#date").html(month + " / " + date_ + " (" + day + ")");
		$("#time").html(hour + " : " + minute + " : " + second);
	};
	
	var csvData = "";
	function csvSend(){
		var sDate, eDate;
		var csvOutput;
		
		sDate = $("#sDate").val();
		eDate = "";
		csvOutput = csvData;
		
		csvOutput = csvOutput.replace(/\n/gi,"");
		
		f.csv.value=encodeURIComponent(csvOutput);
		f.startDate.value = sDate;
		f.endDate.value = eDate;
		f.submit(); 
	};
	
</script>
</head>
<body oncontextmenu="return false">
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	
	<div id="corver"></div>
	
	<div id="machineListForTarget" style="opacity:0">
		<center>
			<table id="machineListTable" style="width: 100%">
			</table>
		</center>
	</div>
				
					
	<div id="container" >
		<div id="panel">
			<table id="panel_table" width="100%">
				<tr>
					<td id="menu0" class="menu">샵 레이아웃</td>
				</tr>
				<tr>
					<td id="menu9" class="menu">개별 장비 가동 현황</td>
				</tr>
				<tr>
					<td id="menu1" class="menu">장비별 가동실적 분석</td>
				</tr>
				<tr>
					<td id="menu2" class="menu">장비 Alarm 발생내역 조회</td>
				</tr>
				<tr>
					<td id="menu3" class="menu">무인장비 가동 현황</td>
				</tr>
				<!-- <tr>
					<td id="menu4" class="menu">야간무인장비 가동 현황</td>
				</tr> -->
			</table>
		</div>
		<img src="${ctxPath }/images/menu.png" id="menu_btn" >
		<div id="svg"></div>
	</div>
	<div id="Legend" style="position: absolute; font-size: 50px;">
		<span style="background-color: GREEN; color: green; border: 1px solid black;">범례</span> <font style="color: white">가동</font><br><br> 
		<span style="background-color: yellow; color: yellow; border: 1px solid black;">범례</span> <font style="color: white">대기</font><br> <br>
		<span style="background-color: red; color: red; border: 1px solid black;">범례</span> <font style="color: white">알람</font> <br><br>
		<span style="background-color: rgb(206,206,206); color: rgb(206,206,206); border: 1px solid black;">범례</span> <font style="color: white">Power Off</font>  
	</div>

	<div id="pieChart1" ></div>
	<div id="pieChart2" ></div>
	

	<img src="${ctxPath }/images/DashBoard/title_main.svg" id="title_main" class="title">
	<%-- <img src="${ctxPath }/images/DashBoard/title_left.svg" id="title_left" class="title"> --%>
	<div id="title_right" class="title">(주) 부광정밀</div>	

	<font id="date"></font>
	<font id="time"></font>
</body>
</html>