<!-- 
최소한의 화면으로 구성된
시작화면 Template
 -->
 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}

.k-grid-header-wrap.k-auto-scrollable table thead tr th{
	text-align: center;
	vertical-align: middle;
}

.k-header.k-grid-toolbar {
	background: linear-gradient( to top, black, gray);
	color: white;
	text-align: right;
}
.ui-jqgrid .ui-jqgrid-bdiv{
	overflow-x:auto;
}

.color1{
	background: #FCFCFC;
}

.color2{
	background: #EAEAEA;
}
/* .test{
	display: none;
} */
</style> 
<script type="text/javascript">

	var chkcell={cellId:undefined, chkval:undefined}; //cell rowspan 중복 체크
	var chkcell2={cellId:undefined, chkval:undefined}; //cell rowspan 중복 체크

	$(function(){
		createNav("inven_nav", 8);
		
//		gridTable();

		if(moment().format("HH:mm")<"09:00"){
			$(".date").val(moment().subtract(2, 'day').format("YYYY-MM-DD"))
		}else{
			$(".date").val(moment().subtract(1, 'day').format("YYYY-MM-DD"))
		}
		
		$(".date").datepicker({
			onSelect: function (e) {
				if (e > moment().format("YYYY-MM-DD")) {
					$(".date").val(nowDateInsert);
					alert("오늘 이후의 날짜를 선택할수 없습니다.")
					return false;
				} else {
					//e == 날짜 
					$(".date").val(e);
					getTable();
				}
			}
		})
		
		// 테이블 데이터 가져오기
		getTable();
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
	});
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function readOnly(container, options){
		container.removeClass("k-edit-cell");
		container.text(options.model.get(options.field));
	}
	function jsFormatterCell(rowid, val, rowObject, cm, rdata){
        var result = "";
        // cm.name = group
       /*  console.log(rowid)
        console.log(cm)
        console.log(chkcell.chkval)
        console.log(val)
        console.log(rdata) */
        if(chkcell.chkval != rdata.prdNo){ //check 값이랑 비교값이 다른 경우
/*         	console.log(this.id)
        	console.log(rowid)
        	console.log(cm)
        	console.log(cm.name) */
            var cellId = this.id + '_row_'+rowid+'-'+rdata.prdNo;
            result = ' rowspan="1" id ="'+cellId+'" + name="cellRowspan"';
//            alert(result);
            chkcell = {cellId:cellId, chkval:rdata.prdNo};
        }else{
            result = 'style="display:none;" rowspanid="'+chkcell.cellId+'"; class="test"'; //같을 경우 display none 처리
//        	console.log(result)
//            alert(result);
        }
        return result;
    }
	function jsFormatterCell2(rowid, val, rowObject, cm, rdata){
        var result = "";
        // cm.name = group
        if(chkcell2.chkval != rdata.prdNo){ //check 값이랑 비교값이 다른 경우
/*         	console.log(this.id)
        	console.log(rowid)
        	console.log(cm)
        	console.log(cm.name) */
            var cellId = this.id + '_row_'+rowid+'-'+rdata.prdNo;
            result = ' rowspan="1" id ="'+cellId+'" + name="cellRowspan"';
//            alert(result);
            chkcell2 = {cellId:cellId, chkval:rdata.prdNo};
        }else{
            result = 'style="display:none;" rowspanid="r'+chkcell2.cellId+'"; class="test"'; //같을 경우 display none 처리
//        	console.log(result)
//            alert(result);
        }
        return result;
    }
/* 	function jsFormatterCell(rowid, val, rowObject, cm, rdata){
        var result = "";
        
        console.log(chkcell.chkval)
        console.log(val)
        
        if(chkcell.chkval != val){ //check 값이랑 비교값이 다른 경우
            var cellId = this.id + '_row_'+rowid+'-'+cm.name;
            result = ' rowspan="1" id ="'+cellId+'" + name="cellRowspan"';
//            alert(result);
            chkcell = {cellId:cellId, chkval:val};
        }else{
            result = 'style="display:none;" rowspanid="'+chkcell.cellId+'"; class="test"'; //같을 경우 display none 처리
//        	console.log(result)
//            alert(result);
        }
        return result;
    } */
	var tt
	function gridTable(json){
		/* arrtSetting = function (rowId, val, rawObject, cm) {
			console.log(cm)
			
			console.log(rawObject.attr)
	        var attr = rawObject.attr[cm.name], result;
	        if (attr.rowspan) {
	            result = ' rowspan=' + '"' + attr.rowspan + '"';
	        } else if (attr.display) {
	            result = ' style="display:' + attr.display + '"';
	        }
	        return result;
	    }; */
	    
	    //기존 jqgrid 제거
	    $("#jqgrid").jqGrid("GridUnload");
	    
	    tt =json;
		console.log(json)
		jQuery("#jqgrid").jqGrid({
			datatype: "local",
//            editurl: 'clientArray',
            cellsubmit : 'clientArray',
//            editurl: 'clientArray'
			data: json,
			cellEdit:true,
			sortable:false,
			shrinkToFit: false,
		   	colNames:['prdNo','item', '기초재고', '입고','출고','불량','현재고'
	   				,'기초재고', '입고','출고','불량','현재고'
	   				,'기초재고', '입고','출고','불량','현재고'
	   				,'기초재고', '입고','출고','불량','현재고'
	   				,'기초재고', '입고','출고','불량','현재고'
	   				],
		   	colModel:[
		   		{name:'prdNo',index:'invdate', width:getElSize(440), align:"center",frozen:true},
		   		{name:'item',index:'id', width:getElSize(400), align:"center",frozen:true },
		   		{name:'iniohdCnt',index:'name asc, invdate', width:getElSize(220), align:"center",editable:true},
		   		{name:'rcvCnt',index:'amount', width:getElSize(150), align:"center", cellattr:jsFormatterCell},
		   		{name:'issCnt',index:'tax', width:getElSize(150), align:"center", cellattr:jsFormatterCell2},		
		   		{name:'notiCnt',index:'total', width:getElSize(150),align:"center"},		
		   		{name:'ohdCnt',index:'note', width:getElSize(220), align:"center", sortable:false,editable:true},
		   		
		   		{name:'iniohdCntR',index:'name asc, invdate', width:getElSize(220), align:"center",editable:true},
		   		{name:'rcvCntR',index:'amount', width:getElSize(150), align:"center", },
		   		{name:'issCntR',index:'tax', width:getElSize(150), align:"center", },		
		   		{name:'notiCntR',index:'total', width:getElSize(150),align:"center"},		
		   		{name:'ohdCntR',index:'note', width:getElSize(220), align:"center", sortable:false,editable:true},		

		   		{name:'iniohdCntM',index:'name asc, invdate', width:getElSize(220), align:"center",editable:true},
		   		{name:'rcvCntM',index:'amount', width:getElSize(150), align:"center", },
		   		{name:'issCntM',index:'tax', width:getElSize(150), align:"center", },		
		   		{name:'notiCntM',index:'total', width:getElSize(150),align:"center"},		
		   		{name:'ohdCntM',index:'note', width:getElSize(220), align:"center", sortable:false,editable:true},

		   		{name:'iniohdCntC',index:'name asc, invdate', width:getElSize(220), align:"center",editable:true},
		   		{name:'rcvCntM',index:'amount', width:getElSize(150), align:"center", },
		   		{name:'issCntM',index:'tax', width:getElSize(150), align:"center", },		
		   		{name:'notiCntM',index:'total', width:getElSize(150),align:"center"},		
		   		{name:'ohdCntM',index:'note', width:getElSize(220), align:"center", sortable:false,editable:true},

		   		{name:'iniohdCntF',index:'name asc, invdate', width:getElSize(220), align:"center",editable:true},
		   		{name:'rcvCntCF',index:'amount', width:getElSize(150), align:"center", },
		   		{name:'issCntCF',index:'tax', width:getElSize(150), align:"center", },		
		   		{name:'notiCntCF',index:'total', width:getElSize(150),align:"center"},		
		   		{name:'ohdCntCF',index:'note', width:getElSize(220), align:"center", sortable:false,editable:true}		
		   	],
		   	gridComplete: function() {  /** 데이터 로딩시 함수 **/
                var grid = this;
                 
                $('td[name="cellRowspan"]', grid).each(function() {
                    var spans = $('td[rowspanid="'+this.id+'"]',grid).length+1;
                    if(spans>1){
                     $(this).attr('rowspan',spans);
                    }
                });    
                
                // jqgrid data값 가져오기
                var gridData = $("#jqgrid").jqGrid('getRowData');
                var ids = $('#jqgrid').jqGrid('getDataIDs');

                console.log(gridData)
//                console.log(ids)

				
				var bPrdNo;	//이전 데이터
				var color = "color1";
				// 데이터 확인후 색상 변경 
				for (var i = 0; i < gridData.length; i++) {
					// 데이터의 is_test 확인 
					if(i==0){
						bPrdNo = gridData.prdNo
						$('#jqgrid tr[id=' + ids[i] + ']').addClass(color); 
					}else{
						bPrdNo = gridData[i-1].prdNo
						if(bPrdNo==gridData[i].prdNo){
							$('#jqgrid tr[id=' + ids[i] + ']').addClass(color); 
						}else{
							// class명 다르게 주기위해서
							if(color=="color1"){
								color="color2";
							}else{
								color="color1";
							}
							$('#jqgrid tr[id=' + ids[i] + ']').addClass(color); 
						}
					}
/* 					if (gridData[i].is_test == 'Y') { 
						// 열의 색상을 변경하고 싶을 때(css는 미리 선언) 
						$('#requestList tr[id=' + ids[i] + ']').addClass('grid-test'); 
						// 칼럼의 색생을 변경하고 싶을 때 
						$('#requestList').jqGrid('setCell', ids[i], 'document_status_text', '', cssGreen);
					} */
				}

            },
            afterSaveCell : function(rowid,name,val,iRow,ICol){ // 로우 데이터 변경하고 엔터치거나 다른 셀 클릭했을때 발동
				console.log("asd")
//                alert(rowid+val+name);

            },
//            viewrecords: true,
//            sortorder: "desc",
/*             onSelectRow : function(e){
            	console.log("선택")
            }, */
//            editurl : asdf(),
//		    cmTemplate: {sortable: false},
		    rowNum: 100,
/* 		    gridview: true,
		    hoverrows: false,
		    autoencode: true,
		    ignoreCase: true,
		    viewrecords: true, */
		    height: getElSize(1500),
		    width: getElSize(3200),
		    caption: 'Grid with rowSpan attributes',
		});
	    
		
		// 해더 위에 그룹 해더 입력하기
		jQuery("#jqgrid").jqGrid('setGroupHeaders', {
		useColSpanStyle: true, 
		groupHeaders:[
			{startColumnName: 'iniohdCnt', numberOfColumns: 5, titleText: '소재창고'},
			{startColumnName: 'iniohdCntR', numberOfColumns: 5, titleText: 'R삭'},
			{startColumnName: 'iniohdCntM', numberOfColumns: 5, titleText: 'MCT삭'},
			{startColumnName: 'iniohdCntC', numberOfColumns: 5, titleText: 'CNC삭'},
			{startColumnName: 'iniohdCntF', numberOfColumns: 5, titleText: '도금'},/*
			{startColumnName: 'iniohdCnt', numberOfColumns: 5, titleText: 'R삭'}/* ,
			{startColumnName: 'closed', numberOfColumns: 2, titleText: 'Shiping'} */
		]	
		});
		
		//column 고정 시키기
		$("#jqgrid").jqGrid("setFrozenColumns")
		
		/* // 수정가능하게하기..
		$(json).each(function(idx,data){
			jQuery("#jqgrid").jqGrid('editRow',idx);
			this.disabled = 'true'
		}) */
		
		// 행 선택시 이벤트 발생하게 하기
//		$("#jqgrid").jqGrid('setGridParam', {ondblClickRow: function(rowid,iRow,iCol,e){alert('double clicked');}});

	}
	
	function saveRow(){
		jQuery("#jqgrid").getRowData()
		console.log("ok")
	}
	function getTable(){

		$.showLoading(); 
		classFlag = true;
		var tablelist=[];
		var url = "${ctxPath}/chart/getStockProUpList.do";
		
		
		
		var param = "deliveryNo=" + $("#deliveryNo").val()+
					"&date=" + $("#date").val() +
 					"&sDate=" + moment().subtract(6,"month").format("YYYY-MM-DD") +
					"&eDate=" + moment().format("YYYY-MM-DD") ;  
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log("완성")
				console.log(data.dataList)
				
				var json = data.dataList;
				
				gridTable(json)
				$.hideLoading(); 
			}
		})
		
	}
	
	
	</script>
</head>
<body>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
				<input type="text" id="date" readOnly="readOnly" class="date">
				<div id="jqDiv">
				<table id="jqgrid">
				
				</table>
				</div>
				<!-- 
				
				이곳에 필요한 DOM 을 작성합니다.
				실질적 화면에 표시되는 부분.
				
				 -->
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back" style="text-align: "></div>
	<span id="intro"></span>
</body>
</html>	