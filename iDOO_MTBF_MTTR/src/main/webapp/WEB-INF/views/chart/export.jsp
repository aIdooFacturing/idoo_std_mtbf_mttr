<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<%-- <link rel="stylesheet" href="${ctxPath }/css/style.css"> --%>
<title></title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<style>
*{
	color:black !important;
}
</style> 
<script type="text/javascript">
	$(function(){
		$("#flagDiv").remove()
		setEl();
		parsingData();
		
	});
	
	function init() {
		
	}
	
	function setEl(){
		$("#corver").remove();
		$("h1").css({
			"font-size" :  "25px"
		});
		
		$("#main_table").css({
			"margin-top" : "10px;",
			"font-size" : "10px"
		})
		$(".table td").css({
			"text-align" : "center",
			"border" : "1px solid black",
			"font-size" : "11px"
		});
	}
	
	var jsonData = '${jsonData}';
	
	function createBarcode(barCode){
		var url = "${ctxPath}/chart/createBarcode.do";
		$.ajax({
			url : url,
			async : false,
			data : "deliveryCd=" + barCode,
			type :"post",
			dataType : "text",
			success :function(data){
				if(data == "success"){
					
				}
			}
		});
	}
	
	var lotsplit;
	function parsingData(){
		var json = JSON.parse(jsonData);
		$(".deliverer").html(decodeURIComponent(json[0].vndNm))
		$(".delivery_date").html(getToday().substring(0,10))
		$(".deliveryNo").html(json[0].deliveryNo)
		$(".barcode").attr("src", "http://bkjm.iptime.org/barcode/" + json[0].deliveryNo + ".png");
		//$("#barcode").attr("src", ctxPath + "/images/1707240003.png");
		
		console.log(json)
		
		var tr;
		var barcdTable = "";
		var barcdList = "";
//		var barcdList = "<div style='page-break-before: always;'>"
			barcdList += "<table class='table' style='width:100%; border-collapse: collapse;'>"
		var totalCnt=0;
		var totalPrce=0;
		
		
		var maxPrint=0;
		$(json).each(function(idx, data){
			
			if(data.afterProj=="0010"){
				data.afterProj="R외주"
			}else if(data.afterProj=="0020"){
				data.afterProj="MCT외주"
			}else if(data.afterProj=="0030"){
				data.afterProj="CNC외주"
			}else if(data.afterProj=="0040"){
				data.afterProj="도금외주"
			}
			
			totalCnt = totalCnt+data.sendCnt;
			tr += "<tr>" + 
						"<td>" + (idx+1) + "</td>" +
						"<td>" + decodeURIComponent(data.vndNm) + "</td>" +
						"<td>" + data.barcode + "</td>" +
						"<td>" + data.prdNo + "</td>" +
						"<td>" + data.item + "</td>" +
						"<td>" + data.ITEMNO + "</td>" +
						"<td>" + data.afterProj + "</td>" +
						"<td>" + numberWithCommas(data.sendCnt) + "</td>" +
//						"<td>" + numberWithCommas(4000*2) + "</td>" +
				 "</tr>;"
			
			//개별 바코드 변수
			var barcodeDB="";
			
			// 0012 => 대동사 일때
			if(data.vndNo=="0012"){
				barcodeDB= data.ITEMNO +"$"+data.barcode+"$"+data.sendCnt;
			}else{	// 대동사를 제외한 나머지 업체
				barcodeDB= data.ITEMNO +"$"+data.barcode+"$"+data.sendCnt+"$"+data.vndNo;
			}
			
			createBarcode(barcodeDB);
			barcdList += "<tr>";
			barcdList += "<td rowspan=2 width='5%'>" + (idx+1) + "</td>";
			barcdList += "<td width='8%'>소재</td> <td width='17.3%'>" + data.prdNo + "</td>";
			barcdList += "<td width='8%'>품번</td> <td width='13.3%'>" + data.ITEMNO + "</td>";
			barcdList += "<td width='8%'>LotNo</td> <td width='12%'>" + data.barcode + "</td>";
			barcdList += "<td rowspan=3 width='28.4%'> <img width='95%' src='http://bkjm.iptime.org/barcode/" + barcodeDB	 +".png'> </td>";
			barcdList += "</tr>";
			barcdList += "<tr>";
			barcdList += "<td>차종</td> <td>" + data.item + "</td>";
			barcdList += "<td>공정</td> <td>" + data.afterProj + "</td>";
			barcdList += "<td>수량</td> <td>" + numberWithCommas(data.sendCnt) + "</td>";
			barcdList += "</tr>"
			if(json.length-1!=idx){
				barcdList += "<tr><td colspan='7'> </td></tr>";
			}
	 
			createBarcode(data.barcode);
			if(maxPrint>3){
				barcdTable += "<div style='page-break-before: always;'>" 
				maxPrint=0;
			}else if(idx==0){
				barcdTable = "<div style='page-break-before: always;'>"  
			}else{
				barcdTable += '<table style="width: 100%;"><tr><td></td></tr></table>';
				barcdTable += "<div>"
			}
			maxPrint++;

			/* barcdTable +=		"<center>" + 
									"<div  style='font-size: 30px; border: 1px solid black; height: 40px;'>" + 
										"<span style='display: table-cell'>" + data.barcode + "</span>" + 
									"</div>" + 
								"</center>"; */
			barcdTable +="<center><div id="+(idx+maxPrint)+" style='font-size: "+getElSize(40)+"; border: 1px solid black;'>"+data.barcode+"</div></center>"							
			for(var i = 0; i < 2; i++){
				barcdTable += "<table class='table' style='border:1px solid black; width:48%; border-collapse: collapse; float : left; margin:7px; height:145px'><tr>" + 
									"<td>반출번호</td>" + 
									"<td>" + json[0].deliveryNo + "</td>" +
									"<td colspan='2'>소재</td>" + 
									"<td colspan='2'>" + data.prdNo + "</td>" +
									
									//"<td rowspan='7' width='50%'><img width='70%' src='http://bkjm.iptime.org/barcode/" + data.barcode +".png'></td>" +
									//"<td rowspan='5' width='50%'><img width='90%' src='" + ctxPath + "/images/1707240003.png'></td>" +
								"</tr>" +
								"<Tr>" +
									"<Td>차종</tD>" +
									"<Td>" + data.item + "</tD>" +
									"<Td colspan='2'>품번</tD>" +
									"<Td colspan='2'>" + data.ITEMNO + "</tD>" +
									
								"</tr>" +
								"<Tr>" +
									"<Td>반출업체</tD>" +
									"<Td>" + decodeURIComponent(data.vndNm) + "</tD>" +
									"<Td colspan='2'>로트번호</tD>" +
									"<Td colspan='2'>" + data.lotNo + "</tD>" +
									
								"</tr>" +
								"<Tr>" +
									"<Td>반출일자</tD>" +
									"<Td>" + getToday().substring(0,10) + "</tD>" +
									"<Td colspan='2'>로트수량</tD>" +
									"<Td colspan='2'>" + data.sendCnt + "</tD>" +
									"</tr>" + 
								"<Tr>" +
									"<td colspan='6'><img width='70%' height='12%' src='http://bkjm.iptime.org/barcode/" + barcodeDB +".png'></td>" +
								"</Tr>" +
								"<Tr>" +
									"<Td rowspan='2'>구분</tD>" +
									"<Td rowspan='2'>일자</tD>" +
									"<Td colspan='3'>수량</tD>" +
									"<Td rowspan='2'>확인</tD>" +
								"</Tr>" +
								"<Tr>" +
									"<Td>로트</tD>" +
									"<Td>부족</tD>" +
									"<Td>수량</tD>" +
								"</Tr>" +
								"<Tr>" + 
									"<Td>업채입고</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
								"</Tr>" + 
								"<Tr>" +
									"<Td>업체반출</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
								"</Tr>" +
								"<Tr>" +
									"<Td>부광반입</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
									"<Td>　</tD>" +
								"</Tr></table>";
			} 
			
			barcdTable += "</div>";
			

		});
		$("body").append(barcdTable)
		
		tr += "<tr rowspan='2' style='font-size : 20px'> <td> 합계 </td> <td colspan='5'> </td> <td colspan='2'> 총 수량 : " + numberWithCommas(totalCnt) + " </td> </tr>" 
		tr += "<tr><td> 비고 </td> <td colspan='7' id='remarks'><h3>" + decodeURI(json[0].remarks) +"</h3></td>"
		$("#main_table").append(tr);
		barcdList += "</table>";
		$("#barcodeList").append(barcdList);

		setEl();
		window.print();
		
	};
	
	function numberWithCommas(x) {
	    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}


</script>
</head>
<body>
	<center><h1>반 출 서 (반출 처리용)</h1>
	
		<table class="top_table" style="width: 95%">
			<Tr>
				<Td style="width: 20%" >반출번호</Td>
				<Td style="width: 20%" class="deliveryNo"></Td>
				<td  rowspan="3" style="width: 60%"><img alt="" src="" width="100%" class="barcode"> </td> 
			</Tr>
			<tr>
				<Td>반출업체</Td>
				<td class="deliverer"></td>
			</tr>
			<tr>
				<Td>반출일자</Td>
				<td class="delivery_date"></td>
			</tr>
		</table>
	</center>
			
	<table id="main_table" style="width: 100%; border-collapse: collapse;" class="table">
		<tr>
			<Td style="width: 10%">항목</Td>
			<Td colspan="8" > 반출 항목 </Td>
		</tr>
		<tr>
			<td>순번</td>
			<td>업체</td>
			<td>로트번호</td>
			<td>소재</td>
			<td>차종</td>
			<td>품번</td>
			<td>공정</td>
			<td>수량</td>
<!-- 			<td>금액</td> -->
		</tr>
	</table>

	<div id="barcodeList" style="page-break-before: always;">
		<center><h1>반 출 서 (바코드용)</h1>
			<table class="top_table" style="width: 95%">
				<Tr>
					<Td style="width: 20%" >반출번호</Td>
					<Td style="width: 20%" class="deliveryNo"></Td>
					<td  rowspan="3" style="width: 60%"><img alt="" src="" width="100%" class="barcode"> </td> 
				</Tr>
				<tr>
					<Td>반출업체</Td>
					<td class="deliverer"></td>
				</tr>
				<tr>
					<Td>반출일자</Td>
					<td class="delivery_date"></td>
				</tr>
			</table>
		</center>
	</div>
	
	<!-- <div id="barcodeDiv" >
		<center>
			<div id="lotTrcerNo" style="font-size: 30px; border: 1px solid black; height: 70px;">
				<span style="display: table-cell">로트 추적 번호</span>
			</div>
		</center>	
	</div> -->
</body>
</html>	