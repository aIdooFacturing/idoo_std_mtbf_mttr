package com.unomic.cnc;


public interface CNC
{
	/**
	 * Setting
	 */
	public static final String TAG 								= "MTES";
	public static final boolean DEBUG							= false;
	public static final int LANGUAGE                            = 1;
	// 0 : ENG
	// 1 : KOR
	/**
	 * MTES MemoryMap
	 */
	public static final int MAX_NUM_OF_AXIS						= 8;
	
	/**
	 * AppEngine
	 */
	public static final String APPENGINE_URL					= "http://mtesds.appspot.com/mtes";	
    public static final String APPENGINE_URL_SUB                = "http://61.76.17.213:8081/main.php";	
	public static final String APPENGINE_CHARTERSET				= "UTF-8";
	public static final String APPENGINE_HEADER_REQTYPE			= "mtes/req-type";
	public static final String APPENGINE_HEADER_IRS_REQTYPE     = "mtes";
	public static final String APPENGINE_HEADER_REQTYPE_IPQUERY = "ip_query";
	public static final int APPENGINE_TIMEOUT					= 30000;
	
	public static final String APPENGINE_PARAM_MAC1				= "mac1";
	public static final String APPENGINE_PARAM_MAC2				= "mac2";
	public static final String APPENGINE_PARAM_RESULT			= "result";
	
	public static final int APPENGINE_RESULT_SUCCESS			= 0;
	public static final int APPENGINE_RESULT_FAIL				= 1;
	
	/**
	 * Activity number
	 */
	public static final int CNC_ACTIVITY_SPLASH								= 0;
	public static final int CNC_ACTIVITY_MACHINE_ADD						= 4;
	public static final int CNC_ACTIVITY_MACHINE_DELETE						= 5;
	public static final int CNC_ACTIVITY_MACHINE_UPDATE						= 6;
	public static final int CNC_ACTIVITY_HOME								= 7;
	public static final int CNC_ACTIVITY_MACHINE_FILE_DELETE				= 8;
	public static final int CNC_ACTIVITY_MACHINE_FILE_OVERWRITE_UPLOAD		= 9;
	public static final int CNC_ACTIVITY_MACHINE_FILE_OVERWRITE_DOWNLOAD	= 10;
	public static final int CNC_ACTIVITY_MACHINE_FILE_CNCLOADING_FAIL		= 11;
	public static final int CNC_ACTIVITY_MACHINE_FILE_VIEW					= 12;
	public static final int CNC_ACTIVITY_MACHINE_FILE_EDIT					= 13;
	public static final int CNC_ACTIVITY_EDITOR_PAGE_NAVIGATION				= 14;
	public static final int CNC_ACTIVITY_FIND_OR_REPLACE_SELECTOR			= 15;
	public static final int CNC_ACTIVITY_SAVE_AS							= 16;
	public static final int CNC_ACTIVITY_RE_FIND_OR_REPLACE					= 17;	
	public static final int CNC_ACTIVITY_SAVE_AS_EMPTY_FILENAME				= 18;
	public static final int CNC_ACTIVITY_SAVE_AS_OVERWRITE					= 19;
	public static final int CNC_ACTIVITY_SAVE                               = 20;
    public static final int CNC_ACTIVITY_QUIT                               = 21;
    public static final int CNC_ACTIVITY_CONN_MACHINE                       = 22;
	
	
	public static final String CNC_ACTIVITY_INTENT_DELETE			= "delete";
	public static final String CNC_ACTIVITY_INTENT_DELETE_POSITION 	= "delposition";
	public static final String CNC_ACTIVITY_INTENT_NAME				= "name";
	public static final String CNC_ACTIVITY_INTENT_MESSAGE			= "message";
	public static final String CNC_ACTIVITY_INTENT_CODE				= "alarm_code";
	public static final String CNC_ACTIVITY_INTENT_IP				= "ip";
	public static final String CNC_ACTIVITY_INTENT_NUMBER			= "number";
	public static final String CNC_ACTIVITY_INTENT_DBID				= "dbid";
	public static final String CNC_ACTIVITY_INTENT_PORT				= "port";
	public static final String CNC_ACTIVITY_INTENT_LOCALPORT		= "local_port";
	public static final String CNC_ACTIVITY_INTENT_LOCALIP			= "local_ip";
	public static final String CNC_ACTIVITY_INTENT_ID				= "id";
	public static final String CNC_ACTIVITY_INTENT_PASSWORD			= "password";
	public static final String CNC_ACTIVITY_INTENT_HOME				= "home";
	public static final String CNC_ACTIVITY_INTENT_CHANGE			= "change";
	public static final String CNC_ACTIVITY_INTENT_TAB				= "tab";
	public static final String CNC_ACTIVITY_INTENT_MACHINE			= "machineNameProgram";
	public static final String CNC_ACTIVITY_INTENT_DIALOG_MESSAGE	= "dialogmessage";
	public static final String CNC_ACTIVITY_INTENT_DIALOG_BTN_YES	= "dialogyes";
	public static final String CNC_ACTIVITY_INTENT_DIALOG_BTN_NO	= "dialogno";
	public static final String CNC_ACTIVITY_INTENT_FILENAME			= "filename";
	public static final String CNC_ACTIVITY_INTENT_FILEPATH			= "filepath";
    public static final String CNC_ACTIVITY_INTENT_ALARMVIEW        = "alarmView";	
	public static final String CNC_ACTIVITY_INTENT_FILEPATH_TEMP	= "tempFilepath";
	public static final String CNC_ACTIVITY_INTENT_MAC1				= "mac1";
	public static final String CNC_ACTIVITY_INTENT_MAC2				= "mac2";
	public static final String CNC_ACTIVITY_INTENT_RUNNING			= "running";
	public static final String CNC_ACTIVITY_INTENT_C2DMMSG			= "c2dmmsg";
	public static final String CNC_ACTIVITY_INTENT_SETTING_TITLE	= "title";
	public static final String CNC_ACTIVITY_INTENT_SETTING_DIVISION	= "division";
	public static final String CNC_ACTIVITY_INTENT_POPUP			= "popup";
	public static final String CNC_ACTIVITY_INTENT_FLAG				= "flag";
	public static final String CNC_ACTIVITY_INTENT_CURRENT_PAGE		= "currentPage";	
	public static final String CNC_ACTIVITY_INTENT_PAGES			= "pages";
	public static final String CNC_ACTIVITY_INTENT_FIND_MENU		= "findMenu";	
	public static final String CNC_ACTIVITY_INTENT_SAVE_AS_NAME		= "saveAsFileName";
	public static final String CNC_ACTIVITY_INTENT_SAVE_AS_NEW_NAME	= "saveAsNewFileName";
	public static final String CNC_ACTIVITY_INTENT_FIND_NEXT_MODE	= "FindNextMode";
	public static final String CNC_ACTIVITY_INTENT_FIND				= "find";	
	
	public static final int POPUP_NETWORK_DISCONNECT				= 1;
	public static final int POPUP_CNC_LOADING_FAIL					= 2;
	public static final int POPUP_ERR_DOWNLOAD_FAIL					= 3;
	public static final int POPUP_ERR_DOWNLOAD_SOCKET				= 31;
	public static final int POPUP_ERR_BROKEN_PIPE					= 4;
	public static final int POPUP_ERR_CRC							= 5;
	public static final int POPUP_ERR_AUTHOR						= 6;
	public static final int POPUP_ERR_FILETRANS_IMPOSSIBLE			= 7;
	public static final int CNC_LOGIN_EXCEPTION_IDPW				= 8;
	public static final int CNC_LOGIN_EXCEPTION_MAXCONNECTION 		= 9;
	public static final int CNC_LOGIN_EXCEPTION_INTERNAL			= 10;
	public static final int CNC_LOGIN_EXCEPTION_CONNECTION			= 11;
	public static final int CNC_LOGIN_EXCEPTION_SOCKET				= 12;
	public static final int CNC_LOGIN_EXCEPTION_UNKNOWN				= 13;
	public static final int CNC_LOGIN_EXCEPTION_NOT_REG				= 14;
	public static final int POPUP_CNC_FILE_DELETE					= 15;
	public static final int POPUP_NETWORK_DISABLED					= 16;
	public static final int POPUP_FILEVIEW_ERR						= 17;
	public static final int POPUP_CHANGE_ALREADY_CONNECT			= 18;
	public static final int CNC_LOGIN_EXCEPTION_ID_DUPLICATE        = 19;
	public static final int POPUP_NETWORK_FEE_INFO                  = 20;
	
	public static final int SETTING_DIVISION_CONTACT				= 0;
	public static final int SETTING_DIVISION_INSTRUCTION			= 1;
	public static final int SETTING_DIVISION_NOTICE					= 2;
	
	public static final int EDITOR_FIND_SELECT						= 0;
	public static final int EDITOR_REPLACE_SELECT					= 1;
	/**
	 * CNC Preference's
	 */
	public static final String CNC_PREF								= "com.unomic.cnc";
	public static final String CNC_PREF_KEY_C2DMREGID				= "C2dmRegid";
	public static final String CNC_PREF_KEY_AUTOLOGIN				= "autoLogin";
	public static final String CNC_PREF_KEY_AUTOVIEW				= "autoView";
	public static final String CNC_PREF_KEY_ALARM_SOUND				= "alarmSound";
	public static final String CNC_PREF_KEY_ALARM_POPUP				= "alarmPopup";
	public static final String CNC_PREF_KEY_ISLOCAL					= "isLocal";
	public static final String CNC_PREF_KEY_LOCAL_REF_TIME          = "localRefTime";
	public static final String CNC_PREF_KEY_PUBLIC_REF_TIME         = "publicRefTime";
	public static final String CNC_PREF_KEY_NAME					= "name";
	public static final String CNC_PREF_KEY_DBID					= "dbid";
	public static final String CNC_PREF_KEY_IP						= "ip";
	public static final String CNC_PREF_KEY_PORT					= "port";
	public static final String CNC_PREF_KEY_ID						= "id";
	public static final String CNC_PREF_KEY_PASSWORD				= "password";
	public static final String CNC_PREF_KEY_NUMBER					= "number";
	public static final String CNC_PREF_KEY_IDPW					= "idpw";
	public static final String CNC_PREF_KEY_LASTMACHINE				= "lastMachine";
	public static final String CNC_PREF_KEY_NEWALARM				= "newAlarm";
	public static final String CNC_PREF_KEY_MACHINENAME				= "machineNameProgram";
	public static final String CNC_PREF_KEY_POLLING					= "polling";
	public static final String CNC_PREF_KEY_PATHNUM					= "pathnum";
	public static final String CNC_PREF_KEY_PATHNUM_CURRENT			= "pathnum_current";
	public static final String CNC_PREF_KEY_LOCALIP					= "localip";
	public static final String CNC_PREF_KEY_LOCALPORT				= "localport";
	public static final String CNC_PREF_KEY_MAC1					= "mac1";
	public static final String CNC_PREF_KEY_MAC2					= "mac2";
	public static final String CNC_PREF_KEY_ALARMRESTART			= "alarmrestart";
	public static final String CNC_PREF_KEY_FONT_SIZE				= "fontSizeDpi";
	public static final String CNC_PREF_KEY_IRS_URL                 = "IRS_URL";
	public static final String CNC_PREF_KEY_DEFAULT_IRS_URL         = "DEFAULT_IRS_URL";
    public static final String CNC_PREF_KEY_IRS_URL_USE             = "IRS_URL_USE";
    public static final String CNC_PREF_KEY_DEFAULT_IRS_URL_USE     = "DEFAULT_IRS_URL_USE";
	public static final String CNC_STATUS_RUN						= "RUN";
    public static final String CNC_STATUS_STR                       = "STR";	
	public static final String CNC_STATUS_STRT                      = "STRT";
	public static final String CNC_STATUS_STARS                     = "****";
	public static final String CNC_STATUS_STOP						= "STOP";
	public static final String CNC_STATUS_HALT						= "HALT";
	public static final String CNC_STATUS_RESET						= "RESET";
	
	/**
	 * UI
	 */
	public static final int CNC_UI_REFRESH_TIME 					= 1000;
	
	/**
	 * Message Type
	 */
	public static final int CMD_LOGIN_SUCCESS				= 0x00;
	public static final int CMD_LOGIN_FAIL_IDPW				= 0x01;
	public static final int CMD_LOGIN_FAIL_MAXCONNECTION 	= 0x02;
	public static final int CMD_LOGIN_FAIL_NOT_REG			= 0x03;
	public static final int CMD_LOGIN_FAIL_ID_DUPLICATE     = 0x04;
	public static final int CMD_LOGIN_FAIL_INTERNAL			= 0x10;
	
	/**
	 * Command.
	 */
	public static final int CMD_LOGIN_REQ 					= 0x01;
	public static final int CMD_LOGIN_REP 					= 0x02;
	public static final int CMD_LOGOUT_REQ 					= 0x03;
	public static final int CMD_STATUSINFO_REQ 				= 0x10;
	public static final int CMD_STATUSINFO_REP 				= 0x11;
	public static final int CMD_FILELIST_REQ				= 0x21;
	public static final int CMD_FILELIST_REP				= 0x22;
	public static final int CMD_FILETRANS_REQ 				= 0x23;
	public static final int CMD_FILETRANS_REP 				= 0x24;
	public static final int CMD_FILETRANS_REPORT			= 0x25;
	public static final int CMD_FILETRANS					= 0x26;
	public static final int CMD_ALARM_HISTORY_REQ			= 0x27;
	public static final int CMD_ALARM_HISTORY_REP			= 0x28;
	public static final int CMD_PING						= 0x80;
	public static final int CMD_PONG						= 0x81;
	
	public static final String CMD_STATUSINFO_REQ_START		= "0";
	public static final String CMD_STATUSINFO_REQ_STOP		= "1";
	
	public static final int CMD_FILETRANS_UPLOAD			= 1;
	public static final int CMD_FILETRANS_DOWNLOAD			= 2;
	public static final int CMD_FILETRANS_CANCEL			= 3;
	public static final int CMD_FILETRANS_ALARM				= 4;
	
	public static final int CMD_FILETRANS_RES_IMPOSSIBLE	= 0;
	public static final int CMD_FILETRANS_RES_POSSIBLE		= 1;
	public static final int CMD_FILETRANS_RES_LOW_DISK_SPACE= 2;
    public static final int CMD_FILETRANS_RES_MAXIMUM_FILE  = 3;
    
	public static final int CMD_FILETRANS_RES_BROKEN		= -1;		// Broken pipe 占쌩삼옙占�
	public static final int CMD_FILETRANS_RES_SUCCESS		= 0x00;
	public static final int CMD_FILETRANS_RES_PART_SUCCESS	= 0x01;
	public static final int CMD_FILETRANS_RES_RETRY			= 0x10;
	public static final int CMD_FILETRANS_RES_FAIL_CRC		= 0x11;
	public static final int CMD_FILETRANS_RES_FILESIZE_ERR	= 0x12;
	public static final int CMD_FILETRANS_RES_FAIL			= 0x99;

	public static final int CNC_CONNECTION_TIMEOUT			= 5000;
	
//	public static final String CNC_CONNECTION_EXCEPTION_UNKOWNHOST		= "uh";
//	public static final String CNC_CONNECTION_EXCEPTION_SUCCESS 		= "connsucc";
//	public static final String CNC_CONNECTION_EXCEPTION_TIMEOUT			= "timeout";
	
	public static final int CMD_LOGIN_REQ_FIELD_ACCOUNT_SIZE	= 30;
	public static final int CMD_LOGIN_REQ_FIELD_PASSWD_SIZE 	= 20;
	public static final int CMD_LOGIN_REQ_FIELD_REG_TOKEN_SIZE	= 512;
	public static final int CMD_FILEUPLOAD_NAME_SIZE			= 32;
	
	/**
	 * C2DM
	 */
	public static final String CNC_INTENT_REGISTER			= "register";
	public static final String CNC_C2DM_SENDER				= "minjung.tiger.yu@gmail.com";
	
	/**
	 * SERVER_DEFAULT
	 */
	public static final String DEFAULT_IP 					= "192.168.0.171";
	public static final String DEFAULT_PORT 				= "9998";
	
	/**
	 * UDP
	 */
	public static final int CNC_UDP_BUFFER_SIZE 			= 8192;
	public static final int CNC_UDP_SOCK_TIMEOUT			= 5000;
	
	public static final int pingTimeout 					= 5000;
	
	public static final int SWIPE_MIN_DISTANCE 				= 50;
	public static final int SWIPE_MAX_OFF_PATH 				= 250;
	public static final int SWIPE_THRESHOLD_VELOCITY 		= 200;
	
	public static final int MTES_HEADER_SIZE = 8;
	
	
	/**
	 * Updater Server
	 */
    public static final String UPDATE_SERVER_URL           = "http://www.s4dm.com/sus/smarti/versionCheck.do";
    public static final String UPDATE_SERVER_URL_TEST      = "http://192.168.0.104:8080/sus/smarti/versionCheck.do";
}
