package com.unomic.cnc;


import java.io.File;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.CharBuffer;
import java.text.DecimalFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CNCUtils implements CNC
{
	public static final boolean DEBUG = true;
	
	/**
	 * time_title_en[6][MTES_MAX_TITLE_CHAR]
	 * @param number
	 * @return
	 */
	public static String timeTitleToString(int number)
	{
		String str = "";
		switch(number)
		{
		case 6750:
			str = "Power On Period";
			break;
		case 6751:
		case 6752:
			str = "Operation Time";
			break;
		case 6753:
		case 6754:
			str = "Cutting Time";
			break;
		case 6755:
		case 6756:
			str = "TMRON on Time";
			break;
		case 6757:
		case 6758:
			str = "Operation Time";
			break;
		}
		return str;
	}
	
	/**
	 * BYTE [] �� EUC-KR�� ���ڵ� �Ѵ�.
	 * @param b
	 * @return
	 */
	public static String encoding(byte [] b)
	{
		try
		{
			return new String(b, "EUC-KR").trim();
		} 
		catch (UnsupportedEncodingException e)
		{
		}
		return "";
	}
	public static String unicodeEncoding(byte[] buff) {
	    try
        {
	        ByteBuffer buffer = ByteBuffer.wrap(buff);
	        buffer.order(ByteOrder.LITTLE_ENDIAN);
	        //buffer.order(ByteOrder.BIG_ENDIAN);
	        CharBuffer chBuf = buffer.asCharBuffer();
	 //       System.out.println("transUnicodeString : " + chBuf.toString().trim());
	        return chBuf.toString().trim();
	    }
	    catch (Exception e)
        {
        }
        return "";
	}

	
	/**
	 * Comma �߰�
	 * @param num
	 * @return
	 */
	public static String addComma(long num)
	{
		String pattern = "#,###";
		DecimalFormat format = new DecimalFormat(pattern);
		return format.format(num);
	}
	
	/**
	 * ���õ� ������ �����Ѵ�.
	 * @param path
	 * @return
	 */
	public static boolean deleteFile(String path)
	{
		boolean delete = false;
		
		File file = new File(path);
		if(file.exists())
		delete = file.delete();
		return delete;
	}
	
	/**
	 * ���Ͼ��ε� ���� �޽���
	 * @param retCode
	 * @return
	 */
	public static String fileTransCommandToString(int retCode)
	{
		String response = "";
		switch(retCode)
		{
		case 0:
			response = "Upload/Download impossible.";
			break;
		case 1:
			response = "Upload/Download possible.";
			break;
		case 2:
			response = "File upload success.";
			break;
		case 3:
			response = "File upload fail.";
			break;
		}
		return response;
	}
	
	/**
	 * Command�� ���� ���·� �����Ѵ�.
	 * @param command
	 * @return
	 */
	public static String commandToString(int command)
	{
		String strCommand = null;
		switch(command)
		{
		case CMD_LOGIN_REQ:
			strCommand = "CMD_LOGIN_REQ";
			break;
		case CMD_LOGIN_REP:
			strCommand = "CMD_LOGIN_REP";
			break;
		case CMD_LOGOUT_REQ:
			strCommand = "CMD_LOGOUT_REQ";
			break;
		case CMD_STATUSINFO_REQ:
			strCommand = "CMD_STATUSINFO_REQ";
			break;
		case CMD_STATUSINFO_REP:
			strCommand = "CMD_STATUSINFO_REP";
			break;
		case CMD_FILELIST_REQ:
			strCommand = "CMD_FILELIST_REQ";
			break;
		case CMD_FILELIST_REP:
			strCommand = "CMD_FILELIST_REP";
			break;
		case CMD_FILETRANS_REQ:
			strCommand = "CMD_FILETRANS_REQ";
			break;
		case CMD_FILETRANS_REP:
			strCommand = "CMD_FILETRANS_REP";
			break;
		case CMD_FILETRANS_REPORT:
			strCommand = "CMD_FILETRANS_REPORT";
			break;
		case CMD_ALARM_HISTORY_REP:
			strCommand = "CMD_ALARM_HISTORY_REP";
			break;
		}
		return strCommand;
	}
	
	
	/**
	 * PORT ����.
	 * @param port
	 * @return
	 */
	public static boolean isMatchedPort(String port)
	{
		Pattern portPattern = Pattern.compile("^[123456789]|\\d{2,4}|[12345]\\d{4}|6[1234]\\d{3}|65[1234]\\d{2}|655[12]\\d{1}|6553[12345]");
		Matcher matcherp = portPattern.matcher(port);
		return matcherp.matches();
	}
	
	/**
	 * IP ����.
	 * @param ip
	 * @return
	 */
	public static boolean isMatchedIp(String ip)
	{
		Pattern ipPattern = Pattern.compile("^(((\\d{1,2})|(1\\d{2})|(2[0-4]\\d)|(25[0-5]))\\.){3}((\\d{1,2})|(1\\d{2})|(2[0-4]\\d)|(25[0-5]))");
		Matcher matcheri = ipPattern.matcher(ip);
		return matcheri.matches();
	}
	

	/**
	 * MAC ����.
	 * @param mac
	 * @return
	 */
	public static boolean isMatchMac(String mac)
	{
		Pattern macPattern = Pattern.compile("[0-9a-fA-F]{2}[-:][0-9a-fA-F]{2}[-:][0-9a-fA-F]{2}[-:][0-9a-fA-F]{2}[-:][0 -9a-fA-F]{2}[-:][0-9a-fA-F]{2}");
		Matcher matcheri = macPattern.matcher(mac);
		return matcheri.matches();
	}
	
	public static void Logi(String mac,String a)
	{
		System.out.println(mac + a);
	}

	public static void Logw(String mac,String a)
	{
		System.out.println(mac + a);
	}

	public static void Loge(String mac,String a)
	{
		System.out.println(mac + a);
	}
}
