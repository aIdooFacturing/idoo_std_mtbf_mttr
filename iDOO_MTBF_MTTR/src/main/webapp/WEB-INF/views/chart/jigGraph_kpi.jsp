<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	font-family:'Helvetica';
}

</style> 
<script type="text/javascript">
	function getGroup(){
		//var url = "${ctxPath}/chart/getGroup.do";
		var url = "${ctxPath}/chart/getMatInfo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				json = data.dataList;
				
				var option = "<option value='all'>${total}</option>";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#group").html(option);
				
				getDvcList();
			}
		});
	};
	
	var kendolist=[];
	var arr={};
	function getDvcList(){
		var url = "${ctxPath}/chart/getTableData.do";

		var sDate = $("#sDate").val();
		var eDate = $("#eDate").val();
		
		var param = "sDate=" + sDate + 
					"&eDate=" + eDate + " 23:59:59" + 
					"&shopId=" + shopId +
					"&group=" + $("#group").val();
		
		window.localStorage.setItem("jig_sDate", sDate);
		window.localStorage.setItem("jig_eDate", eDate);
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				cBarPoint = 0;
				cPage = 1;
				var json = data.tableData;
				
				var start = new Date(sDate);
				var end = new Date(eDate);
				var n = (end - start)/(24 * 3600 * 1000)+1;
				
				var tr = "";

				wcList = new Array();
				var wc = new Array();
				wc.push("${division}");
				wc.push("${ophour}");
				wc.push("${wait}");
				wc.push("${stop}");
				wc.push("${noconnection}");
				
				wcList.push(wc);
				
				inCycleBar = new Array();
				waitBar = new Array();
				alarmBar = new Array();
				noConnBar = new Array();
				wcName = new Array();
				
				$(json).each(function(idx, data){
					dateList.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
					tmpArray = dateList;
					
					
					wcName.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
						 
					var wc = new Array();
					wc.push(decodeURIComponent(data.name).replace(/\+/gi, " "));
					wc.push(data.inCycle_time);
					wc.push(data.wait_time);
					wc.push(data.alarm_time);
					wc.push(Number(Number(n * 24 * 60 * 60 - (data.inCycle_time + data.wait_time + data.alarm_time )).toFixed(1)));
					wc.push(data.WC);
					
					
					wcList.push(wc);
					
					tmpWcList = wcList;
					
					var incycle = Number(Number(data.inCycle_time/60/60).toFixed(1));
					var wait = Number(Number(data.wait_time/60/60).toFixed(1));
					var alarm = Number(Number(data.alarm_time/60/60).toFixed(1));
					var noconn = Number(24 - (incycle + wait + alarm)).toFixed(1);
					
					inCycleBar.push(incycle);
					waitBar.push(wait);
					alarmBar.push(alarm);
					noConnBar.push(noconn);
					
					tmpInCycleBar = inCycleBar;
					tmpWaitBar = waitBar;
					tmpAlarmBar = alarmBar;
					tmpNoConnBar = noConnBar;
				});
			
				var blank = maxBar - json.length
				maxPage = json.length - maxBar;
				
			
				if(blank >= 0){
					for(var i = 0; i < blank; i++){
						wcName.push("");
						var wc = new Array();
						wc.push("______");
						wc.push("");
						wc.push("");
						wc.push("");
						wc.push("");
						
						wcList.push(wc);
						
						inCycleBar.push(0);
						waitBar.push(0);
						alarmBar.push(0);
						noConnBar.push(0);
					};	
				}else{
					for(var i = 0; i < maxBar - json.length % 10; i++){
						wcName.push("");
						var wc = new Array();
						wc.push("______");
						wc.push("");
						wc.push("");
						wc.push("");
						wc.push("");
						
						wcList.push(wc);
						
						inCycleBar.push(0);
						waitBar.push(0);
						alarmBar.push(0);
						noConnBar.push(0);
					}; 	
				}
				
				
				if(json.length > maxBar){
					reArrangeArray();
					$("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
				}else{
					$("#controller font").html("01 / 01");
				} 
				
				$(".chartTable").remove();
				var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
				var table_total = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
				
				table_total += "<tr class='contentTr' >" + 
									"<td align='center' colspan='16' style='font-weight: bolder; background-color: rgb(34,34,34);' class='content'>${avrg}</td>" + 
								"</tr>";
				
			 	var stDate = new Date($("#sDate").val()) ;
			    var endDate = new Date($("#eDate").val()) ;
			 
			    var btMs = endDate.getTime() - stDate.getTime() ;
			    var btDay = btMs / (1000*60*60*24) ;
								    
				var day_cnt = btDay+1;

				for(var i = 0; i < wcList[0].length; i++){
					table += "<tr class='contentTr'>";
					table_total += "<tr class='contentTr'>";
	
					var bgColor;
					if(i==1){
						bgColor = "#A3D800";
						color = "black";
					}else if(i==2){
						bgColor = "#FF9100";
						color = "black";
					}else if(i==3){
						bgColor = "#C41C00";
						color = "white";
					}else if(i==4){
						bgColor = "#6B6C7C";
						color = "white";
					}else{
						bgColor = "#323232";
					}
					
					for(var j = 0; j < wcList.length; j++){
						if(j==0){
							table += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
							
							if(i!=0) table_total += "<td style='font-weight: bolder; background-color: " + bgColor + "; color : " + color + "; width:" + getElSize(300) + "; ' class='title_'>" + wcList[j][i] + "</td>";
						}else if(j==0 || i==0){
							table += "<td style='font-weight: bolder;  background-color: " + bgColor + ";' class='content' onclick='toDailyChart(\"" + wcList[j][i] + "\"," + "\"" + wcList[j][i+5] + "\")'>" + wcList[j][i] + "</td>";
						}else {
							var n;
							if(typeof(wcList[j][i])=="number"){
								n = Number(wcList[j][i]/60/60).toFixed(1)
							}else{
								n = "";
							};
							
							table += "<td class='content'>" + n + "</td>";
							table_total += "<td class='content'>" + Number(n/day_cnt).toFixed(1) + "</td>";
						};
					};
					table += "</tr>";
					table_total += "</tr>";
				};
				
				table += "</table>";
				table_total += "</table>";
				$("#tableContainer").append(table)
			
				$("#tableContainer_avg").append(table_total)
						
				setEl();
				
				$(".title_").css({
					"width" : getElSize(300),
					"padding" : getElSize(5),
					"font-size" : getElSize(40)
				});
				
				$(".content").css({
					"font-size" : getElSize(40),
					"width" : ($(".contentTr").width() - getElSize(300))/10
				})
				
				chart("chart",[wcList[1][0]
							,wcList[2][0]
							,wcList[3][0]
							,wcList[4][0]
							,wcList[5][0]
							,wcList[6][0]
							,wcList[7][0]
							,wcList[8][0]
							,wcList[9][0]
							,wcList[10][0]]);
				
				//addSeries();
				
			//	$("#tableContainer").css("margin-top",$("#chart").offset().top + $("#chart").height() - $(".mainTable").height() + getElSize(50))
			}
		});
	};

	var barChart;
	var maxBar = 10;
	function chart(id, nameList){

		//0값 라벨 제거
/* 		for(var i=0;i<maxBar;i++ ){
			console.log(alarmBar[i])
			console.log(alarmBar[i]==0)
			if(alarmBar[i]==0){
				alarmBar[i]=null;
			}
			if(inCycleBar[i]==0){
				inCycleBar[i]=null;
			}
			if(waitBar[i]==0){
				waitBar[i]=null;
			}
		} */
		
		$("#kendoChart").kendoChart({
			chartArea: {
				height: getElSize(1000),
				background:"#323232",
			},
			title: false,
            legend: {	//범례표?
             	labels:{
            		font:getElSize(48) + "px sans-serif",
            		color:"white"
            	},
            	stroke: {
            		width:100
            	},
            	position: "bottom",
            	orientation: "horizontal",
                offsetX: getElSize(1010),
//                offsetY: getElSize(800)
           
            },
            render: function(e) {	//범례 두께 조절
                var el = e.sender.element;
                el.find("text")
                    .parent()
                    .prev("path")
                    .attr("stroke-width", getElSize(20));
            },
			seriesDefaults: {	//데이터 기본값 ()
				gap: getElSize(5),
				type: "column" ,
				stack:true,
				spacing: getElSize(1),
 				labels:{
 					font:getElSize(45) + "px sans-serif",	//no working
 					margin:0,
 					padding:0
				}/* , 
				visual: function (e) {
	                return createColumn(e.rect, e.options.color);
	            } */
			},	
			categoryAxis: {	//x축값
                categories: wcName,
                line: {
                    visible: true
                },
                labels:{
                	font:getElSize(48) + "px sans-serif",	
                	color:"white"
				} 
            },
            tooltip: {	//커서 올리면 값나옴
                visible: true,
                format: "{0}%",
                template: "#= series.name #: #= value #"
            },
            valueAxis: {	//간격 y축
                labels: {
                    format: "{0}",
                    color:"white"
                }
//                majorUnit: 100	//간격
            },
  
            
            series: [/* {	//데이터
                labels: {
                    visible: true,
                    color: "black",	
                    background:"gray",
                    font:getElSize(60) + "px sans-serif",	
              	    position: "center",
                  },
                  color : "gray",
                  name: "noConnBar",
                  data: noConnBar		
                }, */{	//데이터
                labels: {
                    visible: true,
                    color: "white",	
                    background:"#A3D800",
                    font:getElSize(60) + "px sans-serif",	
              	    position: "center",
                    margin: {
                  		right : 45
                 	}
//              	    margin:10
                  },
                  color : "#A3D800",

                  name: "${ophour}",
                  data: inCycleBar		
                },{	//데이터
                    labels: {
                        visible: true,
                        background:"#FF9100",
                        font:getElSize(60) + "px sans-serif",	
                        position: "center",
                	    color: "white",
                	    margin: {
//                        	bottom : 100,
                        	left : 45
                        }
                    },
	                    color : "#FF9100",
	                    name: "${wait}",
	                    data: waitBar		
                 },{	//데이터
                    labels: {
                        visible: true,
                        background:"#C41C00",
                        font:getElSize(60) + "px sans-serif",	
//                        position: "center",
                        color: "white",
                        margin: {
//                        	bottom : 100,
                        	right : 45
                        }
                    },
						color : "#C41C00",
						name: "${stop}",
						data: alarmBar		
				}]
		})
		
	}
	
	var wcDataList = new Array();
	var dateList = new Array();
	var wcList = new Array();
	var wcName = new Array();
	var maxPage;
	var cBarPoint = 0;

	
	function reArrangeArray(){
		dateList = new Array();
		inCycleBar = new Array();
		waitBar = new Array();
		alarmBar = new Array();
		noConnBar = new Array();
		wcName = new Array();
		wcList = new Array();
		
		for(var i = cBarPoint; i < (maxBar+cBarPoint); i++){
			dateList[i-cBarPoint] = tmpArray[i];
			inCycleBar[i-cBarPoint] = tmpInCycleBar[i];
			waitBar[i-cBarPoint] = tmpWaitBar[i];
			alarmBar[i-cBarPoint] = tmpAlarmBar[i];
			noConnBar[i-cBarPoint] = tmpNoConnBar[i];
			wcName[i-cBarPoint] = tmpArray[i];
		};
		
		for(var i = cBarPoint; i < (maxBar+cBarPoint+1); i++){
			wcList[i-cBarPoint] = tmpWcList[i];	
		};
		
		var wc = new Array();
		wc.push("${division}");
		wc.push("${ophour}");
		wc.push("${wait}");
		wc.push("${stop}");
		wc.push("${noconnection}");
		//wcList.push(wc);
		wcList[0] = wc;
		
		$("#controller font").html(String(cPage) + " / " + Math.ceil((maxPage + maxBar) / 10));
	};

	var cPage = 1;
	
	function nextBarArray(){
		if(cBarPoint>=maxPage || maxPage<=0){
			alert("${end_of_chart}");
			return;
		};
		cBarPoint += maxBar;
		cPage++;
		reArrangeArray();
		
		$(".chartTable").remove();
		
		var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		var table_total = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		
		table_total += "<tr class='contentTr' >" + 
							"<td class='content' align='center' colspan='19' style='font-weight: bolder; background-color: rgb(34,34,34);'>${avrg}</td>" + 
						"</tr>";
		
	 	var stDate = new Date($("#sDate").val()) ;
	    var endDate = new Date($("#eDate").val()) ;
	 
	    var btMs = endDate.getTime() - stDate.getTime() ;
	    var btDay = btMs / (1000*60*60*24) ;
						    
		var day_cnt = btDay+1;

		for(var i = 0; i < wcList[0].length; i++){
			table += "<tr class='contentTr'>";
			table_total += "<tr class='contentTr'>";
			
			var bgColor;
			if(i==1){
				bgColor = "#A3D800";
				color = "black";
			}else if(i==2){
				bgColor = "#FF9100";
				color = "black";
			}else if(i==3){
				bgColor = "#C41C00";
				color = "white";
			}else if(i==4){
				bgColor = "#6B6C7C";
				color = "white";
			}else{
				bgColor = "#323232";
			}
			
			for(var j = 0; j < wcList.length; j++){
				if(j==0){
					table += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
					
					if(i!=0) table_total += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
				}else if(j==0 || i==0){
					table += "<td class='content_' style='font-weight: bolder; background-color: " + bgColor + "; width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\"," + "\"" + wcList[j][i+5] + "\")'>" + wcList[j][i] + "</td>";
				}else {
					var n;
					if(typeof(wcList[j][i])=="number"){
						n = Number(wcList[j][i]/60/60).toFixed(1)
					}else{
						n = "";
					};
					table += "<td class='content_'>" + n + "</td>";
					table_total += "<td class='content_' style='width:" + $("#chart").width()/(maxBar-1) + "'>" + Number(n/day_cnt).toFixed(1) + "</td>";
				};
			};
			table += "</tr>";
			table_total += "</tr>";
		};
		
		table += "</table>";
		table_total += "</table>";
		$("#tableContainer").append(table)
		
		$("#tableContainer_avg").append(table_total)
		
		setEl();
		chart("chart",[wcList[1][0]
		,wcList[2][0]
		,wcList[3][0]
		,wcList[4][0]
		,wcList[5][0]
		,wcList[6][0]
		,wcList[7][0]
		,wcList[8][0]
		,wcList[9][0]
		,wcList[10][0]]);
		
		//addSeries();
		
		$(".title_").css({
			"width" : getElSize(300),
			"padding" : getElSize(5),
			"font-size" : getElSize(40)
		});
		
		$(".content_, .content").css({
			"font-size" : getElSize(40),
			"width" : ($(".contentTr").width() - getElSize(300))/10
		})
		
		$("#arrow_left").attr("src", ctxPath + "/images/left_en.png");
		if(Math.ceil((maxPage + maxBar) / 10)==cPage) $("#arrow_right").attr("src", ctxPath + "/images/right_dis.png");
	};

	function prevBarArray(){
		if(cBarPoint<=0){
			alert("${first_of_chart}");
			return;
		};
		cBarPoint -= maxBar;
		cPage--;
		reArrangeArray();
		
		$(".chartTable").remove();
		var table = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		var table_total = "<table style='width: 100%; text-align: center; color: white;' class='chartTable'>";
		
		table_total += "<tr class='contentTr' >" + 
							"<td class='content' align='center' colspan='19' style='font-weight: bolder; background-color: rgb(34,34,34);'>${avrg}</td>" + 
						"</tr>";
		
	 	var stDate = new Date($("#sDate").val()) ;
	    var endDate = new Date($("#eDate").val()) ;
	 
	    var btMs = endDate.getTime() - stDate.getTime() ;
	    var btDay = btMs / (1000*60*60*24) ;
						    
		var day_cnt = btDay+1;

		for(var i = 0; i < wcList[0].length; i++){
			table += "<tr class='contentTr'>";
			table_total += "<tr class='contentTr'>";
			
			var bgColor;
			if(i==1){
				bgColor = "#A3D800";
				color = "black";
			}else if(i==2){
				bgColor = "#FF9100";
				color = "black";
			}else if(i==3){
				bgColor = "#C41C00";
				color = "white";
			}else if(i==4){
				bgColor = "#6B6C7C";
				color = "white";
			}else{
				bgColor = "#323232";
			}
			
			for(var j = 0; j < wcList.length; j++){
				if(j==0){
					table += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
					
					if(i!=0) table_total += "<td style='font-weight: bolder; background-color: " + bgColor + "; color:" + color + "; width:" + getElSize(300) + ";' class='title_'>" + wcList[j][i] + "</td>";
				}else if(j==0 || i==0){
					table += "<td class='content_' style='font-weight: bolder; background-color: " + bgColor + "; width:" + $("#chart").width()/(maxBar-1) + "' onclick='toDailyChart(\"" + wcList[j][i] + "\"," + "\"" + wcList[j][i+5] + "\")'>" + wcList[j][i] + "</td>";
				}else {
					var n;
					if(typeof(wcList[j][i])=="number"){
						n = Number(wcList[j][i]/60/60).toFixed(1)
					}else{
						n = "";
					};
					
					table += "<td class='content'>" + n + "</td>";
					table_total += "<td class='content' style='width:" + $("#chart").width()/(maxBar-1) + "'>" + Number(n/day_cnt).toFixed(1) + "</td>";
				};
			};
			table += "</tr>";
			table_total += "</tr>";
		};
		
		table += "</table>";
		table_total += "</table>";
		$("#tableContainer").append(table)

		$("#tableContainer_avg").append(table_total)
		
		setEl();
		chart("chart",[wcList[1][0]
		,wcList[2][0]
		,wcList[3][0]
		,wcList[4][0]
		,wcList[5][0]
		,wcList[6][0]
		,wcList[7][0]
		,wcList[8][0]
		,wcList[9][0]
		,wcList[10][0]]);
		//addSeries();
		
		$(".title_").css({
			"width" : getElSize(300),
			"padding" : getElSize(5),
			"font-size" : getElSize(40)
		});
		
		$(".content_, .content").css({
			"font-size" : getElSize(40),
			"width" : ($(".contentTr").width() - getElSize(300))/10
		})
		
		$("#arrow_right").attr("src", ctxPath + "/images/right_en.png");
		if(cBarPoint==0) $("#arrow_left").attr("src", ctxPath + "/images/left_dis.png");
	};

	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$("#sDate").val(caldate(7));
		$("#eDate").val(year + "-" + month + "-" + day);
	};
	
	function caldate(day){
		 var caledmonth, caledday, caledYear;
		 var loadDt = new Date();
		 var v = new Date(Date.parse(loadDt) - day*1000*60*60*24);
		 
		 caledYear = v.getFullYear();
		 
		 if( v.getMonth() < 9 ){
		  caledmonth = '0'+(v.getMonth()+1);
		 }else{
		  caledmonth = v.getMonth()+1;
		 }
		 if( v.getDate() < 9 ){
		  caledday = '0'+v.getDate();
		 }else{
		  caledday = v.getDate();
		 }
		 return caledYear + "-" + caledmonth+ '-' + caledday;
		}
		
	
	var handle = 0;		
	
	$(document).ajaxStart(function(){
		$.showLoading();
	});
	$(document).ajaxStop(function(){
		$.hideLoading();
	});
	
	$(function(){

		createNav("kpi_nav",3);
		getGroup();
		setDate();	
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		setEl();
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			//"background-color" : "black",
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#bannerDiv").css({
			"position" : "absolute",
			"width" : getElSize(1500),
			"display" : "none",
			"height" : getElSize(200),
			"border-radius" : getElSize(20),
			"padding" : getElSize(50),
			"background-color" : "lightgray",
			"z-index" : -9
		});

	
		$("#bannerDiv").css({
			"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
			"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
		});
		
		$("#bannerDiv input[type='text']").css({
			"width" : getElSize(1200),
			"font-size" : getElSize(50)
		});
		
		$("#bannerDiv button").css({
			"margin" : getElSize(50),
			"font-size" : getElSize(50)
		});
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999,
			"pointer-events" : "none"
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999,
			"pointer-events" : "none"
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").not("#intro_back").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#content_table td").css({
			"color" : "white",
			"font-size" : getElSize(50)
		});
		
		$("select, button, input").css({
			"font-size" : getElSize(50),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#chart").css({
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20),
			"margin-top" : getElSize(20),
		});
		
		$("#arrow_left").css({
			"width" : getElSize(60),
			"margin-right" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#arrow_right").css({
			"width" : getElSize(60),
			"margin-left" : getElSize(50), 
			"cursor" : "pointer"
		});
		
		$("#controller").css({
			"display" : "table",
			"background-color" : "black",
			"border-radius" : getElSize(70),
			"position" : "relative",
			"bottom" : getElSize(775),
			"left" : getElSize(1500),
			"padding" : getElSize(15),
			"vertical-align" : "middle"
		});
		
		$("img").css({
			"display" : "inline"
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#search").css({
			"width" : getElSize(200)
		})
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function goGraph(){
		location.href = "${ctxPath}/chart/performanceReport.do"
	};
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="bannerDiv">
	<Center>
		<input type="text" id="banner" size="100"><input type="color" id="color"><br>
		<button onclick="addBanner();">적용</button>
		<button onclick="cancelBanner();">미적용</button>
	</Center>
	</div>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" style="display: none" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right' style="display: none" >
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/analysis_left.png" class='menu_left' style="display: none"  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/green_right.png" class='menu_right' style="display: none" >
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'><spring:message code="performance_chart"></spring:message></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left nav' style="display: none" >
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top;">
					<table id="content_table" style="width: 100%"> 
						<Tr>
							<td><select id="group"></select>
								<spring:message code="op_period"></spring:message> <input type="date" class="date" id="sDate"> ~ <input type="date" class="date" id="eDate"> 
								<button id="search" onclick="getDvcList()" style="cursor: pointer;"><i class="fa fa-search" aria-hidden="true"></i></button>
								<button onclick="goGraph()" id="table"><spring:message code="table"></spring:message> </button>
							</td>
						</Tr>		
						<tr>
							<td>
								<div id="kendoChart" style="margin-left: 100px">
								</div>
<!-- 								<div id="chart" style="width: 100%">
								</div>
 -->								
								<div id="tableContainer" width="100%">
								</div>
								
								<div id="tableContainer_avg" width="100%" >
								</div>
								
								<div id="controller">
									<img alt="" src="${ctxPath }/images/left_en.png" id="arrow_left"  onclick="prevBarArray();">
									<font style="display: table-cell; vertical-align:middle">1 / 0</font>
									<img alt="" src="${ctxPath }/images/right_en.png" id="arrow_right" onclick="nextBarArray();">
								</div>
							</td>
						</tr>			
					</table> 
				</td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class="nav_span"></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back" style="display: none"></div>
	<span id="intro"></span>
</body>
</html>