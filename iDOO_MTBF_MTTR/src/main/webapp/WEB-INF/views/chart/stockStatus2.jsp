<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}

</style> 
<script type="text/javascript">
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	var handle = 0;
	
	$(function(){
		createNav("inven_nav", 8);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#updateBtn").css({
			"position": "absolute",
			"top": getElSize(180),
			"left": getElSize(3220)
		})
		
		$("#stockUp").css({
/* 			"height": getElSize(200),
			"width": getElSize(200), */
			"font-size": getElSize(70),
			"padding-left": getElSize(20),
			"padding-right": getElSize(20),
			"cursor":"pointer"
		})
		
		$("#stockUpt").css({
/* 			"height": getElSize(200),
			"width": getElSize(200), */
			"font-size": getElSize(70),
			"padding-left": getElSize(20),
			"padding-right": getElSize(20),
			"cursor":"pointer"
		})
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	
	var kendotable;
	function titleName(idx){
		if(idx==1){
			return "자재창고"
		}else if(idx==2){
			return "공정창고"
		}else if(idx==3){
			return "외주창고"
		}else if(idx==4){
			return "완성창고"
		}else if(idx==5){
			return "불량창고"
		}else{
			return "기타"
		}
	}
	$(document).ready(function(){
		kendotable = $("#grid").kendoGrid({
			height:getElSize(1680)
			,editable:true
			,filterable: {
			      mode: "row"
			}
			,columns:[{
				field:"idx"
				,groupHeaderTemplate: "#=titleName(value)#"
			},{
				title:"${mat_prd_no}"
				,field:"prdNo"
				,filterable: {
					cell: {
		                   suggestionOperator: "contains"
						}
				}
				,width:getElSize(500)
			},{
				title:"${Currentwarehouse}"
				,field:"proj"
				,filterable: {
					cell: {
		                   suggestionOperator: "contains"
						}
				}
				,width:getElSize(500)
			},{
				title:"${Basic_stock}"
					,field:"iniohdCnt"
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
					,width:getElSize(250)
				},{
				title:"${income_cnt}"
					,field:"rcvCnt"
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
					,width:getElSize(250)
				},{
				title:"${release_count}"
					,field:"issCnt"
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
					,width:getElSize(250)
				},{
				title:"${stock_cnt}"
					,field:"cnt"
					,filterable: {
	                    cell: {
	                        enabled: false
	                    }
		             }
					,width:getElSize(250)
				}]
		}).data("kendoGrid")
		getTable();
	})
	
	function getTable(){
		url = "${ctxPath}/chart/getStockList.do";
		$.showLoading()
		
		var param = "date=" + $(".date").val();
		nowDateInsert = $(".date").val();
		console.log(param)
		console.log(nowDateInsert)
		$.ajax({
			url: url,
			data: param,
			dataType: "json",
			type: "post",
			success: function (data) {
				var json = data.dataList;
				console.log(json)
				
				$(json).each(function (idx, data) {
					data.proj=decode(data.proj)
				});
				
				
				
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: false,
					group: { field: "idx" },
					sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "idx" , dir:"asc"
                    }],
					height: 500,
					schema: {
						model: {
							id: "id",
							fields: {
								prdNo: { editable: false },
								item: { editable: false },
							}
						}
					}
				});
				
				kendotable.setDataSource(kendodata);
				grid = $("#grid").data("kendoGrid");
				grid.hideColumn("idx");
				$.hideLoading()
			}
		})
	}
	
	function pageMove(){
		
		//서버 TIME 시간가져오기 
		url = "${ctxPath}/chart/getServerTime.do";
		$.showLoading()
		
		var param = "date=" + $(".date").val();
		nowDateInsert = $(".date").val();
		$.ajax({
			url: url,
			data: param,
//			dataType: "json",
			type: "post",
			success: function (data) {
				if(data < "09:10" && data >= "06:30"){
					alert("9시10분 이후부터 재고수정이 가능합니다");
				}else if(data=="fail"){
					alert("관리자에게 문의해주세요.")
				}else{
					location.href='${ctxPath}/chart/stockUptPg.do'
				}
				$.hideLoading()
				
			}
		})
//		location.href='${ctxPath}/chart/stockUptPg.do'
	}
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/mainten_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="updateBtn" >
<%-- 						<input type="button" value="공정까지 수정" id="stockUp" onclick="location.href='${ctxPath}/chart/stockProUpPage.do'">
 --%>						
 						<input type="button" value="재고수정" id="stockUpt" onclick="pageMove()">
<%--  						<input type="button" value="수정" id="stockUp" onclick="location.href='${ctxPath}/chart/stockUpPage.do'"> --%>
					</div>
					<div id="wrapper">
						<table style="width: 100%">
							<Tr>
								<Td style="text-align: center; vertical-align: middle;">
									<div id="grid">
									</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	