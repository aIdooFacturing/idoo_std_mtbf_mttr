<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<spring:message code="start_time" var="start_time"></spring:message>
<spring:message code="end_time" var="end_time"></spring:message>
<spring:message code="fixed_time" var="fixed_time"></spring:message>
<spring:message code="alarm" var="alarm"></spring:message>
<spring:message code="content" var="content"></spring:message>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}


.table_title{
	background-color : #222222;
	color : white;
}
	
</style>
<title>Dash Board</title>
<script type="text/javascript">
	var addFaulty = "${addFaulty}";
	var $prdNo = "${prdNo}";
	var $cnt = "${cnt}";
	
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<style type="text/css">
	body{
		overflow :hidden;
	}
</style>
<script type="text/javascript">
var shopId = 1;

var save_type = "init"

$(function(){
	$(".excel").click(csvSend);
	setEl();
	
	//setDate($("#searchForm #sDate"));
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	$(".menu").click(goReport);
	$("#panel_table td").addClass("unSelected_menu");
	$("#menu104").removeClass("unSelected_menu");
	$("#menu104").addClass("selected_menu");
	
	/* getPrdNoList($("#searchForm #prdNo"));
	getCheckTyList($("#searchForm #checkTy"));
	getProcessList($("#searchForm #process"));
	getPartList($("#searchForm #part"));
	getSituationTyList($("#searchForm #situationTy"));
	getSituationList($("#searchForm #situation"));
	getCauseList($("#searchForm #cause"));
	getGChTyList($("#searchForm #gchTy"));
	getGChList($("#searchForm #gch"));
	getActionList($("#searchForm #action"));
	getDevieList($("#searchForm #device")); */
	
	//$(".date").change(getLeadTime);
	//$("#group").change(getLeadTime);
	
	addRow();
	$("#insertForm").css("z-index", 999);
	showCorver();
});

/* function getFaultList(){
	var url = ctxPath + "/chart/getFaultList.do";
	var prdNo = $("#searchForm #prdNo").val();
	var chkTy = $("#searchForm #checkTy").val();
	var process = $("#searchForm #process").val();
	var part = $("#searchForm #part").val();
	var situation = $("#searchForm #situation").val();
	var situationTy = $("#searchForm #situationTy").val();
	var dvcId = $("#searchForm #device").val();
	var cause = $("#searchForm #cause").val();
	var gchTy = $("#searchForm #gchTy").val();
	var gch = $("#searchForm #gch").val();
	var action = $("#searchForm #action").val();
	
	if(prdNo==null) prdNo = "0";
	if(chkTy==null) chkTy = "0";
	if(process==null) process = "0";
	if(part==null) part = "0";
	if(situation==null) situation = "0";
	if(situationTy==null) situationTy = "0";
	if(dvcId==null) dvcId = "0";
	if(cause==null) cause = "0";
	if(gchTy==null) gchTy = "0";
	if(gch==null) gch = "0";
	if(action==null) action = "0";
	
	var param = "prdNo=" + prdNo + 
				"&sDate=" + $("#searchForm #sDate").val() +
				"&chkTy=" + chkTy +
				"&prdPrc=" + process +
				"&part=" + part +
				"&situTy=" + situationTy + 
				"&situ=" + situation + 
				"&dvcId=" + dvcId + 
				"&cause=" + cause + 
				"&gchTy=" + gchTy + 
				"&gch=" + gch +
				"&action=" + action + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		type : "post",
		dataType : "json",
		data : param,
		success : function(data){
			var json = data.dataList;

			$("#tbody").empty();
			$(json).each(function(idx, data){
				if(classFlag){
					className = "row2"
				}else{
					className = "row1"
				};
				classFlag = !classFlag;
				
				var checkTy = document.createElement("select");
				var prdNo = document.createElement("select");
				var process = document.createElement("select");
				var device = document.createElement("select");
				var sDate = document.createElement("input");
				sDate.setAttribute("type", "date");
				var text = document.createElement("input");
				text.setAttribute("class", "checker");
				var cnt = document.createElement("input");
				cnt.setAttribute("class", "cnt");
				cnt.setAttribute("size", 5);
				var part = document.createElement("select");
				var situationTy = document.createElement("select");
				var situation = document.createElement("select");
				var cause = document.createElement("select");
				var gchTY = document.createElement("select");
				var gch = document.createElement("select");
				var action = document.createElement("select");
				
				var tr = document.createElement("tr");
				tr.setAttribute("class", className);
				tr.setAttribute("id", "tr" + data.id);
				
				var checkTy_td = document.createElement("td");
				checkTy_td.append(checkTy);
				tr.append(checkTy_td);
				
				var prdNo_td = document.createElement("td");
				prdNo_td.append(prdNo);
				tr.append(prdNo_td);
				
				var process_td = document.createElement("td");
				process_td.append(process);
				tr.append(process_td);
				
				var device_td = document.createElement("td");
				device_td.append(device);
				tr.append(device_td);
				
				var sDate_td = document.createElement("td");
				sDate_td.append(sDate);
				tr.append(sDate_td);
				
				var text_td = document.createElement("td");
				text_td.append(text);
				tr.append(text_td);
				
				var part_td = document.createElement("td");
				part_td.append(part);
				tr.append(part_td);
				
				var situ_td = document.createElement("td");
				situ_td.append(situationTy);
				tr.append(situ_td);
				
				var situTy_td = document.createElement("td");
				situTy_td.append(situation);
				tr.append(situTy_td);
				
				var cause_td = document.createElement("td");
				cause_td.append(cause);
				tr.append(cause_td);
				
				var gchkTy_td = document.createElement("td");
				gchkTy_td.append(gchTY);
				tr.append(gchkTy_td);
				
				var gch_td = document.createElement("td");
				gch_td.append(gch);
				tr.append(gch_td);
				
				var cnt_td = document.createElement("td");
				cnt_td.append(cnt);
				tr.append(cnt_td);
				
				var action_td = document.createElement("td");
				action_td.append(action);
				tr.append(action_td); 
				
				var button_td = document.createElement("td");
				var button = document.createElement("button");
				button.setAttribute("id", "b" + data.id);
				var button_text = document.createTextNode("삭제");
				button.append(button_text);
				button_td.append(button)
				tr.append(button_td);
				
				$("#tbody").append(tr);
				
				$(button).click(function(){
					chkDel(data.id)
				});
				
				getCheckTyList($(checkTy), data.chkTy);
				getPrdNoList($(prdNo), data.prdNo);
				getProcessList($(process), data.prdPrc);
				getDevieList($(device), data.dvcId);
				setDate($(sDate), data.sDate);
				getChecker($(text), data.id);
				getPartList($(part), data.part);
				getSituationList($(situation), data.situ);
				getSituationTyList($(situationTy), data.situTy);
				getCauseList($(cause), data.cause);
				getGChTyList($(gchTY), data.gchTy);
				getCnt($(cnt), data.id);
				getGChList($(gch), data.gch);
				getActionList($(action), data.action); 	
				
			});
			
			//setEl();
			
			$(".alarmTable, .alarmTable tr, .alarmTable td").css({
				"border": getElSize(5) + "px solid rgb(50,50,50)"
			});
			
			$(".alarmTable td").css({
				"padding" : getElSize(10),
				"height": getElSize(100),
			});
			
			$(".contentTr").css({
				"font-size" : getElSize(60)
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			//$("#wrapper div:last").remove();
			//scrolify($('.alarmTable'), getElSize(1350));
			//$("#wrapper div:last").css("overflow", "auto") 
		}
	});
}; */

var faultId;
function chkDel(id){
	$("#delDiv").css("z-index",9);
	
	faultId = id;
};

function noDel(){
	$("#delDiv").css("z-index",-1);
};

function okDel(){
	var url = ctxPath + "/chart/okDel.do";
	var param = "id=" + faultId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			noDel();
			$("#tr" + faultId).remove();
		}
	});
};

function decode(str){
	return decodeURIComponent(str).replace(/\+/gi, " ")
};

var shopId = 1;
function getDevieList(el, obj){
	var url = ctxPath + "/chart/getDevieList.do"
	var param = "shopId=" + shopId + 
				"&prdNo=" + $(obj).val();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			el.html(options);
		}
	});	
};

function getProcessList(el, val){
	var url = ctxPath + "/chart/getCheckTyList.do"
	var param = "index=" + 3; 	
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			el.html(options);
			if(typeof(val)!="undefined"){
				el.val(val);	
			}
		}
	});	
};

function getPartList(el, val){
	var url = ctxPath + "/chart/getCheckTyList.do"
	var param = "index=" + 4;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			el.html(options);
			if(typeof(val)!="undefined"){
				el.val(val);	
			}
		}
	});	
};

function getSituationTyList(el, val){
	var url = ctxPath + "/chart/getCheckTyList.do"
	var param = "index=" + 5;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			el.html(options);
			if(typeof(val)!="undefined"){
				el.val(val);	
			}
		}
	});	
};

function getCnt(el, id){
	var url = ctxPath + "/chart/getCnt.do";
	var param = "id=" + id;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			el.val(data);
		}
	});	
};

function getChecker(el, id){
	var url = ctxPath + "/chart/getChecker.do";
	var param = "id=" + id;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			el.val(decode(data));
		}
	});	
};

function getSituationList(el, val){
	var url = ctxPath + "/chart/getCheckTyList.do"
	var param = "index=" + 6;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			el.html(options);
			if(typeof(val)!="undefined"){
				el.val(val);	
			}
		}
	});	
};

function getCauseList(el, val){
	var url = ctxPath + "/chart/getCheckTyList.do"
	var param = "index=" + 7;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			el.html(options);
			if(typeof(val)!="undefined"){
				el.val(val);	
			}
		}
	});	
};

function getGChTyList(el,val){
	var url = ctxPath + "/chart/getCheckTyList.do"
	var param = "index=" + 8;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			el.html(options).change(getGch);
			if(typeof(val)!="undefined"){
				el.val(val);	
			}
		}
	});	
};

function getGChList(ty, el){
	//var url = ctxPath + "/chart/getCheckTyList.do"
	var url;
	if(ty=="com"){
		url = ctxPath + "/chart/getComList.do"		
	}else if(ty=="worker"){
		url = ctxPath + "/common/getWorkerList.do"
	}else{
		el.html("<option value='0'>선택</option>");
		return; 
	}
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				
			});
			
			el.html(options);
			if(typeof(val)!="undefined"){
				el.val(val);	
			}
		}
	});	
};

function getActionList(el, val){
	var url = ctxPath + "/chart/getCheckTyList.do"
	var param = "index=" + 10;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			el.html(options);
			if(typeof(val)!="undefined"){
				el.val(val);	
			}
		}
	});	
};

function getCheckTyList(el, val){
	var url = ctxPath + "/chart/getCheckTyList.do"
	var param = "index=" + 2;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
			
			el.html(options);
			
			if(typeof(val)!="undefined"){
				el.val(val);	
			}
		}
	});	
};

function getPrdNoList(el, val){
	var url = ctxPath + "/common/getPrdNoList.do"
	
	$.ajax({
		url : url,
		dataType : "json",
		async : false,
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var options = "<option value='0'>선택</option>";	
			
			$(json).each(function(idx, data){
				options += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
			});
			
			el.html(options).change(function() {getDevieList($("#dvcId_form").children("td").children("select"), this)});
			
			if(typeof(val)!="undefined"){
				el.val(val);	
			}
		}
	});
};

var className = "";
var classFlag = true;

var menu = false;
function getMousePos(){
	$(document).on("mousemove", function(e){
		var target = $("#menu_btn").width();

		if((e.pageX <= target && e.pageY <= target) || panel){
			if(!menu){
				$("#menu_btn").animate({
					"left" : 0,
					"top" : getElSize(20),
				});
				menu = true;
			};
		}else{
			if(menu){
				$("#menu_btn").animate({
					"left" : -getElSize(100),
				});	
				menu = false;
			};
		};
	});
};

function goReport(){
	var type = this.id;
	var url;
	if(type=="menu0"){
		url = "${ctxPath}/chart/main.do";
		location.href = url;
	}else if(type=="menu1"){
		url = "${ctxPath}/chart/performanceReport.do";
		location.href = url;
	}else if(type=="menu2"){
		url = "${ctxPath}/chart/alarmReport.do";
		location.href = url;
	}else if(type=="menu3"){
		url = "${ctxPath}/chart/main3.do";
		location.href = url;
	}else if(type=="menu4"){
		url = ctxPath + "/chart/rotateChart.do";
		location.href = url;
	}else if(type=="menu6"){
		url = "${ctxPath}/chart/toolManager.do";
		location.href = url;
	}else if(type=="menu5"){
		url = "${ctxPath}/chart/traceManager.do";
		location.href = url;
	}else if(type=="menu8"){
		url = "${ctxPath}/chart/jigGraph.do";
		location.href = url;
	}else if(type=="menu9"){
		url = "${ctxPath}/chart/wcGraph.do";
		location.href = url;
	}else if(type=="menu10"){
		url = "${ctxPath}/chart/addTarget.do";
		location.href = url;
	}else if(type=="menu7"){
		url = "${ctxPath}/chart/singleChartStatus.do";
		location.href = url;
	}else if(type=="menu11"){
		url = "${ctxPath}/chart/prdStatus.do";
		location.href = url;
	}else if(type=="menu99"){
		$("#bannerDiv").css({
			"z-index" : 9999
		});
		getBanner();
		closePanel();
		panel = false;
	}else if(type=="menu12"){
		url = "${ctxPath}/chart/performanceAgainstGoal.do";
		location.href = url;
	}else if(type=="menu100"){
		url = "${ctxPath}/chart/stockStatus.do";
		location.href = url;
	}else if(type=="menu101"){
		url = "${ctxPath}/chart/programManager.do";
		location.href = url;
	}else if(type=="menu102"){
		url = "${ctxPath}/chart/leadTime.do";
		location.href = url;
	}else if(type=="menu103"){
		url = "${ctxPath}/chart/faulty.do";
		location.href = url;
	}else if(type=="menu104"){
		url = "${ctxPath}/chart/addFaulty.do";
		location.href = url;
	}
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			location.reload();
			//chkBanner();
		}
	});
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(el, val){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	el.val(year + "-" + month + "-" + day);
	if(typeof(val)!="undefined"){
		el.val(val);	
	}
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight, 
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".table_title").css({
		"padding" : getElSize(10)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".checker").css({
		"width" : getElSize(200)	
	});
	
	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#jigSelector, .goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50)
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".alarmTable, .alarmTable tr, .alarmTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".alarmTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100),
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -999,
		"opacity" : 0
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$("#delDiv").css({
		"position" : "absolute",
		"width" : getElSize(700),
		"height" :getElSize(200),
		"background-color" : "#444444",
		"color" : "white"
	});
	
	$("#delDiv").css({
		"top" : (window.innerHeight/2) - ($("#delDiv").height()/2),
		"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
		"z-index" : -1,
		"border-radius" : getElSize(10),
		"padding" : getElSize(20)
	});
	
	
	$("#delDiv button").css({
		"font-size" :getElSize(40)
	});
	
	$("#prdNo, #sDate, #checkTy, #process, #part, #situationTy, #situation, #device, #cause, #gchTy, #gch, #action, #addRow_btn, #saveRow_btn, button").css({
		"font-size" : getElSize(40)
	});
	
	$("#insertForm").css({
		"position" : "absolute",
		"z-index" : -999,
		"width" : getElSize(2800),
	});
	 
	$("#insertForm table td").css({
		"font-size" : getElSize(100),
		"padding" : getElSize(30),
		"background-color" : "#323232"
	});
	
	$("#insertForm button").css({
		"font-size" : getElSize(100),
		"margin" : getElSize(20)
	});
	
	$("#insertForm").css({
		"top" :getElSize(100),
		"left" : (window.innerWidth	/2) - ($("#insertForm").width()/2)
	});
	
	$(".table_title").css({
		"background-color" : "#222222",
		"color" : "white"
	});
	
	chkBanner();  
};

function updateCd(el, val){
	var url = ctxPath + "/chart/updateCd.do";
	var id = $(el).parent("td").parent("tr").attr("id").substr(2);
	var cd = $(el).val();
	
	var param = "id=" + id + 
				"&cd=" + cd + 
				"&val=" + val;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success :function(data){
		}
	});
};

function addRow(){
	if(classFlag){
		className = "row2"
	}else{
		className = "row1"
	};
	classFlag = !classFlag;
	
	var checkTy = document.createElement("select");
	var prdNo = document.createElement("select");
	prdNo.setAttribute("id", "prdNo_selector_form");
	var process = document.createElement("select");
	var device = document.createElement("select");
	var sDate = document.createElement("input");
	sDate.setAttribute("type", "date");
	var text = document.createElement("input");
	text.style.cssText = "width : " + getElSize(400);
	var cnt = document.createElement("input");
	cnt.setAttribute("class", "cnt");
	cnt.setAttribute("size", 5);
	var part = document.createElement("select");
	var situationTy = document.createElement("select");
	var situation = document.createElement("select");
	var cause = document.createElement("select");
	var gchTY = document.createElement("select");
	var gch = document.createElement("select");
	var action = document.createElement("select");
	
	var tr = document.createElement("tr");
	tr.setAttribute("class", className);
	
	var checkTy_td = document.createElement("td");
	checkTy_td.append(checkTy);
	tr.append(checkTy_td);
	$("#chkTy_form").html(checkTy_td)
	
	var prdNo_td = document.createElement("td");
	prdNo_td.append(prdNo);
	tr.append(prdNo_td);
	$("#prdNo_form").html(prdNo_td)
	
	var process_td = document.createElement("td");
	process_td.append(process);
	tr.append(process_td);
	$("#oprNm_form").html(process_td)
	
	var device_td = document.createElement("td");
	device_td.append(device);
	tr.append(device_td);
	$("#dvcId_form").html(device_td)
	
	var sDate_td = document.createElement("td");
	sDate_td.append(sDate);
	tr.append(sDate_td);
	$("#date_form").html(sDate_td)
	
	var text_td = document.createElement("td");
	text_td.append(text);
	tr.append(text_td);
	$("#checker_form").html(text_td)
	
	var part_td = document.createElement("td");
	part_td.append(part);
	tr.append(part_td);
	$("#part_form").html(part_td)
	
	var situ_td = document.createElement("td");
	situ_td.append(situationTy);
	tr.append(situ_td);
	$("#situation_form").html(situ_td)
	
	var situTy_td = document.createElement("td");
	situTy_td.append(situation);
	tr.append(situTy_td);
	$("#situationTy_form").html(situTy_td)
	
	var cause_td = document.createElement("td");
	cause_td.append(cause);
	tr.append(cause_td);
	$("#cause_form").html(cause_td)
	
	var gchkTy_td = document.createElement("td");
	gchkTy_td.append(gchTY);
	tr.append(gchkTy_td);
	$("#gchTy_form").html(gchkTy_td)
	
	var gch_td = document.createElement("td");
	gch_td.append(gch);
	tr.append(gch_td);
	$("#gch_form").html(gch_td)
	
	var cnt_td = document.createElement("td");
	cnt_td.append(cnt);
	tr.append(cnt_td);
	$("#cnt_form").html(cnt_td)
	
	var action_td = document.createElement("td");
	action_td.append(action);
	tr.append(action_td); 
	$("#action_form").html(action_td)
	
	var button_td = document.createElement("td");
	var button = document.createElement("button");
	//button.setAttribute("id", "b" + data.id);
	var button_text = document.createTextNode("삭제");
	button.append(button_text);
	button_td.append(button)
	tr.append(button_td);
	
	//$("#tbody").append(tr);
	
	getCheckTyList($(checkTy));
	
	getProcessList($(process));
	getDevieList($(device));
	setDate($(sDate));
	getPartList($(part));
	getSituationList($(situation));
	getSituationTyList($(situationTy));
	getCauseList($(cause));
	getGChTyList($(gchTY));
	getGChList(7, $(gch));
	getActionList($(action)); 
	getPrdNoList($(prdNo));
	$(cnt).val(1);
	
	$(".row1").not(".tr_table_fix_header").css({
		"background-color" : "#222222"
	});

	$(".row2").not(".tr_table_fix_header").css({
		"background-color": "#323232"
	});
	
	
	setEl();
	
	$("#insertForm select, #insertForm input").css("font-size",getElSize(80))
	
	
	showCorver();
	$("#insertForm").css("z-index", 999)
	
	if(addFaulty!=""){
		$("#prdNo_selector_form option[value='" + $prdNo + "']").attr("selected", "selected");
		$("#cnt_form").children("td").children("input").val($cnt);
	}
};

function getGch(){
	var val = $(this).val();
	var ty;
	if(val=="47"){			//업체
		ty = "com";
	}else if(val=="48"){	//작업자  
		ty = "worker"	
	};
	
	getGChList(ty, $("#gch_form").children("td").children("select"));
};

var valArray = [];

function saveRow2(){
	valArray = [];
	
	var obj = new Object();
	
	obj.chkTy = $("#chkTy_form select").val();
	obj.prdNo = $("#prdNo_form select").val();
	obj.prdPrc = $("#oprNm_form select").val();
	obj.dvcId = $("#dvcId_form select").val();
	obj.date = $("#date_form input").val();
	obj.checker = $("#checker_form input").val();
	obj.part = $("#part_form select").val();
	obj.situationTy = $("#situationTy_form select").val();
	obj.situation = $("#situation_form select").val();
	obj.cause = $("#cause_form select").val();
	obj.gchTY = $("#gchTy_form select").val();
	obj.gch = $("#gch_form select").val();
	obj.cnt = $("#cnt_form input").val();
	obj.action = $("#action_form select").val();
	
	valArray.push(obj);
	
	var obj = new Object();
	obj.val = valArray;
	
	var url = "${ctxPath}/chart/saveRow.do";
	var param = "val=" + JSON.stringify(obj);
	
	if($("#prdNo_selector_form").val()=="0"){
		alert("품번을 선택하십시오.");
		$("#prdNo_selector_form").focus();
		return;
	}else if($("#dvcId_form select").val()=="0"){
		alert("장비를 선택하십시오.");
		$("#dvcId_form select").focus();
		return;
	};
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			if(data=="success"){
				alert("저장되었습니다.");
			}
		}
	});	
};

function saveRow(){
	valArray = [];
	
	var length = $("#tbody tr").length;
	
	for(var i = 0; i < length; i++){
		var obj = new Object();
		
		obj.chkTy = $("#tbody tr:nth(" + i + ") td:nth(0) select option:selected").val();
		obj.prdNo = $("#tbody tr:nth(" + i + ") td:nth(1) select option:selected").val();
		obj.prdPrc = $("#tbody tr:nth(" + i + ") td:nth(2) select option:selected").val();
		obj.dvcId = $("#tbody tr:nth(" + i + ") td:nth(3) select option:selected").val();
		obj.date = $("#tbody tr:nth(" + i + ") td:nth(4) input").val();
		obj.checker = $("#tbody tr:nth(" + i + ") td:nth(5) input").val();
		obj.part = $("#tbody tr:nth(" + i + ") td:nth(6) select option:selected").val();
		obj.situationTy = $("#tbody tr:nth(" + i + ") td:nth(7) select option:selected").val();
		obj.situation = $("#tbody tr:nth(" + i + ") td:nth(8) select option:selected").val();
		obj.cause = $("#tbody tr:nth(" + i + ") td:nth(9) select option:selected").val();
		obj.gchTY = $("#tbody tr:nth(" + i + ") td:nth(10) select option:selected").val();
		obj.gch = $("#tbody tr:nth(" + i + ") td:nth(11) select option:selected").val();
		obj.cnt = $("#tbody tr:nth(" + i + ") td:nth(12) input").val();
		obj.action = $("#tbody tr:nth(" + i + ") td:nth(13) select option:selected").val();
		
		valArray.push(obj);
	};
	
	var obj = new Object();
	obj.val = valArray;
	
	var url = "${ctxPath}/chart/saveRow.do";
	var param = "val=" + JSON.stringify(obj);
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			if(data=="success"){
				alert("저장되었습니다.")
			}
		}
	});
}

var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

function closeForm(){
	$("#insertForm").css("z-index", -999);
	closeCorver();
};

</script>
</head>

<body >
	<div id="insertForm">
		<table style="width: 100%">
			<Tr> 
				<Td class='table_title' width="20%"> 검사구분 </Td> <Td id="chkTy_form"  width="30%"></Td> <Td class='table_title'  width="20%">공정 </Td> <Td id="oprNm_form"></Td> 
			</Tr>
			<Tr> 
				<Td class='table_title'> 품번  </Td> <Td id="prdNo_form"> </Td><Td class='table_title'> 장비 </Td> <Td id="dvcId_form"></Td>
			</Tr>
			<Tr> 
				<Td class='table_title'> 발생일시 </Td> <Td id="date_form"></Td> <Td class='table_title'> 신고자 </Td> <Td id="checker_form"></Td>
			</Tr>
			<Tr> 
				<Td class='table_title'> 부위 </Td> <Td id="part_form"></Td> <Td class='table_title'> 현상구분 </Td> <Td id="situationTy_form"></Td>
			</Tr>
			<Tr> 
				<Td class='table_title'> 현상 </Td> <Td id="situation_form"></Td> <Td class='table_title' > 원인 </Td> <Td id="cause_form"></Td>
			</Tr>
			<Tr> 
				<Td class='table_title'> 귀책구분 </Td> <Td id="gchTy_form"></Td> <Td class='table_title'> 귀책 </Td> <Td id="gch_form"></Td>
			</Tr>
			<Tr> 
				<Td class='table_title'> 수량 </Td> <Td id="cnt_form"></Td> <Td class='table_title'> 조치 </Td> <Td id="action_form"></Td>
			</Tr>
			<Tr>
				<Td colspan="4" style="text-align: center;"><button onclick="saveRow2()">저장</button></Td>
			</Tr>
		</table> 
	</div>
	
	

	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	<%-- <img src="${ctxPath }/images/DashBoard/title_right.svg" id="title_right"> --%>
	<div id="title_right" class="title"> </div>	
	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
	</div>
</body>
</html>	