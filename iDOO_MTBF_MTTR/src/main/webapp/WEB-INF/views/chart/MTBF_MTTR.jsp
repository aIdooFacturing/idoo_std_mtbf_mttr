<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="device" var="device"></spring:message>
<spring:message code="cycle" var="cycle"></spring:message>
<spring:message code="ophour" var="ophour"></spring:message>
<spring:message code="confirm" var="confirm"></spring:message>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<%-- <link rel="stylesheet" href="${ctxPath }/css/style.css"> --%>
<%-- <link rel="stylesheet" href="${ctxPath }/css/jquery.loading.min.css"> --%>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<title>MTBF / MTTR</title>


<script src="https://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.min.js"></script>
<script src="https://code.highcharts.com/modules/xrange.js"></script>
<script src="https://code.highcharts.com/modules/annotations.js"></script>
         
<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>



<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-rotate.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.loading.min.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden;
	background-color: black;
  	
}
</style> 

<script type="text/javascript">

	const loadPage = () =>{
		createMenuTree("analysis", "MTBF_MTTR");
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});

		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		chkBanner();
	}
	
	$(function(){
		$( document ).ajaxStart(function() {
		    //마우스 커서를 로딩 중 커서로 변경
			$.showLoading();
		});
		//AJAX 통신 종료
		$( document ).ajaxStop(function() {
		    //마우스 커서를 원래대로 돌린다
			$.hideLoading();
		});
		$("#sDate").val(moment().subtract(9, 'day').format("YYYY-MM-DD"));
		$("#eDate").val(moment().format("YYYY-MM-DD"));
		
		$("#setSdate").val("2010-01-01");
		$("#setEdate").val(moment().format("YYYY-MM-DD"));
		
		$( "#sDate" ).datepicker({
			onSelect : function(e){
				//e == 날짜 
				$("#sDate").val(e);
			}
		})
		
		$( "#eDate" ).datepicker({
			onSelect : function(e){
				//e == 날짜 
				$("#eDate").val(e);
			}
		})

		$( "#setSdate" ).datepicker({
			onSelect : function(e){
				//e == 날짜 
				$("#setSdate").val(e);
			}
		})
		
		$( "#setEdate" ).datepicker({
			onSelect : function(e){
				//e == 날짜 
				$("#setEdate").val(e);
			}
		})
		
		setEl();
		
		//getPrdNo() => getType() => getdvcList() => gridTable();
		getPrdNo();
		
		
	})
	
	function getPrdNo(){
		var url = ctxPath + "/chart/commonPrdNo.do";
		
		var param = "shopId=" + shopId;

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				var select = "";
				select +="<option id='ALL'>전체</option>"
				$(json).each(function(idx,data){
					data.prdNo = decode(data.prdNo)
					select +="<option id=" + data.prdNo +">" + data.prdNo + "</option>"
				})
				$("#prdNoList").html(select)
				getType()
			}
		})
	}
	function getType(){
		var url = ctxPath + "/chart/commonType.do";
		
		var param = "shopId=" + shopId;

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				var select = "";
				select +="<option id='ALL'>전체</option>"
				$(json).each(function(idx,data){
					data.type = decode(data.type)
					select +="<option id=" + data.type +">" + data.type + "</option>"
				})
				$("#typeList").html(select)
				getdvcList()
			}
		})
	}
	function setEl(){
		$("#div1").css({
			"height" : getElSize(400)
		});
		
		$("#div2").css({
			"height" : getElSize(1400)
		})
		
		$("select").css({
			"padding-right" : getElSize(100)
		})
		
		$(".btn").css({
			"font-size" : getElSize(45)
			,"margin-right" : getElSize(40)
			,"margin-top" : getElSize(10)
			,"margin-bottom" : getElSize(10)
			,"padding" : getElSize(30)
		})
	}
	
	function getdvcList(){
		var url = ctxPath + "/chart/getJigList4Report.do";
		
		var param = "shopId=" + shopId;

		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dvcList;
				var select = "";
				select +="<option id='ALL'>전체</option>"
				$(json).each(function(idx,data){
					data.name = decode(data.name)
					select +="<option id=" + data.dvcId +">" + data.name + "</option>"
				})
				$("#dvcList").html(select)
				
				gridTable();
				
			}
		})
	}
	
	var kendotable;
	function gridTable(){
		kendotable = $("#grid").kendoGrid({
			height : $("#div2").height()
			,columns : [{
				field : "type"
				,title : "설비유형"
			},{
				field : "prdNo"
				,title : "차종"
			},{
				field : "name"
				,title : "장비"
			},{
				field : "date"
				,title : "설치일"
			},{
				field : "overDate"
				,title : "설치<br>경과일"
			},{
				field : "operDate"
				,title : "가동일"
			},{
				field : "tgRunTime"
				,title : "가용시간"
			},{
				field : "opTime"
				,title : "가동시간"
			},{
				field : "faultCnt11111111"
				,title : "고장횟수"
			},{
				field : "faultTime"
				,title : "총고장<br>간격시간"
			},{
				field : "avgFaultTime11111"
				,title : "평균고장<br>간격시간"
			},{
				field : "repair"
				,title : "총<br>복구시간"
			},{
				field : "avgRepair"
				,title : "평균<br>복구시간"
			}]
		}).data("kendoGrid")
		
		getTable()
	}
	
	function getTable(){
		
		var url = ctxPath + "/chart/getMTBFMTTR.do";
		console.log(url)
		var param = "shopId=" + shopId +
					"&sDate=" + $("#sDate").val() +
					"&eDate=" + $("#eDate").val() +
					"&setSdate=" + $("#setSdate").val() +
					"&setEdate=" + $("#setEdate").val() +
					"&startTime=" + Number($("#startTime").val()) +
					"&endTime=" + Number($("#endTime").val()) +
					"&type=" + $("#typeList option:selected").attr("id") +
					"&prdNo=" + $("#prdNoList option:selected").attr("id") +
					"&dvcId=" + $("#dvcList option:selected").attr("id") ;
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				
				var json = data.dataList
				
				$(json).each(function(idx,data){
					data.type = decode(data.type);
					data.prdNo = decode(data.prdNo);
					data.name = decode(data.name);
				})
				console.log(data.dataList)
				
				kendodata = new kendo.data.DataSource({
					data: json,
					batch: true,
					editable: false,
					/* sort: [{
                    	field: "prdNo" , dir:"asc" 
                    },{
                    	field: "item" , dir:"asc"
                    }], */
					schema: {
						model: {
							id: "idx",
							fields: {
//  								num: { editable: false },
							}
						}
					}
				});
				
				kendotable.setDataSource(kendodata);
			}
		})
	}
</script>
</head>
<body>
	<div id="container">
		<div id="div1">
			<table style="width: 100%; height: 100%; color: white;">
				<tr height="33.3%">
					<td id="row1">
						설비유형 
					</td>
					<td id="row2">
						<select id="typeList"></select>
					 </td>
					<td id="row3">
						조회시간
					</td>
					<td id="row4">
						<input type="text" id="sDate" class="date" readonly="readonly"> ~ 
						<input type="text" id="eDate" class="date" readonly="readonly">
					</td>
					<td id="row5" rowspan="3"  style="text-align: right;">
						<button class="btn" onclick="getTable()">조회</button><br>
<!-- 						<button class="btn">액셀</button> -->
					</td>
				</tr>
				
				<tr height="33.3%">
					<td>
						차종
					</td>
					<td>
						<select id="prdNoList"></select>
					</td>
					<td>
						설치일
					</td>
					<td>
						<input type="text" id="setSdate" class="date" readonly="readonly"> ~ 
						<input type="text" id="setEdate" class="date" readonly="readonly">
					</td>
				</tr>
				
				<tr height="33.3%">
					<td>
						장비
					</td>
					<td>
						<select id="dvcList"></select>
					</td>
					<td>
						고장시간
					</td>
					<td>
						<input type="number" id="startTime" class="date" value="0"> ~ 
						<input type="number" id="endTime" class="date" value="9999">
					</td>
				</tr>
				
			</table>
		</div>
		<div id="div2">
			<div id="grid">
			</div>
		</div>
	 </div>
	 
	 
	 <div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	