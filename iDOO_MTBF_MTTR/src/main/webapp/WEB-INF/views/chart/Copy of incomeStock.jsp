<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<spring:message code="income_no" var="income_no"></spring:message>
<spring:message code="com_name" var="com_name"></spring:message>
<spring:message code="prd_no" var="prd_no"></spring:message>
<spring:message code="spec" var="spec"></spring:message>
<spring:message code="lot_cnt" var="lot_cnt"></spring:message>
<spring:message code="check_cnt" var="check_cnt"></spring:message>
<spring:message code="faulty_cnt" var="faulty_cnt"></spring:message>
<spring:message code="income_cnt" var="income_cnt"></spring:message>
<spring:message code="faulty_history" var="faulty_history"></spring:message>
<spring:message code="del" var="del"></spring:message>
<spring:message code="income_date" var="income_date"></spring:message>
<spring:message code="add_faulty" var="add_faulty"></spring:message>
<spring:message code="save_ok" var="save_ok"></spring:message>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<style type="text/css">
	.selected_span{
	background-color :white;
	color : #008900;
}
	
</style>
<title><spring:message code="income_manage" ></spring:message></title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script src="${ctxPath }/js/jquery-1.11.2.min.js"></script>
<script src="${ctxPath }/js/jquery-ui.js"></script>
<script src="${ctxPath}/js/highstock.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/default.css">
<style type="text/css">
	body{
		overflow :hidden;
	}
</style>
<script type="text/javascript">
var shopId = 1;

var save_type = "init"
var comList = "<select>";

function getComList(){
	var url = "${ctxPath}/chart/getComList.do";
	
	$.ajax({
		url :url,
		dataType :"json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var option = "";
			$(json).each(function(idx, data){
				option += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
			});
		
			comList += option + "</select>";
		}
	});
};

function getPrdNo(){
	var url = "${ctxPath}/chart/getMatInfo.do";
	var param = "shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			json = data.dataList;
			
			var option = "";
			
			$(json).each(function(idx, data){
				option += "<option value='" + decode(data.prdNo) + "'>" + decode(data.prdNo) + "</option>"; 
			});
			
			$("#group").html(option);
			
			getIncomStock();
		}
	});
};

function showCorver(){
	$("#corver").css("z-index", 999);	
};

function hideCorver(){
	$("#corver").css("z-index", -999);
};

function bindEvt(){
	$("#addBtn").click(addRow);
	$("#save").click(addStock);
	$("#modify").click(updateStock);
	
	$("#insert #cancel, #update #cancel").click(closeInsertForm);
	$("#insert #lotCnt").keyup(calcAQL);
	$("#insert #notiCnt").keyup(calcAQL);
	$("#update #lotCnt").keyup(calcAQL_update);
	$("#update #notiCnt").keyup(calcAQL_update);
	
};

function closeInsertForm(){
	hideCorver();
	$("#insertForm, #updateForm").css("z-index",-9999);
	$("#updateForm #rcvCnt").css("color", " white");
	return false;
};

var preRCV_QTY = 0;
var preNOTI_QTY = 0;

function updateStock(){
	var url = "${ctxPath}/chart/updateStock.do";
	var param = "id=" + matId + 
				"&lotNo=" + $("#update #lotNo").val() + 
				"&lotCnt=" + $("#update #lotCnt").val() +
				"&notiCnt=" + $("#update #notiCnt").val() +
				"&rcvCnt=" + $("#update #rcvCnt").html() +
				"&prdNo=" + $("#update #prdNo").html() + 
				"&updatedNotiCnt=" + ($("#update #notiCnt").val() - preNOTI_QTY) +
				"&updatedRcvCnt=" + ($("#update #rcvCnt").html() - preRCV_QTY) +
				"&sDate=" + $("#sDate").val() + " " + $("#update input[type='time']").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success :function(data){
			if(data=="success"){
				$("#insert input").not("input[type=time]").val("");
				closeInsertForm();
				getIncomStock();
			}
		}
	});
	
	return false;
};

function addStock(){
	var url = "${ctxPath}/chart/addStock.do";
	var param = "prdNo=" + $("#insert #prdNo").html() + 
				"&sDate=" + $("#sDate").val() + " " + $("#insert input[type='time']").val() + 
				"&lotNo=" + $("#insert #lotNo").val() + 
				"&lotCnt=" + $("#insert #lotCnt").val() + 
				"&smplCnt=" + $("#insert #smplCnt").html() +
				"&notiCnt=" + $("#insert #notiCnt").val() +
				"&rcvCnt=" + $("#insert #rcvCnt").html();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			if(data=="success"){
				$("#insert input").not("input[type=time]").val("");
				closeInsertForm();
				getIncomStock();
			}	
		}
	});
	
	return false;
};


var a;
function calcAQL(el){
	var lotCnt = $(el).parent("td").parent("tr").children("td:nth(5)").children("input").val();
	var notiCnt = $(el).parent("td").parent("tr").children("td:nth(7)").children("input").val();
	
	$(el).parent("td").parent("tr").children("td:nth(6)").html("5");
	if(notiCnt>0){
		$(el).parent("td").parent("tr").children("td:nth(8)").html("0").css("color","red");	
	}else{
		$(el).parent("td").parent("tr").children("td:nth(8)").html(lotCnt).css("color","white");
	}
};

function calcAQL_update(){
	var lotCnt = $("#update #lotCnt").val();
	var notiCnt = $("#update #notiCnt").val();
	
	$("#update #smplCnt").html("5");
	if(notiCnt>0){
		$("#update #rcvCnt").html("0").css("color","red");	
	}else{
		$("#update #rcvCnt").html(lotCnt).css("color","white");
	}
};


function showInsertForm(){
	/* var date = new Date();
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	
	$("#insert input[type='time']").val(hour + ":" + minute);
	showCorver();
	
	getNewMatInfo(); */
};

function getTime(){
	var date = new Date();
	var hour = addZero(String(date.getHours()));
	var minute = addZero(String(date.getMinutes()));
	var second = addZero(String(date.getSeconds()));
	
	return hour + ":" + minute;
};



function getNewMatInfo(){
	var url = "${ctxPath}/chart/getNewMatInfo.do";
	var param = "prdNo=" + $("#group").val();
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType :"json",
		success : function(data){
			$("#insert #id").html(data.id);
			$("#insert #comName").html(data.comName);
			$("#insert #prdNo").html(data.prdNo);
			$("#insert #spec").html(data.spec);
			$("#insert #lotCnt").val(0);
			$("#insert #notiCnt").val(0);
			
			$("#insertForm").css("z-index",9999);	
		}
	});
};

$(function(){
	getComList();
	bindEvt();
	getPrdNo();
	$(".excel").click(csvSend);
	setEl();
	setDate();
	
	$("#menu_btn").click(function(){
		location.href = "${ctxPath}/chart/index.do"
	});
	
	//$(".date").change(getIncomStock);
	//$("#group").change(getIncomStock);
});
var className = "";
var classFlag = true;

function getIncomStock(){
	classFlag = true;
	var url = "${ctxPath}/chart/getRcvInfo.do";
	var sDate = $("#sDate").val();
	
	var param = "sDate=" + sDate +
				"&eDate=" + sDate + " 23:59:59" + 
				"&prdNo=" + $("#group").val();
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.dataList;
			
			var tr = "<thead>" + 
						"<Tr style='background-color:#222222'>" +
							"<Td>" +
								"${income_no}" +
							"</Td>" +
							"<Td>" +
								"${com_name}" +
							"</Td>" +
							"<Td>" +
								"${prd_no}" +
							"</Td>" +
							"<Td>" +
								"${spec}" +
							"</Td>" +
							"<Td>" +
								"Lot No" +
							"</Td>" +
							"<Td>" +
								"${lot_cnt}" +
							"</Td>" +
							"<Td>" +
								"${check_cnt}" +
							"</Td>" +
							"<Td>" +
								"${faulty_cnt}" +
							"</Td>" +
							"<Td>" +
								"${income_cnt}" +
							"</Td>" +
							"<Td>" +
								"${faulty_history}" +
							"</Td>" +
							"<Td>" +
								"${income_date}" +
							"</Td>" +
							"<Td>" +
								"${del}" +
							"</Td>" +
						"</Tr></thead><tbody>";
						
			$(json).each(function(idx, data){
				if(data.seq!="."){
					if(classFlag){
						className = "row2"
					}else{
						className = "row1"
					};
					classFlag = !classFlag;
					
					tr += "<tr class='contentTr " + className + "' id='tr" + data.id + "'>" +
								"<td >" + data.id + "</td>" +
								"<td>" + decodeURIComponent(data.comName).replace(/\+/gi, " ") + "</td>" + 
								"<td>" + decodeURIComponent(data.prdNo).replace(/\+/gi, " ").replace("내수", "KR").replace("북미", "NA").replace("유럽", "ER") + "</td>" +
								"<td>" + data.spec + "</td>" + 
								"<td>" + data.lotNo + "</td>" + 
								"<td>" + data.lotCnt + "</td>" + 
								"<td>" + data.smplCnt + "</td>" + 
								"<td>" + data.notiCnt + "</td>" + 
								"<td>" + data.rcvCnt + "</td>" +
								"<td><button onclick='addFaulty(this)'>${add_faulty}</button></td>" +
								"<td>" + data.inputDate + "</td>" +
								/* "<td><input type='checkbox'> </td>" + */ 
								"<td><button onclick='chkDel(\"" + data.id + "\", this)'>${del}</button></td>" +
						"</tr>";					
				}
			});
			
			tr += "</tbody>";
			
			$(".alarmTable").html(tr).css({
				"font-size": getElSize(40),
			});
			
			$("button, input[type='time']").css("font-size", getElSize(40));
			
			$(".alarmTable td").css({
				"padding" : getElSize(20),
				"font-size": getElSize(40),
				"border": getElSize(5) + "px solid black"
			});
			
			$("#wrapper").css({
				"height" :getElSize(1650),
				"width" : "95%",
				"overflow" : "hidden"
			});
			
			$("#wrapper").css({
				"margin-left" : (contentWidth/2) -($("#wrapper").width()/2)
			});
			
			$(".row1").not(".tr_table_fix_header").css({
				"background-color" : "#222222"
			});

			$(".row2").not(".tr_table_fix_header").css({
				"background-color": "#323232"
			});
			
			$("#wrapper div:last").remove();
			scrolify($('#table'), getElSize(1450));
			$("#wrapper div:last").css("overflow", "auto")
		}
	});
};

var matId;
var delObj;
function chkDel(id, el){
	$("#delDiv").css("z-index",9);
	
	matId = id;
	delObj = el;
};

function noDel(){
	$("#delDiv").css("z-index",-1);
};

function okDel(){
	if(matId==0){
		$(delObj).parent("td").parent("tr").remove();
		noDel();
		return;
	}
	var url = ctxPath + "/chart/okDelStock.do";
	var param = "id=" + matId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			noDel();
			getIncomStock();
		}
	});
};

function getBanner(){
	var url = "${ctxPath}/chart/getBanner.do";
	var param = "shopId=" + shopId;
	

	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "json",
		success : function(data){
			if(window.localStorage.getItem("banner")=="true"){
				$("#intro").html(data.msg).css({
					"color" : data.rgb,
					"right" : - window.innerWidth
				})
				
				$("#intro").html(data.msg).css({
					"right" : - $("#intro").width()
				})
			}
			
			bannerWidth = $("#intro").width() + getElSize(100);
			$("#intro").width(bannerWidth);
			$("#banner").val(data.msg);
			$("#color").val(data.rgb);
			
			twinkle();
			bannerAnim();
		}
	});		
}

var twinkle_opct = true;
function twinkle(){
	var opacity;
	if(twinkle_opct){
		opacity = 0;
	}else{
		opacity = 1;
	}
	$("#intro").css("opacity",opacity);
	
	twinkle_opct = !twinkle_opct;
	setTimeout(twinkle, 100)
};

var bannerWidth;
function bannerAnim(){
	$("#intro").width(bannerWidth - getElSize(10));
	$("#intro").animate({
		"right" : window.innerWidth  - getElSize(100)
	},15000, function(){
		$("#intro").css("right" , - $("#intro").width())
		//$(this).remove();
		bannerAnim();
	});
};
function chkBanner(){
	if(window.localStorage.getItem("banner")=="true"){
		getBanner();
		$("#intro_back").css("display","block");
	}else{
		$("#intro").html("");
		$("#intro_back").css("display","none");
	}
};

function cancelBanner(){
	window.localStorage.setItem("banner", "false");
	$("#bannerDiv").css("z-index",-9);
	chkBanner();
};

function addBanner(){
	var url = "${ctxPath}/chart/addBanner.do";
	var param = "msg=" + $("#banner").val() + 
				"&rgb=" + $("#color").val() + 
				"&shopId=" + shopId;
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(data){
			window.localStorage.setItem("banner", "true");
			$("#bannerDiv").css("z-index",-9);
			location.reload();
			//chkBanner();
		}
	});
};

var panel = false;
function showPanel(){
	$("#panel").animate({
		"left" : 0
	});
	
	$("#menu_btn").animate({
		"left" : contentWidth * 0.2 + getElSize(50)	
	});
	
	$("#corver").css({
		"z-index" : 9,
		"opacity" : 0.7
	});
};

function closePanel(){
	$("#panel").animate({
		"left" : -contentWidth * 0.2 - getElSize(40)
	});
	
	$("#menu_btn").animate({
		"left" : 0
	});
	
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0.7
	});
};


function addZero(str){
	if(str.length==1){
		str = "0" + str;
	};
	return str;
};

function setDate(){
	var date = new Date();
	var year = date.getFullYear();
	var month = addZero(String(date.getMonth()+1));
	var day = addZero(String(date.getDate()));
	
	$(".date").val(year + "-" + month + "-" + day);
};


function clearMenu(){
	$("#corver").css({
		"z-index" : -1,
		"opacity" : 0
		
	});
	
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight, 
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$("#group").css({
		"font-size" : getElSize(50)	
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(100)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1
	});

	$(".title_right").css({
		"float" : "right",
	});

	$("#menu_btn").css({
		"position" : "absolute",
		"width" : getElSize(130),
		"left" : 0,
		"top" : getElSize(20),
		"cursor" : "pointer",
		"z-index" : 13
	});
	

	$("#panel").css({
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"position" : "absolute",
		"width" : contentWidth * 0.2,
		"top" : 0,
		"left" : -contentWidth * 0.2 - getElSize(40),
		"z-index" : 99999
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "rgba(0,0,0,0.7)",
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(40)
	});

	$("#panel_table td").css({
		"padding" : getElSize(30),
		"cursor" : "pointer"
	});

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
		//"border": getElSize(10) + "px outset white",
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	$(".card_status").css({
		"height" : getElSize(60)
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.01
	});
	
	
	$("#title_left").css({
		"position" : "absolute",
		"width" : contentWidth * 0.07,
		"z-index" : 2,
		"left" : getElSize(100),
		"top" : getElSize(60)
	});

	$("#title_right").css({
		"position" : "absolute",
		"z-index" : 2,
		"right" : getElSize(100),
		"top" : getElSize(40),
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});
	
	$("#jigSelector, .goGraph, .excel, .label").css({
		"margin-right": getElSize(10),
		"margin-left": getElSize(10),
		"border-radius" : getElSize(10),
		"padding" : getElSize(10),
		"cursor": "pointer",
		"font-size": getElSize(50),
		"margin-bottom" : 0
	});
	
	$(".label").css({
		"margin-top" : 0
	});
	
	$(".date").css({
		"height": getElSize(70),
		"font-size": getElSize(50)
	});
	
	$(".thead").css({
		"font-size" : getElSize(60)
	});
	
	$(".alarmTable, .alarmTable tr, .alarmTable td").css({
		"border": getElSize(5) + "px solid rgb(50,50,50)"
	});
	
	$(".alarmTable td").css({
		"padding" : getElSize(10),
		"height": getElSize(100),
	});
	
	$(".contentTr").css({
		"font-size" : getElSize(60)
	});
	
	$(".machineListForTarget").css({
		"position" : "absolute",
		"width" : getElSize(1200),
		"height" : getElSize(1200),
		"overflow" : "auto",
		//"top" : getElSize(50),
		//"background-color" : "rgb(34,34,34)",
		"background-color" : "green",
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(50),
		"overflow" : "auto",
		"border-radius" : getElSize(50),
		"border" : getElSize(10) + "px solid white",
	});
	
	$(".machineListForTarget").css({
		"left" : (originWidth/2) - ($(".machineListForTarget").width()/2),
	});
	
	$("#bannerDiv").css({
		"position" : "absolute",
		"width" : getElSize(1500),
		"height" : getElSize(200),
		"border-radius" : getElSize(20),
		"padding" : getElSize(50),
		"background-color" : "lightgray",
		"z-index" : -9
	});

	
	$("#bannerDiv").css({
		"left" : (originWidth/2) - ($("#bannerDiv").width()/2),
		"top" : (originHeight/2) - ($("#bannerDiv").height()/2)
	});
	
	$("#bannerDiv input[type='text']").css({
		"width" : getElSize(1200),
		"font-size" : getElSize(50)
	});
	
	$("#bannerDiv button").css({
		"margin" : getElSize(50),
		"font-size" : getElSize(50)
	});
	
	$("#intro").css({
		/* "width" : contentWidth, */
		"position" : "absolute",
		"bottom" : 0 + marginHeight,
		"font-size" : getElSize(140),
		"font-weight" : "bolder",
		"z-index" : 9999
	});
	
	$("#intro_back").css({
		"width" : originWidth,
		"display" : "none",
		"height" : getElSize(180),
		"opacity" : 0.3,
		"position" : "absolute",
		"background-color" : "white",
		"bottom" : 0 + marginHeight,
		"z-index" : 9999
	});
	
	$("#insertForm, #updateForm").css({
		"width" : getElSize(3500),
		"position" : "absolute",
		"z-index" : -999,
	});
	
	$("#insertForm, #updateForm").css({
		"left" : (originWidth/2) - ($("#insertForm").width()/2),
		"top" : (originHeight/2) - ($("#insertForm").height()/2)
	});
	
	$("#insert table td, #update table td").css({
		"color" : "white",
		"font-size" : getElSize(50),
		"padding" : getElSize(20),
		//"border" : getElSize(2) + "px solid black",
		"text-align" : "Center"
	});
	
	$("#delDiv").css({
		"position" : "absolute",
		"width" : getElSize(700),
		"height" :getElSize(200),
		"background-color" : "#444444",
		"color" : "white"
	});
	
	$("#delDiv").css({
		"top" : (window.innerHeight/2) - ($("#delDiv").height()/2),
		"left" : (window.innerWidth/2) - ($("#delDiv").width()/2),
		"z-index" : -1,
		"border-radius" : getElSize(10),
		"padding" : getElSize(20)
	});
	
	$("button").css({
		"font-size" : getElSize(40)	
	});
	
	chkBanner();
};

var valueArray = [];
function saveRow(ty){
	valueArray=[];
	
	$(".contentTr2").each(function(idx, data){
		var obj = new Object();
		obj.id = $(data).children("td:nth(0)").html();
		obj.prdNo = $("#group").val();
		obj.date = $("#sDate").val() + " " + $(data).children("td:nth(10)").children("input").val();
		obj.lotNo = $(data).children("td:nth(4)").children("input").val();
		obj.lotCnt = $(data).children("td:nth(5)").children("input").val();
		obj.smplCnt = $(data).children("td:nth(6)").html();
		obj.notiCnt = $(data).children("td:nth(7)").children("input").val();
		obj.rcvCnt = $(data).children("td:nth(8)").html();
		obj.vndNo =  $(data).children("td:nth(1)").children("select").val();
		valueArray.push(obj);
	});
	
	var obj = new Object();
	obj.val = valueArray;
	
	var url = "${ctxPath}/chart/addStock.do";
	var param = "val=" + JSON.stringify(obj);
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		dataType : "text",
		success : function(data){
			if(data=="success") {
				if(ty!="faulty"){
					alert("${save_ok}");
					getIncomStock();					
				};
			}
		}
	});
};

function addRow(){
	var tr = "<tr class='contentTr2'>" + 
				"<td></td>" +
				"<td>" + comList + "</td>" +
				"<td>" + $("#group option:selected").html() + "</td>" +
				"<td></td>" +
				"<td><input type='text' size='5' ></td>" +
				"<td><input type='text' size='5' value='0' onkeyup='calcAQL(this)'></td>" +
				"<td>0</td>" +
				"<td><input type='text' size='5' value='0' onkeyup='calcAQL(this)'></td>" +
				"<td>0</td>" +
				"<td><button onclick='addFaulty(this)'>${add_faulty}</button></td>" +
				"<td><input type='time' value='" + getTime() + "'></td>" +
				"<td><button onclick='chkDel(\"0\", this)'>${del}</button></td>" +
			"</tr>";
	 
	if($(".contentTr").length==0){
		$("#table").append(tr);	
	}else{
		$(".alarmTable").last().append(tr);	
	}
	
	$("button, input[type='time'], select").css("font-size", getElSize(40));
};

function addFaulty(el){
	var prdNo = $(el).parent("td").parent("tr").children("td:nth(2)").html();
	var cnt = $(el).parent("td").parent("tr").children("td:nth(7)").html();
	if(cnt.length>10){
		cnt = $(el).parent("td").parent("tr").children("td:nth(7)").children("input").val();
	}
	
	saveRow("faulty");
	var url = "${ctxPath}/chart/addFaulty.do?addFaulty=true&prdNo=" + prdNo + "&cnt="+ cnt;
	
	location.href = url;
};

var csvOutput;
function csvSend(){
	var sDate, eDate;
	
	sDate = $("#sDate").val();
	eDate = $("#eDate").val();
	
	csvOutput = csvOutput.replace(/\n/gi,"");
	
	f.csv.value=encodeURIComponent(csvOutput);
	f.startDate.value = sDate;
	f.endDate.value = eDate;
	f.submit(); 
};

</script>
</head>

<body >
	<div id="bannerDiv">
		<Center>
			<input type="text" id="banner" size="100"><input type="color" id="color"><br>
			<button onclick="addBanner();">적용</button>
			<button onclick="cancelBanner();">미적용</button>
		</Center>
	</div>
	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<div id="delDiv">
		<Center>
			<span>삭제하시겠습니까?</span><Br><br>
			<button id="resetOk" onclick="okDel();"><spring:message code="del" ></spring:message></button>&nbsp;&nbsp;
			<button id="resetNo" onclick="noDel();"><spring:message code="cancel" ></spring:message></button>
		</Center>
	</div>
	
	<form action='${ctxPath}/csv/csv.do' id="f"  method="post">
		<input type="hidden" name="csv" value="">
		<input type="hidden" name="startDate" value="">
		<input type="hidden" name="endDate" value="">
	</form>
	<div id="title_right" class="title"></div>	
	
	<img src="${ctxPath }/images/home.png" id="menu_btn" >
	
					
	<div id="corver"></div>

	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
				<table class="mainTable">
					<tr>
						<Td align="center" style=" color:white; font-weight: bolder;" class="title" >
							<spring:message code="income_manage" ></spring:message>
						</Td>
					</tr>
				</table>
				
				<div style="width: 95%">
					<div class="label" style="float: right">
						<table>
							<Tr>
								<Td style="color: white"><spring:message code="prd_no" ></spring:message>&nbsp;<select id="group"> </select>&nbsp;<spring:message code="income_date" ></spring:message>&nbsp;<input type="date" class="date" id="sDate"></Td>
							</Tr>
							<Tr>
								<Td align="right"><button onclick="getIncomStock()"><spring:message code="check" ></spring:message></button> <button id="addBtn"><spring:message code="add" ></spring:message></button> <button onclick="saveRow()"><spring:message code="save" ></spring:message></button></Td>
							</Tr>
						</table>
					</div>
				</div>
				<div id="wrapper">
					<table style="width: 100%; color: white; text-align: center; border-collapse: collapse;" class="alarmTable" border="1" id="table">
						
					</table>
				</div>
		</div>
	</div>
</body>
</html>