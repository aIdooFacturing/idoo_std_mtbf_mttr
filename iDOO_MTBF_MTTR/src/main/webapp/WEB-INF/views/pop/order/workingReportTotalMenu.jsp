<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<%
	response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
	response.setHeader("pragma", "no-cache"); // HTTP 1.0
	response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">



<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript"
	src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript"
	src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>

<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

<style>
* {
	margin: 0px;
	padding: 0px;
}

body {
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow: hidden;
	background-color: black;
	font-family: 'Helvetica';
}

html {
	overflow: hidden;
}

input#autocomplete.k-input {
	margin-left:0px !important;
	margin-right:0px !important;
}

input#pwd {
	float:right !important;
	margin-left:0px !important;
	margin-right:0px !important;
}

div.dialog.k-window-content.k-content{
	background-color: rgb(50, 50, 50);
	color:white;
}

button#submit.k-button.k-button-icontext{
	font-size:16px !important;
}

button#cancel.k-button.k-button-icontext{
	font-size:16px !important;
}

input#prdNo.k-input{
	margin-left:0px !important;
}

#tableDiv.k-grid.k-widget.k-display-block.k-editable{
	background-color: gray;
}
select.worker{
	margin-left:0px !important;
}
select#nd{
	margin-left:0px !important;
}
.k-dialog .k-content{
    margin: 0 !important;
    padding: 0 !important;
}
.k-tabstrip-top .k-tabstrip-items .k-state-active{
	background-color: rgb(121, 218, 76);
	border:solid 0px yellow;
	color: rgb(68, 11, 246);
}
button.k-button.k-primary{
    width: 100%;
    /* font-size: 14.5313px; */
    margin-left:0px !important;
    margin-right: 0px !important;
    padding: 0px !important;
}
#details-container{
	background-color: gray;
	color:white;
}
mark{
	background-color:black;
	color:white;
}
#addWork{
	color:white;
	border: 1px solid black;
    text-align: center;
    background: linear-gradient(#6f58fd, #0a28ff);
    border-radius: 5px;
    cursor:pointer;
    font-weight:bolder;
}
#addWork:hover{
	background: #0a28ff;
}
.k-dialog.k-alert .k-dialog-titlebar{
	display:none;
}
.k-link{
	color:black !important;
}
#getWaitTime{
	float : right;
	font-weight: bolder;
}
#reportSubmit{
	font-weight: bolder;  
	background-color: rgb(121, 218, 76); 
	border: 0px solid;
}
#checkSearchBtn{
	background: linear-gradient(#e8e8e8, #929292);
    border-radius: 5px;
    border: 0px solid black;
    color: black;
    font-weight: bolder;
}
#checkSearchBtn:hover{
	background: #929292;
}

.checkSearchBtn{
	background: linear-gradient(#e8e8e8, #929292);
    border-radius: 5px;
    border: 0px solid black;
    color: black;
}

.checkSearchBtn:hover{
	background: #929292;
}
#moveFaulty{
	background: linear-gradient(#549cff, #255188);
	border-radius: 5px;
	border: 0px solid black;
    color: white;
}
#moveFaulty:hover{
	background: #255188;
}
#saveCheckList{
    border: 0px solid black;
    background: linear-gradient(#59ff43, #239027);
    color: white;
    border-radius: 5px;
}
#saveCheckList:hover{
	background: #239027;
}
#saveRow{
	font-weight : bolder;
    background : linear-gradient(#c1ff44, #6c8e28);
	border : 0px solid;
	color: black;
}
#saveRow:hover{
	font-weight : bolder;
    background : #6c8e28;
	border : 0px solid;
}
#addAttendance{
    border: 0px solid black;
    border-radius: 5px;
    color: white;
    background: linear-gradient(#549cff, #255188);
}
#addAttendance:hover{
	background: #255188;
}
#searchAttendance{
  	border: 0px solid black;
    border-radius: 5px;
    color: black;
    background: linear-gradient(lightgray, gray);
}
#searchAttendance:hover{
	background: gray;
}
#saveRow_attendance{
	font-weight : bolder;
    background: linear-gradient(lightgray, gray);
   	border : 0px solid;
   	border-radius : 5px
}
#saveRow_attendance:hover{
	background: gray;
}
#confirmAttendanceBtn{
	font-weight : bolder;
    background: linear-gradient(#59ff43, #239027);
   	border : 0px solid;
   	border-radius : 5px
}
#confirmAttendanceBtn:hover{
	font-weight : bolder;
    background: #239027;
   	border : 0px solid;
   	border-radius : 5px	
}
.k-window-titlebar{
	display: none;
}
</style>
<script type="text/javascript">


	var windowTemplate, delMsg, grid;

	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	
	var handle = 0;
	
	var processWindow;
	var work_logging = $("#work_logging");
	var modal_Window;
	var tabStrip;
	
	var confirm;
	
	
	function getWorkerList(){
		classFlag = true;
		var url = "${ctxPath}/common/getAllWorkerList.do";

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "";
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
					
					var object = {
							"workerCd" : data.id
							,"name" : decode(data.name)
					} 
					
					workerArray.push(object)
				});
				
				options += "<option value='0'> aIdoo 미연결 </option>";
				$(".worker").html(options)
			}
		});
	};
	
	
	var grid_attendance;
	
	function getToday2(){
		if(moment().format("HH:mm")<"08:00"){
			return moment().subtract(1, 'day').format("YYYY-MM-DD")
		}else{
			return moment().subtract(1, 'day').format("YYYY-MM-DD")
		}
	}
	$(function(){
		setDate();
		
//		getToday2();
//		workingList();
		
		tabStrip = $("#tabstrip").kendoTabStrip({
	        animation: false,
	        contentLoad: onContentLoad,
	        show: onShow,
	    }).data("kendoTabStrip").activateTab(work_logging);
		
		
		$("#popup_attendance #date").datepicker({
			
		})
		
		$("#modal_Dialog").kendoDialog({
			  primary: true,
			  visible: false,
			  closable: false,
			  position: {
				    top: "35%",
				    left: "40%",
			  },
			  actions: [{
		          text: "닫기",
		          action: function(e){
		        	  modal_Window.close();
		              return false;
		          },
		          primary: true
		      }],
		      title:false
		});
		modal_Window = $("#modal_Dialog").data("kendoDialog");
		
		work_logging = $("#work_logging");
		
		
		$("#dialog").kendoDialog({
			  closable: false,
			  primary: true,
			  visible:false,
			  resizable: false,
			  draggable: false,
			  scrollable : false,
			  title : false,
			  height: getElSize(2000),
			  width: getElSize(3500),
			  open: function() {
				  $("#tabstrip").kendoTabStrip().data("kendoTabStrip").activateTab(work_logging);
			  }
		});
		
		processWindow = $("#dialog").data("kendoDialog")
		
		
		
		$("#close_Button").kendoButton({
		    icon: "cancel",
		    click: function(e) {
		    	modal_Window.close();
		    }
		});		
		
		
		
	    var onContentLoad = function(e) {
    		console.log(e)
       		console.log("contentLoad!!!")	
	        tabStrip.unbind("contentLoad", onError);
	    };
	    var onShow = function(e) {
	    	console.log(e)
       		console.log("onShow!!!")	
	        tabStrip.unbind("show", onShow);
	    };
		
		
		$("#popup_nonOp #date").attr("disabled",true)
		$("#popup_nonOp .worker").attr("disabled",true)
		$("#popup_nonOp #nonOpTime").attr("disabled",true)
		
		windowTemplate = kendo.template($("#windowTemplate").html());
		
		delMsg = $("#window").kendoWindow({
		    title: "${delete_Confirm}",
		    visible: false, //the window will not appear before its .open method is called
		    width: "300px",
		    height: "100px",
		}).data("kendoWindow");
		
		
		
		getWorkerList();
		createNav("order_nav", 1);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
		
		
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#dialog").css({
			"border" : getElSize(10) + "px solid yellow"
		})
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#_table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$(".nav_span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button").css({
		});
		
		$("button").css({
			"padding" : getElSize(15),
			"margin" : "0 !important"
		})
		
		$("#search").css({
			"cursor" : "pointer",
			"width" : getElSize(80),
		});
		
		$("#content_table td, #content_table2 td").css({
			"color" : "#BFBFBF",
			"font-size" : getElSize(50)
		});
		
		$("#content_table input, #content_table select, #content_table button").css({
			"height" :  getElSize(70),
			"font-size" : getElSize(50)
		})
		
		$(".tmpTable, .tmpTable tr, .tmpTable td").css({
			"border": getElSize(5) + "px solid rgb(50,50,50)"
		});
		
		$(".tmpTable td").css({
			"padding" : getElSize(10),
			"height": getElSize(100)
		});
		
		$(".contentTr").css({
			"font-size" : getElSize(60)
		});
		
		$("#contentDiv").css({
			"overflow" : "auto",
			"width" : $(".menu_right").width()
		});
		
		$("#insertForm").css({
			"width" : getElSize(3000),
			"position" : "absolute",
			"z-index" : 999,
		});
		
		$("#insertForm").css({
			"left" : $(".menu_right").offset().left + ($(".menu_right").width()/2) - ($("#insertForm").width()/2) - marginWidth,
			"top" : getElSize(400)
		});
		
		$("#insertForm table td").css({
			"font-size" : getElSize(70),
			"padding" : getElSize(15),
			"background-color" : "#323232"
		});
		
		$("#insertForm button, #insertForm select, #insertForm input").css({
			"font-size" : getElSize(40),
			"margin" : getElSize(15)
		});
		
		$(".table_title").css({
			"background-color" : "#222222",
			"color" : "white"
		});
		
		$("#contentTable td").css({
			"color" : "white",
			"font-size" : getElSize(30)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$(".sub_menu").css({
			"color" : "rgb(68, 11, 246)",
			/* "float" : "left", */
			"cursor" : "pointer",
			"text-align" : "center",
			"font-size" : getElSize(100) + "px",
			"margin" : getElSize(50) + "px",
			"display" : "table",
			"width" : getElSize(1000) + "px",
			"height" : getElSize(500) + "px",
			"border-radius" : getElSize(30) + "px",
			"background-color" : "rgb(121, 218, 76)"
		}).click(function (){showPopup(this)});
		
		$(".sub_menu span").css({
			"display" : "table-cell",
			"vertical-align" : "middle",
		});
		
		$("#popup").css({
			"background-color" : "rgb(50,50,50)",
			"padding" : getElSize(50) + "px",
			"position" : "absolute",
			"z-index" : 999,
			"display" : "none"
		});
		
		$("#popup table td").css({
			"color" : "white",
			"font-size" : getElSize(80) + "px"
		})
		
		$("#popup table input, #popup table select, #popup button").css({
			"margin" : getElSize(20) + "px",
			"font-size" : getElSize(80) + "px",
			"height" : getElSize(160) + "px"
		});
		
		$("#popup2 button").css({
			"margin" : getElSize(20) + "px",
			"font-size" : getElSize(80) + "px"
		});
		
		$("#popup").css({
			"left" : (originWidth/2) - ($("#popup").width()/2),
			"top" : (originHeight/2) - ($("#popup").height()/2),
		});
		
		$("#popup2 table td").css({
			"color" : "white",
			"font-size" : getElSize(70) + "px"
		});
		
		$("#popup2").css({
			"background-color" : "rgb(50,50,50)"
		});
		
		$("#popup_nonOp").css({
			"width" : getElSize(3500) + "px", 
			"height" : getElSize(1800) + "px",
			"position" : "absolute"
		});
		
		$("#popup_nonOp table td, #popup_attendance table td").css({
			"color" : "white",
			"font-size" : getElSize(40) + "px"
		});
		
		$("#popup_nonOp, #popup_attendance, #popup2").css({
			"background-color" : "rgb(50,50,50)",
			"color" : "white",
			"font-size" : getElSize(70)
		});
		
		$("#popup_nonOp button").css({
			"margin" : getElSize(20) + "px",
			"font-size" : getElSize(80) + "px"
		});
		
		$("#popup2 #tableDiv").css({
			"height" : getElSize(1590)
		});
		
		$("#popup_attendance select, #popup_attendance input, #popup_nonOp select, #popup_nonOp input, #popup2 select, #popup2 input").css({
			"height" : getElSize(90) + "px",
			"font-size" : getElSize(40)
		});
		
		$("#insertForm_Faulty").css({
			"top" : getElSize(300)+"px",
			"left" : getElSize(200)+"px"
		})
		
		$("#popup_nonOp#tableDiv > div.k-grid-content.k-auto-scrollable").css({
			"height" : getElSize(1000) + "px"
		})
		
		$("#confirm").css({
			"font-size" : getElSize(70)+"px"
		})
		
		$("#saveRow").css({
			"font-size" : getElSize(45)+"px",
			"width" : getElSize(350)+ "px",
			"height" : getElSize(100)+ "px",
		})
		
		$("#tabstrip ul li").css({
			"font-size" : getElSize(50)+"px"
		})
		
		$("#init_attendance").css({
			"font-size" : getElSize(40)+"px",
			"width" : getElSize(300)+ "px",
			"height" : getElSize(100)+ "px",
			"font-weight" : "bolder",
	    	"border" : "0px solid"
		})
		
		$("#saveRow_attendance").css({
			"font-size" : getElSize(50)+"px",
			"width" : getElSize(200)+ "px"
		})
		
		$("#login_worker").css({
			"font-size" :  getElSize(80)+"px"
		})
		
		$("#addWork").css({
			"font-size" : getElSize(40)+"px",
		    "padding": getElSize(20)
		})
		
		$("#addWorkMenu").css({
			"padding" : getElSize(40),
			"width" : getElSize(980),
			"height" : getElSize(600)
		})
		
		$("#closeAddWorkMenu").css({
		    "display": "inline",
		    "width": getElSize(265),
	    	"margin-left": getElSize(90)
		})
		
		$("#inputPrdNo").css({
			"margin-left": getElSize(90),
	    	"margin-top": getElSize(90)
		})
		
		$("#inputDvcId").css({
			"margin-left": getElSize(90),
	    	"margin-top": getElSize(90)
		})
		
		$("#addWork").css({
		    "display": "inline",
		    "margin-left": getElSize(90),
		    "width": getElSize(310),
		    "margin-top": getElSize(90)
		})
		
		$("#getWaitTime").css({
			"font-size" : getElSize(55),
		})
		
		$("td#tdGetWaitTime").css({
			"font-size" : getElSize(65),
			"padding-left" : getElSize(60)
		})
		
		$("#checkSearchBtn").css({
		    "width": getElSize(200),
		    "height": getElSize(70)
	    })
	    
	    $("#addAttendance").css({
	    	"font-size" : getElSize(60),
	    	"margin-left" : getElSize(250),
	    	"margin-top" : getElSize(20),
	    	"margin-bottom" : getElSize(20)
	    })
	    
	    $("#searchAttendance").css({
	    	"font-size" : getElSize(60),
	    	"margin-left" : getElSize(1280)
	    })
	    
	    $("#confirmAttendanceBtn").css({
	    	"font-size" : getElSize(60),
	    	"margin-left" : getElSize(100),
	    	"margin-bottom" : getElSize(25)
	    })
	    
	    $("i").css({
	    	"margin-right" : getElSize(20)
	    })
	    
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function addStartWork(){
		$.showLoading()
		var now = moment().hour();
		if(chkWorkeTimeValid()=="success"){
			
			var url = "${ctxPath}/chart/addWorkTimeHist.do";
			var param = "worker=" + $("#popup .worker").val() + 
						"&ty=I";
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					if(data=="success"){
						closePopup();
						alert("출근처리 되었습니다.")
					}else if(data=="duple"){
						alert("이미 출근 하셨습니다.")			
					}
					$.hideLoading()
				}
			});
		}else{
			$.hideLoading()
			alert("출근 시간이 아닙니다.")
		}
		
	}
	
	function addEndWork(){
		$.showLoading()
		var now = moment().hour();
		if(chkWorkeTimeValid()=="success"){
			
			var url = "${ctxPath}/chart/addWorkEndTimeHist.do";
			var param = "worker=" + $("#popup .worker").val() + 
						"&ty=O";
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					if(data=="success"){
						closePopup();
						alert("퇴근처리 되었습니다.")
					}else if(data=="duple"){
						alert("이미 퇴근처리 되었습니다.")			
					}else if(data=="No"){
						alert("출근보고가 되지않았습니다.")
					}
					$.hideLoading()
				}
			});
		}else{
			$.hideLoading()
			alert("퇴근 시간이 아닙니다.")
		}
		
	}
	
	
	function addPrdCmpl(){
//		$(".worker").attr("disabled",true);
		console.log("날짜넣기")
		console.log(getToday())
		$("#workDate").val(getToday().substr(0,10))
		$(".worker").change(function(){
			getOprNameList()
		})
	}
	
	var checkedIds = {};
	function selectRow($grid,el) {
	    var checked = el.checked,
	    row = $(el).closest("tr"),
	    grid = $grid;
	    dataItem = grid.dataItem(row);

	    checkedIds[dataItem.id] = checked;
	    if (checked) {
	        row.addClass("k-state-selected");
	    }else{
	        row.removeClass("k-state-selected");
	    }
	}
	
	var selectedRow = {};
	
	var attendanceList_DateSource;
	
	function addAttendance(){
		
		$.showLoading();
		$("#popup_attendance .worker").attr("disabled",true);
		
		var url = "${ctxPath}/chart/getAttendanceHist.do";
		var param = "worker=" + $("#popup_attendance .worker").val() + 
					"&sDate=" + $("#popup_attendance #date").val() + 
					"&eDate=" + $("#popup_attendance #date").val()
		
		var dataList = [];
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				$(json).each(function(idx, data){
					var object = {
							"id" : data.id,
							"worker" : data.worker,
							"attendanceTy" : data.group1,
							"date" : data.date,
							"attendanceDiv" : data.group2,
							"attendance" : data.group3,
							"sDate" : data.sDate,
							"eDate" : data.eDate,
							"approve" : data.approve,
							"reason" : decode(data.reason),
							"msg" : decode(data.msg)
					}
					
					dataList.push(object)
				});
				
				attendanceList_DateSource = new kendo.data.DataSource({
	                data: dataList,
	                batch: true,
	                schema: {
	                       model: {
	                         id: "id",
	                         fields: {
	                            id: { editable: false},
	                            worker: { editable : false },
	                            attendanceTy: { editable : true },
	                            date : { editable : true },
	                            attendanceDiv : { editable : true },
	                            attendance : { editable : true},
	                            sDate : { editable : true },
	                            eDate : { editable : true },
	                            reason: { editable : true }, 
	                            approve: { editable : false },
	                            msg: { editable : false } 
	                         }
	                       }
	                   }
	        	});
				
				grid_attendance = $("#popup_attendance #tableDiv").kendoGrid({
					dataSource : attendanceList_DateSource,
					selectable: "row",
				    height: getElSize(1540),
				    groupable: false,
				    sortable: false,
				    editable: false,
				    scrollable:true,
				    dataBound:function(){
				          grid = this;
				            grid.tbody.find('tr').each(function(){            
				             var item = grid.dataItem(this);
				             kendo.bind(this,item);
				             console.log("dataBound:function")
				             $("button#deleteAttendance").css({
								"width" : getElSize(200),
								"height" : getElSize(105),
								"border" : "0px solid",
								"font-weight" : "bolder",
								"font-size" : getElSize(40),
								"background": "linear-gradient(#f5522d, rgb(236, 0, 0))",
							   	"border-radius" : "5px"
							})
				         })
				         
						$(".time").kendoTimePicker({
							format: "HH:mm",
						})
						$(".date").datepicker({
						})
						$(".time").css("width","100%")
			
				    },
				    columns: [
					              	{
				        			field: "worker",
				        			title: "${worker}",
				        			editor: categoryDropDownWorkerNameEditor,
				        			template : "#= workerName(worker) #",
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			},
				    			{
				        			field: "attendanceTy",
				        			title: "${attendance_ty}",
				        			template:'<select style="width:100%" data-bind="value:attendanceTy" #=(approve<2)?"":"disabled"#>'+
				        			'<option value="0">===선택해주세요===</option>'+
				        			'<option value="1">일 근태</option>'+
				        			'<option value="2">시 근태</option>'+
				        			'</select>',
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			},
				    			{
				        			field: "date",
				    				title: "등록일자",
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			},
				    			{
				        			field: "attendanceDiv",
				        			title: "${attendace_div}",
				        			template:'<select style="width:100%" data-bind="value:attendanceDiv" #=(approve<2)?"":"disabled"#>'+
				        			'<option value="0">===선택해주세요===</option>'+
				        			'<option value="1">연차/휴가</option>'+
				        			'<option value="2">교육/훈련</option>'+
				        			'<option value="3">근무/출장</option>'+
				        			'<option value="4">기타 일근태</option>'+
				        			'<option value="5">외출/외근</option>'+
				        			'<option value="6">교육/훈련</option>'+
				        			'<option value="7">지각/조퇴</option>'+
				        			'<option value="8">기타 시근태</option>'+
				        			'</select>',
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			},
				    			{
				        			field: "attendance",
				        			title: "${assiduity}",
				        			template:'<select style="width:100%" data-bind="value:attendance" #=(approve<2)?"":"disabled"#>'+
				        			'<option value="0">===선택해주세요===</option>'+
				        			'<option value="1">연차</option>'+
				        			'<option value="2">반차</option>'+
				        			'<option value="3">휴가(무급)</option>'+
				        			'<option value="4">휴가(유급)</option>'+
				        			'<option value="5">민방위교육</option>'+
				        			'<option value="6">예비군훈련</option>'+
				        			'<option value="7">교육</option>'+
				        			'<option value="8">국내출장</option>'+
				        			'<option value="9">해외출장</option>'+
				        			'<option value="10">해외파견</option>'+
				        			'<option value="11">기타사유</option>'+
				        			'<option value="12">공용외출</option>'+
				        			'<option value="13">사용외출</option>'+
				        			'<option value="14">교육훈련</option>'+
				        			'<option value="15">지각</option>'+
				        			'<option value="16">기타사유</option>'+
				        			'</select>',
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			},
				    			{
				        			field: "sDate",
				    				title: "${start}",
				    				template: "#=showSdate(attendanceTy,approve)#",
//				    				template: '<input type="time" data-bind="value:sDate" #=(approve<2)?"":"readonly"#>',
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			},
				    			{
				        			field: "eDate",
				        			format: "HH:mm",
				        			title: "${end}",
				    				template:"#=showEdate(attendanceTy,approve)#",
//				        			template: '<input type="time" data-bind="value:eDate" #=(approve<2)?"":"readonly"# >',
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			},
				    			{
				        			field: "reason",
				        			title: "${reason}",
				        			template: '<input type="text" data-bind="value:reason" #=(approve<2)?"":"readonly"#>',
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			},
				    			{
				        			title: "상황",
				        			template : '#=approveChk(approve)#',
//				        			template : '#=(approve<2)?"미 승인":"승인됨"#',
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			},
				    			{
				        			title: "미승인 사유",
				        			template : '#=msg#',
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			},
				    			{
				        			title: "삭제",
				        			template : '<button id="deleteAttendance" onclick=#=(approve<2)?"deleteAttendanceRow(this)":""#>#=(approve<2)?"삭제":"삭제불가"#</button>',
				        			attributes: {
				            			style: "text-align: center; font-size: " + getElSize(35) + "px"
				          			}
				    			}
				    		]
					}).data("kendoGrid");
				
				$("button#deleteAttendance").css({
					"width" : getElSize(200),
					"height" : getElSize(105),
					"border" : "0px solid",
					"font-weight" : "bolder",
					"font-size" : getElSize(40),
					"background": "linear-gradient(#f5522d, rgb(236, 0, 0))",
				   	"border-radius" : "5px"
				})
				
				$.hideLoading();
			}
		});
	};
	
	function approveChk(chk){
		if(chk==0){
			return "미승인"
		}else if(chk==1){
			return "승인요청중"
		}else if(chk==2){
			return "승인됨"
		}else if(chk==3){
			return "승인거절"
		}
	}
	function showSdate(attendanceTy,approve){
		if(approve>1){
			return "<input type='text' readOnly data-bind='value:sDate' style='height :"+getElSize(70)+"; width:"+getElSize(210)+"; color:rgb(169,169,169)'>"
		}
		if(attendanceTy==1){//일근태
			return "<input type='text' class='date' data-bind='value:sDate' style='height :"+getElSize(70)+"; width:"+getElSize(210)+"'>"
		}else if(attendanceTy==2){//시근태
			return StimeList
		}else{
			return "근태유형 선택해주세요"
		}
	}
	function showEdate(attendanceTy,approve){
		if(approve>1){
			return "<input type='text' readOnly data-bind='value:eDate' style='height :"+getElSize(70)+"; width:"+getElSize(210)+"; color:rgb(169,169,169)'>"
		}
		if(attendanceTy==1){//일근태
			return "<input type='text' class='date' data-bind='value:eDate' style='height :"+getElSize(70)+"; width:"+getElSize(210)+"'>"
		}else if(attendanceTy==2){//시근태
			return EtimeList
		}else{
			return "근태유형 선택해주세요"
		}
	}

	var prdComplArray = [];
	var leaveWork="";
	function leave2Work(){
		
	};
	
	function saveRow_prdCompl(){
		$.showLoading();
		prdComplArray = [];
		var grid=$("#popup2 #tableDiv").data("kendoGrid");
		var gridDataSource=grid.dataSource.data();
		var break_Checker=false;
		var errMsg = "";
		$(gridDataSource).each(function(idx, data){
			console.log("--깎은수량--")
			console.log(data.f)
			var dvcId = data.a
			var prdNo
			console.log(data.prdNo)
			if(data.prdNo==null || data.prdNo==undefined){
				prdNo=" ";
			}else{
				prdNo=data.prdNo
			}
			var deviceName = data.b
			var tgCnt = data.c
			var partCyl = data.d
			var cntCyl = data.e
			var type = $("#popup2 #nd").val();
			var date = $("#popup2 #workDate").val();
			var worker = data.empCd;
//			var worker = $("#popup2 .worker").val();
			var workerCnt = data.f
			var lot1 = data.h
			var lot2 = data.i
			var lot3 = data.j
			var lot4 = data.k
			var lot5 = data.l
//			var prdNo = data.prdNo
			
			var obj = {
				worker : worker,
				prdNo : prdNo,	
				dvcId : dvcId,
				tgCnt : tgCnt,
				partCyl : partCyl,
				date : date,
				cntCyl : cntCyl,
				ty : type,
				workerCnt : workerCnt,
				lot1 : lot1,
				lot2 : lot2,
				lot3 : lot3,
				lot4 : lot4,
				lot5 : lot5,
			};
			
			prdComplArray.push(obj);
		});
		
		var obj = {
				val : prdComplArray
		}
		if(break_Checker==false){
			var url = "${ctxPath}/chart/addPrdComplHist.do";
			var param = "val=" + JSON.stringify(obj);
			
			console.log(obj)
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					if(data=="success"){
						modal_Window.content("저장되었습니다.").open();
						$.hideLoading()
						getPrdCmplData();
//						processWindow.close();
						return true;
						/* var url = "${ctxPath}/chart/upDateWorkTimeHist.do";
						var param = "worker=" + $("#popup2 .worker").val() + "&date="+$("#popup2 #workDate").val();
						$.ajax({
							url : url,
							data : param,
							type : "post",
							dataType : "text",
							success : function(data){
								if(data=="success"){
									modal_Window.content("퇴근 처리 되었습니다").open();
									$.hideLoading()
									getPrdCmplData();
									processWindow.close();
									return true;
								}else if(data=="duple"){
									modal_Window.content("이미 퇴근 처리 되었습니다").open();
									processWindow.close();
									getPrdCmplData();
									$.hideLoading()
									return false;
								}else if(data=="fail"){
									$.hideLoading()
									addWorkTime.open();
								}
							},error : function(error) {
								$.hideLoading()
						        addWorkTime.open();
						        return false;
						    }
						}); */
					}else{
						$.hideLoading()
						modal_Window.content("<center style="+'"'+"margin-top:10px;"+'"'+"><div>저장에 실패 하였습니다.</div></center><center style="+'"'+"margin-top:15px;"+'"'+'>').open();
						return false;
					}
				},error : function(e1,e2,e3){
					$.hideLoading(); 
					console.log(e1,e2,e3)
					alert("저장 오류입니다.")
					return false;
				}
			});
		}else{
			console.log(errMsg)
			modal_Window.content(errMsg).open();
			//alert("몇몇 장비의 생산 실적이 없습니다. 생산이 없을경우 0 을 입력해주세요.")
			return false;
		}
//		processWindow.close();
	};
	
	
	
	/* function saveRow_attendance(){
		$.showLoading();
		
		var deliver_obj = [];
		var update_deliver_obj = [];
		
		var object = {};
		var length = grid_attendance.dataSource.data().length;
		for(var i=0;i<length;i++){
			console.log("--test--")
			console.log(grid_attendance.dataSource.data()[i].approve)
			
			if(grid_attendance.dataSource.data()[i].attendanceTy==0){
				kendo.alert("근태유형을 입력해주세요")
				$.hideLoading()
				return false;
			}
			
			if(grid_attendance.dataSource.data()[i].attendanceDiv==0){
				kendo.alert("근태분류를 입력해주세요")
				$.hideLoading()
				return false;
			}
			
			if(grid_attendance.dataSource.data()[i].attendance==0){
				kendo.alert("근태를 입력해주세요")
				$.hideLoading()
				return false;
			}
			
			if(grid_attendance.dataSource.data()[i].approve==1){
				console.log("Approve List Existed")
			}else{
				if(grid_attendance.dataSource.data()[i].id==null || grid_attendance.dataSource.data()[i].id=='' || typeof grid_attendance.dataSource.data()[i].id=='undefined'){
					object.worker = grid_attendance.dataSource.data()[i].worker;
					object.attendanceTy = grid_attendance.dataSource.data()[i].attendanceTy;
					object.date = grid_attendance.dataSource.data()[i].date;
					object.attendanceDiv = grid_attendance.dataSource.data()[i].attendanceDiv;
					object.attendance = grid_attendance.dataSource.data()[i].attendance;
					object.sDate = grid_attendance.dataSource.data()[i].sDate;
					object.eDate = grid_attendance.dataSource.data()[i].eDate;
					object.reason = grid_attendance.dataSource.data()[i].reason;
					if(grid_attendance.dataSource.data()[i].approve==0 ){
						grid_attendance.dataSource.data()[i].approve=0
					}else if(grid_attendance.dataSource.data()[i].approve==1){
						grid_attendance.dataSource.data()[i].approve=1
					}
					object.approve= grid_attendance.dataSource.data()[i].approve;
					deliver_obj.push(object)
				}else{
					object.id = grid_attendance.dataSource.data()[i].id;
					object.worker = grid_attendance.dataSource.data()[i].worker;
					object.attendanceTy = grid_attendance.dataSource.data()[i].attendanceTy;
					object.date = grid_attendance.dataSource.data()[i].date;
					object.attendanceDiv = grid_attendance.dataSource.data()[i].attendanceDiv;
					object.attendance = grid_attendance.dataSource.data()[i].attendance;
					object.sDate = grid_attendance.dataSource.data()[i].sDate;
					object.eDate = grid_attendance.dataSource.data()[i].eDate;
					object.reason = grid_attendance.dataSource.data()[i].reason;
					if(grid_attendance.dataSource.data()[i].approve==0 || grid_attendance.dataSource.data()[i].approve==1 ){
						grid_attendance.dataSource.data()[i].approve=0
					}
					object.approve= grid_attendance.dataSource.data()[i].approve;
					update_deliver_obj.push(object)
				}
			}
		}
		
		if(deliver_obj.length>0){
			
			var ins_obj = new Object();
			ins_obj.val = deliver_obj;
			
			var url = "${ctxPath}/chart/addAttendanceHst.do";
			
			var param = "val=" + JSON.stringify(ins_obj);
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					$.hideLoading()
					if(data=="success"){
						addAttendance();
					}
				}
			});
		}
		
		if(update_deliver_obj.length>0){
			var ups_obj = new Object();
			ups_obj.val = update_deliver_obj;
			
			var url = "${ctxPath}/chart/updateAttendance.do";
			
			var param = "val=" + JSON.stringify(ups_obj);
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					$.hideLoading()
					if(data=="success"){
						addAttendance();
					}
				}
			});
		}
	}; */
	
	
	
	function saveRow_attendance(){
		$.showLoading();
		
		var deliver_obj = [];
		var update_deliver_obj = [];
		
		var object = {};
		var length = grid_attendance.dataSource.data().length;
		for(var i=0;i<length;i++){
			if(grid_attendance.dataSource.data()[i].attendanceTy==0){
				kendo.alert("근태유형을 입력해주세요")
				$.hideLoading()
				return false;
			}
			
			if(grid_attendance.dataSource.data()[i].attendanceDiv==0){
				kendo.alert("근태분류를 입력해주세요")
				$.hideLoading()
				return false;
			}
			
			if(grid_attendance.dataSource.data()[i].attendance==0){
				kendo.alert("근태를 입력해주세요")
				$.hideLoading()
				return false;
			}
			
			object = {};
			if(grid_attendance.dataSource.data()[i].id==null || grid_attendance.dataSource.data()[i].id=='' || typeof grid_attendance.dataSource.data()[i].id=='undefined'){
				object.worker = grid_attendance.dataSource.data()[i].worker;
				object.attendanceTy = grid_attendance.dataSource.data()[i].attendanceTy;
				object.date = grid_attendance.dataSource.data()[i].date;
				object.attendanceDiv = grid_attendance.dataSource.data()[i].attendanceDiv;
				object.attendance = grid_attendance.dataSource.data()[i].attendance;
				object.sDate = grid_attendance.dataSource.data()[i].sDate;
				object.eDate = grid_attendance.dataSource.data()[i].eDate;
				object.reason = grid_attendance.dataSource.data()[i].reason;
				object.approve= grid_attendance.dataSource.data()[i].approve;
				deliver_obj.push(object)
			}else{
				console.log(i)
				object.id = grid_attendance.dataSource.data()[i].id;
				console.log(object.id)
				object.worker = grid_attendance.dataSource.data()[i].worker;
				object.attendanceTy = grid_attendance.dataSource.data()[i].attendanceTy;
				object.date = grid_attendance.dataSource.data()[i].date;
				object.attendanceDiv = grid_attendance.dataSource.data()[i].attendanceDiv;
				object.attendance = grid_attendance.dataSource.data()[i].attendance;
				object.sDate = grid_attendance.dataSource.data()[i].sDate;
				object.eDate = grid_attendance.dataSource.data()[i].eDate;
				object.reason = grid_attendance.dataSource.data()[i].reason;
				object.approve = grid_attendance.dataSource.data()[i].approve;
				update_deliver_obj.push(object)
			}
			
			
		}
		
		if(deliver_obj.length>0){
			var ins_obj = new Object();
			ins_obj.val = deliver_obj;
			
			var url = "${ctxPath}/chart/addAttendanceHst.do";
			
			var param = "val=" + JSON.stringify(ins_obj);
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					$.hideLoading()
					if(data=="success"){
						addAttendance();
					}
				}
			});
		}
		
		if(update_deliver_obj.length>0){
			var upd_obj = new Object();
			upd_obj.val = update_deliver_obj;
		
			var url = "${ctxPath}/chart/updateAttendance.do";
			
			var param = "val=" + JSON.stringify(upd_obj);
			console.log(JSON.stringify(upd_obj))
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					$.hideLoading()
					if(data=="success"){
						addAttendance();
					}
				}
			});
		}
	}
	
	
	function saveRow(){
		$.showLoading()
		if(typeof(selectedRow.id)=="undefined"){
			deliver_obj = [];
			//update_deliver_obj = [];
			
			addRow();
			
			var worker = $("#popup_nonOp .worker").val();
			var dvcId = $("#popup_nonOp #dvcId").val()
			var date = $("#popup_nonOp #date").val()
			var nonOpTy = $("#popup_nonOp #nonOpTy").val()
			var sDate = $("#popup_nonOp #sTime").val()
			var eDate = $("#popup_nonOp #eTime").val()
			var nonOpTime = $("#popup_nonOp #nonOpTime").val()
			
			var object = {};
	
			object.worker = worker;
			object.dvcId = dvcId;
			object.date = date;
			object.nonOpTy = nonOpTy;
			object.sDate = sDate;
			object.eDate = eDate;
			object.nonOpTime = Number(nonOpTime.substring(0,nonOpTime.lastIndexOf("시간")) * 60) + Number(nonOpTime.substring(nonOpTime.lastIndexOf(" ")+1, nonOpTime.lastIndexOf("분")));
			
			deliver_obj.push(object)

			/* var $checked = $('input[type=checkbox]:checked');
		
			//납품번호, 공급업체, 납품일자, 품번, 규격, 단위, 수량, 단가, 금액
			
			if($checked.length==0){
				alert("항목을 선택하지 않으셨습니다.")
				return
			}
			
			$($checked).each(function(idx, data){
				var checked = data,
		        row = $(data).closest("tr"),
		        grid = $("#popup_nonOp #tableDiv").data("kendoGrid"),
		        dataItem = grid.dataItem(row);
				var selectedItem = grid.dataItem(grid.select());
				
				var id = dataItem.id;
				
				var worker = dataItem.worker;
				var dvcId = dataItem.dvcId;
				var date = dataItem.date
				var nonOpTy = dataItem.nonOpTy;
				var sDate = dataItem.sDate;
				var eDate = dataItem.eDate;
				var nonOpTime = dataItem.nonOpTime;
				
				var object = {};

				object.id = id;
				object.worker = worker;
				object.dvcId = dvcId;
				object.date = date;
				object.nonOpTy = nonOpTy;
				object.sDate = sDate;
				object.eDate = eDate;
				//문자 파싱
				object.nonOpTime = Number(nonOpTime.substring(0,nonOpTime.lastIndexOf("시간")) * 60) + Number(nonOpTime.substring(nonOpTime.lastIndexOf(" ")+1, nonOpTime.lastIndexOf("분")));
				
				
				deliver_obj.push(object)
				
				if(id!=""){
					update_deliver_obj.push(object)
				}
			})
		
			if(update_deliver_obj.length!=0) {
				updateRow()
			}; */
			
			var obj = new Object();
			obj.val = deliver_obj;
		
			var url = "${ctxPath}/chart/addNonOpHst.do";
			
			var param = "val=" + JSON.stringify(obj);
			
			 $.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					if(data=="success"){
						$("#popup_nonOp #tableDiv").empty()
						addNonOp();
						isAddRow = false;
					}
					$.hideLoading()
				}
			});	 			
		}else{
			var nonOpTime = $("#popup_nonOp #nonOpTime").val()
			var url = "${ctxPath}/chart/updateNonOpHist.do";
			var param = "id=" + selectedRow.id + 
						"&worker=" + $("#popup_nonOp .worker").val() + 
						"&dvcId=" + $("#popup_nonOp #dvcId").val() + 
						"&date=" + $("#popup_nonOp #date").val() + 
						"&nonOpTy=" + $("#popup_nonOp #nonOpTy").val() +
						"&sDate=" + $("#popup_nonOp #sTime").val()  + 
						"&eDate=" + $("#popup_nonOp #eTime").val() + 
						"&nonOpTime=" + Number(Number(nonOpTime.substring(0,nonOpTime.lastIndexOf("시간")) * 60) + Number(nonOpTime.substring(nonOpTime.lastIndexOf(" ")+1, nonOpTime.lastIndexOf("분"))));
						
			
			$.ajax({
				url : url,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					if(data=="success"){
						selectedRow = {}
						addNonOp();	
						isAddRow = false;
					}
					$.hideLoading()
				}
			});
		}
		
	};
	
	function updateRow_attendance(){
		var obj = new Object();
		obj.val = update_deliver_obj;
	
		var url = "${ctxPath}/chart/updateAttendance.do";
		
		var param = "val=" + JSON.stringify(obj);
		
		var str;
		$.ajax({
			url : url,
			async : false,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				str = data
			}
		});
		
		return str;
	};
	
	function updateRow(){
		var obj = new Object();
		obj.val = update_deliver_obj;
	
		var url = "${ctxPath}/chart/updateNonOpHist.do";
		
		var param = "val=" + JSON.stringify(obj);
		
		var str;
		$.ajax({
			url : url,
			async : false,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				str = data
				console.log(str)
			}
		});
		
		return str;
	};
	
	
	function addRow_attendance(){
		isAddRow = true;
		checkedIds = {}
		grid_attendance.dataSource.add({
			"선택" : "<input type='checkbox' class='checkbox' />",
			"worker" : $("#popup_attendance .worker option:selected").val(),
			"attendanceTy" : $("#popup_attendance #attendance_ty").val(),
			"date" : $("#popup_attendance #date").val(),
			"attendanceDiv" : $("#popup_attendance #attendance_div").val(),
			"attendance" : $("#popup_attendance #assiduity").val(),
			"sDate" : $("#popup_attendance #sTime").val(),
			"eDate" : $("#popup_attendance #eTime").val(),
			"reason" : $("#popup_attendance #reason").val()
		});	
		
		
		$(".k-button").css({
			"padding" : getElSize(10)
		})	
	};
	
	var isAddRow = false;
	function addRow(){
		isAddRow = true;
		checkedIds = {}
		grid.dataSource.add({
			"선택" : "<input type='checkbox' class='checkbox' />",
			"worker" : $("#popup_nonOp .worker option:selected").val(),
			"dvcId" : $("#popup_nonOp #dvcId").val(),
			"date" : $("#popup_nonOp #date").val(),
			"nonOpTy" : $("#popup_nonOp #nonOpTy").val() ,
			"sDate" : $("#popup_nonOp #sDate").val() + " " + $("#popup_nonOp #sTime").val(),
			"eDate" : $("#popup_nonOp #eDate").val() + " " + $("#popup_nonOp #eTime").val(),
			"nonOpTime" : $("#popup_nonOp #nonOpTime").val()
		});	
		
		
		$(".k-button").css({
			"padding" : getElSize(10)
		})
	};
	
	/* login_worker() 임시 주석
	function login_worker(){
		var url = "${ctxPath}/chart/login_worker.do";
		var param = "id=" + $("#popup .worker").val() +
					"&pwd=" + $("#popup #pwd").val();
		
		var str;
		$.ajax({
			url : url,
			async : false,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				str = data;
			}
		}); 
		
		return str;
	}; */
	
	function login_worker(){
		var str;
		
//		aIdoo 미연결일때
		if(workerCd==0){
			str="success"
		}else {
			var url = "${ctxPath}/chart/login_worker.do";
			var param = "id=" + workerCd +
						"&pwd=" + $(".dialog #pwd").val();
			
			$.ajax({
				url : url,
				async : false,
				data : param,
				type : "post",
				dataType : "text",
				success : function(data){
					str = data;
				}
			}); 
		}
		
		
		return str;
	};
	
	function delNonOpHistory(id){
		var url = "${ctxPath}/chart/delNonOpHistory.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				
			}
		});
	};
	
	function delAttendanceHistory(id){
		$.showLoading();
		var url = "${ctxPath}/chart/delAttendanceHistory.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				$.hideLoading();
			}, error:function(data){
				alert("저장 오류")
				$.hideLoading();
			}
		});
	};
	
	function dvcName(dvcId) {
		for (var i = 0; i < dvcArray.length; i++) {
			if (dvcArray[i].dvcId == dvcId) {
				return dvcArray[i].dvcName;
			}
	    }
	}
	
	
	function workerName(workerCd) {
		for (var i = 0; i < workerArray.length; i++) {
			if (workerArray[i].workerCd == workerCd) {
				return workerArray[i].name;
			}
	    }
	}
	
	function nonOpTyName(nonOpTy) {
		for (var i = 0; i < nonOpTyArray.length; i++) {
			if (nonOpTyArray[i].nonOpTy == nonOpTy) {
				return nonOpTyArray[i].nonOpName;
			}
	    }
	}
	
	function attendanceTyName(attendanceTy) {
		
		for (var i = 0; i < attendanceTyArray.length; i++) {
			if (attendanceTyArray[i].attendanceTy == attendanceTy) {
				return attendanceTyArray[i].attendanceTyName;
			}
	    }
	}
	
	function attendanceDivName(attendanceDiv) {
		for (var i = 0; i < attendanceDivArray.length; i++) {
			if (attendanceDivArray[i].attendanceDiv == attendanceDiv) {
				return attendanceDivArray[i].attendanceDivName;
			}
	    }
	}
	
	function attendanceName(attendance) {
		for (var i = 0; i < attendanceArray.length; i++) {
			if (attendanceArray[i].attendance == attendance) {
				return attendanceArray[i].attendanceName;
			}
	    }
	}
	
	
	
	var workerArray = [];
	var dvcArray = [];
	var nonOpTyArray = [];
	var attendanceTyArray = [];
	var attendanceDivArray = [];
	var attendanceArray = [];
	
	function categoryDropDownAttendanceTyNameEditor(container, options){
		$('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "attendanceTyName",
			 dataValueField: "attendanceTy",
			 dataSource: attendanceTyArray
		});	
	}
	
	function categoryDropDownAttendanceDivNameEditor(container, options){
		$('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "attendanceDivName",
			 dataValueField: "attendanceDiv",
			 dataSource: attendanceDivArray
		});	
	}
	
	function categoryDropDownAttendanceEditor(container, options){
		$('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "attendanceName",
			 dataValueField: "attendance",
			 dataSource: attendanceArray
		});	
	}
	
	
	function categoryDropDownWorkerNameEditor(container, options) {
		 $('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "name",
			 dataValueField: "workerCd",
			 dataSource: workerArray
		});
 	}
	
	function categoryDropDownDvcNameEditor(container, options) {
		 $('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "dvcName",
			 dataValueField: "dvcId",
			 dataSource: dvcArray
		});
  	}
	
	function categoryDropDownNonOpTyEditor(container, options) {
		 $('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoBind: false,
			 dataTextField: "nonOpName",
			 dataValueField: "nonOpTy",
			 dataSource: nonOpTyArray
		});
  }
	
	
	function calcTimeDiff(start, end){
		var startHour = Number(start.substr(0,2));
		var startMinute = Number(start.substr(3,2));
		var endHour = Number(end.substr(0,2));
		var endMinute = Number(end.substr(3,2));
		
		var startTime = new Date($("#popup_nonOp #sDate").val() + "," + $("#popup_nonOp #sTime").val());
		var endTime = new Date($("#popup_nonOp #eDate").val() + "," + $("#popup_nonOp #eTime").val());
		
		var timeDiff = (endTime.getTime() - startTime.getTime()) / 1000 /60;
		
		return parseInt(timeDiff/60) + "시간 " + parseInt(timeDiff%60) + "분";
	};
	
	var dayTime = 510; //08:30
	var nightTime = 1230; //20:30

	function chkWorkeTimeValid(){
		var hour = Number(new Date().getHours()) * 60;
		var minute = Number(new Date().getMinutes());
		var current = hour + minute;
		
		var min = 120;
		var max = 360;
		
		var str;
		
		if((dayTime - min) <= current && (dayTime + max) >= current){
			str = "success";	
			console.log("주간")
		}else if((nightTime - min) <= current && (24*60) >= current){
			str = "success";	
			console.log("야간")	
		}else{
			console.log("no check")
			str = "success";
		}
	
		//02:30
		if(150 >= current){
			console.log("야간")	
			str = "success";	
		}	
		
		return str;
	} 
	
	/* 2017.09.20 John
		작업자 입력 로그인창을  Kendo 의 Windows로 대체하고
		Close 이벤트에서 함수를 불러오도록 수정
	*/
	function clickOk(){
		console.log("Click Ok")
		selectedRow = {}
		console.log(cMenu)
		$(".worker").val(workerCd)
		if(login_worker()=="success"){
			if(cMenu == "report2work"){//출근보고
				console.log("report2work!!!!")
				addStartWork();
				dialog.close();
				cMenu=""
			}else if(cMenu == "report2leave"){
				console.log("report2leave!!!!")
				addEndWork();
				dialog.close();
				cMenu=""
			}else if(cMenu == "report2leave_work"){//퇴근보고
				processWindow.open();
				console.log("report2leave_work!!!!")
				addPrdCmpl();
				getOprNameList();
				dialog.close();
				cMenu=""
			}else if(cMenu=="report2Attendance"){//근태보고 보고
				dialog.close();
				cMenu=""
			}else if(cMenu=="report2NonOp"){//비가동 보고
				dialog.close();
				cMenu=""
			}else if(cMenu=="test"){//자주검사
				dialog.close();
				cMenu=""
			}else if(cMenu="Report2Faulty"){//불량신고
				dialog.close();
				cMenu=""
			}	
		}else{
			alert("패스워드가 일치하지 않습니다.")
			$("#pwd").focus();
		}
	};
	
	var listTemp = [];
	
	/*===================================================================== */
	
	function uniqArr(arr) {
        var chk = [];
        for (var i = 0; i < arr.length; i++) {
            if (chk.length == 0) {
                chk.push(arr[i]);
            } else {
                var flg = true;
                for (var j = 0; j < chk.length; j++) {
                    if (chk[j].prdNo == arr[i].prdNo) {
                        flg = false;
                        break;
                    }
                }
                if (flg) {
                    chk.push(arr[i]);
                }
            }
        }
        return chk;
    }
	/*===================================================================== */
	
	
	
	function getOprNameList(){
		
		var url = ctxPath + "/chart/getOprNameList.do"
		/* var param = "shopId=" + shopId + 
					"&prdNo=" + $("#popup2 #prdNo").val(); 	 */
		var param = "shopId=" + shopId;/*  + 
					"&prdNo=" + prdNoInput.value();  */	
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				if(json.length==0) options += "<option>공정 없음</option>";
				
				$(json).each(function(idx, data){
					var oprNo;
					if(data.name=="0010"){
						oprNo = "R삭";
					}else if(data.name=="0020"){
						oprNo = "MCT";
					}else if(data.name=="0030"){
						oprNo = "CNC";
					}
					options += "<option value='" + data.name + "'>" + oprNo + "</option>";
				});
				
				$("#popup2 #operation").html(options).change(getPrdCmplData);
				getPrdCmplData();
			}
		});	
	};
	
	function viewWorker(empCd){
		var nm;
		if(empCd==0){
			return "미배치"
		}
		for(i=0, len=workerList.length; i<len; i++){
			if(workerList[i].workerCd==empCd){
				nm=workerList[i].name
				return nm
			}else{
				nm="미배치"
			}
		}
		return nm
	}
	firstIn=true
	//작업보고
	function getPrdCmplData(){
		$.showLoading(); 
		var date=$("#popup2 #workDate").val() ;
		
		if(firstIn){
			if(moment().format("HH:mm")<"09:00"){
				date = moment().subtract(1, 'day').format("YYYY-MM-DD")
				$("#nd").val(1)
			}else{
				date = moment().format("YYYY-MM-DD")
				$("#nd").val(2)
			}
			firstIn=false;
		}
		
		$("#popup2 #workDate").val(date)
		var list=[];
		var url = "${ctxPath}/chart/getTodayWorkByName.do";
		var param = "&date=" + $("#popup2 #workDate").val() + 
		"&empCd=" + $("#popup2 .worker").val() +
		"&workIdx=" +$("#nd").val();
		console.log(param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				try{
					if(typeof json[0].workTy != 'undefined'){
						$("#popup2 #nd").val(json[0].workTy)
					}
				}catch(e){
					modal_Window.content("등록된 작업이 없거나 작업한 장비가 라우팅 되지 않았습니다.<br> 장비가 라우팅 되었는지, 일 생산계획에 작업자가 등록 되었는지 확인 하세요.").open();
				}
				
				
				var tr = "";
				$(".content").remove();
				$(json).each(function(idx, data){
					var targetRatio = Math.round(Number(data.workerCnt) / Number(data.tgCnt) * 100);
					
					if(Number(data.tgCnt)==0){
						targetRatio = 0
					}
					
					var arr=new Object();
					arr.prdNo=data.prdNo
					arr.a=data.dvcId;
					arr.b=decode(data.name);
					arr.c=data.tgCnt;
					arr.d=data.partCyl;
					arr.e=data.cntCyl
					arr.f=data.workerCnt;
					arr.g=targetRatio;
					arr.h=data.lot1;
					arr.i=data.lot2;
					arr.j=data.lot3;
					arr.k=data.lot4;
					arr.l=data.lot5;
					arr.action=''
					arr.empCd=data.empCd
					list.push(arr);
				});
				
				var dataSource = new kendo.data.DataSource({
	                data: list,
	                autoSync: true,
	                schema: {
	                    model: {
	                      id: "a",	 
	                      fields: {
	                    	 attributes:{style:"text-align:right;"},
	                    	 prdNo :{ editable: false, nullable: true , attributes:{style:"text-align:right;"}},
	                         b: { editable: false, nullable: true , attributes:{style:"text-align:right;"}},
	                         c: { editable: false,validation: { required: true } },
	                         d: { editable: false,validation: { required: true } },
	                         e: { editable: false,validation: { required: false },nullable:true },
	                         f: { editable: true,validation: { required: false },nullable:true },
	                         g: { editable: false,validation: { required: true } },
	                         h: { editable: true,validation: { required: false },nullable:true },
	                         i: { editable: true,validation: { required: false },nullable:true },
	                         j: { editable: true,validation: { required: false },nullable:true },
	                         k: { editable: true,validation: { required: false },nullable:true },
	                         l: { editable: true,validation: { required: false },nullable:true },
	                         action: {
	     		          		editable: false,
		     		        },
		     		        newRow: {
		     		          		editable: false,
		     		          		 type: "Boolean"
		     		        }
	                      }
	                    }
	                }
	             });
				
				$("#popup2 #tableDiv").kendoGrid({
					dataSource:dataSource,
					editable:false,
					dataBound:function(){
				          grid = this;
				            grid.tbody.find('tr').each(function(){
				             var item = grid.dataItem(this);
				             kendo.bind(this,item);
				         })
				    },
				    change: function(e) {
				        var selectedRows = this.select();
				        console.log(selectedRows)
			      	},
			      	edit: function(e) {
			      		console.log(e)
			      	    if (!e.model.isNew()) {
			      	      // Disable the editor of the "id" column when editing data items
			      	      var numeric = e.container.find("input[name=id]").data("kendoNumericTextBox");
			      	      numeric.enable(false);
			      	    }
			      	},
					columns:[
					{
						field:"empCd",title:"작업자",template:"#=viewWorker(empCd)#",width:getElSize(200),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white;  font-size:" + getElSize(47)
            			}
					},{
						field:"prdNo",title:"차종",width:getElSize(370),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white;  font-size:" + getElSize(47)
            			}							
					},
					{
						field:"b",title:$("#device").val(),width:getElSize(280),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white;  font-size:" + getElSize(47)
            			}							
					},
					{
						field:"c",title:$("#production_Plan_Quantity").val(),width:getElSize(180),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(30)
            			}							
					},{
						title:$("#prdct_performance").val(),attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			},
						columns:[{
							field:"d",title:"HMI",width:getElSize(150),attributes: {
	            				style: "text-align: center; color:white; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
	            			}							
						},{
							field:"e",title:"aIdoo",width:getElSize(180),attributes: {
	            				style: "text-align: center; color:white; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
	            			}							
						},{
							field:"f",
							title:$("#production_quantity").val(),
							width:getElSize(210),
							template:'<input type="number" min="0" data-role="number" style="width:100%;"  data-bind="value:f"/>',
							attributes: {
	            				style: "text-align: center; color:white; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(30)
	            			}					
						}]
					},{
						field:"g",
						title:$("#target_ratio").val(),
						width:getElSize(250),
						template:"#=kendo.toString((f/c)*100, 'n0')#%",
						attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							
					},{
						title:"LOT Number",attributes: {
            				style: "text-align: center; color:white; font-size:" + getElSize(45)
            			},headerAttributes: {
            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
            			}							,
						columns:[{
							field:"h",
							title:"#1",width:getElSize(130),attributes: {
	            				style: "text-align: center; color:white; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
	            			}							,template:"<input data-role='text'style='width:100%;' data-bind='value:h'/>"
						},{
							field:"i",
							title:"#2",width:getElSize(130),attributes: {
	            				style: "text-align: center; color:white; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
	            			}							,template:"<input data-role='text' style='width:100%;'  data-bind='value:i'/>"
						},{
							field:"j",
							title:"#3",width:getElSize(130),attributes: {
	            				style: "text-align: center; color:white; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
	            			}							,template:"<input data-role='text' style='width:100%;' data-bind='value:j'/>"
						},{
							field:"k",
							title:"#4",width:getElSize(130),attributes: {
	            				style: "text-align: center; color:white; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
	            			}							,template:"<input data-role='text' style='width:100%;' data-bind='value:k'/>"
						},{
							field:"l",
							title:"#5",width:getElSize(130),attributes: {
	            				style: "text-align: center; color:white; font-size:" + getElSize(45)
	            			},headerAttributes: {
	            				style: "text-align: center; background-color:black; color:white; font-size:" + getElSize(47)
	            			}							,template:"<input data-role='text' style='width:100%;' data-bind='value:l'/>"
						}/* ,{
							field:'action',
							title:"버튼",
							template : '#=action#',
							width:getElSize(130)
						} */
						]
					}]
				})
				
				$("#popup2 #table").append(tr);
				$("#popup2 #table td, #popup2 #table td input").css({
					"font-size" : getElSize(80) + "px",
					"text-align" : "center",
				})				
				$("#popup2 #table td").css({
					"border" : getElSize(10) + "px solid black",
					"color" : "white"
				})
				$.hideLoading(); 
			}
		});
	};
	
	function showPopup(el){
		$("#pwd").val("");
		var id = el.id;
		cMenu = id;
		console.log("CMenu : " + cMenu)
		if(id=="report2work"){//출근보고
			/* $("#popup").css({
				"display" : "inline"
			}); */
			dialog.open();
			$("#submit").html("${go2Work}")
			
		}else if(id=="report2leave"){
			dialog.open();
			$("#submit").html("퇴근")	
		}else if(id=="report2leave_work"){//퇴근보고
			//getPrdNo();
			/* $("#popup").css({
				"display" : "inline"
			}); */
			dialog.center().open();
			$("#submit").html("${report_to_Work}");
			$("#date").val(getToday().substr(0,10))
			
		}else if(id=="report2Attendance"){//근태보고
			getAttendanceTy();
			
			/* $("#popup").css({
				"display" : "inline"
			}); */
			dialog.open();
			$("#submit").html("${report2Attendance}");
		}else if(id=="report2NonOp"){//비가동보고
			getDvcList();
			getNonOpTy();
			/* $("#popup").css({
				"display" : "inline"
			}); */
			dialog.open();
			$("#submit").html("${confirm}");
			
			
		}else if(id=="test"){//자주검사
			/* $("#popup").css({
				"display" : "inline"
			}); */
			dialog.open();
			$("#submit").html("${confirm}");
		}else if(id=="Report2Faulty"){//불량신고
			/* $("#popup").css({
				"display" : "inline"
			}); */
			dialog.open();
			$("#submit").html("${confirm}");
		}
	}

	function getAttendanceTy(){
		var url = "${ctxPath}/chart/getCdGroup1.do";
		
		$.ajax({
			url : url,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
			
				var options = "";
				$(json).each(function(idx, data){
					options += "<option value='" + data.attendanceTy + "'>" + decode(data.attendanceTyName) + "</option>";
					var object = {
							"attendanceTy" : data.attendanceTy
							,"attendanceTyName" : decode(data.attendanceTyName)
					}
					
					attendanceTyArray.push(object)
				});
				var object = {
					"attendanceTy" : 0
					,"attendanceTyName" :"===선택해주세요==="
				}
				attendanceTyArray.push(object)
				$("#popup_attendance #attendance_ty").html(options).change(getAttendanceDiv);
				getAttendanceDiv();
			}
		});
	};
	
	function getAttendance(){
		var url = "${ctxPath}/chart/getCdGroup3.do";
		var param = "group2=" + $("#popup_attendance #attendance_div").val()
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			async : false,
			success : function(data){
				var json = data.dataList;
			
				var options = "";
				$(json).each(function(idx, data){
					options += "<option value='" + data.attendance + "'>" + decode(data.attendanceName) + "</option>";
					var object = {
							"attendance" : data.attendance
							,"attendanceName" : decode(data.attendanceName)
					}
					
					attendanceArray.push(object)
				});
				
				$("#popup_attendance #assiduity").html(options);
				$.hideLoading()
				/* kendoUI GRID 근태보고  */
				
				/* kendoUI GRID 근태보고  */
			}
		});
	};
	
	function getAttendanceDiv(){
		var url = "${ctxPath}/chart/getCdGroup2.do";
		var param = "group1=" + $("#popup_attendance #attendance_ty").val()
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			async : false,
			dataType : "json",
			success : function(data){
				var json = data.dataList;
			
				var options = "";
				$(json).each(function(idx, data){
					options += "<option value='" + data.attendanceDiv + "'>" + decode(data.attendanceDivName) + "</option>";
					var object = {
							"attendanceDiv" : data.attendanceDiv
							,"attendanceDivName" : decode(data.attendanceDivName)
					}
					
					attendanceDivArray.push(object)
				});
				
				$("#popup_attendance #attendance_div").html(options).change(getAttendance);;
				getAttendance();
			}
		});
	};
	
	function closePopup(){
		$("#popup").css({
			"display" : "none"
		});
	}
	
	function closePopup2(){
		$("#popup2").css({
			"display" : "none"
		});
	}
	
	function closePopup_nonOp(){
		$("#reason").val("")
		if(isAddRow){
			if(confirm("입력한 내용을 무시하시겠습니까?")){
				$("#popup_nonOp, #popup_attendance").css({
					"display" : "none"
				});
				isAddRow = false;
			}else{
				return;
			}	
		}else{
			$("#popup_nonOp, #popup_attendance").css({
				"display" : "none"
			});
		}
	};
	
	function onKeyEvt(evt){
		if(evt.keyCode==13) clickOk();
	};
	
	$(function() {
		
		$("#work_logging").click(function(){
			firstIn=true
		})
    	$("#popup2 #workDate").datepicker({
    		onSelect:function(){
    			console.log($("#popup2 #workDate").val());
    			getOprNameList()
    		}
    		,maxDate: 0
    	})
		$("#popup2 #nd").change(function(){
			getOprNameList()
		})	    
	});
	
	function cMenuNone(){
		cMenu = "";
		console.log("cMenu : " + cMenu)
	}
	
	var maintenance_report;
	var report_check, detailsTemplate;
	var combobox;
	var addWorkTime;
	$(function(){
		
		
		
		$("#saveRow").kendoButton({
			click: function(e) {
//				confirm.content("<div style="+'"'+"font-weight:bolder;"+"color:red;"+"margin:5% 25%;"+'"'+'>'+$("#workDate").val()+"    "+$("#nd option:selected").text() +" </div><div style="+'"'+"margin:5%;"+'"'+'>'+"의 일자로 퇴근 보고가 맞습니까?</div>");			
				confirm.content("<div style="+'"'+"font-weight:bolder;"+"color:red;"+"margin:5% 25%;"+'"'+'>'+$("#workDate").val()+"    "+$("#nd option:selected").text() +" </div><div style="+'"'+"margin:5%;"+'"'+'>'+"의 일자로 저장하시겠습니까?</div>");			
				confirm.open();
		    }
		});
		
		addWorkTime = $("#addWorkTime").kendoDialog({
		   visible: false,
	       title:false,
	       closable: false,
	       height : getElSize(800),
	       width : getElSize(1200),
			actions: [{
			        text: "닫기",
			        action: function(e){
			        },
			        primary: true
			    }]
			,open: function() {
				$("#start_time_date").val(moment())
				$("#start_time_date").val(moment())
			  }
			}).data("kendoDialog")
		
		confirm = $("#confirm").kendoDialog({
		   visible: false,
	       title:false,
	       closable: false,
	       height : getElSize(500),
	       width : getElSize(1200),
			actions: [{
			        text: "네",
			        action: function(e){
			        	var m = moment();
			        	var workStart;
			        	var workEnd;
						if($("#nd").val()==2){
							workStart = moment($("#workDate").val(), "YYYY-MM-DD HH:mm:ss");
							workStart.add("hours", 8).add("m", 00)
							workEnd = moment(workStart, "YYYY-MM-DD HH:mm:ss")
							workEnd.add("hours", 14)
						}else if($("#nd").val()==1){
							workStart = moment($("#workDate").val(), "YYYY-MM-DD HH:mm:ss");
							workStart.add("hours", 20).add("m", 00)
							workEnd = moment(workStart, "YYYY-MM-DD HH:mm:ss")
							workEnd.add("hours", 14)
						}
						if(m > workStart && m < workEnd){
							saveRow_prdCompl();
						}else{
							saveRow_prdCompl();
//							modal_Window.content("올바른 근무 보고 시간이 아닙니다. 일자를 명확히 선택하거나 주간/야간 구분을 명확히 해주세요.").open();
						}
			        },
			        primary: true
			    },{
			        text: "아니오"
			    }]
		}).data("kendoDialog")
		
		maintenance_report = $("#maintenance_report").kendoGrid({
			height : getElSize(1720),
			selectable: "multiple",
			change: function(e){
				 var selected = $.map(this.select(), function(item) {
                     return $(item).text();
                 });
                 console.log("Selected: " + selected.length + " item(s), [" + selected.join(", ") + "]");
			},
			columns: [
			/* { selectable: true, width: "50px" }, */
			{
			    field: "date",
			    title: "일시" ,
		    	headerAttributes:{
                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
                },attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                }
			}, {
			  field: "name",
			  title: "장비",
		    	headerAttributes:{
                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
                },attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                }
			}, {
				field: "lastAlarmMsg",
			   title: "최근 알람내용",
		    	headerAttributes:{
                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
                } ,attributes: {
                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
                }
			},
			{ command: { text: "보수 신청하기", click: maintenance_Do_report }, title: "보수 <br> 신청하기 ", width: "180px",headerAttributes:{
            	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
            }}]
		}).data("kendoGrid")
		
		report_check = $("#report_check")
	    .kendoWindow({
	        title: "보수 요청 메뉴",
	        modal: true,
	        visible: false,
	        resizable: false,
	        width: getElSize(1500),
	        open: function(e) {
	        	combobox = $("#worker_combo").kendoComboBox({
	    			dataTextField:"name",
	    			dataValueField:"workerCd",
	    			noDataTemplate: '작업자를 발견하지 못하였습니다.',
	    			placeholder: "선택해주세요"
	    			}).data("kendoComboBox");
	        	combobox.setDataSource(ds)
	        	
	        	var url = "${ctxPath}/common/getAllWorkerList.do";

	    		$.ajax({
	    			url : url,
	    			dataType : "json",
	    			type : "post",
	    			success : function(data){
	    				var json = data.dataList;
	    				
	    				var options = "";
	    				
	    				$(json).each(function(idx, data){
	    					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
	    					
	    					var object = {
	    							"workerCd" : data.id
	    							,"name" : decode(data.name)
	    					} 
	    					
	    					workerArray.push(object)
	    				});
	    				$(".worker_report").html(options)
	    				$(".worker_report").val($(".worker").val())
	    				$(".worker_report").attr("disabled",true);
	    			}
	    		});
	        	
	        	
	          },close:function(e){
	        	  maintenance_report_function()
	          }
	    }).data("kendoWindow");
		
		detailsTemplate = kendo.template($("#template").html());
		detailsTemplate_1 = kendo.template($("#nonOpr_template").html());
		nonOpr_Do_report_window = $("#nonOpr_Do_report")
	    .kendoWindow({
	        title: "비가동 보고하기",
	        modal: true,
	        visible: false,
	        resizable: false,
	        width: getElSize(3000),
          	open: function(e) {
	      		
          	},close:function(e){
        	  
          	}
	    }).data("kendoWindow");
		
		
	})
	
	function maintenance_Do_report(e){
		var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
		report_check.content(detailsTemplate(dataItem));
		report_check.center().open();
	}
	var nonOpTy=[];
	
	
	var sDate;
	var eDate;
	var sDateTime;
	var eDateTime;
	
	function nonOpr_Do_report(e){
		
		var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
		console.log(dataItem)
		nonOpr_Do_report_window.content(detailsTemplate_1(dataItem));
		nonOpr_Do_report_window.center().open();
		
    	sDate= moment(dataItem.sDate).format("YYYY-MM-DD");
      	eDate= moment(dataItem.eDate).format("YYYY-MM-DD");
      	sDateTime= moment(dataItem.sDate).format("HH:mm:ss");
      	eDateTime= moment(dataItem.eDate).format("HH:mm:ss");
		
		dataSource = new kendo.data.DataSource({
		  data: [
		    { 
		      sDate: moment(dataItem.sDate).format("YYYY-MM-DD"), 
		      eDate: moment(dataItem.eDate).format("YYYY-MM-DD"), 
		      sDateTime: moment(dataItem.sDate).format("HH:mm:ss"), 
		      eDateTime: moment(dataItem.eDate).format("HH:mm:ss"),
		      reason : 0
		    }
		  ]
		});
		
		nonOpTyDataSource = new kendo.data.DataSource({});
		
		//이유 선택박스 가지고오기
		var url = ctxPath + "/chart/getNonOpTy.do"
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				var options = ""
				$(json).each(function(idx, data){
					
					var obj = {
							"nonOpTy" : data.id,
							"nonOpName" : decode(data.nonOpTy)
					}
					
					nonOpTyDataSource.add(obj)
					nonOpTy.push(obj)
				});
			}	
		})
		
		var url = ctxPath + "/chart/getNonopHistory.do"
		var param = "idx="+dataItem.seq+
					"&sDate="+dataItem.sDate+
					"&dvcId="+dataItem.dvcId+
					"&workIdx="+dataItem.workIdx ;
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				console.log(json)
				if(json.length>0){
					console.log("넣을 데이터 있음")
					dataSource = new kendo.data.DataSource({})
					$(json).each(function(idx, data){
						data.sDate = moment(data.startTime).format("YYYY-MM-DD")
						data.sDateTime = moment(data.startTime).format("HH:mm:ss")
						
						data.eDate = moment(data.endTime).format("YYYY-MM-DD")
						data.eDateTime = moment(data.endTime).format("HH:mm:ss")
						dataSource.add(data);
					});
				}
				
				reportGrid = $("#reportGrid").kendoGrid({
					dataSource : dataSource,
		    		selectable: "row",
		            height: getElSize(1500),
		            pageable: false,
		            scrollable: true,
		            groupable: false,
		            sortable: false,
		            editable: true,
		            columns: [
		    			{
		        			field: "sDate",
		    				title: "${start}",
		    				editor: categorySDateEditor,
		        			attributes: {
		            			style: "text-align: center; font-size: " + getElSize(35) + "px"
		          			},
		          			format: "{0:yyyy-MM-dd}"
		    			},
		    			{
		        			field: "sDateTime",
		    				title: "시작시간",
		    				width:getElSize(480),
		    				editor: categorySDateTimeEditor,
		        			attributes: {
		            			style: "text-align: center; font-size: " + getElSize(35) + "px"
		          			}
		    			},
		    			{
		        			field: "eDate",
		    				title: "${end}",
		    				editor: categoryEDateEditor,
		        			attributes: {
		            			style: "text-align: center; font-size: " + getElSize(35) + "px"
		          			},
		          			format: "{0:yyyy-MM-dd}"
		    			},
		    			{
		        			field: "eDateTime",
		    				title: "종료시간",
		    				width:getElSize(480),
		    				editor: categoryEDateTimeEditor,
		        			attributes: {
		            			style: "text-align: center; font-size: " + getElSize(35) + "px"
		          			}
		    			},
		    			{
		        			field: "time",
		    				title: "비가동시간",
		    				template: "#=(typeof sDate == 'undefined' || typeof eDate =='undefined')?'':caculateNonTime(sDate+' '+sDateTime, eDate+' '+eDateTime)#",
		    				editable : true,
		        			attributes: {
		            			style: "text-align: center; font-size: " + getElSize(35) + "px"
		          			}
		    			},
		    			{
		        			field: "reason",
		    				title: "비가동사유",
		    				template : "#=changeNonOpty(reason)# ",
		    				editor: categoryDropDownEditorByNonOpty,
		    				editable : false,
		        			attributes: {
		            			style: "text-align: center; font-size: " + getElSize(35) + "px"
		          			}
		    			},{
				        	field:"action",	
				        	headerTemplate : "버튼",
				        	editable : true,
				        	template:"#=(typeof action=='undefined')?'<button onclick=insertRow(this)>분할</button>':action#",
				        	width:getElSize(180),
							headerAttributes: {
								style: "text-align: center; background-color:black; font-size:" + getElSize(37)+ "; color:white;"
							 },
							attributes: {
									style: "text-align: center; font-size:" + getElSize(30) + "; color:white;"
							  }
				        }
		    		]
		    	}).data("kendoGrid")
		    	
		    	$("#reportSubmit").css({
					"font-size":getElSize(65),
					"margin-right": getElSize(145),
					"width" : getElSize(430),
					"height" : getElSize(150),
					"margin-bottom" : getElSize(10),
					"margin-top" : getElSize(30)
				})
					
				$("#closeSubmit").css({
					"font-size":getElSize(65),
					"margin-right": getElSize(145),
					"width" : getElSize(290),
					"height" : getElSize(150)
				})
				
				$(".k-widget.k-window").css({
					"top" : getElSize(280)
				})
				
				$("#reportGrid").css({
					"width" : getElSize(2940)
				})
				
				$("#noonOpReporter").css({
					"display": "inline",
				    "float": "right",
				    "position": "absolute",
				    "font-size": getElSize(85),
				    "right": getElSize(50),
				    "top" : getElSize(65)
				})
				
				$("h2#noonOpTime").css({
					"left" : getElSize(440)
				})
				
			}	
		})
	}
	
	function insertRow(e){
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		 var row = $("#reportGrid").data("kendoGrid")
         .tbody
         .find("tr[data-uid='" + dataItem + "']");
		 var initData = reportGrid.dataItem(row)
		 var idx = reportGrid.dataSource.indexOf(reportGrid.dataItem(row));
		 reportGrid.dataItem(row).checkSelect=true
		 reportGrid.dataSource.insert(idx + 1, {
			 
			 sDate:initData.eDate,
			 sDateTime:initData.eDateTime,
			 eDate:initData.eDate,
			 eDateTime:initData.eDateTime,
			 reason:0,
			 action:"<button onclick=deleteRow1(this)>삭제</button>"
			 
		 })
	}
	
	function deleteRow1(e){
		var dataItem = $(e).closest("tr")[0].dataset.uid;
		var row = $("#reportGrid").data("kendoGrid")
        .tbody
        .find("tr[data-uid='" + dataItem + "']");
		 var idx = reportGrid.dataSource.indexOf(reportGrid.dataItem(row));
		 var dataItem = reportGrid.dataSource.at(idx);
		 reportGrid.dataSource.remove(dataItem)
	}
	
	
	function caculateNonTime(sDate, eDate){
		if(typeof sDate == 'undefined'){
			
		}else if(typeof eDate =='undefined'){
			
		}else{
			console.log(eDate)
			console.log(sDate)
			return moment(eDate).diff(sDate)/1000/60
		}
	}
	
	function changeNonOpty(num){
		var length = nonOpTy.length;
		for(var i=0;i<length;i++){
			if(nonOpTy[i].nonOpTy==num){
				return nonOpTy[i].nonOpName
			}
		}
		return "사유를 선택해주세요"
	}
	
	function categoryDropDownEditorByNonOpty(container, options) {
		$('<input name="' + options.field + '"/>')
           .appendTo(container)
           .kendoDropDownList({
           	valuePrimitive: true,
               dataTextField: "nonOpName",
               dataValueField: "nonOpTy",
               optionLabel: "==선택==",
               dataSource: nonOpTyDataSource
         });
    }
	
	function categorySDateEditor(container, options) {
		var input = $("<input type='date' class='date' min='"+sDate+"' max='"+eDate+"' style='width : " + getElSize(390) + "px; font-size:"+getElSize(32)+"'/>");
	    input.attr("name", options.field);
	    input.appendTo(container);
	}
	
	function categorySDateTimeEditor(container, options) {
		$('<input id="' + options.field + '"/>')
           .appendTo(container)
           
           var syear = moment(sDate +' '+sDateTime).year();
           var smonth = moment(sDate +' '+sDateTime).month();
           var sday = moment(sDate +' '+sDateTime).day();
           var shour = moment(sDate +' '+sDateTime).hour();
           var sminute = moment(sDate +' '+sDateTime).minute();
           
           var eyear = moment(eDate +' '+eDateTime).year();
           var emonth = moment(eDate +' '+eDateTime).month();
           var eday = moment(eDate +' '+eDateTime).day();
           var ehour = moment(eDate +' '+eDateTime).hour();
           var eminute = moment(eDate +' '+eDateTime).minute();
           
           console.log(syear +" "+smonth+" "+sday+" "+sminute)
           console.log(eyear +" "+emonth+" "+eday+" "+eminute)
           
           $("#"+options.field).kendoTimePicker({
               format: "HH:mm",
               min: new Date(syear, smonth-1, sday, shour, sminute, 0), 
               max: new Date(eyear, emonth-1, eday, ehour, eminute, 0), 
               change: function() {
                   var value = this.value();
                   options.model.sDateTime=moment(value).format("HH:mm:ss")
                   console.log(moment(options.model.eDate+' '+options.model.eDateTime).diff(options.model.sDate+' '+options.model.sDateTime)/1000/60)
                   options.model.time=moment(options.model.eDate+' '+options.model.eDateTime).diff(options.model.sDate+' '+options.model.sDateTime)/1000/60
                   reportGrid.dataSource.fetch()
               }
           });
	}
	
	
	
	function categoryEDateEditor(container, options) {
		var input = $("<input type='date' class='date' min='"+sDate+"' max='"+eDate+"' style='width : " + getElSize(390) + "px; font-size:"+getElSize(32)+"'/>");
	    input.attr("name", options.field);
	    input.appendTo(container);
	}
	
	
	function categoryEDateTimeEditor(container, options) {
		$('<input type="time" id="' + options.field + '" min="'+sDateTime+'" max=""/>')
           .appendTo(container)
           
           var syear = moment(sDate +' '+sDateTime).year();
           var smonth = moment(sDate +' '+sDateTime).month();
           var sday = moment(sDate +' '+sDateTime).day();
           var shour = moment(sDate +' '+sDateTime).hour();
           var sminute = moment(sDate +' '+sDateTime).minute();
           
           var eyear = moment(eDate +' '+eDateTime).year();
           var emonth = moment(eDate +' '+eDateTime).month();
           var eday = moment(eDate +' '+eDateTime).day();
           var ehour = moment(eDate +' '+eDateTime).hour();
           var eminute = moment(eDate +' '+eDateTime).minute();
           console.log(syear +" "+smonth+" "+sday+" "+sminute)
           console.log(eyear +" "+emonth+" "+eday+" "+eminute)
           $("#"+options.field).kendoTimePicker({
               format: "HH:mm",
               min: new Date(syear, smonth-1, sday, shour, sminute, 0), 
               max: new Date(eyear, emonth-1, eday, ehour, eminute, 0), 
               change: function() {
                   var value = this.value();
                   options.model.eDateTime=moment(value).format("HH:mm:ss")
                   console.log(moment(options.model.eDate+' '+options.model.eDateTime).diff(options.model.sDate+' '+options.model.sDateTime)/1000/60)
                   options.model.time=moment(options.model.eDate+' '+options.model.eDateTime).diff(options.model.sDate+' '+options.model.sDateTime)/1000/60
                   reportGrid.dataSource.fetch()
               }
           });
	}
	
	
	function maintenance_report_function(){
		var today = moment();
		today = today.hours(0)
		today = today.minute(0);
		today = today.format("YYYY-MM-DD")
		
		var dataSource = new kendo.data.DataSource({
			schema: {
                model: {
                    id: "Report",
                    fields: {
                    	dvcId: { editable: false },
                    	name: { editable: false },
                        resolver: { type: "String", editable: true },
                        workIdx: { type: "number" },
                        date: { type: "Date",  editable: false },
                        report: {type: "String" },
                        lastAlarmMsg:{ type:"String" }
                    }
                }
            }
		});
		
		var url = "${ctxPath}/chart/getMaintenanceReport.do";
		var param = "empCd=" + $(".worker").val() + "&date=" + today;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			async : false,
			dataType : "json",
			success : function(data){
				var json = data.dataList;
			
				var options = "";
				$(json).each(function(idx, data){
					
					var object = {
							"dvcId" : data.dvcId
							,"name" : decode(data.name)
							,"workIdx" : data.workIdx
							,"date" : data.date
							,"lastAlarmMsg" : data.lastAlarmMsg
					}
					dataSource.add(object)
				});
				maintenance_report.setDataSource(dataSource)
				
				$("#maintenance_report").css({
					"background-color":"black",
					"color" : "white"
				})
				$("#maintenance_report .k-grid-header th.k-header").css({
					"background-color":"gray",
					"color" : "white"
				})
				
				$("#maintenance_report tr:odd").css({
					"background-color":"rgb(50,50,50)",
				      "color" : "white"
			   });
				
				$("#maintenance_report tr:even").css({
				    "background-color":"rgb(33,33,33)",
				    "color" : "white"
				});
			}
		});
	}
	
	function do_Report(device){
		
		var today = moment();
		today = today.hours(0)
		today = today.minute(0);
		today = today.format("YYYY-MM-DD")
		
		if($("#report").val()==null || typeof $("#report").val() == "undefined" || $("#report").val()==""){
			kendo.alert("내용을입력하여 주십시오.")
			
			$("#report").focus();
			
			return;
		}
		
		if( combobox.value()==null || typeof combobox.value() == "undefined" || combobox.value()==""){
			kendo.alert("조치자를 입력 해주십시오.")
			
			$("#worker_combo").focus();
			return;
		}
		
		var url = "${ctxPath}/chart/setMaintenanceReport.do";
		var param = "empCd=" + $(".worker").val() + "&date=" + today + "&dvcId=" + device + "&solver=" + combobox.value() + "&report=" + $("#report").val();
		$.showLoading()
		$.ajax({
			url : url,
			data : param,
			type : "post",
			async : false,
			success : function(data){
				report_check.close();
				$.hideLoading()
			},error : function(error){
				console.log(error)
				$.hideLoading()
			}
		});
		
		
	}
	
	function nonOpr_Report_f(dvcId, empCd, chartStatus, seq){
		
		var waitTime = moment(eDate+' '+eDateTime).diff(sDate+" "+sDateTime)/1000/60
		var url = "${ctxPath}/chart/addNonOpHst_One.do";
		
		var dataList = reportGrid.dataSource.data();
		var length = dataList.length;
		
		var diffTime=0;
		for(var i=0;i<length;i++){
			if(typeof dataList[i].idx=='undefined' || dataList[i].idx==null || dataList[i].idx==''){
				dataList[i].idx=0
			}
			dataList[i].empCd=empCd;
			dataList[i].dvcId=dvcId;
			dataList[i].chartStatus=chartStatus;
			dataList[i].seq=seq;
			if(typeof dataList[i].reason=='undefined' || dataList[i].reason==null || dataList[i].reason==0){
				alert("사유를 등록해주세요")
//				kendo.alert("사유를 등록해주세요")
				return;
			}
			dataList[i].time = moment(dataList[i].eDate+' '+dataList[i].eDateTime).diff(dataList[i].sDate+" "+dataList[i].sDateTime)/1000/60
			diffTime += dataList[i].time
			dataList[i].sDate = dataList[i].sDate+" "+dataList[i].sDateTime
			dataList[i].eDate = dataList[i].eDate+" "+dataList[i].eDateTime
			if(diffTime>waitTime){
				kendo.alert("보고시간이 비가동 시간을 초과합니다")
				return;
			}
		}
		
		var obj = new Object();
		obj.val = dataList;
		var param = "val=" + JSON.stringify(obj);

		console.log("보고저장")
		console.log(param)
		
		$.ajax({
				url : url,
				data : param,
				type : "post",
				success : function(data){
					getWorkerWatingTime();
					alert("저장되었습니다.")
					nonOpr_Do_report_window.close()
				},error : function(request,status,error){
					getWorkerWatingTime();
					nonOpr_Do_report_window.close()
				}
		});
	}
	
	
</script>

<script type="text/x-kendo-template" id="template">
<div id="details-container">
    <p>장비명 : <mark>#= name #</mark> 일자 : <mark>#= date #</mark></p>
    <dl style="margin-top:10px;">최근 알람 내용 :<mark> #= lastAlarmMsg # </mark></dl>
	<dl style="margin-top:10px;">
	    보고자 : <select class="worker_report" style="width:20%; font-size:20px;"></select> 개선 조치 담당자 : <input id="worker_combo" style="width:30%; font-size:15px;">
	</dl>
	<dl style="margin-top:10px;">
           내용 : <input id="report" type="text" name="report" style="width:60%; font-size:20px;" placeholder="내용을 입력하세요">
    </dl>
	<dl style="margin-top:10px;">
	    <center><input type="submit" onclick="do_Report(#= dvcId #)" value="보고하기" 
style="font-size:25px; font-weight: bolder; margin-right: 50px; width: 150px; background-color: rgb(121, 218, 76); border: 0px solid;height: 50px;
 margin-bottom: 10px; border: 0px solid; background-color: rgb(121, 218, 76);"> 
		<input type="submit" onclick="report_check.close()" value="닫기" style="font-size:25px; font-weight: bolder; width: 100px; height: 50px;"></center>
	   
	</dl>
</div>
</script>

<script type="text/x-kendo-template" id="nonOpr_template">
<div id="details-container">
    <p>장비명 : <mark>#= oprNo #</mark> </p>
	<div>
		<p style="display: inline">비가동 시작시간 : <h2 id="noonOpTime" style="display: inline; color: darkred; position: absolute;">#=sDate#</h2></p>
		<p style="display: inline">종료시간 :  <h2 id="noonOpTime" style="color: darkblue; display: inline; position: absolute;">#=eDate#</h2> </p>
	</div>
	비가동 시간 :<mark> #= time # 분</mark> 비가동 유형 :<mark> #= chartStatus # </mark> <p id="noonOpReporter">보고자 : #=name#</p>
	<div id="reportGrid">
	</div>
	<div>
	    <center>
			<input type="submit" id="reportSubmit" onclick="nonOpr_Report_f('#=dvcId#','#=empCd#', '#=chartStatus#', '#=seq#')" value="보고하기" > 
			<input type="submit" id="closeSubmit" onclick="nonOpr_Do_report_window.close()" value="닫기">
		</center>
	</div>
</div>
</script>

<script>
$(function(){
	$("#details-container").css({
		"font-size" : getElSize(55),
		"margin" : getElSize(10)
	})
})
</script>

</head>
<!-- <body oncontextmenu="return false"> -->
<body>

	<div id="report_check" style="background: gray;"></div>
	<div id="nonOpr_Do_report" style="background: gray;"></div>

	<div id="window"></div>
	
	<!-- 퇴근 보고 확인 창 -->
	<div style="display: none;">
		<div id="confirm" ></div>
		
		<div id="modal_Dialog" style="width: 300px; height : 100px; border: 5px solid red !important; background-color: rgb(50, 50, 50); color : white;">
			<center style="margin-top:10px;"><div><spring:message code="fail_to_Save"></spring:message></div></center>
			<center style="margin-top:15px;"><button id="close_Button" type="button"><spring:message code="cancel"></spring:message></button></center>
		</div>
		<div id="dialog" style="background-color: rgb(50, 50, 50);">
			<div id="tabstrip">
			    <ul>
			        <li id="work_logging" onclick="getOprNameList()" style="font-size:20px; font-weight:bold"><spring:message code="report_to_Work"></spring:message></li>
			        <li onclick="addAttendance()" style="font-size:20px;"><spring:message code="report2Attendance"></spring:message></li>
			        <li onclick="getDvcList()" style="font-size:20px;"><spring:message code="report2NonOp"></spring:message></li>
			        <li onclick="getGroup()" style="font-size:20px;"><spring:message code="test"></spring:message></li>
		         	<li onclick="addRow_Faulty()" style="font-size:20px;"><spring:message code="Report2Faulty"></spring:message></li>
		         	<li onclick="maintenance_report_function()" style="font-size:20px;"><spring:message code="maintenance_Request"></spring:message></li>
		         	<li onclick="processWindow.close()" style="font-size: 20px; background-color: red; float: right; font-weight:bold;"><font color="yellow"><spring:message code="cancel"></spring:message></font></li>
			    </ul>
			    <!-- 첫번째 Tab -->
			    
			    <div id="popup2" style="width:100%;">
			      
					<table style="width: 95%">
						<tr>
							<td><spring:message code="worker"></spring:message> : <select class="worker"></select></td>
							<td><spring:message code="date_"></spring:message> : <input type="text" id="workDate" class="workDate"/></td>
							<td>
								<spring:message code="division"></spring:message>
								<select id="nd">
										<option value="2"><spring:message code="day"></spring:message> </option>
										<option value="1"><spring:message code="night"></spring:message> </option>
								</select>
							</td>
							<td>
								<p id="addWork" onclick="addWorkMenu.open()">장비추가하기</p>
							</td>
						</tr>
					</table>
						
					<div id="tableDiv">
								<input type='hidden' id="device" value=<spring:message code="device"></spring:message>>
								<input type='hidden' id="production_Plan_Quantity" value=<spring:message code="production_Plan_Quantity"></spring:message>>
								<input type='hidden' id="production_quantity" value=<spring:message code="production_quantity"></spring:message>>
								<input type='hidden' id="prdct_performance" value=<spring:message code="prdct_performance"></spring:message>>
								<input type='hidden' id="target_ratio" value=<spring:message code="target_ratio"></spring:message>>
					</div>
					<center>
						<button id="saveRow" type="button">
							<spring:message code="report_on_work_performance"></spring:message>
						</button>
					</center>
				</div>
				<script type="text/javascript">
				var addWorkMenu , prdNoInput, dvcIdInput;
				
				$(function(){
					
					prdNoInput = $("#prdNo1").kendoComboBox({
						  animation: false,
						  dataTextField : "prdNo",
						  dataValueField : "prdNo",
						  change:function(e){
							  console.log(prdNoInput.value())
							  var url = "${ctxPath}/order/getDvcIdByPrdNo.do";
							  var param = "prdNo=" + prdNoInput.value()
								$.ajax({
									url : url,
									data : param,
									type : "post",
									dataType : "json",
									success : function(data){
										var json = data.dataList;
										console.log(json)
										dvcIdInput.setDataSource(json)
									}
								});
						  },
						  noDataTemplate: '해당 차종이 없습니다.'
					}).data("kendoComboBox");
					
					dvcIdInput = $("#dvcId").kendoDropDownList({
						dataTextField : "name"
					}).data("kendoDropDownList");
					
					
					addWorkMenu = $("#addWorkMenu").kendoDialog({
						visible:false,
						open: function() {
							var url = "${ctxPath}/common/getPrdNoListByRtng.do";
							$.ajax({
								url : url,
								type : "post",
								dataType : "json",
								success : function(data){
									var json = data.dataList;
									console.log(json)
									prdNoInput.setDataSource(json)
								}
							});
						  }
					  }).data("kendoDialog");
					
					$("input[type=radio][name=hasH]").change(function() {
	
						if(prdNoInput.value().indexOf("_RH") != -1
								|| prdNoInput.value().indexOf("_LH") != -1){
							if(this.value=="_RH"){
								prdNoInput.value(prdNoInput.value().substring(0, prdNoInput.value().indexOf("_LH")))
							}else{
								prdNoInput.value(prdNoInput.value().substring(0, prdNoInput.value().indexOf("_RH")))
							}
							
							prdNoInput.value(prdNoInput.value() + this.value)
						}else{
							prdNoInput.value(prdNoInput.value() + this.value)
						}
						getOprNameList();
					}); 
				})
				
			function getPrdNo(){
			
			var url = "${ctxPath}/chart/getMatInfo.do";
			var param = "shopId=" + shopId;
			
					$.ajax({
						url : url,
						data : param,
						type : "post",
						dataType : "json",
						success : function(data){
							
							var json = data.dataList;
							
							for(var i=0; i<json.length; i++){
								if(json[i].prdNo.indexOf('_LH') != -1 && json[i].prdNo.indexOf('_RW') == -1){
									json[i].prdNo = json[i].prdNo.substring(0,json[i].prdNo.indexOf('_LH'))
									json[i].hasH = 1
								}else if(json[i].prdNo.indexOf('_RH') != -1 && json[i].prdNo.indexOf('_RW') == -1){
									json[i].prdNo = json[i].prdNo.substring(0,json[i].prdNo.indexOf('_RH'))
									json[i].hasH = 1
								}else{
									json[i].hasH = 0
								}
							}
							
							var arrayList = uniqArr(json);
							json=arrayList;
							console.log(json)
							
							var option = "";
							
							$(json).each(function(idx, data){
								var object = {
										"prdNo" : data.prdNo
										,"hasH" : data.hasH
								}
								prdNoName.add(object);
								prdNoList.push(object)
							});
							prdNoName.fetch()
							prdNoInput.setDataSource(prdNoName)
							
						}
					});
				};
				
				function addDevice(){
					var selectItem = dvcIdInput.dataSource.data()[dvcIdInput.select()]
					console.log(selectItem)
					console.log(prdNoInput.value())
					if(typeof selectItem =='undefined' || typeof prdNoInput.value()==''){
						kendo.alert("차종과 장비를 선택해주세요.")
						return false;
					}else{
						var gridList = $("#popup2 #tableDiv").data("kendoGrid").dataSource;
						var gridListData = $("#popup2 #tableDiv").data("kendoGrid").dataSource.data();
						var length = gridListData.length
						console.log(length)
						for(var i=0; i<length; i++){
							console.log(gridListData[i].a)
							console.log(selectItem.dvcId)
							if(gridListData[i].a == selectItem.dvcId){
								kendo.alert("이미 등록되어 있는 장비를 선택하였습니다.<br>다른 장비를 선택하여 주십시오.")
								return false;							
							}
						}
						var url = "${ctxPath}/common/getDeviceCnt.do";
						var param = "date=" + $("#workDate").val() + "&dvcId=" + selectItem.dvcId + "&workIdx=" + $("#nd").val();
						
						$.ajax({
							url : url,
							data : param,
							type : "post",
							dataType : "json",
							success : function(data){
								
								var json = data.dataList;
								
								gridList.insert({
									a : selectItem.dvcId,
									prdNo : prdNoInput.value(),
									b : selectItem.name,
									d : json[0].partCyl,
									e : json[0].cntCyl,
									f : json[0].cntCyl,
									c : json[0].tgCnt,
									action:"<button id='delete' onclick=deleteRow(this)>삭제</button>",
									newRow:true
								})
								
								addWorkMenu.close();
							}
						});
						
					}
				}
				
				function deleteRow(e){
					var dataItem = $(e).closest("tr")[0].dataset.uid;
					var grid = $("#popup2 #tableDiv").data("kendoGrid")
					 var row = $("#popup2 #tableDiv").data("kendoGrid")
			        .tbody
			        .find("tr[data-uid='" + dataItem + "']");
					 var idx = grid.dataSource.indexOf(grid.dataItem(row));
					 var dataSource = grid.dataSource;
					 var dataItem = dataSource.at(idx);
					 grid.dataSource.remove(dataItem)
				}
				</script>
				
				<!-- 두번째 Tab -->
			    <div style="width:100%;">
					<div id="popup_attendance" style="width:100%;">
							<spring:message code="worker"></spring:message> : 
							<select class="worker"></select>
							<spring:message code="date_"></spring:message> : 
							<input type="date" id="date" readonly></select>
							<button id="searchAttendance" onclick='addAttendance()'><i class="fa fa-search" aria-hidden="true"></i>검색</button>
							<button id="addAttendance" onclick='addAttendanceRow(this)'><i class="fa fa-plus-circle" aria-hidden="true"></i>추가</button>
						<%-- 	<table style="width: 95%">
								<tr>
									
									<td><spring:message code="attendance_ty"></spring:message></td>
									<td><select id="attendance_ty"></select></td>
									
								</tr>
								<tr>
									<td><spring:message code="attendace_div"></spring:message></td>
									<td><select id="attendance_div"></select></td>
									<td><spring:message code="assiduity"></spring:message></td>
									<td><select id="assiduity"></select></td>
									<td><spring:message code="start"></spring:message></td>
									<td><input type="time" id="sTime"></td>
								</tr>
								<tr>
									<Td><spring:message code="reason"></spring:message></Td>
									<Td colspan="3"><input id="reason"></Td>
									<td><spring:message code="end"></spring:message></td>
									<td><input type="time" id="eTime"></td>
								</tr>
							</table> --%>
							<div id="tableDiv"></div>
						<center>
							<div id="buttonGroup" style="margin-top: 10px !important;">
							<button id="saveRow_attendance" type="button" onclick="saveRow_attendance()">
								<i class="fa fa-floppy-o" aria-hidden="true"></i><spring:message code="save"></spring:message>
							</button>
							<button id="confirmAttendanceBtn" type="button" onclick="confirm_attendance()">
								<i class="fa fa-check-square-o" aria-hidden="true"></i>승인요청하기
							</button>
							</div>
						</center>
					</div>
				</div>
				<div id="confirm_attendance" ></div>
				<script>
				function confirm_attendance(){
					$.showLoading();
					
					var deliver_obj = [];
					var update_deliver_obj = [];
					
					var object = {};
					var length = grid_attendance.dataSource.data().length;
					for(var i=0;i<length;i++){
						if(grid_attendance.dataSource.data()[i].attendanceTy==0){
							kendo.alert("근태유형을 입력해주세요")
							$.hideLoading()
							return false;
						}
						
						if(grid_attendance.dataSource.data()[i].attendanceDiv==0){
							kendo.alert("근태분류를 입력해주세요")
							$.hideLoading()
							return false;
						}
						
						if(grid_attendance.dataSource.data()[i].attendance==0){
							kendo.alert("근태를 입력해주세요")
							$.hideLoading()
							return false;
						}
						
						object = {};
						if(grid_attendance.dataSource.data()[i].id==null || grid_attendance.dataSource.data()[i].id=='' || typeof grid_attendance.dataSource.data()[i].id=='undefined'){
							object.worker = grid_attendance.dataSource.data()[i].worker;
							object.attendanceTy = grid_attendance.dataSource.data()[i].attendanceTy;
							object.date = grid_attendance.dataSource.data()[i].date;
							object.attendanceDiv = grid_attendance.dataSource.data()[i].attendanceDiv;
							object.attendance = grid_attendance.dataSource.data()[i].attendance;
							object.sDate = grid_attendance.dataSource.data()[i].sDate;
							object.eDate = grid_attendance.dataSource.data()[i].eDate;
							object.reason = grid_attendance.dataSource.data()[i].reason;
							if(grid_attendance.dataSource.data()[i].approve==0 || grid_attendance.dataSource.data()[i].approve==1 ){
								grid_attendance.dataSource.data()[i].approve=1
							}
							object.approve= grid_attendance.dataSource.data()[i].approve;
							deliver_obj.push(object)
						}else{
							console.log(i)
							object.id = grid_attendance.dataSource.data()[i].id;
							console.log(object.id)
							object.worker = grid_attendance.dataSource.data()[i].worker;
							object.attendanceTy = grid_attendance.dataSource.data()[i].attendanceTy;
							object.date = grid_attendance.dataSource.data()[i].date;
							object.attendanceDiv = grid_attendance.dataSource.data()[i].attendanceDiv;
							object.attendance = grid_attendance.dataSource.data()[i].attendance;
							object.sDate = grid_attendance.dataSource.data()[i].sDate;
							object.eDate = grid_attendance.dataSource.data()[i].eDate;
							object.reason = grid_attendance.dataSource.data()[i].reason;
							if(grid_attendance.dataSource.data()[i].approve==0 || grid_attendance.dataSource.data()[i].approve==1){
								grid_attendance.dataSource.data()[i].approve=1
							}
							object.approve = grid_attendance.dataSource.data()[i].approve;
							update_deliver_obj.push(object)
						}
						
						
					}
					
					if(deliver_obj.length>0){
						var ins_obj = new Object();
						ins_obj.val = deliver_obj;
						
						var url = "${ctxPath}/chart/addAttendanceHst.do";
						
						var param = "val=" + JSON.stringify(ins_obj);
						
						$.ajax({
							url : url,
							data : param,
							type : "post",
							dataType : "text",
							success : function(data){
								$.hideLoading()
								if(data=="success"){
									addAttendance();
								}
							}
						});
					}
					
					if(update_deliver_obj.length>0){
						var upd_obj = new Object();
						upd_obj.val = update_deliver_obj;
					
						var url = "${ctxPath}/chart/updateAttendance.do";
						
						var param = "val=" + JSON.stringify(upd_obj);
						console.log(JSON.stringify(upd_obj))
						$.ajax({
							url : url,
							data : param,
							type : "post",
							dataType : "text",
							success : function(data){
								$.hideLoading()
								if(data=="success"){
									addAttendance();
								}
							}
						});
					}
				}
				
				function deleteAttendanceRow(e){
					
					kendo.confirm("이 항목을 삭제 하시겠습니까?").then(function () {
						
						var dataItem = $(e).closest("tr")[0].dataset.uid;
						var row = $("#popup_attendance #tableDiv").data("kendoGrid")
				        .tbody
				        .find("tr[data-uid='" + dataItem + "']");
						var idx = grid_attendance.dataSource.indexOf(grid_attendance.dataItem(row));
						var dataItem = grid_attendance.dataSource.at(idx);
						if(dataItem.id==null || dataItem.id==''||typeof dataItem.id=='undefined'){
							grid_attendance.dataSource.remove(dataItem);
						}else{
							var url = "${ctxPath}/chart/delAttendanceHistory.do";
							var param = "id=" + dataItem.id;
							$.ajax({
								url : url,
								data : param,
								type : "post",
								dataType : "text",
								success : function(data){
									grid_attendance.dataSource.remove(dataItem);
									$.hideLoading()
									if(data=="success"){
									}
								},error : function(error){
									console.log(error)
								}
							});
						}
		            }, function () {
		            	
		            });
				}
				
				function addAttendanceRow(e){
					 grid_attendance.dataSource.insert(grid_attendance.dataSource.data().length+1, {
						 worker : $("#popup_attendance .worker").val(),
						 attendanceTy : 0,
						 date: moment().format("YYYY-MM-DD"),
						 attendanceDiv: 0,
						 attendance: 0,
						 sDate: moment().format("YYYY-MM-DD"),
						 eDate: moment().format("YYYY-MM-DD"),
						 reason: '',
						 msg:'',
						 approve: '0'
					 })
					 
				}
				
				var attendance_dialog;
				
				attendance_dialog = $("#confirm_attendance").kendoDialog({
							visible: false,
					       title:false,
					       closable: false,
					       height : getElSize(500),
					       width : getElSize(1200),
							actions: [{
						        text: "네",
						        action: function(e){
						        	saveRow_attendance();
						        },
						        primary: true
							    },{
						        text: "아니오"
						    }]
				}).data("kendoDialog")
				
				
				
				
				</script>
				<!-- 세번째 탭 -->
			    <div style="width:100%;">
			    	<div id="popup_nonOp" style="width:100%;">
						<div id="tabstrip_Oprreport">
					    	<ul>
					    		<li id="OprList" onclick="addNonOp()">
			                        	비가동 현황
			                    </li>
			                    <li onclick="listReportNonOp()">
			                        	비가동 보고 내역
			                    </li>
					    	</ul>
					    	<div style="background: rgb(50,50,50);">
					    		<table style="width: 95%">
										<tr>
											<td id="tdGetWaitTime" style=""><spring:message code="worker"></spring:message>  :  <select class="worker"></select></td>
											<td id="tdGetWaitTime"><spring:message code="date_"></spring:message> : <input type="test" id="sDate" class="date" readonly></td>
											<td><button class="checkSearchBtn" id="getWaitTime" onclick="getWorkerWatingTime()"><i class="fa fa-search" aria-hidden="true"></i>비가동 조회</button></td>
										</tr>
								</table>
								<div id="tableDiv"></div>
								<div id="confirmNonOp" style="display: none;"></div>
					    	</div>
					    	<div id="tableDiv2"></div>
			    		</div>
					</div>
			    </div>
				
			    <script>
			    
			    $("#tableDiv,#tableDiv2").css({
			    	"width" : getElSize(3460)
			    })
			    
			    $("input#sDate").css({
			    	"width" : getElSize(329),
			    	"font-size" : getElSize(60)
			    })
			    $(function(){
			    	$("#sDate").datepicker({
			    	})
			    })
			    
			    $("#confirmNonOp").kendoDialog({
			        actions: [{
			            text: "OK",
			            visible: false,
			            action: function(e){
			            	
			                return false;
			            },
			            primary: true
			        },{
			            text: "Cancel"
			        }]
			      });
			    
			    var popup_nonOp, popup_nonOp2;
			    
			    function getDvcList(){
					addNonOp();
					getWorkerWatingTime();
				};
				
				function getWorkerWatingTime(){
					$.showLoading()
					var url = "${ctxPath}/chart/getWorkerWatingTime.do";
					var param = "sDate="+$("#sDate").val()+"&eDate="+$("#sDate").val()+"&empCd="+$("#popup2 .worker").val();
					var workerArray=[];
					console.log(url+"?"+param)
					$.ajax({
						url : url,
						dataType : "json",
						data : param,
						type : "post",
						success : function(data){
							var json = data.dataList;
							console.log(json)
							$(json).each(function(idx, data){
								
								var object = {
										"name" : decode(data.name),
										"oprNo" : decode(data.oprNo),
										"dvcId" : data.dvcId,
										"sDate" : data.sDate,
										"eDate" : data.eDate,
										"seq" : data.seq,
										"workIdx" : data.workIdx,
										"chartStatus" : data.chartStatus,
										"time" : data.waitTime,
										"checkNum" : data.checkNum,
										"totalWaitTime" : data.totalWaitTime,
										"empCd" : data.empCd,
										"exist" : data.exist
								} 
								
								workerArray.push(object)
							});
							
							var dataSource = new kendo.data.DataSource({
					                data: workerArray,
					                schema: {
					                    model: {
					                      id: "ProductID",
					                      fields: {
					                    	dvcId: {
					                    		editable: false
					                        },
					                        eDate: {
					                        	editable: false
					                        },
					                        name: {
					                        	editable: false
					                        },
					                        nonOpTy: {
					                        	editable: true, type: "string", 
					                        },
					                        sDate: {
					                        	editable: false
					                        },
					                        oprNo:{
					                        	editable: false
					                        },
					                        time:{
					                        	editable: false
					                        }
					                      }
					                    }
					                  },
					                group: [{ field: "oprNo", title:"${machine_Name}" , dir: "desc",  aggregates:[ {field: "time", aggregate:"sum"} ]}],
					                sort: [{
				                    	field: "sDate" , dir:"asc" 
				                    }]
					        	});
							dataSource.fetch().then(function(){
								  var data = dataSource.data();
							});
							popup_nonOp.setDataSource(dataSource);
							
							$.hideLoading()
						}
					});
				}
				
				function getNonOpTy(){
					var url = ctxPath + "/chart/getNonOpTy.do"
					
					$.ajax({
						url : url,
						dataType : "json",
						type : "post",
						success : function(data){
							var json = data.dataList;
							
							var options = ""
							$(json).each(function(idx, data){
								options += "<option value='" + data.id + "'>" + decodeURIComponent(data.nonOpTy).replace(/\+/gi, " ") + "</option>";
								var obj = {
										"nonOpTy" : data.id,
										"nonOpName" : decode(data.nonOpTy)
								}
								
								nonOpTyArray.push(obj)
							});
							
							$("#popup_nonOp #nonOpTy").html(options);
						}	
					})
				}
			    
				function addNonOp(){
					console.log("비가동 보고 시작")
					$("#popup_nonOp").css({
						"display" : "inline"
					});
					
					$("#popup_nonOp #date").val(getToday().substr(0,10));
					
					$("#popup_nonOp #tableDiv th").css({
						"font-size" : getElSize(35) + "px",
						"text-align" : "center"
					})
					
					$(".k-button").css({
						"padding" : getElSize(10)
					})
				};
			    
			    function listReportNonOp(){
			    	$.showLoading()
			    	 var url = "${ctxPath}/chart/getNonOpHist.do";
					var param = "empCd=" + $("#popup_nonOp .worker").val()
					
					var dataList = [];
					var dataSource = new kendo.data.DataSource();
					$.ajax({
						url : url,
						data : param,
						async : false,
						type : "post",
						dataType : "json",
						success : function(data){
							var json = data.dataList;
							console.log(json)
							$(json).each(function(idx, data){
								var obj = {
										"time" : data.time,
										"name" : decode(data.name),
										"worker" : decode(data.worker),
										"reason" : decode(data.reason),
										"chartStatus" : data.chartStatus,
										"sDate" : data.sDate,
										"eDate" : data.eDate
								}	
								dataSource.add(obj)
							});
							
						
						dataSource.fetch().then(function(){
							  var data = dataSource.data();
						});
						popup_nonOp2.setDataSource(dataSource);
							
						$.hideLoading();
						}
					});
			    }
			    
			    function checkAll(e){
					console.log($("#checkall").is(":checked"))
					if($("#checkall").is(":checked")){
						gridlist=popup_nonOp.dataSource.data();
			        	$(gridlist).each(function(idx,data){
			        		data.checkSelect = true;
			        	})
					}else{
						gridlist=popup_nonOp.dataSource.data();
			        	$(gridlist).each(function(idx,data){
			        		data.checkSelect = false;
			        	})
					}
					popup_nonOp.dataSource.fetch();
				}
			    
			    function checkRow(e){
					console.log("event click")
					var gridList=popup_nonOp.dataSource.data();
					var dataItem = $(e).closest("tr")[0].dataset.uid;
					 var row = $("#popup_nonOp #tableDiv").data("kendoGrid")
			         .tbody
			         .find("tr[data-uid='" + dataItem + "']");
					 var initData = popup_nonOp.dataItem(row)
					 console.log(initData)
					 var uid = initData.uid;
					 var length = gridList.length
					 for(var i=0; i<length; i++){
						 if(uid==gridList[i].uid){
							 if(gridList[i].checkSelect){
								 gridList[i].checkSelect=false;
								 popup_nonOp.dataSource.fetch();
							 }else{
								 gridList[i].checkSelect=true;
								 popup_nonOp.dataSource.fetch();
							 }
						 }
					 }
				}
			   
			    
			    $(function(){
			    	
			    	popup_nonOp = $("#popup_nonOp #tableDiv").kendoGrid({
				      	selectable: "row",
			            height: getElSize(1550),
			            pageable: false,
			            scrollable: true,
			            groupable: false,
			            sortable: false,
			            editable: true,
			            columns: [
			            			{
			                			field: "oprNo",
			            				title: "${device}",
			            				editor: categoryDropDownDvcNameEditor,
			            				editable : false,
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			},headerAttributes: {
			                  				style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			            			},
			            			{
			    	        			field: "chartStatus",
			    	    				title: "${chartStatus}",
			    	    				template : "#= chartStatus #",
			    	    				editable : false,
			    	    				attributes: {
			                				style: "text-align: center; color:white; font-size:" + getElSize(50)
			                			},
			                			filterable: {
			              		          multi: true,
			              		          search: true
			              		        },headerAttributes: {
			                  				style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			    	    			},
			            			{
			                			field: "sDate",
			            				title: "${start}",
			            				editable : false,
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			},headerAttributes: {
			                  				style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			            			},
			            			{
			                			field: "eDate",
			                			title: "${end}",
			                			editable : false,
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			},headerAttributes: {
			                  				style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			            			},
			            			{
			                			field: "time",
			                			title: "${noon_Operation_time}",
			                			editable : false,
			                			template : "#=time# 분",
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			},groupFooterTemplate: " ${total}: #= sum # ${minute}",filterable: false,headerAttributes: {
			                  				style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			            			},
			            			{ command: { text: "비가동 보고하기", click: nonOpr_Do_report }, title: "비가동<br> 보고하기 ", width: "180px",headerAttributes:{
				                        	style: "text-align: center; font-size:" + getElSize(50) +"px; white-space: initial;vertical-align: middle;"
				                        }
			            			}
			            		],
			        }).data("kendoGrid");
			    	
			    	popup_nonOp2 = $("#popup_nonOp #tableDiv2").kendoGrid({
				      	selectable: "row",
			            height: getElSize(1750),
			            pageable: false,
			            scrollable: true,
			            groupable: false,
			            sortable: false,
			            editable: false,
			            columns: [
			                      	{
			                			field: "worker",
			                			title: "${worker}",
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			            			}, 
			            			{
			                			field: "name",
			            				title: "${device}",
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			            			},
			            			{
			                			field: "chartStatus",
			                			title: "비가동 유형",
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			            			},
			            			{
			                			field: "reason",
			                			title: "비가동 사유",
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			            			},
			            			{
			                			field: "sDate",
			            				title: "${start}",
			            				width: getElSize(600),
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			            			},
			            			{
			                			field: "eDate",
			                			title: "${end}",
			                			width: getElSize(600),
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			}
			            			},
			            			{
			                			field: "time",
			                			title: "${noon_Operation_time}",
			                			width: getElSize(300),
			                			attributes: {
			                    			style: "text-align: center; font-size: " + getElSize(50) + "px"
			                  			},groupFooterTemplate: " ${total}: #= sum # ${minute}",filterable: false
			            			}
			            		],
			        }).data("kendoGrid");
			    	
			    	
			    })
			    </script>
			    <!-- 네번째 탭 -->
			    <div style="width:100%;">
			    	<div id="container" style="background: rgb(50,50,50); width:100%">
						<table id="table" style="border-collapse: collapse; width: 100%;">
							<Tr>
								<td rowspan="10" id="svg_td" style="vertical-align: top;">
									<table id="content_table" style="width: 100%"> 
										<tr>
											<Td><spring:message  code="prd_no"></spring:message></Td>
											<Td><select id="prdNo"></select ></Td>
											<Td><spring:message  code="device"></spring:message></td>
											<td ><select id="Check_dvcId"></select></td>
											<Td ><spring:message  code="check_cycle"></spring:message></Td>
											
											<Td>
												<select id="checkCycle"><option value="1"><spring:message  code="first_prdct"></spring:message></option><option value="2"><spring:message  code="mid_prdct"></spring:message></option><option value="3"><spring:message  code="last_prdct"></spring:message></option></select>
												<select id="workTime"><option value="2"><spring:message  code="day"></spring:message></option><option value="1"><spring:message  code="night"></spring:message></option></select>
											</Td>

											<Td rowspan="2">
												<div id="buttonGroup" style="float: right;">
													<%-- <button id="moveFaulty" onclick="goAddFaulty()"><i class="fa fa-share" aria-hidden="true"></i><spring:message  code="add_faulty"></spring:message></button> --%>
													<button id="saveCheckList" onclick="saveRow();"><i class="fa fa-floppy-o" aria-hidden="true"></i><spring:message  code="save_check_result"></spring:message></button>
												</div>
											</Td>
										</tr>
										<tr>
											<Td><spring:message  code="check_date"></spring:message></Td>
											<Td><input type="date" id="date" class="date"/></Td>
											<Td><spring:message  code="check_type"></spring:message></Td>
											<Td><select id="chkTy"></select></Td>
											<Td><spring:message  code="checker"></spring:message></Td><Td><select id="checker" class="worker" disabled></select></Td>
											<Td rowspan="2">
											<button id="checkSearchBtn" onclick="getCheckList()"> <i class="fa fa-search" aria-hidden="true"></i>조회</button>
											</Td>
										</tr>				
									</table> 
									<div id="checkGrid"></div>
								</td>
							</Tr>
						</table>
					 </div>
			    </div>
			    <script>
			    
			    $("div#buttonGroup").css({
			    	"margin-right" : getElSize(60)
			    })
			    
			    $("#moveFaulty").css({
			    })
			    
			    $("#checkGrid").css({
			    	"width" : getElSize(3470)
			    })
			    
			    function categoryDropDownEditor(container, options) {
			        $('<input name="' + options.field + '"/>')
			            .appendTo(container)
			            .kendoDropDownList({
			            	valuePrimitive: true,
			                dataTextField: "text",
			                dataValueField: "value",
			                optionLabel: "==선택==",
			                dataSource: [
			                	{ text: "양호", value: 2 },
			                	{ text: "불량", value: 1 }
			                ]
			            });
			    }
			    
			    function selectOption(num){
					if(num==1){
						return "불량";
					}else if(num==2){
						return "양호";
					}else{
						return "선택하세요"
					}
				}
			    
			    var checkGrid;
				$(function(){
					checkGrid = $("#checkGrid").kendoGrid({
						scrollable:true,
						editable: true,
						selectable: "row",
						height:getElSize(1680),
						columns: [
						  {
							  field:"checker",
							  editable: true,
							  title:"<input type='checkbox' id='checkall' onclick='checkAll(this)' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'>",
							  width:getElSize(133),
							  template: "<input type='checkbox' onclick='checkRow(this)' #= (typeof checkSelect!='undefined' && checkSelect!=false)? 'checked=checked' : '' # #= (typeof newRow!='undefined') ? \'disabled=checked\' : '' # class='checkbox' style='width: "+getElSize(80)+"; height: "+getElSize(80)+";'/>", 
							  attributes: {
			          				style: "text-align: center; font-size:" + getElSize(35)
			       			  },headerAttributes: {
			          				style: "text-align: center; background-color:black; font-size:" + getElSize(37)
			       			  }						
						  },
						  {
						    field: "prdNo",
						    title: "<spring:message  code='prd_no'></spring:message>" ,
						    width: getElSize(300),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "name",
						    title: "<spring:message  code='device'></spring:message>" ,
						    width: getElSize(300),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "chkTy",
						    title: "<spring:message  code='check_type'></spring:message>" ,
						    width: getElSize(300),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "attrTy",
						    title: "<spring:message  code='character_type'></spring:message>" ,
						    width: getElSize(300),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "attrNameKo",
						    title: "<spring:message  code='character_name'></spring:message>" ,
						    width: getElSize(300),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "spec",
						    title: "<spring:message  code='drawing'></spring:message>Spec" ,
						    width: getElSize(350),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "target",
						    title: "<spring:message  code='target_val'></spring:message>" ,
						    width: getElSize(200),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "low",
						    title: "<spring:message  code='min_val'></spring:message>" ,
						    width: getElSize(200),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "up",
						    title: "<spring:message  code='max_val'></spring:message>" ,
						    width: getElSize(200),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "measurer",
						    title: "<spring:message  code='measurer'></spring:message>" ,
						    width: getElSize(350),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "resultVal",
						    template:"#=selectOption(resultVal)#",
						    title: "<spring:message  code='check_result'></spring:message>" ,
						    width: getElSize(300),
						    editable: false,
						    editor: categoryDropDownEditor,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "resultVal2",
						    template:"#=selectOption(resultVal2)#",
						    title: "<spring:message  code='check_result'></spring:message><br>계측기 후" ,
						    width: getElSize(300),
						    editable: false,
						    editor: categoryDropDownEditor,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "result",
						    template:"#=selectOption(result)#",
						    title: "<spring:message  code='result'></spring:message>" ,
						    width: getElSize(300),
						    editable: false,
						    editor: categoryDropDownEditor,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "date",
						    template: "#=moment().format('YYYY-MM-DD')#",
						    editable: true,
						    title: "<spring:message  code='check_date'></spring:message>" ,
						    width: getElSize(300),
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "checkCycle",
						    title: "<spring:message  code='check_cycle'></spring:message>" ,
						    width: getElSize(200),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  },
						  {
						    field: "workTy",
						    title: "<spring:message  code='division'></spring:message>" ,
						    width: getElSize(200),
						    editable: true,
						    headerAttributes: {
						        style: "font-size :" + getElSize(30)
						    },
						    attributes: {
						    	style: "font-size :" + getElSize(30)
							}
						  }
						]
						
					}).data("kendoGrid")
				})
			    </script>
			    
			    <!-- 다섯번째 탭 -->
				<div style="width:100%;">
					<div id="container2" style="background-color: rgb(50, 50, 50);">
						<table id="table" style="border-collapse: collapse;">
							<Tr>
								<td rowspan="10" id="svg_td" style="vertical-align: top; ">
							<div id="insertForm_Faulty" style="width: 1197.66px;
							    position: absolute;
							    z-index: 999;">
								<table style="width: 100%; background-color: #444444;">
									<Tr> 
										<Td class='table_title'> <spring:message code="prd_no"></spring:message> * </Td> <Td id="prdNo_form"> </Td> <Td class='table_title'  width="20%"><spring:message code="operation"></spring:message> * </Td> <Td id="oprNm_form"></Td> 
									</Tr>
									<Tr> 
										<Td class='table_title' width="20%"> <spring:message code="check_ty"></spring:message> </Td> <Td id="chkTy_form"  width="30%"></Td> <Td class='table_title'> <spring:message code="device"></spring:message> </Td> <Td id="dvcId_form"></Td>
									</Tr>
									<Tr> 
										<Td class='table_title'> <spring:message code="event_date"></spring:message> </Td> <Td id="date_form"><input type="text" class="date_fault" disabled="disabled"> </Td> <Td class='table_title'> <spring:message code="reporter"></spring:message> * </Td> <Td id="checker_form"><select class="worker" disabled></select></Td>
										
									</Tr>
									<Tr> 
										<Td class='table_title'> <spring:message code="part"></spring:message> </Td> <Td id="part_form"></Td> <Td class='table_title'> <spring:message code="divide_situ"></spring:message> * </Td> <Td id="situation_form"></Td> 
									</Tr>
									<Tr> 
										<Td class='table_title'> <spring:message code="situ"></spring:message> * </Td> <Td id="situationTy_form"></Td><Td class='table_title' > <spring:message code="cause"></spring:message> </Td> <Td id="cause_form"></Td>
									</Tr>
									<Tr> 
										<Td class='table_title'>  <spring:message code="gch_ty"></spring:message>  </Td> <Td id="gchTy_form"></Td> <Td class='table_title'> <spring:message code="gch"></spring:message> </Td> <Td id="gch_form"></Td>
									</Tr>
									<Tr> 
										<Td class='table_title'> <spring:message code="count"></spring:message> * (<label id="exCnt">0</label>) </Td> <Td id="cnt_form"></Td> <Td class='table_title'> <spring:message code="action"></spring:message>  </Td> <Td id="action_form"></Td>
									</Tr>
									<Tr>
										<Td class='table_title'> <spring:message code="badMoveCnt"></spring:message> * </Td> <Td id="sendCnt_form">  </Td> <Td> </Td> <Td> </Td>
									</Tr>
									<Tr>
										<Td colspan="4" style="text-align: center;"><button onclick="saveRow2()" style="font-size : 30px; margin-top: 30px;"><spring:message code="save"></spring:message> </button></Td>
									</Tr>
								</table> 
							</div>
								</td>
							</Tr>
						</table>
					</div>
				</div>
			    <!-- 여섯번 째 -->
			    <div style="width:100%;">
			    	<div id="tabstrip_report">
			    	<ul>
			    		<li id="do_report">
	                        	요청하기
	                    </li>
	                    <li id="show_report_list" onclick="maintenance_report_list()">
	                        	요청 내역
	                    </li>
			    	</ul>
			    	<div id="maintenance_report"></div>
			    	<div id="maintenance_report_list"></div>
			    	</div>
			    	<div id="confirm_dialog" style="background: gray;"></div>
			    </div>
			    <script type="text/x-kendo-template" id="template_report">
                <div id="details-container">
                   	 <p style="margin:10px;">장비명 : <mark>#= device #</mark> 일자 : <mark>#= regDate #</mark></p>
                     <dl style="margin:10px;">최근 알람 내용 : <mark>#= report #</mark></dl>
					<dl style="margin:10px;">
                      	조치자 :  <mark>#= solver_Name #</mark>
                    </dl>
					<dl style="margin:10px;">
					보고자 : #= name #
					</dl>
				<center>
		<button class="k-button" id="yesButton" style="font-size:25px; font-weight: bolder; width: 150px; background-color: rgb(121, 218, 76); border: 0px solid;height: 50px;
 margin-bottom: 10px; margin-right:30px; border: 0px solid; background-color: rgb(121, 218, 76);">삭제</button>


    			<button class="k-button" id="noButton" style="height:50px; width:150px; margin-bottom: 10px;">닫기</button></center>
                </div>
			</script>
			    <script>
			    var tabstrip_report, tabstrip_index, report_Grid, confirm_report_delete, tabstrip_Oprreport ;
			    $(function(){
			    	tabstrip_report = $("#tabstrip_report").kendoTabStrip({
						animation: false
					}).data("kendoTabStrip").activateTab($("#do_report"));
			    	
			    	
			    	tabstrip_Oprreport = $("#tabstrip_Oprreport").kendoTabStrip({
						animation: false
					}).data("kendoTabStrip").activateTab($("#OprList"));
					
			    	report_Grid = $("#maintenance_report_list").kendoGrid({
			    		height : getElSize(1720),
						columns: [{
						    field: "device",
						    title: "장비" ,
					    	headerAttributes:{
			                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
			                },attributes: {
			                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
			                }
						  }, {
						    field: "name",
						    title: "작업자" ,
					    	headerAttributes:{
			                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
			                },attributes: {
			                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
			                }
						  }, {
						    field: "solver_Name",
						    title: "조치자" ,
					    	headerAttributes:{
			                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
			                },attributes: {
			                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
			                }
						  }
						  , {
						    field: "report",
						    title: "보고내용",
					    	headerAttributes:{
			                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
			                },attributes: {
			                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
			                }
						  }
						  , {
						    field: "regDate",
						    title: "일자",
					    	headerAttributes:{
			                	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
			                },attributes: {
			                    style: "text-align: center; font-size:" + getElSize(35) +"px; white-space: initial;overflow: visible !important;"
			                }
						  },
						  { command: {name:"delete", text: "요청 삭제하기", click: function(e){
							  var currentItem = this.dataItem($(e.currentTarget).closest("tr"))
						    	confirm_template = kendo.template($("#template_report").html());
						    	confirm_report_delete.content(confirm_template(currentItem))
						    	confirm_report_delete.open();
						    	 $("#yesButton").click(function(){
						    		$.showLoading();
					    			var url = "${ctxPath}/chart/delMaintenanceReport.do";
									var param = "idx=" + currentItem.idx
						    		 
						    		 $.ajax({
											url : url,
											data : param,
											type : "post",
											success : function(data){
												report_Grid.dataSource.remove(currentItem) 
									    		report_Grid.dataSource.sync()  
									    		confirm_report_delete.close();
												$.hideLoading()
											},error : function(request,status,error){
												console.log("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
												$.hideLoading();
											}
									});
						    		
		                        })
		                        $("#noButton").click(function(){
		                        	confirm_report_delete.close();
		                        })
						  } }, title: "삭제 ", width: "180px",headerAttributes:{
				            	style: "text-align: center; color: white !important; font-size:" + getElSize(35) +"px; white-space: initial; background:gray; vertical-align: middle;"
			              }}
						  ]
					}).data("kendoGrid")
					
					confirm_report_delete = $("#confirm_dialog").kendoDialog({
						title:false,
						closable: false
					}).data("kendoDialog");
			    })
			    var confirm_template;
			    
			    function maintenance_report_list(){
			    	$.showLoading();
					var url = "${ctxPath}/chart/getMaintenanceReportListPageList.do";
					var param = "empCd=" + $(".worker").val()
					    var report_List = new kendo.data.DataSource();
					$.ajax({
						url : url,
						data : param,
						type : "post",
						async : false,
						dataType : "json",
						success : function(data){
							var json = data.dataList;
							console.log(json)
							var options = "";
							$(json).each(function(idx, data){
								
	
								var object = {
										"dvcId" : data.dvcId
										,"name" : decode(data.name)
										,"device" : data.device
										,"solver" : data.solver
										,"solver_Name" : decode(data.solver_Name)
										,"lastAlarmMsg" : data.lastAlarmMsg
										,"regDate" : data.regDate
										,"report" : decode(data.report)
										,"alarmCode" : data.alarmCode
										,"idx" : data.idx
								}
								report_List.add(object)
								console.log(object)
							});
							report_Grid.setDataSource(report_List)
							$("#maintenance_report_list").css({
								"background-color":"black",
								"color" : "white"
							})
							
							$("#maintenance_report_list tr:odd").css({
								"background-color":"rgb(50,50,50)",
							      "color" : "white"
						   });
							
							$("#maintenance_report_list tr:even").css({
							    "background-color":"rgb(33,33,33)",
							    "color" : "white"
							});
							$.hideLoading();
						}
					});
			    }
			    
			    </script>
			</div>
		</div>
	</div>

	

	<!-- 작업자 선택 다이얼로그 메뉴 OLD Start  
	2017. 09. 05.
	주석자 : John (조부관) -->
	<div style="display: none;">
		<div id="popup">
			<Table>
				<Tr>
					<td><spring:message code="worker"></spring:message></td>
					<td><select class="worker"></select></td>
				</Tr>
				<Tr>
					<td><spring:message code="pwd"></spring:message></td>
					<td><input type="password" id="pwd" onkeyup="onKeyEvt(event)">
					</td>
				</Tr>
				<tr>
					<Td colspan="2" style="text-align: center;">
						<button id="okBtn" onclick="clickOk()">
							<spring:message code="go2Work"></spring:message>
						</button>
						<button onclick="closePopup()">
							<spring:message code="cancel"></spring:message>
						</button>
					</Td>
				</tr>
			</Table>
		</div>
		<!-- 작업자 선택 다이얼로그 메뉴 OLD End  -->
	
		<!-- 작업자 선택 다이얼로그 메뉴 NEW Start 
		2017. 09. 05.
		작업자 : John (조부관)
		작업내용 : 기존의 popup의 id를 가진 div를 노출 시키는 방식에서
		KendoUI에서 제공하는 dialog로 창 교체. 작업자 입력 방식 변경.
		-->
		<div class="dialog">
			<table style="padding-left: 25px; padding-top: 15px;">
				<tr>
					<td id="login_worker" style="color:white;">
						<spring:message code="worker"></spring:message>
					</td>
					<td>
						<input id="autocomplete" style="width:100%; font-size:20px;">
					</td>
				</tr>
				<tr>
					<td style="font-size: 30px; color:white; padding-top:10%;">
						<spring:message code="pwd"></spring:message>
					</td>
					<td style="padding-top:10%;">
						<input type="password" placeholder="${password_Placeholder }" id="pwd" onkeyup="onKeyEvt(event)" style="height:40px !important; float:right; margin-left:0px !important;
			margin-right:0px !important; border: 1px solid black; width: 360px; padding:0px 0px 0px 10px; border-radius: 4px;">
					</td>
				</tr>
				<tr>
					<td style="padding-top:10%;">
						<button id="submit" type="button" style="width:140px; height:50px; font-size:17px !important" onclick="clickOk()"><spring:message code="go2Work"></spring:message></button>
					</td>
					<td style="padding-top:10%;">
						<button id="cancel" type="button" style="float:right; width:140px; height:50px; font-size:17px !important;" onclick="cMenuNone()"><spring:message code="cancel"></spring:message></button>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<!-- 작업자 선택 다이얼로그 메뉴 NEW End -->
	
	
	<!-- KendoUI 로 인해 추가된 Script Start -->
	<script>
	
	$body = $("body");

	$(document).on({
	    
	    ajaxStop: function() { $body.removeClass("loading"); }    
	});
	
	var prdNoList=[];
	var kendoWorkerList;
	var arrayWorkers=[];
	var workerCd;
	var workerList = [];
	var ds = new kendo.data.DataSource();
	var prdNoName = new kendo.data.DataSource();
	
	var dataWorkerList = $("#autocomplete").kendoComboBox({
		dataTextField:"name",
		dataValueField:"workerCd",
		noDataTemplate: '작업자를 발견하지 못하였습니다.',
		placeholder: "${worker}",
		change : function(e){
			var value = this.value();
			console.log(value)
			workerCd = value
			/* for(var i=0; i<workerList.length; i++){
				if(dataWorkerList.value() == workerList[i].name){
					workerCd = workerList[i].workerCd
				} else {
					
				}
			} */
		},select : function(e){
			var value = this.value();
			console.log(value)
			workerCd = value
			/* for(var i=0; i<workerList.length; i++){
				if(dataWorkerList.value() == workerList[i].name){
					workerCd = workerList[i].workerCd
				} else {
					
				}
			} */
		}
		}).data("kendoComboBox");

	$(function(){
		$.ajax({
			url : "${ctxPath}/common/getAllWorkerList.do",
			dataType : "json",
			type : "post",
			success : function(data){
				
				var json = data.dataList;
				var options = "";
				
				$(json).each(function(idx, data){
					var object = {
							"workerCd" : data.id
							,"name" : decode(data.name)
					}
					workerList.push(object)
					ds.add(object)
					
				});
				
				var noCon = {
						"workerCd" : "0"
						,"name" : "aIdoo 미연결"
				}
				workerList.push(noCon)
				ds.add(noCon)
				
				ds.fetch().then(function(){
					  var data = ds.data();
				});
				dataWorkerList.setDataSource(ds)
			}, error: function(result) {
				
		        console.log(error);
		    }
		});
	});
	
	$("#submit").kendoButton({
		icon:"check"
	})
	
	$("#cancel").kendoButton();
	var cancel_button = $("#cancel").data("kendoButton");
	cancel_button.bind("click", function(e) {
		dialog.close();
	}); 
	
	$("#cancel").kendoButton({
		icon:"cancel"
	})
	
	
	$(".dialog").kendoWindow({
		  visible: false,
		  resizable: false,
		  height: "300px",
		  width: "600px",
		  buttonLayout: "normal",
		  title: false,
		  animation: false,
		  position: {
			    top: "35%",
			    left: "35%",
			  },
		  open: function() {
			  dataWorkerList.value("");
		  },
		  close:function() {
			  $(".dialog #pwd").val("");
			  workerCd=""
		  }
	});
	var dialog = $(".dialog").data("kendoWindow");
	
	
	
	</script>
	<!-- KendoUI 로 인해 추가된 Script END -->
	
	<!-- 자주검사를 위해 추가된 Script Start -->
	<script>
	
	function setDate(){
		var date = new Date();
		var year = date.getFullYear();
		var month = addZero(String(date.getMonth()+1));
		var day = addZero(String(date.getDate()));
		
		$(".date").val(year + "-" + month + "-" + day);
	};
	
	
	function getGroup(){
		console.log("getGroup is Start")
		$.showLoading()
		var url = "${ctxPath}/common/getPrdNo.do";
		var param = "shopId=" + shopId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log("getGroup is Complete")
				json = data.dataList;
				
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>"; 
				});
				
				$("#prdNo").html(option).change(getDvcListByPrdNo)
				 
				$("#chkTy").html(getCheckType());
				
				getDvcListByPrdNo();
			}
		});
	};
	
	function chkFaulty(el, ty){
		if(ty=="select"){
			var val = $(el).val();
			
			if(val==2){
				$(el).parent("Td").parent("tr").css("background-color" , "red");
			}else{
				if($(el).parent("Td").parent("tr").hasClass("row1")){
					$(el).parent("Td").parent("tr").css("background-color" , "#222222");	 
				}else{
					$(el).parent("Td").parent("tr").css("background-color" , "#323232");
				}
			};	
		}else{
			var min = Number($(el).parent("td").parent("tr").children("td:nth(7)").html());
			var max = Number($(el).parent("td").parent("tr").children("td:nth(8)").html());
			var val = Number($(el).val());
			
			if(val>max || val < min){
				$(el).parent("Td").parent("tr").css("background-color" , "red");
			}else{
				if($(el).parent("Td").parent("tr").hasClass("row1")){
					$(el).parent("Td").parent("tr").css("background-color" , "#222222");	 
				}else{
					$(el).parent("Td").parent("tr").css("background-color" , "#323232");
				}
			}
		}
	};
	
	function getCheckType(){
		var url = "${ctxPath}/chart/getCheckType.do";
		var param = "codeGroup=INSPPNT";
		
		var option = "";
		
		$.ajax({
			url : url,
			data : param,
			async : false,
			type : "post",
			dataType : "json",
			success : function(data){
				var json = data.dataList;
				
				$(json).each(function(idx, data){
					var codeName;
					if(decode(data.codeName)=="입고검사"){
						codeName = "${in_check}";
					}else{
						codeName = decode(data.codeName);
					}
					
					option += "<option value='" + data.id + "'>" + codeName + "</option>"; 
				});
				
				chkTy = "<select>" + option + "</select>";
			}
		});
		
		return option;
	};
	
	function getDvcListByPrdNo(){
		var url = "${ctxPath}/chart/getDvcListByPrdNo.do";
		var param = "prdNo=" + $("#prdNo").val();
		console.log(url+'?'+param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log("getDvcListByPrdNo is Complete")
				$.hideLoading();
				var json = data.dataList;
				console.log(json)
				var option = "";
				
				$(json).each(function(idx, data){
					option += "<option value='" + data.dvcId + "'>" + decode(data.name) + "</option>"; 
				});
				
				if(json.length==0){
					option += "<option value='0'>장비 없음</option>";
				}
				console.log($("#Check_dvcId").html(option).val())
				$("#Check_dvcId").html(option).val(json[0].dvcId);
				//getCheckList();
				
			}
		});
	};
	
	var className = "";
	var classFlag = true;
	var result = "<select style='font-size:" + getElSize(40) + "' onchange='chkFaulty(this, \"select\")'><option value='0' >${selection}</option><option value='1'>${ok}</option><option value='2'>${faulty}</option></select>";
	
	function getCheckList(){
		classFlag = true;
		var url = "${ctxPath}/chart/getCheckList.do";
		var sDate = $(".date").val();
		
		var param = "prdNo=" + $("#prdNo").val() + 
					"&chkTy=" + $("#chkTy").val() +
					"&checkCycle=" + $("#checkCycle").val() + 
					"&date=" + sDate + 
					"&ty=" + $("#workTime").val() + 
					"&dvcId=" + $("#Check_dvcId option:selected").html();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var dataSource = new kendo.data.DataSource({});
				console.log(json)
				$(json).each(function(idx, data){
					if(data.seq!="."){
						if(classFlag){
							className = "row2"
						}else{
							className = "row1"
						};
						classFlag = !classFlag;
						
						var attrTy;
						if(data.attrTy==1){
							//attrTy = "정성검사";
							attrTy = "${js_check}";
						}else{
							//attrTy = "정량검사";
							attrTy = "${jr_check}";
						};
					
						var date;
						if(data.date==""){
							date = $(".date").val();
						}else{
							date = data.date;
						};
						
						var resultVal, resultVal2, resultVal3, resultVal4;
						
						if(data.attrTy==1){
							resultVal = resultVal2 = resultVal3 = resultVal4 = result;
						}else{
							resultVal = "<input type='text' value='" + data.result + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal2 = "<input type='text' value='" + data.result2 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal3 = "<input type='text' value='" + data.result3 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
							resultVal4 = "<input type='text' value='" + data.result4 + "' size='6' onkeyup='chkFaulty(this, \"input\")'>";
						};
						
						var chkTy;
						if(decodeURIComponent(data.chkTy).replace(/\+/gi, " ")=="입고검사"){
							chkTy = "${in_check}"	
						}else{
							chkTy = decodeURIComponent(data.chkTy).replace(/\+/gi, " ");
						}
						
						data.name = decode(data.name);
						data.attrNameKo = decode(data.attrNameKo);
						data.spec = decode(data.spec);
						data.measurer = decode(data.measurer);
						data.chkTy = chkTy;
						data.attrTy = attrTy;
						data.resultVal = resultVal;
						data.resultVal2 = resultVal2;
						data.checkSelect = false;
						if($("#checkCycle").val()==1){
							data.checkCycle = "초물"
						}else if($("#checkCycle").val()==2){
							data.checkCycle = "중물"
						}else{
							data.checkCycle = "종물"
						}
						
						if($("#workTime").val()==1){
							data.workTy="야간";
						}else if($("#workTime").val()==2){
							data.workTy="주간";
						}
						
					}
					dataSource.add(data);
				});
				checkGrid.setDataSource(dataSource);
			}
		});
	};
	
	function goGraph(){
		location.href = "${ctxPath}/chart/jigGraph.do"
	};
	
	var valueArray = [];
	var selectedRow = []
	function saveRow(){
		valueArray = [];
		
		
		if($("#checker").val()==null){
			kendo.alert("검사자를 선택하여 주세요.");
			return;
		}
		
		gridlist=checkGrid.dataSource.data();
		for(var i=0;i<gridlist.length;i++){
			if(gridlist[i].checkSelect){
				console.log("gridlist[i].result : "+gridlist[i].result)
				console.log("gridlist[i].resultVal : "+gridlist[i].resultVal)
				console.log("gridlist[i].resultVal2 : " + gridlist[i].resultVal2)
				if(gridlist[i].result!=1 && gridlist[i].result!=2){
					kendo.alert("검사 결과를 선택하여 주세요.")
					return;
				}
				if(gridlist[i].resultVal!=1 && gridlist[i].resultVal!=2){
					kendo.alert("검사 결과를 선택하여 주세요.")
					return;
				}
				if(gridlist[i].resultVal2!=1 && gridlist[i].resultVal2!=2){
					kendo.alert("검사 결과를 선택하여 주세요.")
					return;
				}
				gridlist[i].checker=$("#checker").val()
				gridlist[i].chkCycle=$("#checkCycle").val()
				gridlist[i].date=$(".date").val()
				gridlist[i].workTy=$("#workTime").val()
				gridlist[i].dvcId=$("#dvcId").val()
				gridlist[i].chkTy=$("#chkTy").val()
				gridlist[i].fResult=gridlist[i].result
				gridlist[i].result=gridlist[i].resultVal
				gridlist[i].result2=gridlist[i].resultVal2
				valueArray.push(gridlist[i]);
			}
		}
		if(valueArray.length==0){
			kendo.alert("저장할 항목을 선택하여 주세요.")
			return;
		}
		
		var obj = new Object();
		obj.val = valueArray;
		
		var url = "${ctxPath}/chart/addCheckStandardList.do";
		var param = "val=" + JSON.stringify(obj);
		console.log(url+"?"+param)
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType :"text",
			success : function(data){
				if(data=="success") {
					alert ("${save_ok}");
					getCheckList();
				}
			}
		});
	};

	function goAddFaulty(el){
		
		var prdNo = $("#prdNo").val();
		var cnt = 0;
		var url = "${ctxPath}/chart/addFaulty.do?addFaulty=true&prdNo=" + prdNo + "&cnt="+ cnt;
		
		location.href = url;
	};
	
	</script>
	<!-- 자주검사를 위해 추가된 Script END -->
	
	
	<!-- 불량 신고를 위해 추가된 Script Start -->
	<script type="text/javascript">
	function getGch(){
		var val = $(this).val();
		var ty;
		if(val=="47"){			//업체
			ty = "com";
		}else if(val=="48"){	//작업자  
			ty = "worker"	
		};
		
		getGChList(ty, $("#gch_form").children("td").children("select"));
	};
	
	var className = "";
	var classFlag = true;

	var menu = false;

	function getProcessList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 3; 	
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getPartList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 4;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getSituationTyList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 5;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCnt(el, id){
		var url = ctxPath + "/chart/getCnt.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				el.val(data);
			}
		});	
	};

	function getChecker(el, id){
		var url = ctxPath + "/chart/getChecker.do";
		var param = "id=" + id;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "text",
			type : "post",
			success : function(data){
				el.val(decode(data));
			}
		});	
	};

	function getSituationList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 6;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCauseList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 7;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getGChTyList(el,val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 8;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options).change(getGch);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getGChList(ty, el){
		//var url = ctxPath + "/chart/getCheckTyList.do"
		var url;
		if(ty=="com"){
			url = ctxPath + "/chart/getComList.do"		
		}else if(ty=="worker"){
			url = ctxPath + "/common/getWorkerList.do"
		}else{
			el.html("<option value='0'>${selection}</option>");
			return; 
		}
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
					
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getActionList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 10;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	function getCheckTyList(el, val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 2;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};

	var comPrdList;
	function getPrdNoList(el, val){
		var url = ctxPath + "/common/getPrdNoList.do"
		
		$.ajax({
			url : url,
			dataType : "json",
			async : false,
			type : "post",
			success : function(data){
				var json = data.dataList;
				comPrdList=json;
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.prdNo + "'>" + data.prdNo + "</option>";
				});
				
				el.html(options).change(function() {getDevieList($("#dvcId_form").children("td").children("select"), this)});
				
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
				$.hideLoading();
			}
		});
	};

	function getGChTyList(el,val){
		var url = ctxPath + "/chart/getCheckTyList.do"
		var param = "index=" + 8;
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options).change(getGch);
				if(typeof(val)!="undefined"){
					el.val(val);	
				}
			}
		});	
	};
	
	var addFaulty = "${addFaulty}";
	
	function getDevieList(el, obj){
		var url = ctxPath + "/chart/getDevieList.do"
		var param = "shopId=" + shopId + 
					"&prdNo=" + $(obj).val();
		
		$.ajax({
			url : url,
			data : param,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "<option value='0'>${selection}</option>";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decode(data.name) + "</option>";
				});
				
				el.html(options);
			}
		});	
	};
	
	var valArray = [];

	function saveRow2(){
		$.showLoading()
		valArray = [];
		
		var obj = new Object();
		
		obj.chkTy = $("#chkTy_form select").val();
		obj.prdNo = $("#prdNo_form select").val();
		obj.prdPrc = $("#oprNm_form select").val();
		obj.dvcId = $("#dvcId_form select").val();
		obj.date = $("#date_form input").val();
		obj.checker = $("#container2 .worker").val();
		obj.part = $("#part_form select").val();
		obj.situationTy = $("#situationTy_form select").val();
		obj.situation = $("#situation_form select").val();
		obj.cause = $("#cause_form select").val();
		obj.gchTY = $("#gchTy_form select").val();
		obj.gch = $("#gch_form select").val();
		obj.cnt = $("#cnt_form input").val();
		obj.action = $("#action_form select").val();
		obj.sendCnt = $("#sendCnt_form input").val();
		
		if($("#oprNm_form select").val()=="20"){
			obj.proj="0000"
		}else if($("#oprNm_form select").val()=="20"){	//소재 자재
			obj.proj="0000"
		}else if($("#oprNm_form select").val()=="21"){	//라인 공정
			obj.proj="0005"
		}else if($("#oprNm_form select").val()=="22"){	//R	공정
			obj.proj="0010"
		}else if($("#oprNm_form select").val()=="23"){	//MCT 공정
			obj.proj="0020"
		}else if($("#oprNm_form select").val()=="24"){	//CNC 공정
			obj.proj="0030"
		}else if($("#oprNm_form select").val()=="25"){	//완성창고
			obj.proj="0090"
		}else if($("#oprNm_form select").val()=="26"){	//고객사
			obj.proj="0"
		}else if($("#oprNm_form select").val()=="27"){	//필드
			obj.proj="0"
		}
		
		valArray.push(obj);
		
		var obj = new Object();
		obj.val = valArray;
		
		var url = "${ctxPath}/chart/saveRow.do";
		var param = "val=" + JSON.stringify(obj);
		
		if(Number($("#exCnt").html()) < Number($("#cnt_form input").val()) || Number($("#exCnt").html()) < Number($("#sendCnt_form input").val())){
			alert("재고보다 수량이 많을수 없습니다.");
			$.hideLoading();
			return;
		}
		
		if($("#prdNo_selector_form").val()=="0"){
			alert("품번을 선택하십시오.");
			$("#prdNo_selector_form").focus();
			$.hideLoading()
			return;
		}else if($("#prdNo_selector_form").val().indexOf("RW")==-1 && ($("#oprNm_form select").val()==20 || $("#oprNm_form select").val()==21)){
			alert("완성품번일때 소재,라인창고를 선택하실수 없습니다.")
			$.hideLoading()
			return;
		}else if($("#prdNo_selector_form").val().indexOf("RW")!=-1 && ($("#oprNm_form select").val()!=20 && $("#oprNm_form select").val()!=21)){
			alert("소재일때는 소재,라인창고만 선택 가능합니다.")
			$.hideLoading()
			return;
		}else if($("#oprNm_form select").val()=="0"){
			alert("공정을 선택하십시오.");
			$("#oprNm_form select").focus();
			$.hideLoading()
			return;
		}else if($("#checker_form select").val()=="0"){
			alert("신고자를 선택하십시오.");
			$("#checker_form select").focus();
			$.hideLoading()
			return;
		}else if($("#situation_form select").val()=="0"){
			alert("현상구분 선택하십시오.");
			$("#situation_form").focus();
			$.hideLoading();
			return;
		}else if($("#situationTy_form select").val()=="0"){
			alert("현상을 선택하십시오.");
			$("#situationTy_form select").focus();
			$.hideLoading()
			return;
		}else if($("#cnt_form input").val()=="0" || $("#cnt_form input").val()==0){
			alert("수량을 입력하세요.");
			$("#cnt_form input").focus();
			$("#cnt_form input").select();
			$.hideLoading()
			return;
		}else if($("#sendCnt_form input").val()=="0" || $("#sendCnt_form input").val()==0){
			alert("수량을 입력하세요.");
			$("#sendCnt_form input").focus();
			$("#sendCnt_form input").select();
			$.hideLoading()
			return;
		}else if(isNaN($("#cnt_form input").val())){
			alert("수량은 숫자만 입력가능합니다.");
			$("#cnt_form input").focus();
			$("#cnt_form input").select();
			$.hideLoading()
			return;
		}else if(isNaN($("#sendCnt_form input").val())){
			alert("수량은 숫자만 입력가능합니다.");
			$("#sendCnt_form input").focus();
			$("#sendCnt_form input").select();
			$.hideLoading()
			return;
		}else if($("#cnt_form input").val() < $("#sendCnt_form input").val()){
			alert("불량등록 수량보다 이동수량이 많을수 없습니다.");
			$("#sendCnt_form input").focus();
			$("#sendCnt_form input").select();
			$.hideLoading()
			return;
		}
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "text",
			success : function(data){
				if(data=="success"){
					alert("${save_ok}");
					addRow_Faulty();
				}
				$.hideLoading()
			}
		});	
	};
	
	function addRow_Faulty(){
		$.showLoading()
		$("#container2 .worker").val(workerCd)
		
		if(classFlag){
			className = "row2"
		}else{
			className = "row1"
		};
		classFlag = !classFlag;
		
		var checkTy = document.createElement("select");
		var prdNo = document.createElement("select");
		prdNo.setAttribute("id", "prdNo_selector_form");
		var process = document.createElement("select");
		var device = document.createElement("select");
		var worker = document.createElement("select");
		worker.setAttribute("class", "worker");
		var sDate = document.createElement("input");
		sDate.setAttribute("class", "Fdate");
		sDate.setAttribute("type", "date");
//		sDate.setAttribute("readonly", "readonly");
		var text = document.createElement("input");
		
		text.style.cssText = "width : " + getElSize(400);
		var cnt = document.createElement("input");
		cnt.setAttribute("class", "cnt");
		cnt.setAttribute("size", 5);
		var part = document.createElement("select");
		var situationTy = document.createElement("select");
		var situation = document.createElement("select");
		var cause = document.createElement("select");
		var gchTY = document.createElement("select");
		var gch = document.createElement("select");
		var action = document.createElement("select");

		var sendCnt = document.createElement("input");
		sendCnt.setAttribute("size", 5);
		
		var tr = document.createElement("tr");
		tr.setAttribute("class", className);
		
		var checkTy_td = document.createElement("td");
		checkTy_td.append(checkTy);
		tr.append(checkTy_td);
		$("#chkTy_form").html(checkTy_td)
		
		var prdNo_td = document.createElement("td");
		prdNo_td.append(prdNo);
		tr.append(prdNo_td);
		$("#prdNo_form").html(prdNo_td)
		
		var process_td = document.createElement("td");
		process_td.append(process);
		tr.append(process_td);
		$("#oprNm_form").html(process_td)
		
		var device_td = document.createElement("td");
		device_td.append(device);
		tr.append(device_td);
		$("#dvcId_form").html(device_td)
		
		var sDate_td = document.createElement("td");
		sDate_td.append(sDate);
		tr.append(sDate_td);
		$("#date_form").html(sDate_td)
		
		/* var text_td = document.createElement("td");
		text_td.append(worker);
		tr.append(text_td);
		$("#checker_form").html(text_td) */
		
		var part_td = document.createElement("td");
		part_td.append(part);
		tr.append(part_td);
		$("#part_form").html(part_td)
		
		var situ_td = document.createElement("td");
		situ_td.append(situationTy);
		tr.append(situ_td);
		$("#situation_form").html(situ_td)
		
		var situTy_td = document.createElement("td");
		situTy_td.append(situation);
		tr.append(situTy_td);
		$("#situationTy_form").html(situTy_td)
		
		var cause_td = document.createElement("td");
		cause_td.append(cause);
		tr.append(cause_td);
		$("#cause_form").html(cause_td)
		
		var gchkTy_td = document.createElement("td");
		gchkTy_td.append(gchTY);
		tr.append(gchkTy_td);
		$("#gchTy_form").html(gchkTy_td)
		
		var gch_td = document.createElement("td");
		gch_td.append(gch);
		tr.append(gch_td);
		$("#gch_form").html(gch_td)
		
		var cnt_td = document.createElement("td");
		cnt_td.append(cnt);
		tr.append(cnt_td);
		$("#cnt_form").html(cnt_td)
		
		var action_td = document.createElement("td");
		action_td.append(action);
		tr.append(action_td); 
		$("#action_form").html(action_td)
		
		var sendCnt_td = document.createElement("td");
		sendCnt_td.append(sendCnt);
		tr.append(sendCnt_td);
		$("#sendCnt_form").html(sendCnt_td)
		
		var button_td = document.createElement("td");
		var button = document.createElement("button");
		//button.setAttribute("id", "b" + data.id);
		var button_text = document.createTextNode("삭제");
		button.append(button_text);
		button_td.append(button)
		tr.append(button_td);
		
		//$("#tbody").append(tr);
		
		getCheckTyList($(checkTy));
		getProcessList($(process));
		getDevieList($(device));
	 	//getWorkerList_Faulty($(worker));
		console.log($(worker))
		$(sDate).val(getToday().substr(0,10));
		getPartList($(part));
		getSituationList($(situation));
		getSituationTyList($(situationTy));
		getCauseList($(cause));
		getGChTyList($(gchTY));
		getGChList(7, $(gch));
		getActionList($(action)); 
		getPrdNoList($(prdNo));
		$(cnt).val(0);
		$(sendCnt).val(0);
		
		$("#checker_form .worker").val($(".worker").val())
		
		$(".row1").not(".tr_table_fix_header").css({
			"background-color" : "#222222"
		});

		$(".row2").not(".tr_table_fix_header").css({
			"background-color": "#323232"
		});
		
		
		$("#insertForm select, #insertForm input, #insertForm_Faulty select, #insertForm_Faulty input, #insertForm_Faulty button").css("font-size",getElSize(80))
		
		showCorver();
		$("#insertForm").css("z-index", 999)
		
		if(addFaulty!=""){
			$("#prdNo_selector_form option[value='" + $prdNo + "']").attr("selected", "selected");
			$("#cnt_form").children("td").children("input").val($cnt);
		}
		
		
		$("#oprNm_form select").change(function(){
			if($("#oprNm_form select").val()=="0" || $("#prdNo_form select").val()=="0"){
				return false;
			}
			var proj;
			if($("#oprNm_form select").val()=="20"){
				proj="0000"
			}else if($("#oprNm_form select").val()=="20"){	//소재 자재
				proj="0000"
			}else if($("#oprNm_form select").val()=="21"){	//라인 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="22"){	//R	공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="23"){	//MCT 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="24"){	//CNC 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="25"){	//완성창고
				proj="0090"
			}else if($("#oprNm_form select").val()=="26"){	//고객사
				proj="0"
			}else if($("#oprNm_form select").val()=="27"){	//필드
				proj="0"
			}
			if(proj=="0"){
				$("#exCnt").html("9999")
				return false;
			}
			var prd= $("#prdNo_form select").val()
			if(proj=="0005"){
				for(i=0,len=comPrdList.length ;i<len; i++){
					if(prd==comPrdList[i].prdNo && prd.indexOf("RW")==-1){
						prd=comPrdList[i].matNo;
					}
				}
			}
			
			console.log(prd)
			var url = "${ctxPath}/chart/stockTotalCntCheck.do";
			var param = "prdNo=" + prd +
						"&proj=" + proj
			
			var str;
			$.ajax({
				url : url,
				data : param,
				async :false,
				type : "post",
				dataType : "json",
				success : function(data){
					if(data.dataList.length!=0){
						$("#exCnt").html(data.dataList[0].cnt)
					}else{
						$("#exCnt").html(0)
					}
				}
			});
		})
		
		$("#prdNo_form select").change(function(){
			if($("#oprNm_form select").val()=="0" || $("#prdNo_form select").val()=="0"){
				return false;
			}
			var proj;
			if($("#oprNm_form select").val()=="20"){
				proj="0000"
			}else if($("#oprNm_form select").val()=="20"){	//소재 자재
				proj="0000"
			}else if($("#oprNm_form select").val()=="21"){	//라인 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="22"){	//R	공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="23"){	//MCT 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="24"){	//CNC 공정
				proj="0005"
			}else if($("#oprNm_form select").val()=="25"){	//완성창고
				proj="0090"
			}else if($("#oprNm_form select").val()=="26"){	//고객사
				proj="0"
			}else if($("#oprNm_form select").val()=="27"){	//필드
				proj="0"
			}
			if(proj=="0"){
				$("#exCnt").html("9999")
				return false;
			}
			var prd= $("#prdNo_form select").val()
			if(proj=="0005"){
				for(i=0,len=comPrdList.length ;i<len; i++){
					if(prd==comPrdList[i].prdNo && prd.indexOf("RW")==-1){
						prd=comPrdList[i].matNo;
					}
				}
			}
			
			console.log(prd)
			var url = "${ctxPath}/chart/stockTotalCntCheck.do";
			var param = "prdNo=" + prd +
						"&proj=" + proj
			
			var str;
			$.ajax({
				url : url,
				data : param,
				async :false,
				type : "post",
				dataType : "json",
				success : function(data){
					if(data.dataList.length!=0){
						$("#exCnt").html(data.dataList[0].cnt)
					}else{
						$("#exCnt").html(0)
					}
				}
			});
		})
		
		
	};
	
	function getWorkerList_Faulty(el){
		var url = "${ctxPath}/common/getAllWorkerList.do";

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
				});
				
				el.html(options).val(workerCd);
				console.log(workerCd)
			}
		});	
	};
	
	var StimeList;
	var EtimeList;
	$(function(){
		var now = moment();
    	console.log(now.hour())
    	if(now.hour()<9){
    		now.subtract(1, 'days');
    	}
    	console.log(now)
    	
    	now = now.format("YYYY-MM-DD")
    	console.log(now)
    	
    	StimeList="<select data-bind='value:sDate' style='height :"+getElSize(70)+"; width:"+getElSize(210)+"'>"
		for(i=0; i<24; i++){
		  	for(j=0; j<60; j=j+30){
		  		addZero(String(i))
		  		addZero(String(j))
		  		StimeList+="<option>"+addZero(String(i))+":"+addZero(String(j))+"</option>"
		    }
		}
    	StimeList+="</select>"

    	EtimeList="<select data-bind='value:eDate' style='height :"+getElSize(70)+"; width:"+getElSize(210)+"'>"
		for(i=0; i<24; i++){
		  	for(j=0; j<60; j=j+30){
		  		addZero(String(i))
		  		addZero(String(j))
		  		EtimeList+="<option>"+addZero(String(i))+":"+addZero(String(j))+"</option>"
		    }
		}
    	EtimeList+="</select>"
    })
	
	function addworkTimeHist(){
		if($("#start_time_date").val()==null || $("#start_time").val()==null || $("#start_time_date").val()=="" || $("#start_time").val()==""){
    		kendo.alert("시간 / 일자를 입력해주세요.")
    		
    	}else{
    		
    		var now = moment();
	    	console.log(now.hour())
	    	if(now.hour()<9){
	    		now.subtract(1, 'days');
	    	}
	    	console.log(now)
	    	
	    	now = now.format("YYYY-MM-DD")
    	
    		var time = $("#start_time_date").val()+" "+$("#start_time").val()
        	var url = "${ctxPath}/chart/addWorkTimeHist_Correct.do";
        	var param = "worker="+$("#popup2 .worker").val()+"&startTime="+time+"&date="+now;
        	console.log($("#popup2 .worker").val())
        	console.log($("#start_time_date").val())
        	console.log($("#start_time").val())
			$.ajax({
				url : url,
				data : param,
				type : "post",
				async : false,
				success : function(data){
    				kendo.alert("입력완료")
    				addWorkTime.close();
    			},error: function(error){
    				console.log(error)
    			}
    		});
    	}
	}
	
	function workingList(){
		var url = "${ctxPath}/chart/getworkingList.do";

		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			success : function(data){
				var json = data.dataList;
				
				var options = "";	
				
				$(json).each(function(idx, data){
					options += "<option value='" + data.id + "'>" + decodeURIComponent(data.name).replace(/\+/gi, " ") + "</option>";
				});
				
//				el.html(options).val(workerCd);
				console.log(workerCd)
			}
		});	
	}

	
	</script>	
	<!-- 불량 신고를 위해 추가된 Script END -->
	
	
	<div id="time"></div>
	<div id="title_right"></div>
	<div id="addWorkTime" style="border: 2px solid yellow; background:black;">
	<center style="font-size:25px; color:white;">출근 기록을 하지 않았습니다.</center><br>
	<center style="font-size:25px; color:white;">출근 시간을 입력해주세요.</center><br> 
	<center style="font-size:25px; color:white;">일자 : <input type="date" min="2014-09-08" id="start_time_date" style="font-size:25px;"></center><br> 
	<center style="font-size:25px; color:white;">시간 : <input type="time" step="900" id="start_time" style="font-size:25px;"></center>
	<button style="width:100%; margin-top:35px; margin-left:0px !important; font-size:30px !important; background:greenyellow; border: 0px solid;" onclick="addworkTimeHist()">입력</button>
	</div>

	<div id="container">
		<table id="_table" style="border-collapse: collapse;">
				<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<!-- <div id='workingList' style="float: left; background: blue; position: absolute; left: 15%; top: 22%" >
					리스트 뿌리자
				</div> -->

				<td rowspan="10" id="svg_td">
				
					<center>
						<div id="report2work" class="sub_menu">
							<span><spring:message code="report2work"></spring:message></span>
						</div>
						<div id="report2leave_work" class="sub_menu">
							<span><spring:message code="work_report_totalmenu"></spring:message></span>
						</div>
						<div id="report2leave" class="sub_menu">
							<span>퇴근보고</span>
						</div>
					</center>					
				</td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class="nav_span"></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><span class='nav_span'></span> <img alt=""
					src="${ctxPath }/images/unselected.png" class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><img alt="" src="${ctxPath }/images/unselected.png"
					class='menu_left'></Td>
			</Tr>
			<Tr>
				<Td><img alt="" src="${ctxPath }/images/unselected.png"
					class='menu_left'></Td>
			</Tr>
		</table>
	</div>

	<div id="intro_back"></div>
	<span id="intro"></span>
	
	<div id="addWorkMenu">
	     <div id="inputPrdNo">차종 선택 : <input id="prdNo1"></div>
	     <div id="inputDvcId">장비 선택 : <input id="dvcId"></div>
	     <button style="display:inline;"id="addWork" onclick="addDevice()">추가</button><button id="closeAddWorkMenu" style="display:inline;" onclick="addWorkMenu.close()">닫기</button>
     </div>
</body>
</html>
