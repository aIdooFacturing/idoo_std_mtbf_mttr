<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
html{
	overflow : hidden;
}

</style> 
<script type="text/javascript">
	
	function replaceHash(str){
		return str.replace(/#/gi,"-");
	};

	function replaceHyphen(str){
		return str.replace(/#/gi,"-");	
	};

	var handle = 0;
	
	$(function(){
		createNav("order_nav", 2);

		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		
		chkBanner();
	});
	
	function getTime(){
		var date = new Date();
		var hour = addZero(String(date.getHours()));
		var minute = addZero(String(date.getMinutes()));
		var second = addZero(String(date.getSeconds()));
		
		return hour + ":" + minute;
	};
	
	function addZero(n){
		if(n.length=="1"){
			n = "0" + n;
		};
		return n;
	};
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : contentWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$("#table2 td").css({
			"padding" : getElSize(20),
			"font-size": getElSize(40),
			"border": getElSize(5) + "px solid black"
		});
		
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		
		$("select, button, input").css({
			"font-size" : getElSize(40),
			"margin-left" : getElSize(20),
			"margin-right" : getElSize(20)
		});
		
		$("button").css({
			"padding" : getElSize(15),
		})
		
		$("#banner").css({
			"font-size" : getElSize(80),
			"width" : getElSize(2000)
		});
		
		$("#intro").css({
			"font-size" : getElSize(100)
		});
		
		$("#tabstrip").css("height",getElSize(1764))
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	var dvc = [];
	
	//장비 SELECT 박스
	function dvcList(){
		var url = "${ctxPath}/common/dvcList.do";
		
		$.ajax({
			url : url,
			type : "post",
			dataType : "json",
			async: false,
			success : function(data){
				json = data.dataList;
				$(json).each(function(idx,data){
					dvc.push(data)
				})
				dvc.sort(function(a, b) { // 오름차순
				    return a.dvcName < b.dvcName ? -1 : a.dvcName > b.dvcName ? 1 : 0;
				    // 광희, 명수, 재석, 형돈
				});
			}
		});
	}
	
	//장비 dvc_id 번호를 이름으로 바꿔서 표시
	function dvcIdtoName(dvcId){
		for(var i=0;i<dvc.length;i++){
			if(dvc[i].dvcId==dvcId){
				return dvc[i].dvcName
			}
		}
		return dvcId;
	}
	
	
	var selectlist=[];
	
	//작업자 선택
	function getWorkerList(){
		var url = ctxPath + "/common/getWorkerList.do";
		var param = "shopId=" + shopId;
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			async: false,
			success : function(data){
				var json = data.dataList;
				var table = "<tbody>";
							
				$(json).each(function(idx, data){
					
					
					var arr=new Object();
					arr.text=decode(data.name);
					arr.value=decode(data.id);
					arr.textname=decode(data.name);
					selectlist.push(arr)
				});
					console.log(selectlist)
			}
		});
	}
	
	//작업자 empCd 번호를 이름으로 바꿔서 표시
	function empCdtoName(value){
		for(var i=0;i<selectlist.length;i++){
			if(selectlist[i].value==value){
				return selectlist[i].text
			}
		}
		return value;
	}
	
	//grid 테이블에서 장비 선택
	function dvcIdDropDownEditor(container, options){
		$('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "dvcName",
			 dataValueField: "dvcId",
			 dataSource: dvc,
//			 select: onSelect,
//			 change: onChange
		});
	}
	
	//grid 테이블에서 작업자 선택
	function categoryDropDownEditor(container, options) {
		
		 $('<input required name="' + options.field + '"/>')
		 .appendTo(container)
		 .kendoDropDownList({
			 autoWidth: true,
			 autoBind: false,
			 dataTextField: "text",
			 dataValueField: "value",
			 dataSource: selectlist,
//			 select: onSelect,
//			 change: onChange
		});
	}
	$(document).ready(function(){
		getWorkerList();		//이름
		dvcList();//장비
		
		$("#tabstrip").kendoTabStrip({
            animation:  {
                open: {
                    effects: "fadeIn"
                }
            }
        });
		
		$("#tabstrip ul li").click(function(){
			dvcWorkerlist();
			workerDvclist();

			$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1036));
		});
		
		//첫번째 TAB 작업자별 장비등록  
		$("#combobox").kendoComboBox({
            dataTextField: "text",
            dataValueField: "value",
            dataSource: selectlist,
            filter: "contains",
            change: autoSelectWorker,
            suggest: true,
            index: 0
        });
		
		function autoSelectWorker(){
			dvcWorkerlist();
		}
		//첫번째 TAB GRID 그리기
		kendotable=$("#grid").kendoGrid({
			toolbar:[{template:"#=create()#"},{template:"#=save()#"}],
			editable:true,
			height:getElSize(1350),
			columns:[{
				field:"empCd",title:"작업자",editor: categoryDropDownEditor ,template: "#= empCdtoName(empCd) #" 
			},{
				field:"dvcId",title:"장비", editor: dvcIdDropDownEditor ,template:"#=dvcIdtoName(dvcId)#"
			},{
				command :[{
					
					name :"${del}",
					click : function(e){
						var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
						if (confirm("정말 삭제하시겠습니까??") == true){    //확인
							dvc_del(dataItem);
						}else{   //취소
						    return;
						}
					}
				}],
			}],dataBound:function(e){
				$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1036));
			}
		}).data("kendoGrid");
		dvcWorkerlist();
		
		
		//두번째 TAB 장비 조회 선택 박스
		$("#dvccombobox").kendoComboBox({
	            dataTextField: "dvcName",
	            dataValueField: "dvcId",
	            dataSource: dvc,
	            filter: "contains",
	            change:autoSelectDvc,
	            suggest: true,
	            index: 0
	    });
		
		function autoSelectDvc(){
			workerDvclist();
		}
		
		//두번째 TAB GRID 그리기
		dvckendotable=$("#dvcgrid").kendoGrid({
			toolbar:[{template:"#=TwoTabCreate()#"},{template:"#=TwoTabSave()#"}],
			editable:true,
			height:getElSize(1350),
			columns:[{
				field:"dvcId",title:"장비",editor: dvcIdDropDownEditor ,template:"#=dvcIdtoName(dvcId)#"
			},{
				field:"empCd",title:"작업자", editor: categoryDropDownEditor ,template: "#= empCdtoName(empCd) #" 
			},{
				command :[{
					
					name :"${del}",
					click : function(e){
						var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
						if (confirm("정말 삭제하시겠습니까??") == true){    //확인
							two_dvc_del(dataItem);
						}else{   //취소
						    return;
						}
					}
				}],
			}],dataBound:function(e){
				$(".k-grid-content.k-auto-scrollable").css("height",getElSize(1036));
			}
		}).data("kendoGrid");
		
	})
	
	/* 첫번째 탭 추가 , 저장 , 삭제  , 데이터 불러오기  464 ~ 580 */
	function create(e){
		return '<a class="k-button" href="#" id="toolbar-add_user" onclick="t_create()">${ add}</a>';
	}
	function save(e){
		return '<a class="k-button" href="#" id="toolbar-add_user" onclick="t_save()">${ save}</a>';
	}
	
	// 첫번째 추가
	function t_create(){
		var dataItem=kendotable.dataSource.data();
		kendotable.dataSource.add({
			empCd:$("#combobox").val(),
			dvcId:"",
			idx:""
		});
	}
	
	// 첫번째 저장
	function t_save(){
		var dataItem=kendotable.dataSource.data();
		var list=[];
		for(i=0; i<dataItem.length; i++){
			var arr={};
			arr.empCd=dataItem[i].empCd;
			arr.dvcId=dataItem[i].dvcId;
			arr.idx=dataItem[i].idx;
			list.push(arr)
			
			if(dataItem[i].empCd=="" || dataItem[i].dvcId==""){
				kendo.alert("입력해라");
				return;
			}
		}
		
		var obj = new Object();
		obj.val = list;
		var param = JSON.stringify(obj);
		
		var url = ctxPath + "/order/saveWorkerList.do";
		var param = "val=" + param;
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				dvcWorkerlist();
				workerDvclist();
				kendo.alert("${save_ok}")
			}, error:function(request,status,error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				$.hideLoading();
		    }
		});
		console.log(list);
	}
	
	//첫번째 삭제
	function dvc_del(dataItem){
        var url = "${ctxPath}/order/delWorkerList.do";
		var param = "idx=" + dataItem.idx;

		$.showLoading();
		
 		$.ajax({
			url:url,
			data:param,
			type:"post",
			success : function(data){
				kendotable.dataSource.remove(dataItem);
				dvckendotable.dataSource.remove(dataItem);
				$.hideLoading();
			}
		});
 	}
	
	//첫번째 tab 테이블 데이터 불러오기
	function dvcWorkerlist(){
		var url = "${ctxPath}/order/dvcWorkerlist.do";
		var param = "empCd=" + $("#combobox").val();
		$.showLoading();
		$.ajax({
			url:url,
			dataType : "json",
			data:param,
			type:"post",
			success : function(data){
				var json=data.dataList;
				console.log(data.dataList);
				
				var kendodata = new kendo.data.DataSource({
					data:json,
					batch: true,
					schema: {
						model: {
							id: "id",
							fields: {
								idx:{ editable : false, nullable: true},
								empCd:{ editable : true, nullable: true},
								dvcId:{ editable : true, nullable: false}
							}
						}
					}
				})
				
				kendotable.setDataSource(kendodata);
				$.hideLoading();

			}
		});
	}
	
	/* 	두번째 tab 수정 , 추가 , 삭제 데이터 불러오기 */
	function TwoTabCreate(e){
		return '<a class="k-button" href="#" id="toolbar-add_user" onclick="t_two_create()">${ add}</a>';
	}
	function TwoTabSave(e){
		return '<a class="k-button" href="#" id="toolbar-add_user" onclick="t_two_save()">${ save}</a>';
	}
	
	// 두번째 추가
	function t_two_create(){
		var dataItem=kendotable.dataSource.data();
		dvckendotable.dataSource.add({
			empCd:"",
			dvcId:$("#dvccombobox").val(),
			idx:""
		});
	}
	
	// 두번째 저장
	function t_two_save(){
		var dataItem=dvckendotable.dataSource.data();
		var list=[];
		for(i=0; i<dataItem.length; i++){
			var arr={};
			arr.empCd=dataItem[i].empCd;
			arr.dvcId=dataItem[i].dvcId;
			arr.idx=dataItem[i].idx;
			list.push(arr)
			
			if(dataItem[i].empCd=="" || dataItem[i].dvcId==""){
				kendo.alert("입력해라");
				return;
			}
		}
		
		var obj = new Object();
		obj.val = list;
		var param = JSON.stringify(obj);
		
		var url = ctxPath + "/order/saveWorkerList.do";
		var param = "val=" + param;
		$.showLoading();
		$.ajax({
			url : url,
			data : param,
			type : "post",
			success : function(data){
				dvcWorkerlist();
				workerDvclist();
				kendo.alert("${save_ok}")
			}, error:function(request,status,error){
				alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				$.hideLoading();
		    }
		});
		console.log(list);
	}
	
	//두번째 삭제
	function two_dvc_del(dataItem){
        var url = "${ctxPath}/order/delWorkerList.do";
		var param = "idx=" + dataItem.idx;

		$.showLoading();
		
 		$.ajax({
			url:url,
			data:param,
			type:"post",
			success : function(data){
				dvckendotable.dataSource.remove(dataItem);
				kendotable.dataSource.remove(dataItem);
				$.hideLoading();
			}
		});
 	}
	//두번째 tab 테이블 데이터 불러오기
	function workerDvclist(){
		var url = "${ctxPath}/order/workerDvclist.do";
		var param = "dvcId=" + $("#dvccombobox").val();
		$.showLoading();
		$.ajax({
			url:url,
			dataType : "json",
			data:param,
			type:"post",
			success : function(data){
				var json=data.dataList;
				console.log(data.dataList);
				var dvckendodata = new kendo.data.DataSource({
					data:json,
					batch: true,
					schema: {
						model: {
							id: "id",
							fields: {
								idx:{ editable : false, nullable: true},
								empCd:{ editable : true, nullable: true},
								dvcId:{ editable : true, nullable: false}
							}
						}
					}
				})
				dvckendotable.setDataSource(dvckendodata);
				$.hideLoading();

			}
		});
	}
	
		
</script>
</head>
<!-- <body oncontextmenu="return false"> -->
<body>
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/mainten_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
					<div id="wrapper">
						<table style="width: 100%">
							<Tr>
								<Td style="text-align: left; vertical-align: middle;">
									
									
									
									<div class="demo-section k-content">
				                        <div id="tabstrip">
				                            <ul>
				                                <li class="k-state-active" style="background-color: gray">
								                                    작업자별 장비등록
				                                </li>
				                                <li style="background-color: gray">
								                                    장비별 작업자등록
				                                </li>
				                            </ul>
				                            <div>
					                            <div id="combobox"></div>
												<button onclick="dvcWorkerlist()">조회</button>
												<div id="grid"></div>
				                            </div>
				                            <div >
					                            <div id="dvccombobox"></div>
												<button onclick="workerDvclist()">조회</button>
												<div id="dvcgrid"></div>
				                            </div>
				                        </div>
				               		</div>
								</Td>
							</Tr>
						</table>
					</div>
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="confirm"></div>
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	