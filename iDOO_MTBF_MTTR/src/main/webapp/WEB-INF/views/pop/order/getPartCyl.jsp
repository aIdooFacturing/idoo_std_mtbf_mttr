<!-- 
최소한의 화면으로 구성된
시작화면 Template
 -->
 <%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>

<% 
response.setHeader("cache-control", "max-age=0, must-revalidate, no-cache, no-store, private"); // HTTP 1.1
response.setHeader("pragma", "no-cache"); // HTTP 1.0
response.setDateHeader("expires", -1); // Stop proxy caching
%>
<!DOCTYPE html>
<html> 
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">
<link rel="stylesheet" href="${ctxPath }/css/style.css">
<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var fromDashboard = "${fromDashBoard}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;
	
	var contentWidth = originWidth;
	var contentHeight = targetHeight/(targetWidth/originWidth);
	
	var screen_ratio = getElSize(240);
	
	if(originHeight/screen_ratio<9){
		contentWidth = targetWidth/(targetHeight/originHeight)
		contentHeight = originHeight; 
	};
	
	function getElSize(n){
		return contentWidth/(targetWidth/n);
	};
	
	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
	
	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
</script>
<script type="text/javascript" src="${ctxPath }/js/smil.user.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-3d.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/highcharts-more.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jqMap.js"></script>
<script type="text/javascript" src="${ctxPath }/js/multicolor_series.js"></script>
<script type="text/javascript" src="${ctxPath }/js/chart/moment.js"></script>
<style>
*{
	margin: 0px;
	padding: 0px;
}
body{
	margin: 0px;
	padding: 0px;
	width: 100%;
	height: 100%;
	overflow : hidden; 
	background-color: black;
  	font-family:'Helvetica';
}
#searchBtn, #nowBtn{
	float:right;
}
</style> 
<script type="text/javascript">

	$(function(){
		createNav("order_nav", 3);
		
		setEl();
		time();
		
		$("#home").click(function(){ location.href= "${ctxPath}/chart/index.do" });
		
		window.setInterval(function(){
			var width = window.innerWidth;
			var height = window.innerHeight;

			if(width!=originWidth || height!=originHeight){
				location.reload();
			};
		},1000*10);
		
		chkBanner();
		$("#date").val(moment().format("YYYY-MM-DD"))
		
		$("#date").datepicker({
			minDate: '2017-12-21',
			maxDate: '0'
		})
		
	});
	
	function setEl(){
		var neonColor = "#0096FF";
		
		var width = window.innerWidth;
		var height = window.innerHeight;
		
		$(".right").css({
			"height" : getElSize(120)
		});
		
		$(".left, .menu_left").css({
			"width" : getElSize(495)			
		})
		
		$("#container").css({
			"width" : contentWidth,
			"height" : contentHeight,
		});
		
		$("#container").css({
			"margin-left" : (originWidth/2) - ($("#container").width()/2),
			"margin-top" : (originHeight/2) - ($("#container").height()/2)
		})
		
		$("#intro").css({
			"position" : "absolute",
			"bottom" : 0 + marginHeight,
			"font-size" : getElSize(140),
			"font-weight" : "bolder",
			"z-index" : 9999
		});
		
		$("#intro_back").css({
			"width" : originWidth,
			"display" : "none",
			"height" : getElSize(180),
			"opacity" : 0.5,
			"position" : "absolute",
			"background-color" : "black",
			"bottom" : 0 + marginHeight,
			"z-index" : 9999
		})
		
		$("#time").css({
			"color" : "white",
			"position" : "absolute",
			"font-size" : getElSize(40),
			"top" : getElSize(25) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("#table").css({
			"position" : "absolute",
			"width" : $("#container").width(),
			"top" : getElSize(100) + marginHeight
		});
		
		$(".right").css({
			"width" : contentWidth - $(".left").width() 
		});
		
		$(".menu_right").css({
			"width" : $(".right").width()
		})
		
		$("#home").css({
			"cursor" : "pointer"
		})
		
		$("#title_right").css({
			"position" : "absolute",
			"z-index" : 2,
			"color" : "white",
			"font-size" : getElSize(40),
			"top" : getElSize(130) + marginHeight,
			"right" : getElSize(30) + marginWidth
		});
		
		$("span").css({
			"color" : "#8D8D8D",
			"position" : "absolute",
			"font-size" : getElSize(45),
			"margin-top" : getElSize(20),
			"margin-left" : getElSize(20)
		});
		
		$("#selected").css({
			"color" : "white",
		});
		
		$("span").parent("td").css({
			"cursor" : "pointer"
		});
		
		$(".title_span").css({
			"color" : "white",
			"font-size" : getElSize(40),
			"background-color" : "#353535",
			"padding" : getElSize(15)
		});
		
		$("#btnDiv").css({
			"height" : getElSize(130),
		})
		
		$("#searchBtn").css({
			"height":  getElSize(105),
	    	"margin-top": getElSize(14),
	    	"margin-right": getElSize(44),
	   		"width": getElSize(380),
	   		"font-size":getElSize(50),
	   		"border-radius": "5px",
	    	"font-weight": "bolder"
		})
		
		$("#nowBtn").css({
			"height":  getElSize(105),
	    	"margin-top": getElSize(14),
	    	"margin-right": getElSize(544),
	   		"width": getElSize(380),
	   		"font-size":getElSize(35),
	   		"border-radius": "5px",
	    	"font-weight": "bolder"
		})
		
		$("i").css({
			"margin-right" : getElSize(60)
		})
		
		$("#date").css({
		    "font-size": getElSize(55),
		    "margin-top": getElSize(18)
		})
		
		$("#workIdx").css({
		    "font-size": getElSize(70),
		    "position": "absolute",
		    "top": getElSize(300),
		    "left": getElSize(1120)
		})
		
		
	};
	
	function time(){
		$("#time").html(getToday());
		 handle = requestAnimationFrame(time)
	};
	
	function checkPartCnt(){
			
		var eDate;
		var sDate;
		if($("#workIdx").val()==1){
			sDate = moment($("#date").val()).format("YYYY-MM-DD HH:mm:ss");
			sDate = moment(sDate).add(20, 'hours');
			sDate = moment(sDate).add(30, 'minutes');
			sDate = moment(sDate).format("YYYY-MM-DD HH:mm:ss");
			
			eDate = moment($("#date").val()).add(1, 'days').format("YYYY-MM-DD HH:mm:ss");
			eDate = moment(eDate).add(08, 'hours');
			eDate = moment(eDate).add(30, 'minutes');
			eDate = moment(eDate).format("YYYY-MM-DD HH:mm:ss");
		}else{
			sDate = moment($("#date").val()).format("YYYY-MM-DD HH:mm:ss");
			sDate = moment(sDate).add(8, 'hours');
			sDate = moment(sDate).add(30, 'minutes');
			sDate = moment(sDate).format("YYYY-MM-DD HH:mm:ss");
			
			eDate = moment($("#date").val()).format("YYYY-MM-DD HH:mm:ss");
			eDate = moment(eDate).add(20, 'hours');
			eDate = moment(eDate).add(30, 'minutes');
			eDate = moment(eDate).format("YYYY-MM-DD HH:mm:ss");
		}
		
		var url = "${ctxPath}/order/checkPartCnt.do";
		var param = "sDate=" + sDate  + "&eDate=" + eDate + "&date=" +$("#date").val()
		
		console.log(url + "?" + param)
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			beforeSend : function(){
				$.showLoading();
				$("#searchBtn").attr("disabled", "disabled");
			},
			complete : function(){
				$.hideLoading();
				$("#searchBtn").removeAttr("disabled");
			},
			success : function(data){
				var json = data.dataList;
				var length = json.length
				console.log(json)
				var dataSource = new kendo.data.DataSource({});
				for(var i=0;i<length;i++){
					json[i].date=$("#date").val();
					json[i].name=decode(json[i].name)
					dataSource.add(json[i])
				}
				grid.setDataSource(dataSource)
				
				$.hideLoading();
			}
		})
		
	}
	
	$(function(){
		grid = $("#grid").kendoGrid({
			height:getElSize(1650),
			columns: [
			    {
			        title: "dvcId",
			        field: "dvcId"
			    },
			    {
			        title: "장비명",
			        field: "name"
			    },
			    {
			        title: "이전 Part Count",
			        field: "minPartCyl",
			    },
			    {
			        title: "Part Count",
			        field: "maxPartCyl",
			    },
			    {
			        title: "싸이클 당 생산량",
			        field: "cpc"
			    },
			    {
			        title: "생산량",
			        field: "count",
			        template : "#=(maxPartCyl-minPartCyl)*cpc#"
			    }
			  ]
		}).data("kendoGrid")
	})
	
	</script>
</head>
<body>
	
	<div id="time"></div>
	<div id="title_right"></div>

	<div id="container">
		<table id="table" style="border-collapse: collapse;">
			<Tr>
				<td >
					<img alt="" src="${ctxPath }/images/gray_left.png" class='left' id="home" >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/gray_right.png" class='right'>
				</td>
			</Tr>
			<tr>
				<td >
					<img alt="" src="${ctxPath }/images/order_left.png" class='menu_left'  >
				</td>
				<td >
					<img alt="" src="${ctxPath }/images/purple_right.png" class='menu_right'>
				</td>
			</tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/selected_purple.png" class='menu_left'>
				</Td>
				<td rowspan="10" id="svg_td" style="vertical-align: top; ">
				
				<div id="btnDiv">
					일자 : <input type="date" id="date" readonly>
					<select id="workIdx">
						<option value='1'>야간</option>
						<option value='2'>주간</option>
					</select>
					<button id="searchBtn" onclick="checkPartCnt()"><i class="fa fa-search" aria-hidden="true"></i>검색</button>
				</div>
				
				<div id="grid"></div>
				
				</td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class="nav_span"></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'>
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left' >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span'></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<span class='nav_span' ></span>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
			<Tr>
				<Td>
					<img alt="" src="${ctxPath }/images/unselected.png" class='menu_left'  >
				</Td>
			</Tr>
		</table>
	 </div>
	
	<div id="intro_back"></div>
	<span id="intro"></span>
</body>
</html>	