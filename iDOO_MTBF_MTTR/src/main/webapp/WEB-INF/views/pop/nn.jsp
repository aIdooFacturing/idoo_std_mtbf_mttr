<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>
body{
	background: #484848;
	margin: 0;
	color: white;
	width: 100%;
	height: 100%;
	position: absolute;
}
#header{
	width: 100%;
	height: 12%;
}
#leftH{
	background: linear-gradient(black,#484848);
	height: 100%;
	width: 30%;
	float: left;
	display: table;
	text-align: center;
}
#leftH span{
	display: table-cell;
	vertical-align: middle;
	font-size: 200%;
}
#backBtn{
	background: linear-gradient( darkslateblue,#7112FF);
	border-radius: 5%;
	width: 40%;
	height: 100%;
	display: table;
	float: left;
	
}
#rightH{
	background: linear-gradient(black,#484848);
	height: 100%;
	width: 30%;
	float: left;
	display:table;
	text-align: center;
}
#time{
	display:table-cell;
	vertical-align:middle;
	font-size: 250%;
}

#backBtn span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 300%;
}
#title{
	background: linear-gradient( #000000,#484848);
	width: 80%;
	height: 100%;
	display: table;
	float: left;
}
#title span{
	width:100%;
	height:100%;
	display: table-cell;
/* 	vertical-align: middle; */
	text-align: right;
	font-size: 300%;
}
#aside{
	background: green;
	width: 100%;
	height: 15%;
	display: table;
	background: darkorchid;
	color:indigo;
}
#aside span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 400%;
}
#content{
	width: 100%;
	height: 70%;
	background: #242424;
}
#space{
	width: 100%;
	height: 20%;
	background: black;
}
#aside{
	background: green;
	width: 100%;
	height: 15%;
	display: table;
	background: darkorchid;
	color:indigo;
}
#aside span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 400%;
}
#content{
	width: 100%;
	height: 50%;
	background: #242424;
}
#empCd{
	margin-top: -500;
}
</style>

<script>

	var empCd;
	var evtMsg;
	
	$(function(){
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		
		<% 
			session.removeAttribute("empCd");
			System.out.println(session.getAttribute("empCd"));
			
		%>
		//focus TEXT 맞추기
		var chk_short = true;
		
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#empCd").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#empCd").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
		
		//시간
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})
	
		//enter key event
	function enterEvt(event) {
		if(event.keyCode == 13){
			getTable()
		}
	}
	//사원 바코드를 입력해주세요
	function alarmMsg(){
		clearTimeout(evtMsg)
		return evtMsg = setTimeout(function() {$("#aside").html("<span>바코드를 입력해주세요</span>")}, 10000)
//		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>사원 바코드를 입력해주세요</marquee></span>")}, 10000)
	}
	
	function getTable(){
		if($("#empCd").val()==0){
			return;
		}
		$("#empCd").focus();
		$("#empCd").select();
		
		empCd=$("#empCd").val();
		console.log(empCd)
		
		//앞에 4자리는 바코드 구분 현재 1111이면 사원증
		if(empCd.substring(0,4)!="1111"){
			$("#aside").html("<span style='color:red;'>바코드를 다시 확인해 주세요</span>")
			$("#jobList").empty()
			alarmMsg();
			return;
		}
		
		empCd = empCd.substring(4,empCd.length);
		
		var url = "${ctxPath}/pop/popLoginChk.do";
		var param = "empCd=" + empCd ;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log(data);
				var json = data.dataList;
				location.href="${ctxPath}/pop/popMainMenu.do?empCd=" + empCd
//				console.log(json);
			},error : function(request,status,error){
				if(request.responseText=="no"){
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:red;'>등록된 사원번호가 아닙니다.</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
				}else{
					$("#aside").html("<span><marquee loop='2' scrollamount='20' id='alarmText' style='color:blue;'>관리자에게 문의해주세요.</marquee></span>")
					$("#jobList").empty()
					alarmMsg();
					
				}
				$.hideLoading(); 
//				 alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
				   


			}
		})
	}
	
</script>

<body>
	<div id="header">
		<div id="leftH">
			<span></span>
		</div>
		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
			<span>부광정밀공업(주)</span>
		</div>
		<div id="rightH">
			<span id="time"></span>
		</div>
	</div>
	
	<div id="aside">
		<span>
<!-- 			<marquee behavior=alternate scrollamount="20"> -->
				바코드를 입력해주세요
<!-- 			</marquee> -->
		</span>
	</div>
	<div id="content">
		
	</div>
</body>
</html>