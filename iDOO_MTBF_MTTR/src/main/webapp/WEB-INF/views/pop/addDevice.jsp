<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>
body{
	background: #484848;
	margin: 0;
	color: white;
	width: 100%;
	height: 100%;
	position: absolute;
}
#header{
	width: 100%;
	height: 15%;
}
#backBtn{
	background: linear-gradient( darkslateblue,#7112FF);
	border-radius: 5%;
	width: 20%;
	height: 100%;
	display: table;
	float: left;
	
}
#backBtn span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 300%;
}
#title{
	background: linear-gradient( #000000,#484848);
	width: 80%;
	height: 100%;
	display: table;
	float: left;
}
#title span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 500%;
}
#aside{
	background: green;
	width: 100%;
	height: 15%;
	display: table;
	background: darkorchid;
	color:indigo;
}
#aside span{
	width:100%;
	height:100%;
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	font-size: 400%;
}
#content{
	width: 100%;
	height: 70%;
	background: #242424;
}

#selectDevices{
	background: aqua;
}

#workContent{
	background: blue;
}
</style>

<script>
	
</script>

<body>
	<div id="header">
		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
			<span>BKJM</span>
		</div>
		<div id="title">
			<span>장 비 추 가</span>
		</div>
	</div>
	<div id="aside">
		<span>
			<marquee behavior=alternate scrollamount="20">
				추가할 장비를 선택해주세요
			</marquee>
		</span>
	</div>
	<div id="content">
		<div id="selectDevices">
		
		</div>
		<div id="workContent">
		
		</div>
	</div>
</body>
</html>