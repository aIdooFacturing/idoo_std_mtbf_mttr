<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ include file="/WEB-INF/views/include/unomiclib.jsp"%>
<%@ include file="/WEB-INF/views/pop/lib.jsp"%>
<%@ page import="com.unomic.dulink.common.domain.CommonCode"%>
<%@ page import="com.unomic.dulink.chart.domain.*"%>
<%@ page session = "true" %>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" scope="request"/>

<script src="${ctxPath }/js/jquery.js"></script>
<script src="${ctxPath }/js/jquery-ui.min.js"></script>
<script src="${ctxPath }/js/jquery.loading.min.js"></script>
<script src="${ctxPath }/js/moment.js"></script>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<style>

</style>

<script>

	var evtMsg;
	var nm = '<%=(String)session.getAttribute("nm")%>';
		nm = decode(nm);
	var empCd = '<%=(String)session.getAttribute("empCd")%>';
	
	$(function(){
		
		//session 있는지 체크
		sessionChk();

		getTable();
		$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		msg();
		
		//focus TEXT 맞추기
		var chk_short = true;
		
		$(document).bind("keydown keyup", function(e) {
	        var key = e.keyCode;
	        var tg = e.target;
	        if(tg.tagName == "INPUT" ||  tg.tagName == "TEXTAREA") return true;
	        
	        var specific = key >= 8 && key <= 46;
	        if(e.type == "keydown") {
	            if(specific) {
	                chk_short = false;
	                return true;
	            }
	            if(!specific && chk_short) {
	            	 $("#empCd").focus().select();
	                //target_input.focus().select(); return false;
	            }
	            if(e.ctrlKey && e.keyCode == 86){
	            	 $("#empCd").focus().select();
	            }
	        } else {
	            if(specific) {
	                chk_short = true;
	            }
	        }
	    });
		
		//시간
		setInterval(function() {
			$("#time").html(moment().format("YYYY-MM-DD HH:mm:ss"))
		}, 1000);
	})
	
	
	function sessionChk(){
		if(empCd==null || empCd=="null"){
			location.href='${ctxPath}/pop/popIndex.do';
		}
		
	}
		//enter key event
	function enterEvt(event) {
		if(event.keyCode == 13){

		}
	}
	//사원 바코드를 입력해주세요
	function alarmMsg(){
		clearTimeout(evtMsg)
		return evtMsg = setTimeout(function() {$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>"+ nm +"</marquee></span>")}, 10000)
	}
	function msg(){
		$("#aside").html("<span><marquee behavior=alternate scrollamount='20' id='alarmText'>"+ nm +" 님 반갑습니다.</marquee></span>")
	}
	//한글 인코딩
	function decode(str){
		return decodeURIComponent(str).replace(/\+/gi, " ")
	};
	
	function getParameterByName(paramName){ 
		var _tempUrl = window.location.search.substring(1); 
		//url에서 처음부터 '?'까지 삭제 
		var _tempArray = _tempUrl.split('&'); 
		
		// '&'을 기준으로 분리하기 
		for(var i = 0; _tempArray.length; i++) { 
			var _keyValuePair = _tempArray[i].split('='); 
				// '=' 을 기준으로 분리하기 
			if(_keyValuePair[0] == paramName){ 
				// _keyValuePair[0] : 파라미터 명 
				// _keyValuePair[1] : 파라미터 값
				return _keyValuePair[1]; 
			 }
		 }
	}
	function getTable(){
		
		var name = getParameterByName('name');

		$("#backBtn span").html(decode(name))
		
		dvcId = getParameterByName('dvcId');
		ty = getParameterByName('ty');
		var idx = getParameterByName('idx');
		$.showLoading(); 
		var url = "${ctxPath}/pop/getEndJobInfo.do";
		
		
		
		var param = "name=" + name +
					"&NM=" + nm +
					"&dvcId=" + dvcId + 
					"&empCd=" + empCd +
					"&idx=" + idx;
//					"&ty=" + ty;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				console.log(data);
				$.hideLoading(); 
			}
		})
	}
	
	
</script>

<body>
	<div id="header">
		<div id="leftH">
			<span></span>
		</div>
		<div id="backBtn" onclick="location.href='${ctxPath}/pop/popIndex.do'" style="cursor: pointer;">
			<span>BKJM</span>
		</div>
		<div id="rightH">
			<span id="time"></span>
		</div>
	</div>
	<div id="aside">
		<span>
			<marquee behavior=alternate scrollamount="20">
			</marquee>
		</span>
	</div>
	<div id="content">
		
	</div>
</body>
</html>